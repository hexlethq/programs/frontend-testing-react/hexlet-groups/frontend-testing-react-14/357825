const appUrl = 'http://localhost:8080';
const timeout = 15000;

const inputEl = '[data-testid="task-name-input"]';
const buttonEl = '[data-testid="add-task-button"]'

const addOneTask = async (text) => {
    await expect(page).toFill(inputEl, text);
    await expect(page).toClick(buttonEl);
};

describe('todo test', () => {
    beforeAll(async () => {
        await page.goto(appUrl, { waitUntil: 'domcontentloaded' });
    });

    test('app opened, has elements', async () => {
        await expect(page).toMatchElement(inputEl);
        await expect(page).toMatchElement(buttonEl);
    }, timeout);

    test('add tasks', async () => {
        const titleTaskOne = 'title one';
        const titleTaskSecond = 'title two';
        await addOneTask(titleTaskOne);
        await expect(page).toMatch(titleTaskOne);
        await addOneTask(titleTaskSecond);
        await expect(page).toMatch(titleTaskSecond);
    }, timeout);

    test('remove tasks', async () => {
        const titleTaskOne = 'title three';
        await addOneTask(titleTaskOne);
        await expect(page).toMatch(titleTaskOne);
        await expect(page).toClick('[data-testid*="remove-task-"]:last-child');
        await expect(page).not.toMatch(titleTaskOne);
    }, timeout);
});
