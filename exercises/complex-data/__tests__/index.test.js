const faker = require('faker');

describe('complex data', () => {

    expect.extend({
        toBeInArray(received) {
            const arrList = ['invoice', 'deposit', 'withdrawal', 'payment'];
            const pass = arrList.includes(received);
            if (pass) {
                return {
                    message: () =>
                        `expected ${received} to be in array ${arrList}`,
                    pass: true,
                };
            } else {
                return {
                    message: () =>
                        `expected ${received} to be in array ${arrList}`,
                    pass: false,
                };
            }
        },
    });

    test('check type', () => {
        const trans = faker.helpers.createTransaction()
        expect(trans).toEqual(expect.objectContaining({
            type: expect.toBeInArray(),
        }))
    })
    test('check date', () => {
        const trans = faker.helpers.createTransaction()
        expect(trans).toMatchObject({
            date: expect.any(Date),
            business: expect.any(String),
            name: expect.any(String),
            account: expect.stringMatching(/[0-9]/),
            amount: expect.stringMatching(/^[0-9]+(\.[0-9]{,2})?/),
        })
    })

})
