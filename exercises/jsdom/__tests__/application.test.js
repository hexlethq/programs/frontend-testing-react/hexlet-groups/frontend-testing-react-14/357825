require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const faker = require('faker');

const run = require('../src/application');

// BEGIN
const getListsContainer = () => document.querySelector('[data-container="lists"]');
const getTasksContainer = () => document.querySelector('[data-container="tasks"]');

const getListForm = () => document.querySelector('[data-container="new-list-form"]');
const getListInput = () => document.querySelector('[data-testid="add-list-input"]');
const getTaskInput = () => document.querySelector('[data-testid="add-task-input"]');
const getTaskForm = () => document.querySelector('[data-container="new-task-form"]');
const getListLink = (listId) => document.querySelector(`[data-testid="${listId}-list-item"]`);

const FIRST_KEY = 'general';
const FIRST_TEXT = 'General';
const SECOND_TEXT = 'second';

const createList = (name) => {
  getListInput().value = name;
  getListForm().submit();
}
const createTask = (name) => {
  getTaskInput().value = name;
  getTaskForm().submit();
}

describe('jsdom', () => {
  beforeEach(() => {
    document.body.innerHTML = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
    run();
  });

  test('default list, no tasks', () => {
    expect(getTasksContainer()).toBeEmptyDOMElement();
    expect(getListsContainer()).toHaveTextContent(FIRST_TEXT);
  });

  test('add task to default list', () => {
    const tasks = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];

    tasks.forEach((task) => {
      createTask(task)
      expect(getTaskInput().value).toBe('');
      expect(getTasksContainer()).toHaveTextContent(task);
    });
  });

  test('Create second task list', () => {
    const tasks = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];
    createList(SECOND_TEXT)
    expect(getListInput().value).toBe('');
    getListLink(SECOND_TEXT).click();

    expect(getListsContainer()).toHaveTextContent(SECOND_TEXT);
    tasks.forEach((task) => {
      createTask(task)
      expect(getTaskInput().value).toBe('');
      expect(getTasksContainer()).toHaveTextContent(task);
    });
  });

  test('При переключении между списками отображаются задачи соответствующие спискам', () => {
    const firstListTasks = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];
    const secondListTasks = [faker.lorem.word(), faker.lorem.word(), faker.lorem.word()];

    firstListTasks.forEach((task) => {
      createTask(task)
      expect(getTaskInput().value).toBe('');
      expect(getTasksContainer()).toHaveTextContent(task);
    });

    getListInput().value = SECOND_TEXT;
    getListForm().submit();
    expect(getListInput().value).toBe('');

    getListLink(SECOND_TEXT).click();

    secondListTasks.forEach((task) => {
      createTask(task)
      expect(getTaskInput().value).toBe('');
      expect(getTasksContainer()).toHaveTextContent(task);
    });

    getListLink(FIRST_KEY).click();
    firstListTasks.forEach((task) => {
      expect(getTasksContainer()).toHaveTextContent(task);
    });
  });
});
// END
