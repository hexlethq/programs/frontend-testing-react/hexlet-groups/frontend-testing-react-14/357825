package ru.hh.content.engine.packets;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.enums.PacketType;

class PacketServiceTest extends AppTestBase {
  @Inject
  private PacketService packetService;

  @Test
  public void testPacketsTypeGet() {
    Set<PacketDto> expectedPackets = Set.of(
        new PacketDto(PacketType.CUSTOM, PacketType.CUSTOM.getTranslation(), PacketType.CUSTOM.getCanChooseRegions()),
        new PacketDto(PacketType.RUSSIA, PacketType.RUSSIA.getTranslation(), PacketType.RUSSIA.getCanChooseRegions()),
        new PacketDto(
            PacketType.RUSSIA_WITHOUT_MOSCOW,
            PacketType.RUSSIA_WITHOUT_MOSCOW.getTranslation(),
            PacketType.RUSSIA_WITHOUT_MOSCOW.getCanChooseRegions()
        )
    );

    List<PacketDto> packets = packetService.getPackets();

    Assertions.assertEquals(expectedPackets, new HashSet<>(packets));
  }
}
