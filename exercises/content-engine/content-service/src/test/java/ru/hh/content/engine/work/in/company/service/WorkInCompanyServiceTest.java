package ru.hh.content.engine.work.in.company.service;

import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.clients.xmlback.EmployerClient;
import ru.hh.content.engine.clients.xmlback.EmployerInfoListResponse;
import ru.hh.content.engine.clients.xmlback.EmployerInfoResponse;
import ru.hh.content.engine.model.enums.ImageType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import static ru.hh.content.engine.model.enums.TargetingDeviceType.DESKTOP;
import static ru.hh.content.engine.model.enums.TargetingDeviceType.MOBILE;
import static ru.hh.content.engine.model.enums.WorkInCompanyStatus.ACTIVE;
import static ru.hh.content.engine.model.enums.WorkInCompanyStatus.DELETED;
import static ru.hh.content.engine.model.enums.WorkInCompanyStatus.PAUSED;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;
import ru.hh.content.engine.model.work.in.company.WorkInCompanyLogo;
import ru.hh.content.engine.model.work.in.company.WorkInCompanyReservation;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.utils.CommonTestUtils.nextMonday;
import ru.hh.content.engine.utils.ContentImage;
import static ru.hh.content.engine.utils.JClientMockUtils.success;
import static ru.hh.content.engine.work.in.company.WorkInCompanyConstants.MAX_COMPANIES_ON_PAGE_SETTING_NAME;
import ru.hh.content.engine.work.in.company.dto.ReserveNextFreePeriod;
import ru.hh.content.engine.work.in.company.dto.ReserveStatus;
import ru.hh.content.engine.work.in.company.dto.WorkInCompaniesListItem;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyCheckRequestDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyFormDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyLogoDto;
import ru.hh.nab.common.properties.FileSettings;

class WorkInCompanyServiceTest extends AppTestBase {
  @Inject
  private WorkInCompanyService workInCompanyService;
  @Inject
  private FileSettings fileSettings;
  @Inject
  private EmployerClient employerClient;

  @BeforeEach
  public void setUp() {
    getSettingsTestClient().addSetting(MAX_COMPANIES_ON_PAGE_SETTING_NAME, "1");
  }

  @Test
  public void createWorkInCompany() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";
    String comment = "comment";

    byte[] bytes = Base64.getDecoder().decode(fileSettings.getString("base64.example"));
    WorkInCompanyLogoDto workInCompanyLogoDto = workInCompanyService.createLogoWorkInCompany(
        ImageType.XS, ContentImage.fromBytes(bytes)
    );

    long returnedId = runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            workInCompanyLogoDto.getWorkInCompanyLogoId(),
            Set.of(TargetingDeviceType.DESKTOP),
            comment
        )
    ));

    WorkInCompany workInCompanyFromDb = getInTransaction(session -> session.get(WorkInCompany.class, returnedId));
    WorkInCompanyReservation workInCompanyReservation = getInTransaction(session ->
        session.createQuery(
            "from WorkInCompanyReservation where workInCompanyId = :workInEmployerId",
            WorkInCompanyReservation.class)
            .setParameter("workInEmployerId", workInCompanyFromDb.getWorkInCompanyId())
            .uniqueResult()
    );

    assertEquals(employerId, workInCompanyFromDb.getEmployerId(), "Wrong employer id");
    assertEquals(displayName, workInCompanyFromDb.getDisplayName(), "Wrong display name");
    assertEquals(comment, workInCompanyFromDb.getComment(), "Wrong comment");

    assertNotNull(workInCompanyReservation, "Reservation must not be null");
    assertEquals(0, workInCompanyReservation.getReservationNumber(), "Wrong reservation number");

    assertNotNull(workInCompanyFromDb.getWorkInCompanyLogoId(), "WorkInCompany have logoId");
    WorkInCompanyLogo workInCompanyLogo = getInTransaction(
        session -> session.get(WorkInCompanyLogo.class, workInCompanyFromDb.getWorkInCompanyLogoId())
    );
    assertNotNull(workInCompanyLogo, "WorkInCompanyLogo doesn't exist");
  }

  @Test
  public void editWorkInCompany() throws Exception {
    int regionId = nextId();
    int regionIdAfterEdit = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    int employerIdAfterEdit = nextId();
    String displayName = "Cute name";
    String displayNameAfterEdit = "Pretty name";
    String comment = "comment";
    String commentAfterEdit = "edited comment";

    byte[] bytes = Base64.getDecoder().decode(fileSettings.getString("base64.example"));
    WorkInCompanyLogoDto workInCompanyLogoDto = workInCompanyService.createLogoWorkInCompany(
        ImageType.XS, ContentImage.fromBytes(bytes)
    );

    WorkInCompanyLogoDto workInCompanyLogoDtoAfterEdit = workInCompanyService.createLogoWorkInCompany(
        ImageType.XS, ContentImage.fromBytes(bytes)
    );

    int creatorUserId = nextId();

    long returnedId = runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            workInCompanyLogoDto.getWorkInCompanyLogoId(),
            Set.of(TargetingDeviceType.DESKTOP),
            comment)
    ), creatorUserId);

    runAsBackoffice(() -> {
          workInCompanyService.editWorkInCompany(
              new WorkInCompanyFormDto(
                  Set.of(regionIdAfterEdit),
                  dateStart.plusWeeks(1),
                  dateEnd.plusWeeks(1),
                  displayNameAfterEdit,
                  employerIdAfterEdit,
                  workInCompanyLogoDtoAfterEdit.getWorkInCompanyLogoId(),
                  Set.of(TargetingDeviceType.MOBILE),
                  commentAfterEdit),
              returnedId
          );
          return null;
        }, creatorUserId
    );

    WorkInCompany workInCompanyFromDbAfterEdit = getInTransaction(session -> session.get(WorkInCompany.class, returnedId));
    WorkInCompanyReservation workInCompanyReservation = getInTransaction(session ->
        session.createQuery(
            "from WorkInCompanyReservation where workInCompanyId = :workInEmployerId",
            WorkInCompanyReservation.class)
            .setParameter("workInEmployerId", workInCompanyFromDbAfterEdit.getWorkInCompanyId())
            .uniqueResult()
    );

    assertEquals(employerIdAfterEdit, workInCompanyFromDbAfterEdit.getEmployerId(), "Wrong employer id");
    assertEquals(displayNameAfterEdit, workInCompanyFromDbAfterEdit.getDisplayName(), "Wrong display name");
    assertEquals(commentAfterEdit, workInCompanyFromDbAfterEdit.getComment(), "Wrong comment");

    assertNotNull(workInCompanyReservation, "Reservation must not be null");
    assertEquals(0, workInCompanyReservation.getReservationNumber(), "Wrong reservation number");

    assertNotNull(workInCompanyFromDbAfterEdit.getWorkInCompanyLogoId(), "WorkInCompany have logoId");
    WorkInCompanyLogo workInCompanyLogo = getInTransaction(
        session -> session.get(WorkInCompanyLogo.class, workInCompanyFromDbAfterEdit.getWorkInCompanyLogoId())
    );
    assertNotNull(workInCompanyLogo, "WorkInCompanyLogo doesn't exist");
  }

  @Test
  public void getWorkInCompanyInfoTest() {
    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    WorkInCompanyReservation workInCompanyReservation1 = getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });
    WorkInCompanyReservation workInCompanyReservation2 = getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(MOBILE);
    });

    WorkInCompanyFormDto workInCompanyFormDto = workInCompanyService.getWorkInCompany(workInCompany.getWorkInCompanyId());

    assertEquals(workInCompany.getDisplayName(), workInCompanyFormDto.getDisplayName(), "Wrong display name");
    assertEquals(workInCompany.getEmployerId(), workInCompanyFormDto.getEmployerId(), "Wrong employer id");
    assertEquals(workInCompany.getWorkInCompanyLogoId(), workInCompanyFormDto.getWorkInCompanyLogoId(), "Wrong work in company logo id");
    assertEquals(workInCompanyReservation1.getDateStart(), workInCompanyFormDto.getDateStart(), "Wrong date start");
    assertEquals(workInCompanyReservation2.getDateEnd(), workInCompanyFormDto.getDateEnd(), "Wrong date end");
    assertEquals(
        Set.of(workInCompanyReservation1.getRegionId(), workInCompanyReservation2.getRegionId()),
        workInCompanyFormDto.getRegionIds(),
        "Wrong region ids"
    );
    assertEquals(
        Set.of(workInCompanyReservation1.getDeviceType(), workInCompanyReservation2.getDeviceType()),
        workInCompanyFormDto.getDeviceTypes(),
        "Wrong device types"
    );
  }

  @Test
  public void createEmployerWithWrongLogo() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";

    byte[] bytes = Base64.getDecoder().decode(fileSettings.getString("base64.big_example"));
    assertThrows(
        WebApplicationException.class,
        () -> workInCompanyService.createLogoWorkInCompany(ImageType.XS, ContentImage.fromBytes(bytes)),
        "Should fail cause image too big"
    );

    long returnedId = runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            null,
            Set.of(TargetingDeviceType.DESKTOP))
    ));
    WorkInCompany workInCompanyFromDb = getInTransaction(session -> session.get(WorkInCompany.class, returnedId));

    refreshHibernateEntity(workInCompanyFromDb);
    assertNull(workInCompanyFromDb.getWorkInCompanyLogoId(), "WorkInCompany have logoId, but shouldn't");
  }

  @Test
  public void testAllRegionsFree() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.FREE, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
  }

  @Test
  @DisplayName("All regions is busy but free on the next week")
  public void testOne() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions is busy but free on the next week. With two target devic types")
  public void testTwo() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP, TargetingDeviceType.MOBILE)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions filled only on half")
  public void testThree() {
    getSettingsTestClient().addSetting(MAX_COMPANIES_ON_PAGE_SETTING_NAME, "2");

    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP, TargetingDeviceType.MOBILE)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.FREE, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
  }

  @Test
  @DisplayName("One region is busy but free on the next week. Another region is free")
  public void testFour() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId1 = nextId();
    int regionId2 = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId1);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId1, regionId2),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(2, reserveNextFreePeriods.size(), "Size of return array must be 1");

    for (ReserveNextFreePeriod reserveNextFreePeriod : reserveNextFreePeriods) {
      if (reserveNextFreePeriod.getRegionId() == regionId1) {
        assertEquals(ReserveStatus.BUSY, reserveNextFreePeriod.getStatus(), "Status must be free");
        assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriod.getDateFrom(), "Wrong next free dateFrom");
      } else {
        assertEquals(ReserveStatus.FREE, reserveNextFreePeriod.getStatus(), "Status must be free");
      }
    }
  }

  @Test
  @DisplayName("All regions is free but busy in another region")
  public void testFive() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(nextId());
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.FREE, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
  }

  @Test
  @DisplayName("All regions free but busy on another target device type")
  public void testSix() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setDeviceType(TargetingDeviceType.MOBILE);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.FREE, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
  }

  @Test
  public void testAllRegionsBusyFreeAfterTwoWeeks() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(1));
      it.setDateEnd(localDateTime.plusWeeks(1).plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(2), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions is busy and after three week, but free after two weeks")
  public void testSeven() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(2));
      it.setDateEnd(localDateTime.plusWeeks(2).plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions busy on next week, after three weeks but free after two weeks, duration is two weeks")
  public void testEight() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(2));
      it.setDateEnd(localDateTime.plusWeeks(2).plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusWeeks(1).plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(3), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions busy on next week, after four week, but free after two weeks and duration is two weeks")
  public void testNine() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(3));
      it.setDateEnd(localDateTime.plusWeeks(3).plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusWeeks(1).plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  public void getWorkInCompanyById() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    String employerName = "EmployerName";

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });
    when(employerClient.getEmployerInfo(workInCompany.getEmployerId()))
        .thenReturn(success(new EmployerInfoResponse(workInCompany.getEmployerId(), employerName)));

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            workInCompany.getWorkInCompanyId(), null, null, null, Set.of(), false, Set.of(), Set.of()
        );

    assertEquals(1, workInCompaniesByFilter.size(), "Work in company list size must be 1");
    assertEquals(workInCompany.getWorkInCompanyId(), workInCompaniesByFilter.get(0).getWorkInCompanyId(), "Wrong work in company id");
    assertEquals(localDateTime, workInCompaniesByFilter.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusDays(6), workInCompaniesByFilter.get(0).getDateTo(), "Wrong date to");
    assertEquals(workInCompany.getEmployerId(), workInCompaniesByFilter.get(0).getEmployer().getEmployerId(), "Wrong employer id");
    assertEquals(employerName, workInCompaniesByFilter.get(0).getEmployer().getEmployerName(), "Wrong employer name");
  }

  @Test
  @DisplayName("Get work in company list for period")
  public void testTen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    String employerName = "EmployerName";

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    mockEmployerInfoListResponse(workInCompany, employerName);

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(), Set.of()
        );

    assertEquals(1, workInCompaniesByFilter.size(), "Work in company list size must be 1");
    assertEquals(workInCompany.getWorkInCompanyId(), workInCompaniesByFilter.get(0).getWorkInCompanyId(), "Wrong work in company id");
    assertEquals(localDateTime, workInCompaniesByFilter.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusDays(6), workInCompaniesByFilter.get(0).getDateTo(), "Wrong date to");
    assertEquals(workInCompany.getEmployerId(), workInCompaniesByFilter.get(0).getEmployer().getEmployerId(), "Wrong employer id");
    assertEquals(employerName, workInCompaniesByFilter.get(0).getEmployer().getEmployerName(), "Wrong employer name");
  }

  @Test
  @DisplayName("Get work in company list for period when period of work in company is two weeks")
  public void testEleven() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(1));
      it.setDateEnd(localDateTime.plusWeeks(1).plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(), Set.of()
        );

    assertEquals(1, workInCompaniesByFilter.size(), "Work in company list size must be 1");
    assertEquals(workInCompany.getWorkInCompanyId(), workInCompaniesByFilter.get(0).getWorkInCompanyId(), "Wrong work in company id");
    assertEquals(localDateTime, workInCompaniesByFilter.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusWeeks(1).plusDays(6), workInCompaniesByFilter.get(0).getDateTo(), "Wrong date to");
  }

  @Test
  @DisplayName("Get work in company list for period wrong employer")
  public void testTwelve() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(null, nextId(), localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(), Set.of());

    assertEquals(0, workInCompaniesByFilter.size(), "Work in company list size must be 0");
  }

  @Test
  @DisplayName("Get work in company list for period wrong period")
  public void testThirteen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, nextId(), localDateTime.plusWeeks(1), localDateTime.plusWeeks(2), Set.of(), false, Set.of(), Set.of()
        );

    assertEquals(0, workInCompaniesByFilter.size(), "Work in company list size must be 0");
  }

  @Test
  @DisplayName("Get work in company list for period without employer id")
  public void testFourteen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(null, null, localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(), Set.of());

    assertEquals(1, workInCompaniesByFilter.size(), "Work in company list size must be 1");
    assertEquals(workInCompany.getWorkInCompanyId(), workInCompaniesByFilter.get(0).getWorkInCompanyId(), "Wrong work in company id");
    assertEquals(localDateTime, workInCompaniesByFilter.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusDays(6), workInCompaniesByFilter.get(0).getDateTo(), "Wrong date to");
  }

  @Test
  @DisplayName("Get work in company list for period with not exactly region filter")
  public void testFifteen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });
    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(regionId), false, Set.of(), Set.of()
        );

    assertEquals(1, workInCompaniesByFilter.size(), "Work in company list size must be 1");
    assertEquals(workInCompany.getWorkInCompanyId(), workInCompaniesByFilter.get(0).getWorkInCompanyId(), "Wrong work in company id");
    assertEquals(localDateTime, workInCompaniesByFilter.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusDays(6), workInCompaniesByFilter.get(0).getDateTo(), "Wrong date to");
  }

  @Test
  @DisplayName("Get work in company list for period with different region filter")
  public void testSixteen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(nextId()), false, Set.of(), Set.of()
        );

    assertEquals(0, workInCompaniesByFilter.size(), "Work in company list size must be 0");
  }

  @Test
  @DisplayName("Get work in company list for period with device type filter")
  public void testSeventeen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(DESKTOP), Set.of()
        );

    assertEquals(1, workInCompaniesByFilter.size(), "Work in company list size must be 1");
    assertEquals(workInCompany.getWorkInCompanyId(), workInCompaniesByFilter.get(0).getWorkInCompanyId(), "Wrong work in company id");
    assertEquals(localDateTime, workInCompaniesByFilter.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusDays(6), workInCompaniesByFilter.get(0).getDateTo(), "Wrong date to");
  }

  @Test
  @DisplayName("Get work in company list for period with different device type filter")
  public void testEighteen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(MOBILE), Set.of()
        );

    assertEquals(0, workInCompaniesByFilter.size(), "Work in company list size must be 0");
  }

  @Test
  @DisplayName("Edit work in company status")
  public void testNineteen() throws Exception {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });

    assertEquals(ACTIVE, workInCompany.getStatus(), "Wrong status");

    runAsBackoffice(() -> {
          workInCompanyService.editWorkInCompanyStatus(PAUSED, workInCompany.getWorkInCompanyId());
          return null;
        }, workInCompany.getManagerUserId()
    );
    refreshHibernateEntity(workInCompany);

    assertEquals(PAUSED, workInCompany.getStatus(), "Wrong status");
  }

  @Test
  @DisplayName("Edit work in company status with same status")
  public void testTwenty() throws Exception {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });

    assertEquals(ACTIVE, workInCompany.getStatus(), "Wrong status");

    runAsBackoffice(() -> {
          workInCompanyService.editWorkInCompanyStatus(ACTIVE, workInCompany.getWorkInCompanyId());
          return null;
        }, workInCompany.getManagerUserId()
    );
    refreshHibernateEntity(workInCompany);

    assertEquals(ACTIVE, workInCompany.getStatus(), "Wrong status");
  }

  @Test
  @DisplayName("Delete work in company")
  public void testTwentyOne() throws Exception {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });

    assertEquals(ACTIVE, workInCompany.getStatus(), "Wrong status");

    runAsBackoffice(() -> {
          workInCompanyService.deleteWorkInCompanyStatus(workInCompany.getWorkInCompanyId());
          return null;
        }, workInCompany.getManagerUserId()
    );
    refreshHibernateEntity(workInCompany);

    assertEquals(DELETED, workInCompany.getStatus(), "Wrong status");

    List<WorkInCompanyReservation> workInCompanyReservation = getInTransaction(session ->
        session.createQuery(
            "from WorkInCompanyReservation where workInCompanyId = :workInEmployerId",
            WorkInCompanyReservation.class)
            .setParameter("workInEmployerId", workInCompany.getWorkInCompanyId())
            .list()
    );

    assertEquals(0, workInCompanyReservation.size(), "Reservation must be cleared after delete");
  }

  @Test
  @DisplayName("Get work in company list for period with status filter")
  public void testTwentyTwo() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {
      it.setStatus(ACTIVE);
    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(), Set.of(ACTIVE)
        );

    assertEquals(1, workInCompaniesByFilter.size(), "Work in company list size must be 1");
    assertEquals(workInCompany.getWorkInCompanyId(), workInCompaniesByFilter.get(0).getWorkInCompanyId(), "Wrong work in company id");
    assertEquals(localDateTime, workInCompaniesByFilter.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusDays(6), workInCompaniesByFilter.get(0).getDateTo(), "Wrong date to");
  }

  @Test
  @DisplayName("Get work in company list for period with different status filter")
  public void testTwentyThree() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {
      it.setStatus(ACTIVE);
    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
      it.setDeviceType(DESKTOP);
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(), false, Set.of(), Set.of(PAUSED)
        );

    assertEquals(0, workInCompaniesByFilter.size(), "Work in company list size must be 0");
  }

  @Test
  @DisplayName("Get work in company list for period with exactly region filter")
  public void testTwentyFour() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    int regionIdTwo = nextId();

    WorkInCompany workInCompany = getEntityGenerators().createWorkInCompany(it -> {

    });

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });
    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionIdTwo);
      it.setWorkInCompanyId(workInCompany.getWorkInCompanyId());
    });

    mockEmployerInfoListResponse(workInCompany, "EmployerName");

    List<WorkInCompaniesListItem> workInCompaniesByFilter = workInCompanyService
        .getWorkInCompaniesByFilter(
            null, workInCompany.getEmployerId(), localDateTime, localDateTime.plusWeeks(1), Set.of(regionId), true, Set.of(), Set.of()
        );

    assertEquals(0, workInCompaniesByFilter.size(), "Work in company list size must be 0");
  }

  @Test
  @DisplayName("All regions filled on first week and only on half on second week")
  public void testTwentyFive() {
    getSettingsTestClient().addSetting(MAX_COMPANIES_ON_PAGE_SETTING_NAME, "2");

    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(1));
      it.setDateEnd(localDateTime.plusWeeks(1).plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP, TargetingDeviceType.MOBILE)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions filled on first and second week and only on half on third week")
  public void testTwentySix() {
    getSettingsTestClient().addSetting(MAX_COMPANIES_ON_PAGE_SETTING_NAME, "2");

    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(1));
      it.setDateEnd(localDateTime.plusWeeks(1).plusDays(6));
      it.setRegionId(regionId);
    });
    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(1));
      it.setDateEnd(localDateTime.plusWeeks(1).plusDays(6));
      it.setRegionId(regionId);
    });
    getEntityGenerators().createWorkInCompanyReservation(it -> {
      it.setDateStart(localDateTime.plusWeeks(1));
      it.setDateEnd(localDateTime.plusWeeks(1).plusDays(6));
      it.setRegionId(regionId);
    });

    List<ReserveNextFreePeriod> reserveNextFreePeriods = workInCompanyService.checkBusy(new WorkInCompanyCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        Set.of(TargetingDeviceType.DESKTOP, TargetingDeviceType.MOBILE)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(2), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  private void mockEmployerInfoListResponse(WorkInCompany workInCompany, String employerName) {
    EmployerInfoListResponse listResponse = new EmployerInfoListResponse();
    listResponse.setItems(List.of(new EmployerInfoResponse(workInCompany.getEmployerId(), employerName)));
    when(employerClient.getEmployerListByIds(anyCollection())).thenReturn(success(listResponse));
  }
}
