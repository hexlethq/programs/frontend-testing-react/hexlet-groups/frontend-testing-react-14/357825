package ru.hh.content.engine;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class EnumArrayConverter extends SimpleArgumentConverter {
  @Override
  protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
    if (source instanceof String && Enum[].class.isAssignableFrom(targetType)) {
      List<Object> objects = Arrays.stream(((String) source).split("\\s*,\\s*"))
          .map(it -> valueOfEnum(targetType.getComponentType(), it)).collect(Collectors.toList());

      Object array = Array.newInstance(targetType.componentType(), objects.size());

      for (int i = 0; i < objects.size(); i++) {
        Array.set(array, i, objects.get(i));
      }

      return array;
    } else {
      throw new IllegalArgumentException(String.format("Conversion from %s to %s not supported.", source.getClass(), targetType));
    }
  }

  @SuppressWarnings({ "unchecked", "rawtypes" })
  private Object valueOfEnum(Class targetType, String source) {
    return Enum.valueOf(targetType, source);
  }
}
