package ru.hh.content.engine.utils;

import java.util.HashMap;
import java.util.Map;
import ru.hh.content.engine.services.Translations;
import ru.hh.translations2.TranslationKey;

public class TranslationsMock extends Translations {
  private static final Map<TranslationKey, String> TRANSLATIONS = new HashMap<>();

  @Override
  public String getTranslationWithoutFallback(String name, int siteId, String lang) {
    return TRANSLATIONS.get(TranslationKey.of(name, siteId, lang));
  }

  public void setTranslation(String name, int siteId, String lang, String value) {
    TRANSLATIONS.put(TranslationKey.of(name, siteId, lang), value);
  }

  public void deleteTranslation(String name, int siteId, String lang) {
    TRANSLATIONS.remove(TranslationKey.of(name, siteId, lang));
  }

  public void clear() {
    TRANSLATIONS.clear();
  }
}
