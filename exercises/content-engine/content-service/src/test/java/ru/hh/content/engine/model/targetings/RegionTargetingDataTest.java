package ru.hh.content.engine.model.targetings;

import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.utils.IntegerArrayConverter;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class RegionTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "'1','2',false",
          "'1,2','1',true",
          "'1,2','3',false",
          "'1,2','2,3',true",
      },
      nullValues = "null")
  public void matchRegionTargetingTest(@ConvertWith(IntegerArrayConverter.class) Integer[] targetingRegionIds,
                                       @ConvertWith(IntegerArrayConverter.class) Integer[] userRegionIds,
                                       boolean isMatch) {
    RegionTargetingData regionTargetingData = new RegionTargetingData(Set.of(targetingRegionIds));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(userRegionIds));

    assertEquals(isMatch, regionTargetingData.match(userDataForTargeting), "Region targeting wrong match");
  }
}
