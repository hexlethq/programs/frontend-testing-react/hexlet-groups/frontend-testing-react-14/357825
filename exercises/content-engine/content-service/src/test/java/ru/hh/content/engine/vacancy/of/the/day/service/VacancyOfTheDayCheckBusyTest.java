package ru.hh.content.engine.vacancy.of.the.day.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.utils.CommonTestUtils.nextMonday;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayCheckRequestDto;
import static ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayService.MAX_VACANCIES_ON_PAGE;
import ru.hh.content.engine.work.in.company.dto.ReserveNextFreePeriod;
import ru.hh.content.engine.work.in.company.dto.ReserveStatus;

public class VacancyOfTheDayCheckBusyTest extends AppTestBase {
  @Inject
  private VacancyOfTheDayService vacancyOfTheDayService;

  @Test
  public void testAllRegionsFree() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.FREE, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
  }

  @Test
  @DisplayName("All regions is busy but free on the next week")
  public void testOne() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions is busy but free on the next week. With two target devic types")
  public void testTwo() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP, TargetingDeviceType.MOBILE)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("One region is busy but free on the next week. Another region is free")
  public void testThree() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId1 = nextId();
    int regionId2 = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId1);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId1, regionId2),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(2, reserveNextFreePeriods.size(), "Size of return array must be 1");

    for (ReserveNextFreePeriod reserveNextFreePeriod : reserveNextFreePeriods) {
      if (reserveNextFreePeriod.getRegionId() == regionId1) {
        assertEquals(ReserveStatus.BUSY, reserveNextFreePeriod.getStatus(), "Status must be free");
        assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriod.getDateFrom(), "Wrong next free dateFrom");
      } else {
        assertEquals(ReserveStatus.FREE, reserveNextFreePeriod.getStatus(), "Status must be free");
      }
    }
  }

  @Test
  @DisplayName("All regions is free but busy in another region")
  public void testFour() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(nextId());
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.FREE, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
  }

  @Test
  @DisplayName("All regions free but busy on another target device type")
  public void testFive() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId);
        it.setDeviceType(TargetingDeviceType.MOBILE);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.FREE, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
  }

  @Test
  public void testAllRegionsBusyFreeAfterTwoWeeks() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId);
      });
    }

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime.plusWeeks(1));
        it.setDateEnd(localDateTime.plusWeeks(1).plusDays(6));
        it.setRegionId(regionId);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(2), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions is busy and after three week, but free after two weeks")
  public void testSix() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId);
      });
    }

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime.plusWeeks(2));
        it.setDateEnd(localDateTime.plusWeeks(2).plusDays(6));
        it.setRegionId(regionId);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions busy on next week, after three weeks but free after two weeks, duration is two weeks")
  public void testSeven() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId);
      });
    }

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime.plusWeeks(2));
        it.setDateEnd(localDateTime.plusWeeks(2).plusDays(6));
        it.setRegionId(regionId);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusWeeks(1).plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(3), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }

  @Test
  @DisplayName("All regions busy on next week, after four week, but free after two weeks and duration is two weeks")
  public void testEight() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime);
        it.setDateEnd(localDateTime.plusDays(6));
        it.setRegionId(regionId);
      });
    }

    for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
      getEntityGenerators().createVacancyOfTheDayReservation(it -> {
        it.setDateStart(localDateTime.plusWeeks(3));
        it.setDateEnd(localDateTime.plusWeeks(3).plusDays(6));
        it.setRegionId(regionId);
      });
    }

    List<ReserveNextFreePeriod> reserveNextFreePeriods = vacancyOfTheDayService.checkBusy(new VacancyOfTheDayCheckRequestDto(
        localDateTime,
        localDateTime.plusWeeks(1).plusDays(6),
        Set.of(regionId),
        PacketType.CUSTOM,
        Set.of(TargetingDeviceType.DESKTOP)
    ));

    assertEquals(1, reserveNextFreePeriods.size(), "Size of return array must be 1");
    assertEquals(regionId, reserveNextFreePeriods.get(0).getRegionId(), "Wrong region id");
    assertEquals(ReserveStatus.BUSY, reserveNextFreePeriods.get(0).getStatus(), "Status must be free");
    assertEquals(localDateTime.plusWeeks(1), reserveNextFreePeriods.get(0).getDateFrom(), "Wrong next free dateFrom");
  }
}
