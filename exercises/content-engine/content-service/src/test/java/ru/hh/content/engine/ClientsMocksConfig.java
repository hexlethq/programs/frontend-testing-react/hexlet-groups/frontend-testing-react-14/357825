package ru.hh.content.engine;

import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.hh.analytics.jclient.ExperimentsCheckClient;
import ru.hh.content.engine.clients.banner.BannerClient;
import ru.hh.content.engine.clients.logic.LogicClient;
import ru.hh.content.engine.clients.xmlback.EmployerClient;
import ru.hh.content.engine.clients.xmlback.HHAreasClient;
import ru.hh.content.engine.clients.xmlback.HHVacancyClient;
import ru.hh.content.engine.clients.xmlback.WorkInCompanyClient;
import ru.hh.content.engine.services.ContentWebDavService;
import ru.hh.hh.session.client.HhSessionClientFactory;
import ru.hh.hhid.client.impl.HhidJClientFactory;
import ru.hh.hhinvoker.client.InvokerClient;

@Configuration
public class ClientsMocksConfig {

  @Bean
  @Qualifier("mock")
  public HhSessionClientFactory sessionClientFactory() {
    return Mockito.mock(HhSessionClientFactory.class);
  }

  @Bean
  @Qualifier("mock")
  public HhidJClientFactory hhidJClientFactory() {
    return Mockito.mock(HhidJClientFactory.class);
  }

  @Bean
  @Qualifier("mock")
  public ContentWebDavService webDavService() {
    return Mockito.mock(ContentWebDavService.class);
  }

  @Bean
  @Qualifier("mock")
  public WorkInCompanyClient workInCompanyClient() {
    return Mockito.mock(WorkInCompanyClient.class);
  }

  @Bean
  @Qualifier("mock")
  public EmployerClient employerClient() {
    return Mockito.mock(EmployerClient.class);
  }

  @Bean
  @Qualifier("mock")
  public HHAreasClient hhAreasClient() {
    return Mockito.mock(HHAreasClient.class);
  }

  @Bean
  @Qualifier("mock")
  public HHVacancyClient hhVacancyClient() {
    return Mockito.mock(HHVacancyClient.class);
  }

  @Bean
  @Qualifier("mock")
  public InvokerClient invokerClient() {
    return Mockito.mock(InvokerClient.class);
  }

  @Bean
  @Qualifier("mock")
  public LogicClient logicClient() {
    return Mockito.mock(LogicClient.class);
  }

  @Bean
  @Qualifier("mock")
  public BannerClient bannerClient() {
    return Mockito.mock(BannerClient.class);
  }

  @Bean
  @Qualifier("mock")
  public ExperimentsCheckClient experimentsCheckClient() {
    return Mockito.mock(ExperimentsCheckClient.class);
  }
}
