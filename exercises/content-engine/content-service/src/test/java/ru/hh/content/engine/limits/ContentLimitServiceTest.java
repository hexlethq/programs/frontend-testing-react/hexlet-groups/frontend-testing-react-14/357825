package ru.hh.content.engine.limits;

import javax.inject.Inject;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.enums.ContentLimitType;
import ru.hh.content.engine.model.limits.ContentLimit;
import ru.hh.memcached.HHMemcachedTestClient;

class ContentLimitServiceTest extends AppTestBase {
  @Inject
  private HHMemcachedTestClient hhMemcachedTestClient;
  @Inject
  private ContentLimitService contentLimitService;

  @Test
  public void testGetReachedFromMemcached() {
    ContentLimit contentLimit = getEntityGenerators().createContentLimit(it -> {
      it.setContentLimitType(ContentLimitType.IMPRESSIONS);
    });

    hhMemcachedTestClient.set(ContentLimitService.MEMCACHED_REGION, contentLimit.getContentLimitId().toString(), 0, true);

    assertTrue(contentLimitService.isReached(contentLimit.getContentLimitId()), "Wrong reached");
  }

  @Test
  public void testGetNotReachedFromMemcached() {
    ContentLimit contentLimit = getEntityGenerators().createContentLimit(it -> {
      it.setContentLimitType(ContentLimitType.IMPRESSIONS);
    });

    hhMemcachedTestClient.set(ContentLimitService.MEMCACHED_REGION, contentLimit.getContentLimitId().toString(), 0, false);

    assertFalse(contentLimitService.isReached(contentLimit.getContentLimitId()), "Wrong reached");
  }

  @Test
  public void testGetReached() {
    ContentLimit contentLimit = getEntityGenerators().createContentLimit(it -> {
      it.setContentLimitType(ContentLimitType.IMPRESSIONS);
    });

    getEntityGenerators().createImpressionsLimit(it -> {
      it.setContentLimitId(contentLimit.getContentLimitId());
      it.setLimitDailyImpressions(10L);
      it.setCurrentDailyImpressions(20L);
    });

    assertTrue(contentLimitService.isReached(contentLimit.getContentLimitId()), "Wrong reached");
  }

  @Test
  public void testGetNotReached() {
    ContentLimit contentLimit = getEntityGenerators().createContentLimit(it -> {
      it.setContentLimitType(ContentLimitType.IMPRESSIONS);
    });

    getEntityGenerators().createImpressionsLimit(it -> {
      it.setContentLimitId(contentLimit.getContentLimitId());
      it.setLimitDailyImpressions(20L);
      it.setCurrentDailyImpressions(10L);
    });

    assertFalse(contentLimitService.isReached(contentLimit.getContentLimitId()), "Wrong reached");
  }

  @Test
  public void testGetReachedForTwoLimits() {
    ContentLimit contentLimit1 = getEntityGenerators().createContentLimit(it -> {
      it.setContentLimitType(ContentLimitType.IMPRESSIONS);
    });
    ContentLimit contentLimit2 = getEntityGenerators().createContentLimit(it -> {
      it.setContentLimitType(ContentLimitType.CLICKS);
      it.setContentLimitId(contentLimit1.getContentLimitId());
    });

    getEntityGenerators().createImpressionsLimit(it -> {
      it.setContentLimitId(contentLimit1.getContentLimitId());
      it.setLimitDailyImpressions(10L);
      it.setCurrentDailyImpressions(20L);
    });
    getEntityGenerators().createClicksLimit(it -> {
      it.setContentLimitId(contentLimit1.getContentLimitId());
      it.setLimitDailyClicks(20L);
      it.setCurrentDailyClicks(10L);
    });

    assertTrue(contentLimitService.isReached(contentLimit1.getContentLimitId()), "Wrong reached");
  }
}
