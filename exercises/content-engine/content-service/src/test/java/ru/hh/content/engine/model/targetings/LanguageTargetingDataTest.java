package ru.hh.content.engine.model.targetings;

import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.EnumArrayConverter;
import ru.hh.content.engine.model.enums.TargetingLanguage;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class LanguageTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "'RUSSIAN','RUSSIAN',true",
          "'GERMAN','RUSSIAN',false",
          "'GERMAN,FRENCH','RUSSIAN,FRENCH',true",
          "'ENGLISH','RUSSIAN,FRENCH,GERMAN',false",
          "'RUSSIAN,ENGLISH,FRENCH','GERMAN',false",
          "'GERMAN,FRENCH','RUSSIAN,ENGLISH',false",
          "'GERMAN,FRENCH,ENGLISH,RUSSIAN','RUSSIAN,ENGLISH,FRENCH,GERMAN',true",
      },
      nullValues = "null")
  public void matchLanguageTargetingTest(@ConvertWith(EnumArrayConverter.class) TargetingLanguage[] targetingLanguages,
                                         @ConvertWith(EnumArrayConverter.class) TargetingLanguage[] userLanguages,
                                         boolean isMatch) {
    LanguageTargetingData languageTargetingData = new LanguageTargetingData(Set.of(targetingLanguages));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setLanguages(Set.of(userLanguages));

    assertEquals(isMatch, languageTargetingData.match(userDataForTargeting), "Language targeting wrong match");
  }
}
