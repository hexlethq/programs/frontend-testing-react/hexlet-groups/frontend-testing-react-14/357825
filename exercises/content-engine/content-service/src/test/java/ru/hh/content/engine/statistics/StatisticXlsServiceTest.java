package ru.hh.content.engine.statistics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.Iterator;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static ru.hh.content.api.dto.ContentType.CAMPAIGN_OF_THE_DAY;

class StatisticXlsServiceTest {
  private static final String DATES_XLS_FILE_NAME = "dates.xlsx";
  private static final String REGIONS_XLS_FILE_NAME = "regions.xlsx";

  @Test
  public void testRegions(@TempDir File file) throws Exception {
    CHStatisticsDao chStatisticsDao = mock(CHStatisticsDao.class);

    SumStatisticByRegion sumStatisticByRegion = new SumStatisticByRegion();

    StatisticRow finalStatisticRow = new StatisticRow(
        new Clicks(0L, 2L, 2L, new Percent(0, 100)),
        new Views(0L, 3L, 3L, new Percent(0, 100)),
        new Ctr(0.0f, 66f, 66f)
    );

    StatisticRow statisticRowRegion = new StatisticRow(
        new Clicks(0L, 2L, 2L, new Percent(0, 100)),
        new Views(0L, 3L, 3L, new Percent(0, 100)),
        new Ctr(0.0f, 66f, 66f)
    );

    sumStatisticByRegion.setTotalStatistic(finalStatisticRow);
    sumStatisticByRegion.setStatisticByRegions(List.of(
        new StatisticByRegion("Москва", statisticRowRegion)
    ));

    when(chStatisticsDao.getStatisticsByRegion(any(), any(), anyInt(), any())).thenReturn(sumStatisticByRegion);

    StatisticXlsService statisticXlsService = new StatisticXlsService(chStatisticsDao);

    String fileName = file.getAbsolutePath() + "/" + REGIONS_XLS_FILE_NAME;
    URI testFile = this.getClass().getClassLoader().getResource(REGIONS_XLS_FILE_NAME).toURI();
    StatisticXlsService.saveWorkBookToFile(statisticXlsService.getStatisticByRegionXls(
        LocalDate.now(), LocalDate.now(), 0, CAMPAIGN_OF_THE_DAY), fileName
    );
    checkFiles(Paths.get(fileName).toFile(), Paths.get(testFile).toFile());
  }

  @Test
  public void testDates(@TempDir File file) throws Exception {
    CHStatisticsDao chStatisticsDao = mock(CHStatisticsDao.class);

    SumStatisticByDate sumStatisticByDate = new SumStatisticByDate();

    StatisticRow finalStatisticRow = new StatisticRow(
        new Clicks(0L, 2L, 2L, new Percent(0, 100)),
        new Views(0L, 3L, 3L, new Percent(0, 100)),
        new Ctr(0f, 66f, 66f)
    );

    StatisticRow statisticRowDayFirst = new StatisticRow(
        new Clicks(0L, 1L, 1L, new Percent(0, 100)),
        new Views(0L, 1L, 1L, new Percent(0, 100)),
        new Ctr(0f, 100f, 100f)
    );

    StatisticRow statisticRowDayNotFirst = new StatisticRow(
        new Clicks(0L, 1L, 1L, new Percent(0, 100)),
        new Views(0L, 2L, 2L, new Percent(0, 100)),
        new Ctr(0f, 50f, 50f)
    );

    sumStatisticByDate.setTotalStatistic(finalStatisticRow);
    sumStatisticByDate.setStatisticsByDates(List.of(
        new StatisticsByDate(LocalDate.of(2020, 9, 23), statisticRowDayFirst),
        new StatisticsByDate(LocalDate.of(2020, 9, 24), statisticRowDayNotFirst)
    ));

    when(chStatisticsDao.getStatisticsByDate(any(), any(), anyInt(), any())).thenReturn(sumStatisticByDate);

    StatisticXlsService statisticXlsService = new StatisticXlsService(chStatisticsDao);

    String fileName = file.getAbsolutePath() + "/" + DATES_XLS_FILE_NAME;
    URI testFile = this.getClass().getClassLoader().getResource(DATES_XLS_FILE_NAME).toURI();
    StatisticXlsService.saveWorkBookToFile(
        statisticXlsService.getStatisticByDateXls(LocalDate.now(), LocalDate.now(), 0, CAMPAIGN_OF_THE_DAY), fileName
    );
    checkFiles(Paths.get(fileName).toFile(), Paths.get(testFile).toFile());
  }

  private void checkFiles(File file1, File file2) {
    try (FileInputStream input1 = new FileInputStream(file1);
         Workbook book1 = new XSSFWorkbook(input1);
         FileInputStream input2 = new FileInputStream(file2);
         Workbook book2 = new XSSFWorkbook(input2)) {
      Sheet sheet1 = book1.getSheetAt(0);
      Sheet sheet2 = book2.getSheetAt(0);
      Iterator<Row> rowIterator1 = sheet1.iterator();
      Iterator<Row> rowIterator2 = sheet2.iterator();
      while (rowIterator1.hasNext() && rowIterator2.hasNext()) {
        Row currentRow1 = rowIterator1.next();
        Row currentRow2 = rowIterator2.next();
        Iterator<Cell> cellIterator1 = currentRow1.iterator();
        Iterator<Cell> cellIterator2 = currentRow2.iterator();
        while (cellIterator1.hasNext() && cellIterator2.hasNext()) {
          Cell currentCell1 = cellIterator1.next();
          Cell currentCell2 = cellIterator2.next();
          assertEquals(((XSSFCell) currentCell1).getRawValue(), ((XSSFCell) currentCell2).getRawValue(),
              "Data in cell is not equals to expected!");
        }
      }
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }
}
