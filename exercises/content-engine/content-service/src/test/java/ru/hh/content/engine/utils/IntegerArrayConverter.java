package ru.hh.content.engine.utils;

import java.util.Arrays;
import org.junit.jupiter.params.converter.ArgumentConversionException;
import org.junit.jupiter.params.converter.SimpleArgumentConverter;

public class IntegerArrayConverter extends SimpleArgumentConverter {
  @Override
  protected Object convert(Object source, Class<?> targetType) throws ArgumentConversionException {
    if (source instanceof String && Integer[].class.isAssignableFrom(targetType)) {
      return Arrays.stream(((String) source).split("\\s*,\\s*"))
          .map(Integer::valueOf).toArray(Integer[]::new);
    } else {
      throw new IllegalArgumentException(String.format("Conversion from %s to %s not supported.", source.getClass(), targetType));
    }
  }
}
