package ru.hh.content.engine.model.targetings;

import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.EnumArrayConverter;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class DeviceTypeTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "'DESKTOP',DESKTOP,true",
          "'DESKTOP',MOBILE,false",
          "'MOBILE',DESKTOP,false",
          "'MOBILE',MOBILE,true",
          "'DESKTOP,MOBILE',DESKTOP,true",
          "'DESKTOP,MOBILE',MOBILE,true",
          "'DESKTOP,MOBILE',null,false",
          "'MOBILE',null,false",
          "'DESKTOP',null,false",
      },
      nullValues = "null")
  public void matchDeviceTypeTargetingTest(@ConvertWith(EnumArrayConverter.class) TargetingDeviceType[] deviceTypes,
                                           TargetingDeviceType userDeviceType,
                                           boolean isMatch) {
    DeviceTypeTargetingData deviceTypeTargetingData = new DeviceTypeTargetingData(Set.of(deviceTypes));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setDeviceType(userDeviceType);

    assertEquals(isMatch, deviceTypeTargetingData.match(userDataForTargeting), "Device type targeting wrong match");
  }
}
