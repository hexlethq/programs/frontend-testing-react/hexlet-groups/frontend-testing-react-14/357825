package ru.hh.content.engine.vacancy.of.the.day.service;

import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import static ru.hh.content.engine.security.SecurityContext.ADMIN_USER_IDS_SETTING_NAME;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayDao;
import ru.hh.mailer.utils.MailService;

class VacancyOfTheDayActionServiceTest extends AppTestBase {
  @Inject
  private VacancyOfTheDayActionsService vacancyOfTheDayActionsService;
  @Inject
  private VacancyOfTheDayDao vacancyOfTheDayDao;
  @Inject
  private MailService mailService;
  
  @Test
  public void setPaymentStatusBySuperAdminTest() throws Exception {
    int userId = Integer.parseInt(getSettingsTestClient().getString(ADMIN_USER_IDS_SETTING_NAME).orElseThrow().split(",")[0]);
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setPayedStatus(VacancyOfTheDayPayedStatus.NOT_PAYED)
    );
    
    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setPaymentStatus(vacancyOfTheDay.getVacancyOfTheDayId(), VacancyOfTheDayPayedStatus.PAYED),
        "Cannot change because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayPayedStatus.NOT_PAYED, vacancyOfTheDay.getPayedStatus(), "Should be initial");
    
    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setPaymentStatus(vacancyOfTheDay.getVacancyOfTheDayId(), VacancyOfTheDayPayedStatus.PAYED);
              return null;
            }
        ),
        "Cannot change because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayPayedStatus.NOT_PAYED, vacancyOfTheDay.getPayedStatus(), "Should be initial");
    
    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.setPaymentStatus(vacancyOfTheDay.getVacancyOfTheDayId(), VacancyOfTheDayPayedStatus.PAYED);
          return null;
        },
        userId
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayPayedStatus.PAYED, vacancyOfTheDay.getPayedStatus(), "Should changed");
  }
  
  @Test
  public void setPaymentStatusByOwnerTest() throws Exception {
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setPayedStatus(VacancyOfTheDayPayedStatus.NOT_PAYED)
    );
    
    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setPaymentStatus(vacancyOfTheDay.getVacancyOfTheDayId(), VacancyOfTheDayPayedStatus.PAYED),
        "Cannot change because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayPayedStatus.NOT_PAYED, vacancyOfTheDay.getPayedStatus(), "Should be initial");
    
    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setPaymentStatus(vacancyOfTheDay.getVacancyOfTheDayId(), VacancyOfTheDayPayedStatus.PAYED);
              return null;
            }
        ),
        "Cannot change because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayPayedStatus.NOT_PAYED, vacancyOfTheDay.getPayedStatus(), "Should be initial");
    
    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.setPaymentStatus(vacancyOfTheDay.getVacancyOfTheDayId(), VacancyOfTheDayPayedStatus.PAYED);
          return null;
        },
        vacancyOfTheDay.getManagerUserId()
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayPayedStatus.PAYED, vacancyOfTheDay.getPayedStatus(), "Should changed");
  }
  
  @Test
  public void deleteBySuperAdminTest() throws Exception {
    int userId = Integer.parseInt(getSettingsTestClient().getString(ADMIN_USER_IDS_SETTING_NAME).orElseThrow().split(",")[0]);
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> {}
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.delete(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Cannot delete because of rights"
    );
    
    assertNotNull(
        vacancyOfTheDayDao.get(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Should be deleted because of rights"
    );

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.delete(vacancyOfTheDay.getVacancyOfTheDayId());
              return null;
            }
        ),
        "Cannot delete because of rights"
    );
    
    assertNotNull(
        vacancyOfTheDayDao.get(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Should be deleted because of rights"
    );

    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.delete(vacancyOfTheDay.getVacancyOfTheDayId());
          return null;
        },
        userId
    );

    assertNull(
        vacancyOfTheDayDao.get(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Should be deleted because of rights"
    );
  }

  @Test
  public void deleteByOwnerTest() throws Exception {
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> {}
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.delete(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Cannot delete because of rights"
    );
    
    assertNotNull(
        vacancyOfTheDayDao.get(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Should be deleted because of rights"
    );

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.delete(vacancyOfTheDay.getVacancyOfTheDayId());
              return null;
            }
        ),
        "Cannot delete because of rights"
    );
    
    assertNotNull(
        vacancyOfTheDayDao.get(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Should be deleted because of rights"
    );

    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.delete(vacancyOfTheDay.getVacancyOfTheDayId());
          return null;
        },
        vacancyOfTheDay.getManagerUserId()
    );

    assertNull(
        vacancyOfTheDayDao.get(vacancyOfTheDay.getVacancyOfTheDayId()),
        "Should be deleted because of rights"
    );
  }

  @Test
  public void setStatusToModerationBySuperUserTest() throws Exception {
    int userId = Integer.parseInt(getSettingsTestClient().getString(ADMIN_USER_IDS_SETTING_NAME).orElseThrow().split(",")[0]);
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setStatus(VacancyOfTheDayStatus.NEW)
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setStatusToModeration(List.of(vacancyOfTheDay.getVacancyOfTheDayId())),
        "Cannot set status to moderation because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusToModeration(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
              return null;
            }
        ),
        "Cannot set status to moderation because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.setStatusToModeration(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
          return null;
        },
        userId
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.ON_MODERATION, vacancyOfTheDay.getStatus(), "Should be on moderation");
  }

  @Test
  public void setStatusToModerationByOwnerTest() throws Exception {
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setStatus(VacancyOfTheDayStatus.NEW)
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setStatusToModeration(List.of(vacancyOfTheDay.getVacancyOfTheDayId())),
        "Cannot set status to moderation because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusToModeration(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
              return null;
            }
        ),
        "Cannot set status to moderation because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.setStatusToModeration(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
          return null;
        },
        vacancyOfTheDay.getManagerUserId()
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.ON_MODERATION, vacancyOfTheDay.getStatus(), "Should be on moderation");
  }

  @Test
  public void setStatusApproveBySuperUserTest() throws Exception {
    int userId = Integer.parseInt(getSettingsTestClient().getString(ADMIN_USER_IDS_SETTING_NAME).orElseThrow().split(",")[0]);
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setStatus(VacancyOfTheDayStatus.NEW)
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setStatusApprove(List.of(vacancyOfTheDay.getVacancyOfTheDayId())),
        "Cannot set status to approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusApprove(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
              return null;
            }
        ),
        "Cannot set status to approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.setStatusApprove(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
          return null;
        },
        userId
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.APPROVED, vacancyOfTheDay.getStatus(), "Should be on approve");
  }

  @Test
  public void setStatusApproveByOwnerFailTest() {
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setStatus(VacancyOfTheDayStatus.NEW)
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setStatusApprove(List.of(vacancyOfTheDay.getVacancyOfTheDayId())),
        "Cannot set status to approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusApprove(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
              return null;
            }
        ),
        "Cannot set status to approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusApprove(List.of(vacancyOfTheDay.getVacancyOfTheDayId()));
              return null;
            },
            vacancyOfTheDay.getManagerUserId()
        ),
        "Cannot set status to approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");
  }

  @Test
  public void setStatusNotApprovedBySuperUserTest() throws Exception {
    int userId = Integer.parseInt(getSettingsTestClient().getString(ADMIN_USER_IDS_SETTING_NAME).orElseThrow().split(",")[0]);
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setStatus(VacancyOfTheDayStatus.NEW)
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setStatusNotApproved(List.of(vacancyOfTheDay.getVacancyOfTheDayId()), "reason"),
        "Cannot set status to not approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusNotApproved(List.of(vacancyOfTheDay.getVacancyOfTheDayId()), "reason");
              return null;
            }
        ),
        "Cannot set status to not approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    runAsBackoffice(
        () -> {
          vacancyOfTheDayActionsService.setStatusNotApproved(List.of(vacancyOfTheDay.getVacancyOfTheDayId()), "reason");
          return null;
        },
        userId
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.REJECTED, vacancyOfTheDay.getStatus(), "Should be on not approve");
    verify(mailService, times(1)).sendMailCommand(any());
    verifyNoMoreInteractions(mailService);
  }

  @Test
  public void setStatusNotApprovedByOwnerFailTest() {
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(
        it -> it.setStatus(VacancyOfTheDayStatus.NEW)
    );

    assertThrows(
        NotAuthorizedException.class,
        () -> vacancyOfTheDayActionsService.setStatusNotApproved(List.of(vacancyOfTheDay.getVacancyOfTheDayId()), "reason"),
        "Cannot set status to not approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusNotApproved(List.of(vacancyOfTheDay.getVacancyOfTheDayId()), "reason");
              return null;
            }
        ),
        "Cannot set status to not approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");

    assertThrows(
        SecurityException.class,
        () -> runAsBackoffice(
            () -> {
              vacancyOfTheDayActionsService.setStatusNotApproved(List.of(vacancyOfTheDay.getVacancyOfTheDayId()), "reason");
              return null;
            },
            vacancyOfTheDay.getManagerUserId()
        ),
        "Cannot set status to not approve because of rights"
    );
    refreshHibernateEntity(vacancyOfTheDay);
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDay.getStatus(), "Should be initial");
  }
}
