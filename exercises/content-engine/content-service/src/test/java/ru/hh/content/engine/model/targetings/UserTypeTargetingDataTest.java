package ru.hh.content.engine.model.targetings;

import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.EnumArrayConverter;
import ru.hh.content.engine.model.enums.TargetingUserType;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class UserTypeTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "'AGENCY',EMPLOYER,false",
          "'EMPLOYER',EMPLOYER,true",
          "'APPLICANT',EMPLOYER,false",
          "'ANONYMOUS',EMPLOYER,false",
          "'AGENCY',AGENCY,true",
          "'EMPLOYER',AGENCY,false",
          "'APPLICANT',AGENCY,false",
          "'ANONYMOUS',AGENCY,false",
          "'AGENCY',APPLICANT,false",
          "'EMPLOYER',APPLICANT,false",
          "'APPLICANT',APPLICANT,true",
          "'ANONYMOUS',APPLICANT,false",
          "'AGENCY',ANONYMOUS,false",
          "'EMPLOYER',ANONYMOUS,false",
          "'APPLICANT',ANONYMOUS,false",
          "'ANONYMOUS',ANONYMOUS,true",
          "'AGENCY,ANONYMOUS',ANONYMOUS,true",
          "'EMPLOYER,ANONYMOUS',ANONYMOUS,true",
          "'APPLICANT,EMPLOYER',ANONYMOUS,false",
          "'ANONYMOUS,APPLICANT',ANONYMOUS,true",
      },
      nullValues = "null")
  public void matchLanguageTargetingTest(@ConvertWith(EnumArrayConverter.class) TargetingUserType[] targetingUserTypes,
                                         TargetingUserType userType,
                                         boolean isMatch) {
    UserTypeTargetingData userTypeTargetingData = new UserTypeTargetingData(Set.of(targetingUserTypes));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setUserType(userType);

    assertEquals(isMatch, userTypeTargetingData.match(userDataForTargeting), "User type targeting wrong match");
  }
}
