package ru.hh.content.engine.services;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.dto.RegionDto;

class FrontRegionServiceTest extends AppTestBase {
  @Test
  public void getRegionsTest() {
    RegionsService regionsService = mock(RegionsService.class);
    int coreRegionId = 113;
    when(regionsService.getCountryRegionsMap()).thenReturn(Map.of(coreRegionId, List.of(12, 1)));
    String coreRegionName = "Россия";
    Map<Integer, String> translations = Map.of(coreRegionId, coreRegionName, 12, "Благовещенск", 1, "Москва");
    when(regionsService.getTranslatedRegions(anyList())).thenReturn(translations);
    FrontRegionService frontRegionService = new FrontRegionService(regionsService);
    List<RegionDto> regions = frontRegionService.getRegions();

    assertEquals(1, regions.size(), "Regions list is not equals to expected");
    assertNotNull(regions.get(0), "Core region must be not null");
    assertEquals(coreRegionName, regions.get(0).getText(), "Core region translation is not equals to expected");
    assertEquals(2, regions.get(0).getChildren().size(), "Core region children list size is not equals to expected");

    translations.forEach((id, value) -> {
      if (id != coreRegionId) {
        Optional<RegionDto> dto = regions.get(0).getChildren().stream().filter(it -> it.getId().equals(id.toString())).findFirst();
        assertTrue(dto.isPresent(), "Expected children id is not present in regions list");
        assertEquals(value, dto.get().getText(), "Translation value is not equals to expected");
      }
    });
  }
}
