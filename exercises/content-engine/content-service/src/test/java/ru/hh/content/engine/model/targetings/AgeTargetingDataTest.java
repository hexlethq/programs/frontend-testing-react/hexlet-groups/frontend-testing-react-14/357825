package ru.hh.content.engine.model.targetings;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class AgeTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "20,30,false,19,false",
          "20,30,false,21,true",
          "20,30,false,29,true",
          "20,30,false,31,false",
          "null,30,false,20,true",
          "null,30,false,31,false",
          "20,null,false,21,true",
          "20,null,false,19,false",
          "20,30,false,null,false",
          "20,30,true,null,true",
      },
      nullValues = "null")
  public void matchAgeTargetingTest(Integer minAge, Integer maxAge, boolean includeAnonymousAge, Integer userAge, boolean isMatch) throws Exception {
    AgeTargetingData ageTargetingData = new AgeTargetingData(minAge, maxAge, includeAnonymousAge);

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setAge(userAge);

    assertEquals(isMatch, ageTargetingData.match(userDataForTargeting), "Age targeting wrong match");
  }
}
