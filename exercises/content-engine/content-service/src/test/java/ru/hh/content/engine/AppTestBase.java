package ru.hh.content.engine;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.inject.Inject;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import static ru.hh.content.engine.security.SecurityContext.ADMIN_USER_IDS_SETTING_NAME;
import ru.hh.content.engine.services.HHSessionContext;
import static ru.hh.content.engine.targetings.TargetingDao.TARGETING_ID_SEQ;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.work.in.company.WorkInCompanyConstants.MAX_COMPANIES_ON_PAGE_SETTING_NAME;
import ru.hh.hh.session.client.HhLocale;
import ru.hh.hh.session.client.HhSession;
import ru.hh.hh.session.client.HhUser;
import ru.hh.hh.session.client.UserType;
import ru.hh.hhid.client.HhidName;
import ru.hh.microcore.DisplayType;
import ru.hh.nab.starter.NabApplication;
import ru.hh.nab.testbase.extensions.NabJunitWebConfig;
import ru.hh.nab.testbase.extensions.OverrideNabApplication;
import ru.hh.settings.SettingsTestClient;

@NabJunitWebConfig(AppTestConfig.class)
public abstract class AppTestBase {
  @Inject
  private PlatformTransactionManager transactionManager;
  @Inject
  private SessionFactory sessionFactory;
  @Inject
  private EntityGenerators entityGenerators;
  @Inject
  @Qualifier("mock")
  private List<Object> mocks;
  @Inject
  private SettingsTestClient settingsTestClient;
  @Inject
  private HHSessionContext hhSessionContext;

  @BeforeEach
  public void setUpCommon() {
    getSettingsTestClient().addSetting(MAX_COMPANIES_ON_PAGE_SETTING_NAME, "1");
    getSettingsTestClient().addSetting(ADMIN_USER_IDS_SETTING_NAME, String.valueOf(nextId()));

    createSequence(TARGETING_ID_SEQ);
  }

  @AfterEach
  public void afterTest() {
    mocks.forEach(Mockito::reset);
    settingsTestClient.clear();
  }

  public EntityGenerators getEntityGenerators() {
    return entityGenerators;
  }

  public static class BaseNabApplication implements OverrideNabApplication {
    @Override
    public NabApplication getNabApplication() {
      return App.buildApplication();
    }
  }

  protected Session getSession() {
    return sessionFactory.getCurrentSession();
  }

  protected SettingsTestClient getSettingsTestClient() {
    return settingsTestClient;
  }


  protected <T> T getInTransaction(Function<Session, T> callback) {
    DefaultTransactionDefinition transactionDefinition = new DefaultTransactionDefinition();
    transactionDefinition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
    TransactionStatus transaction = transactionManager.getTransaction(transactionDefinition);
    T result = callback.apply(getSession());
    transactionManager.commit(transaction);

    return result;
  }

  protected void runInTransaction(Consumer<Session> callback) {
    getInTransaction(session -> {
      callback.accept(session);
      return null;
    });
  }

  protected <T> T runAsBackoffice(Callable<T> callable) throws Exception {
    return runAsBackoffice(callable, nextId(), nextId());
  }

  protected <T> T runAsBackoffice(Callable<T> callable, int userId) throws Exception {
    return runAsBackoffice(callable, userId, nextId());
  }

  protected <T> T runAsBackoffice(Callable<T> callable, int userId, int regionId) throws Exception {
    return runAsBackoffice(callable, userId, regionId, Collections.emptySet(), Collections.emptySet());
  }
  
  protected <T> T runAsBackoffice(Callable<T> callable,
                                  int userId,
                                  int regionId,
                                  Set<String> activeAbExperimentNames,
                                  Set<String> controlAbExperimentNames) throws Exception {
    HhUser hhUser = new HhUser(
        userId,
        nextInt(),
        UserType.back_office_user,
        "",
        new HhidName("First", "Mid", "Last"),
        null,
        null,
        null,
        false,
        null);

    HhLocale hhLocale = new HhLocale(1,
        "RU",
        "hh.ru",
        "HEADHUNTER",
        regionId,
        1,
        1,
        "http://hh.ru",
        true,
        false,
        false,
        1,
        null,
        regionId,
        "hh.ru",
        String.format(".%s.", regionId)
    );

    HhSession hhSession = new HhSession(hhUser, null, Collections.emptySet(), Collections.emptyList(), null);
    ru.hh.hh.session.client.Session session = new ru.hh.hh.session.client.Session.Builder()
        .setDisplayType(DisplayType.DESKTOP)
        .setLocale(hhLocale)
        .setHhSession(hhSession)
        .setActiveAbExperimentNames(activeAbExperimentNames)
        .setControlAbExperimentNames(controlAbExperimentNames)
        .build();

    return hhSessionContext.runInContext(session, callable);
  }

  protected void createSequence(String sequenceName) {
    runInTransaction(session -> {
      if (sessionFactory.getCurrentSession().createNativeQuery("SELECT 0 FROM pg_class where relname = '" + sequenceName + '\'')
          .uniqueResultOptional().isEmpty()) {
        sessionFactory.getCurrentSession().createNativeQuery("CREATE SEQUENCE " + sequenceName + " START WITH 1").executeUpdate();
      }
    });
  }

  protected void refreshHibernateEntity(Object entity) {
    runInTransaction((session) -> session.refresh(entity));
  }
}
