package ru.hh.content.engine.limits;

import java.util.List;
import javax.inject.Inject;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;
import static ru.hh.content.engine.utils.CommonTestUtils.nextLongId;
import ru.hh.content.engine.model.enums.ContentLimitType;
import ru.hh.content.engine.model.limits.ContentLimit;

class ContentLimitDaoTest extends AppTestBase {
  @Inject
  private ContentLimitDao contentLimitDao;

  @Test
  public void testEmptyLimits() {
    List<ContentLimit> contentLimits = contentLimitDao.getContentLimits(nextLongId());

    assertTrue(contentLimits.isEmpty(), "Content limits is not empty");
  }

  @Test
  public void testSaveOneLimit() {
    long contentLimitId = nextLongId();

    List<ContentLimit> contentLimits = contentLimitDao.getContentLimits(contentLimitId);

    assertTrue(contentLimits.isEmpty(), "Content limits is not empty");

    contentLimitDao.saveContentLimits(List.of(new ContentLimit(contentLimitId, ContentLimitType.IMPRESSIONS)));

    contentLimits = contentLimitDao.getContentLimits(contentLimitId);
    assertEquals(1, contentLimits.size(), "Wrong content limits size");
  }

  @Test
  public void testSaveTwoLimit() {
    long contentLimitId = nextLongId();

    List<ContentLimit> contentLimits = contentLimitDao.getContentLimits(contentLimitId);

    assertTrue(contentLimits.isEmpty(), "Content limits is not empty");

    contentLimitDao.saveContentLimits(List.of(
        new ContentLimit(contentLimitId, ContentLimitType.IMPRESSIONS),
        new ContentLimit(contentLimitId, ContentLimitType.CLICKS)
    ));

    contentLimits = contentLimitDao.getContentLimits(contentLimitId);
    assertEquals(2, contentLimits.size(), "Wrong content limits size");
  }

  @Test
  public void testSaveTwoLimitForDifferentContentLimitId() {
    long contentLimitId1 = nextLongId();
    long contentLimitId2 = nextLongId();

    List<ContentLimit> contentLimits1 = contentLimitDao.getContentLimits(contentLimitId1);
    List<ContentLimit> contentLimits2 = contentLimitDao.getContentLimits(contentLimitId2);

    assertTrue(contentLimits1.isEmpty(), "Content limits is not empty");
    assertTrue(contentLimits2.isEmpty(), "Content limits is not empty");

    contentLimitDao.saveContentLimits(List.of(new ContentLimit(contentLimitId1, ContentLimitType.IMPRESSIONS)));
    contentLimitDao.saveContentLimits(List.of(new ContentLimit(contentLimitId2, ContentLimitType.IMPRESSIONS)));

    contentLimits1 = contentLimitDao.getContentLimits(contentLimitId1);
    assertEquals(1, contentLimits1.size(), "Wrong content limits size");

    contentLimits2 = contentLimitDao.getContentLimits(contentLimitId2);
    assertEquals(1, contentLimits2.size(), "Wrong content limits size");
  }
}
