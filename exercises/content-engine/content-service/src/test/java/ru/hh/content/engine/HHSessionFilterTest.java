package ru.hh.content.engine;

import java.util.Collections;
import java.util.concurrent.Callable;
import javax.inject.Inject;
import javax.servlet.FilterChain;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.resources.filter.HHSessionFilter;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.hh.session.client.HhSession;
import ru.hh.hh.session.client.HhSessionClientFactory;
import ru.hh.hh.session.client.HhUser;
import ru.hh.hh.session.client.Session;
import ru.hh.hh.session.client.UserType;
import ru.hh.hh.session.jclient.HhSessionJClient;

public class HHSessionFilterTest extends AppTestBase {
  @Inject
  private HhSessionClientFactory sessionClientFactory;

  @AfterEach
  public void afterTest() {
    Mockito.reset(sessionClientFactory);
  }

  @Test
  public void backOfficeUserSessionTest() throws Exception {
    HttpServletRequest request = mock(HttpServletRequest.class);
    HttpServletResponse response = mock(HttpServletResponse.class);
    HhSessionJClient sessionClient = mock(HhSessionJClient.class);
    HHSessionContext sessionContext = mock(HHSessionContext.class);
    FilterChain chain = mock(FilterChain.class);
    HhUser hhUser = new HhUser(1, nextInt(), UserType.back_office_user, "", null, null, null, null, false, null);
    HhSession hhSession = new HhSession(hhUser, null, Collections.emptySet(), Collections.emptyList(), null);
    Session session = new Session.Builder().setHhSession(hhSession).build();

    when(sessionClientFactory.getNormalClient(any())).thenReturn(sessionClient);
    when(sessionClient.session()).thenReturn(session);
    when(request.getPathInfo()).thenReturn("somePath");

    HHSessionFilter filter = new HHSessionFilter(sessionClientFactory, sessionContext);
    filter.doFilter(request, response, chain);
    verify(sessionContext).runInContext(eq(session), ArgumentMatchers.<Callable<Object>>any());
  }
}
