package ru.hh.content.engine.security;

import javax.inject.Inject;
import javax.ws.rs.NotAuthorizedException;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;

class SecurityContextTest extends AppTestBase {
  @Inject
  private SecurityContext securityContext;

  @Test
  public void testCheckCurrentUserIsBackOfficeWhenNoSession() {
    assertThrows(NotAuthorizedException.class, () -> securityContext.checkCurrentUserIsBackOffice());
  }

  @Test
  public void testCheckCurrentUserIsBackOfficeWhenBackoffice() throws Exception {
    runAsBackoffice(() -> {
          securityContext.checkCurrentUserIsBackOffice();
          return null;
        }
    );
  }

  @Test
  public void testCheckCurrentUserIsBackOfficeWhenEmployer() throws Exception {
    runAsBackoffice(() -> {
          securityContext.checkCurrentUserIsBackOffice();
          return null;
        }
    );
  }
}
