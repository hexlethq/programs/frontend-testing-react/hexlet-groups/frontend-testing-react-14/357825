package ru.hh.content.engine.model.targetings;

import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.utils.IntegerArrayConverter;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class SpecializationTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "'1','2',false",
          "'1,2','1',true",
          "'1,2','3',false",
          "'1,2','2,3',true",
      },
      nullValues = "null")
  public void matchRegionTargetingTest(@ConvertWith(IntegerArrayConverter.class) Integer[] targetingSpecsIds,
                                       @ConvertWith(IntegerArrayConverter.class) Integer[] userSpecsIds,
                                       boolean isMatch) {
    SpecializationTargetingData specializationTargetingData = new SpecializationTargetingData(Set.of(targetingSpecsIds));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setSpecializationIds(Set.of(userSpecsIds));

    assertEquals(isMatch, specializationTargetingData.match(userDataForTargeting), "Specialization targeting wrong match");
  }

}
