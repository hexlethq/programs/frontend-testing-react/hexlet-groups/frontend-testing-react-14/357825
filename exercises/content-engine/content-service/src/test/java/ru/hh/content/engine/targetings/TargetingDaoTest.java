package ru.hh.content.engine.targetings;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;
import static ru.hh.content.engine.utils.CommonTestUtils.nextLongId;
import ru.hh.content.engine.model.enums.Targeting;
import ru.hh.content.engine.model.enums.TargetingType;
import static ru.hh.content.engine.model.enums.TargetingUserType.EMPLOYER;
import ru.hh.content.engine.model.targetings.AgeTargetingData;
import ru.hh.content.engine.model.targetings.RegionTargetingData;
import ru.hh.content.engine.model.targetings.UserTypeTargetingData;

class TargetingDaoTest extends AppTestBase {
  @Inject
  private TargetingDao targetingDao;

  @Test
  @DisplayName(value = "Get targetings that does not exist. Must return an empty list")
  void getNotExistingTargetings() {
    List<Targeting> targetings = getInTransaction((session) -> targetingDao.getTargetings(nextLongId()));

    assertTrue(targetings.isEmpty(), "Targetings is not empty");
  }

  @Test
  @DisplayName(value = "Get one targeting. Must return a list with one targeting")
  void getOneTargeting() {
    Targeting targeting = getEntityGenerators().createTargeting((it) -> {
      it.setTargetingType(TargetingType.AGE);
      it.setTargetingData(new AgeTargetingData(15, 75, false));
    });

    List<Targeting> targetings = getInTransaction((session) -> targetingDao.getTargetings(targeting.getTargetingId()));

    assertEquals(targetings.size(), 1, "Targetings has a wrong size");
    assertEquals(targetings.get(0).getTargetingData(), new AgeTargetingData(15, 75, false), "Targetings has a wrong size");
  }

  @Test
  @DisplayName(value = "Get two targetings. Must return a list with two targetings")
  void getTwoTargeting() {
    Targeting targeting = getEntityGenerators().createTargeting((it) -> {
      it.setTargetingType(TargetingType.AGE);
      it.setTargetingData(new AgeTargetingData());
    });
    Targeting targeting2 = getEntityGenerators().createTargeting((it) -> {
      it.setTargetingType(TargetingType.REGION);
      it.setTargetingData(new RegionTargetingData());
      it.setTargetingId(targeting.getTargetingId());
    });

    List<Targeting> targetings = getInTransaction((session) -> targetingDao.getTargetings(targeting.getTargetingId()));

    assertEquals(targetings.size(), 2, "Targetings has a wrong size");
  }

  @Test
  @DisplayName(value = "Get one targetings, when exist another targeting. Must return a list with right targeting")
  void getOneTargetingWhenExistAnotherTargeting() {
    Targeting targeting = getEntityGenerators().createTargeting((it) -> {
      it.setTargetingType(TargetingType.AGE);
      it.setTargetingData(new AgeTargetingData());
    });
    Targeting targeting2 = getEntityGenerators().createTargeting((it) -> {
      it.setTargetingType(TargetingType.REGION);
      it.setTargetingData(new RegionTargetingData());
    });

    List<Targeting> targetings = getInTransaction((session) -> targetingDao.getTargetings(targeting.getTargetingId()));

    assertEquals(targetings.size(), 1, "Targetings has a wrong size");
    assertEquals(targeting, targetings.get(0), "Get wrong targeting");
  }

  @Test
  @DisplayName(value = "Save targetings. Get targetings. Must return all saved targetings")
  void saveAndGetTargetings() {
    List<Targeting> targetings = getInTransaction(session -> targetingDao.getTargetings(nextLongId()));

    assertTrue(targetings.isEmpty(), "Targetings is not empty");

    List<Targeting> targetingsToSave = new ArrayList<>();
    Long targetingId = getInTransaction(session -> targetingDao.getNewTargetingId());
    targetingsToSave.add(new Targeting(targetingId, TargetingType.AGE, new AgeTargetingData(1, 3, false)));
    targetingsToSave.add(new Targeting(targetingId, TargetingType.USER_TYPE, new UserTypeTargetingData(Set.of(EMPLOYER))));

    runInTransaction(session -> targetingDao.saveTargetings(targetingsToSave));

    targetings = getInTransaction(session -> targetingDao.getTargetings(targetingId));

    assertEquals(targetingsToSave, targetings, "Wrong targetings returned");
  }
}
