package ru.hh.content.engine.utils;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import javax.ws.rs.core.Response;
import ru.hh.jclient.common.EmptyWithStatus;
import ru.hh.jclient.common.ResultOrErrorWithStatus;
import ru.hh.jclient.common.ResultWithStatus;

public class JClientMockUtils {
  public static <T> CompletableFuture<ResultWithStatus<T>> success(T value) {
    return CompletableFuture.completedFuture(new ResultWithStatus<>(value, Response.Status.OK.getStatusCode()));
  }

  public static <T> CompletableFuture<EmptyWithStatus> successEmpty() {
    return CompletableFuture.completedFuture(new EmptyWithStatus(Response.Status.OK.getStatusCode()));
  }

  public static <T, U> CompletableFuture<ResultOrErrorWithStatus<T, U>> successOrError(T value) {
    return CompletableFuture.completedFuture(new ResultOrErrorWithStatus<>(Optional.of(value), Optional.empty(), Response.Status.OK.getStatusCode()));
  }

  public static <T, U> CompletableFuture<ResultOrErrorWithStatus<T, U>> successOrError(U error, Response.Status status) {
    return CompletableFuture.completedFuture(new ResultOrErrorWithStatus<>(Optional.empty(), Optional.of(error), status.getStatusCode()));
  }

  public static <T> CompletableFuture<ResultWithStatus<T>> created(T value) {
    return CompletableFuture.completedFuture(new ResultWithStatus<>(value, Response.Status.CREATED.getStatusCode()));
  }

  public static <T> CompletableFuture<ResultWithStatus<T>> serverError(T value) {
    return CompletableFuture.completedFuture(new ResultWithStatus<>(value, Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()));
  }

  public static <T> CompletableFuture<ResultWithStatus<T>> status(Response.Status status) {
    return CompletableFuture.completedFuture(new ResultWithStatus<>(null, status.getStatusCode()));
  }
}
