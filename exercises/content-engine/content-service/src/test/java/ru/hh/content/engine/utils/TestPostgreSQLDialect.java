package ru.hh.content.engine.utils;

import java.sql.Types;
import org.hibernate.dialect.PostgreSQL95Dialect;

/**
 * Нужно для кастомных типов массивов, иначе хибер отказывается создавать таблицы с ними в тестах
 */
public class TestPostgreSQLDialect extends PostgreSQL95Dialect {
  public TestPostgreSQLDialect() {
    super();
    this.registerColumnType(Types.JAVA_OBJECT, "jsonb");
  }
}
