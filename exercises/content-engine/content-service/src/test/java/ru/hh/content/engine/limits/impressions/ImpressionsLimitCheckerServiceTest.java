package ru.hh.content.engine.limits.impressions;

import javax.inject.Inject;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.limits.ImpressionsLimit;

class ImpressionsLimitCheckerServiceTest extends AppTestBase {
  @Inject
  private ImpressionsLimitCheckerService impressionsLimitCheckerService;

  @ParameterizedTest
  @CsvSource(
      value = {
          "0,0,10,10,false",
          "5,0,10,10,false",
          "0,5,10,10,false",
          "6,6,10,10,false",
          "0,15,10,10,true",
          "15,0,10,10,true",
          "15,15,10,10,true",
          "0,0,10,null,false",
          "0,20,10,null,false",
          "5,0,null,10,false",
          "0,5,null,10,false",
          "6,6,10,null,false",
          "0,15,null,10,true",
          "15,0,null,10,false",
          "15,15,10,null,true",
      },
      nullValues = "null")
  public void matchIsImpressionsReached(Long currentDailyImpressions,
                                        Long currentTotalImpressions,
                                        Long limitDailyImpressions,
                                        Long limitTotalImpressions,
                                        boolean isReached) {
    ImpressionsLimit impressionsLimit = getEntityGenerators().createImpressionsLimit(it -> {
      it.setCurrentDailyImpressions(currentDailyImpressions);
      it.setCurrentTotalImpressions(currentTotalImpressions);
      it.setLimitDailyImpressions(limitDailyImpressions);
      it.setLimitTotalImpressions(limitTotalImpressions);
    });
    assertEquals(isReached, impressionsLimitCheckerService.isReached(impressionsLimit.getContentLimitId()), "Wrong reached");
  }
}
