package ru.hh.content.engine.model.targetings;

import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.targetings.SalaryTargetingData.SalaryRange;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class SalaryTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "100,200,150,null,false,true",
          "100,200,250,null,false,false",
          "100,200,50,null,false,false",
          "100,200,null,null,false,false",
          "100,200,150,250,false,true",
          "100,200,250,150,false,false",
          "100,200,50,150,false,false",
          "100,200,null,150,false,false",
          "100,200,150,250,true,true",
          "100,200,250,150,true,false",
          "100,200,50,150,true,false",
          "100,200,null,150,true,true",
          "100,200,null,250,true,false",
          "100,200,null,50,true,false",
          "100,null,null,50,true,false",
          "100,null,null,300,true,true",
          "null,200,null,50,true,true",
          "null,200,null,250,true,false",
      },
      nullValues = "null")
  public void matchSalaryTargetingTest(Long min,
                                       Long max,
                                       Long userSalary,
                                       Long userPredictedSalary,
                                       boolean usePredictedSalary,
                                       boolean isMatch) {
    SalaryTargetingData salaryTargetingData = new SalaryTargetingData(min, max, usePredictedSalary);

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setSalary(userSalary);
    userDataForTargeting.setPredictedSalary(userPredictedSalary);

    assertEquals(isMatch, salaryTargetingData.match(userDataForTargeting), "Salary targeting wrong match");
  }

  @ParameterizedTest
  @CsvSource(
      value = {
          "100,200,300,400,150,null,false,true",
          "100,200,300,400,250,null,false,false",
          "100,200,300,400,350,null,false,true",
          "100,200,300,400,450,null,false,false",
          "100,200,300,400,50,250,false,false",
      },
      nullValues = "null")
  public void matchManySalaryPeriodsTargetingTest(Long firstMin,
                                                  Long firstMax,
                                                  Long secondMin,
                                                  Long secondMax,
                                                  Long userSalary,
                                                  Long userPredictedSalary,
                                                  boolean usePredictedSalary,
                                                  boolean isMatch) {
    SalaryTargetingData salaryTargetingData = new SalaryTargetingData(
        Set.of(new SalaryRange(firstMin, firstMax), new SalaryRange(secondMin, secondMax)),
        usePredictedSalary);

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setSalary(userSalary);
    userDataForTargeting.setPredictedSalary(userPredictedSalary);

    assertEquals(isMatch, salaryTargetingData.match(userDataForTargeting), "Salary targeting wrong match");
  }

}
