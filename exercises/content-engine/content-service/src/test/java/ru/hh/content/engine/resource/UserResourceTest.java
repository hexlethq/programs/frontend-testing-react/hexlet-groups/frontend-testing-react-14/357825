package ru.hh.content.engine.resource;

import java.util.Collections;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.dto.UserDataDto;
import ru.hh.content.engine.resources.UserResource;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.hh.session.client.HhSession;
import ru.hh.hh.session.client.HhUser;
import ru.hh.hh.session.client.UserType;
import ru.hh.hhid.client.HhidName;
import ru.hh.microcore.DisplayType;

class UserResourceTest extends AppTestBase {

  @Test
  public void getUserData() {
    int userId = nextInt();
    HhidName hhidName = new HhidName("Firstname", null, "Lastname");
    HHSessionContext sessionContext = mock(HHSessionContext.class);
    HhUser hhUser = new HhUser(userId, nextInt(), UserType.back_office_user, "", hhidName, null, null, null, false, null);
    HhSession hhSession = new HhSession(hhUser, null, Collections.emptySet(), Collections.emptyList(), null);
    ru.hh.hh.session.client.Session session = new ru.hh.hh.session.client.Session.Builder()
        .setDisplayType(DisplayType.DESKTOP)
        .setHhSession(hhSession)
        .build();
    when(sessionContext.getSession()).thenReturn(session);
    when(sessionContext.getAccount()).thenReturn(hhUser);

    SecurityContext securityContext = new SecurityContext(sessionContext, getSettingsTestClient());
    UserResource userResource = new UserResource(sessionContext, securityContext);
    UserDataDto userData = (UserDataDto) userResource.getUserData().getEntity();
    assertEquals(String.format("%s %s", hhidName.getFirst(), hhidName.getLast()), userData.getName(),
        "user name is not equals to expected"
    );
    assertEquals(userId, userData.getId(), "user id is not equals to expected");
  }
}
