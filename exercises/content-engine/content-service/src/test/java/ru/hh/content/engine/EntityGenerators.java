package ru.hh.content.engine;

import java.time.LocalDateTime;
import java.util.Random;
import java.util.function.Consumer;
import javax.inject.Inject;
import static org.apache.commons.text.CharacterPredicates.DIGITS;
import static org.apache.commons.text.CharacterPredicates.LETTERS;
import org.apache.commons.text.RandomStringGenerator;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.Targeting;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.limits.ClicksLimit;
import ru.hh.content.engine.model.limits.ContentLimit;
import ru.hh.content.engine.model.limits.ImpressionsLimit;
import ru.hh.content.engine.model.packets.PacketToRegion;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDayReservation;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;
import ru.hh.content.engine.model.work.in.company.WorkInCompanyReservation;
import ru.hh.content.engine.targetings.TargetingDao;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.utils.CommonTestUtils.nextLongId;
import static ru.hh.content.engine.utils.CommonTestUtils.nextMonday;

public class EntityGenerators {
  private final SessionFactory sessionFactory;
  private final TargetingDao targetingDao;

  private final Random random;
  private final RandomStringGenerator stringGenerator;

  @Inject
  public EntityGenerators(SessionFactory sessionFactory, TargetingDao targetingDao) {
    this.sessionFactory = sessionFactory;
    this.targetingDao = targetingDao;
    random = new Random();
    stringGenerator = new RandomStringGenerator.Builder()
        .withinRange('0', 'z')
        .filteredBy(LETTERS, DIGITS)
        .build();
  }

  @Transactional
  public WorkInCompany createWorkInCompany(Consumer<WorkInCompany> updateBeforeSave) {
    WorkInCompany workInCompany = new WorkInCompany();

    workInCompany.setDisplayName(stringGenerator.generate(10));
    workInCompany.setEmployerId(nextId());
    workInCompany.setManagerUserId(nextId());
    workInCompany.setManagerName(stringGenerator.generate(10));

    return updateAndSave(updateBeforeSave, workInCompany);
  }

  @Transactional
  public WorkInCompanyReservation createWorkInCompanyReservation(Consumer<WorkInCompanyReservation> updateBeforeSave) {
    WorkInCompanyReservation workInCompanyReservation = new WorkInCompanyReservation();

    workInCompanyReservation.setWorkInCompanyId(createWorkInCompany(it -> {

    }).getWorkInCompanyId());
    LocalDateTime monday = nextMonday();
    workInCompanyReservation.setDateStart(monday.toLocalDate());
    workInCompanyReservation.setDateEnd(monday.plusDays(6).toLocalDate());
    workInCompanyReservation.setRegionId(nextId());
    workInCompanyReservation.setDeviceType(TargetingDeviceType.DESKTOP);
    workInCompanyReservation.setReservationNumber(0);

    return updateAndSave(updateBeforeSave, workInCompanyReservation);
  }

  @Transactional
  public VacancyOfTheDay createVacancyOfTheDay(Consumer<VacancyOfTheDay> updateBeforeSave) {
    VacancyOfTheDay vacancyOfTheDay = new VacancyOfTheDay();

    vacancyOfTheDay.setVacancyId(nextId());
    vacancyOfTheDay.setPayedStatus(VacancyOfTheDayPayedStatus.PAYED);
    vacancyOfTheDay.setStatus(VacancyOfTheDayStatus.APPROVED);
    vacancyOfTheDay.setComment(stringGenerator.generate(10));
    vacancyOfTheDay.setManagerUserId(nextId());
    vacancyOfTheDay.setManagerName(stringGenerator.generate(10));
    vacancyOfTheDay.setEmployerId(nextId());
    vacancyOfTheDay.setEmployerName(stringGenerator.generate(10));
    vacancyOfTheDay.setManagerEmail(stringGenerator.generate(10));
    vacancyOfTheDay.setCampaignType(VacancyOfTheDayCampaignType.COMMERCIAL);
    vacancyOfTheDay.setPacketType(PacketType.CUSTOM);

    return updateAndSave(updateBeforeSave, vacancyOfTheDay);
  }

  @Transactional
  public VacancyOfTheDayReservation createVacancyOfTheDayReservation(Consumer<VacancyOfTheDayReservation> updateBeforeSave) {
    VacancyOfTheDayReservation vacancyOfTheDayReservation = new VacancyOfTheDayReservation();

    vacancyOfTheDayReservation.setVacancyOfTheDayId(createVacancyOfTheDay(it -> {

    }).getVacancyOfTheDayId());
    LocalDateTime monday = nextMonday();
    vacancyOfTheDayReservation.setDateStart(monday.toLocalDate());
    vacancyOfTheDayReservation.setDateEnd(monday.plusDays(6).toLocalDate());
    vacancyOfTheDayReservation.setRegionId(nextId());
    vacancyOfTheDayReservation.setDeviceType(TargetingDeviceType.DESKTOP);
    vacancyOfTheDayReservation.setReservationNumber(0);

    return updateAndSave(updateBeforeSave, vacancyOfTheDayReservation);
  }

  @Transactional
  public Targeting createTargeting(Consumer<Targeting> updateBeforeSave) {
    Targeting targeting = new Targeting();

    targeting.setTargetingId(targetingDao.getNewTargetingId());

    return updateAndSave(updateBeforeSave, targeting);
  }

  @Transactional
  public PacketToRegion createPacketToRegion(Integer regionId, PacketType packetType) {
    PacketToRegion packetToRegion = new PacketToRegion();

    packetToRegion.setPacketType(packetType);
    packetToRegion.setRegionId(regionId);

    sessionFactory.getCurrentSession().save(packetToRegion);

    return packetToRegion;
  }

  @Transactional
  public ContentLimit createContentLimit(Consumer<ContentLimit> updateBeforeSave) {
    ContentLimit contentLimit = new ContentLimit();

    contentLimit.setContentLimitId(nextLongId());

    return updateAndSave(updateBeforeSave, contentLimit);
  }

  @Transactional
  public ImpressionsLimit createImpressionsLimit(Consumer<ImpressionsLimit> updateBeforeSave) {
    ImpressionsLimit impressionsLimit = new ImpressionsLimit();

    impressionsLimit.setContentLimitId(nextLongId());

    return updateAndSave(updateBeforeSave, impressionsLimit);
  }

  @Transactional
  public ClicksLimit createClicksLimit(Consumer<ClicksLimit> updateBeforeSave) {
    ClicksLimit clicksLimit = new ClicksLimit();

    clicksLimit.setContentLimitId(nextLongId());

    return updateAndSave(updateBeforeSave, clicksLimit);
  }

  public <T> T updateAndSave(Consumer<T> updateBeforeSave, T object) {
    updateBeforeSave.accept(object);
    sessionFactory.getCurrentSession().save(object);
    return object;
  }
  
  public RandomStringGenerator getStringGenerator() {
    return stringGenerator;
  }
}
