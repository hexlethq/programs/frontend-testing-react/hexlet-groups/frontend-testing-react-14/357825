package ru.hh.content.engine.model.targetings;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.converter.ConvertWith;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.EnumArrayConverter;
import ru.hh.content.engine.utils.IntegerArrayConverter;

class TimeTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "'MONDAY','2',MONDAY,2,true",
          "'MONDAY,TUESDAY','2',MONDAY,2,true",
          "'MONDAY,TUESDAY','2',SUNDAY,2,false",
          "'MONDAY','2',MONDAY,3,false",
          "'MONDAY','2,4',MONDAY,3,false",
      },
      nullValues = "null")
  public void matchTimeTargetingTest(@ConvertWith(EnumArrayConverter.class) DayOfWeek[] dayOfWeeks,
                                     @ConvertWith(IntegerArrayConverter.class) Integer[] hours,
                                     DayOfWeek currentDay,
                                     Integer currentHour,
                                     boolean isMatch) {
    TimeTargetingData timeTargetingData = new TimeTargetingData(Set.of(dayOfWeeks), Set.of(hours));

    // Календарь говорит, что 22 июня 2020 года было понедельником
    // Надеюсь, это не изменится в ближайшие пару десятков лет
    LocalDateTime mondayWithCurrentHour = LocalDateTime.of(2020, Month.JUNE, 22, currentHour, 0);
    assertEquals(isMatch,
        timeTargetingData.innerMatch(mondayWithCurrentHour.plusDays(currentDay.ordinal())),
        "Time targeting wrong match"
    );
  }
}
