package ru.hh.content.engine.vacancy.of.the.day.service;

import java.util.Base64;
import java.util.List;
import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.enums.ImageType;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.utils.ContentImage;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyScreenshotDto;
import ru.hh.nab.common.properties.FileSettings;

class VacancyOfTheDayScreenshotServiceTest extends AppTestBase {
  @Inject
  private FileSettings fileSettings;
  @Inject
  private VacancyOfTheDayScreenshotService vacancyOfTheDayScreenshotService;
  
  @Test
  public void createVacancyOfTheDayScreenshotSuccess() throws Exception {
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(it -> {
    });
    byte[] bytes = Base64.getDecoder().decode(fileSettings.getString("base64.example"));
    
    runAsBackoffice(
        () -> vacancyOfTheDayScreenshotService.createVacancyScreenshot(
            vacancyOfTheDay.getVacancyOfTheDayId(), ImageType.XS, ContentImage.fromBytes(bytes)
        )
    );
    
    List<VacancyScreenshotDto> vacancyScreenshotDtoList = runAsBackoffice(
        () -> vacancyOfTheDayScreenshotService.getVacancyScreenshots(vacancyOfTheDay.getVacancyOfTheDayId()));
    
    assertEquals(1, vacancyScreenshotDtoList.size(), "Screenshot should be one");
  
    runAsBackoffice(
        () -> vacancyOfTheDayScreenshotService.deleteScreenshot(vacancyScreenshotDtoList.get(0).getScreenId())
    );
    
    List<VacancyScreenshotDto> vacancyScreenshotDtoListAfterDelete = runAsBackoffice(
        () -> vacancyOfTheDayScreenshotService.getVacancyScreenshots(vacancyOfTheDay.getVacancyOfTheDayId()));
    
    assertEquals(0, vacancyScreenshotDtoListAfterDelete.size(), "Screenshot should be deleted");
  }
  
  @Test
  public void createVacancyOfTheDayScreenshotFail() throws Exception {
    VacancyOfTheDay vacancyOfTheDay = getEntityGenerators().createVacancyOfTheDay(it -> {
    });
    byte[] bytes = Base64.getDecoder().decode(fileSettings.getString("base64.big_example"));
    
    assertThrows(
        WebApplicationException.class,
        () -> runAsBackoffice(
            () -> vacancyOfTheDayScreenshotService.createVacancyScreenshot(
                vacancyOfTheDay.getVacancyOfTheDayId(), ImageType.XS, ContentImage.fromBytes(bytes)
            )
        ),
        "Should raise error if image is too big"
    );
    
    List<VacancyScreenshotDto> vacancyScreenshotDtoList = runAsBackoffice(
        () -> vacancyOfTheDayScreenshotService.getVacancyScreenshots(vacancyOfTheDay.getVacancyOfTheDayId()));
    
    assertEquals(0, vacancyScreenshotDtoList.size(), "Screenshot should be zero");
  }
}
