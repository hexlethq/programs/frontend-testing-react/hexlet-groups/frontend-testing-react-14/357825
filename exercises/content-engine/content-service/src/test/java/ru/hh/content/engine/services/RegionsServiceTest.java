package ru.hh.content.engine.services;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.clients.xmlback.HHAreasClient;
import ru.hh.content.engine.clients.xmlback.dto.AreasDto;
import static ru.hh.content.engine.utils.JClientMockUtils.success;
import ru.hh.content.engine.utils.TranslationsMock;
import ru.hh.hh.session.client.HhLocale;

class RegionsServiceTest extends AppTestBase {

  @Inject
  private HHAreasClient hhAreasClient;

  @Inject
  private RegionsService regionsService;

  @Inject
  private TranslationsMock translationsMock;

  @Test
  public void getCountryRegionsMapTest() {
    Map<Integer, List<Integer>> input = Map.of(113, List.of(12, 1), 5, List.of(10, 56));
    when(hhAreasClient.getAreasList()).thenReturn(success(createAreasDtoFromMap(input)));
    Map<Integer, List<Integer>> regionsMap = regionsService.getCountryRegionsMap();
    assertEquals(input, regionsMap, "Regions map is not equals to expected");
    Mockito.reset(hhAreasClient);
  }

  @Test
  public void getTranslatedRegionsMapTest() {
    String region12 = "Благовещенск";
    String region1 = "Москва";
    Map<Integer, List<Integer>> input = Map.of(113, List.of(12, 1));
    HHAreasClient hhAreasClient = mock(HHAreasClient.class);
    when(hhAreasClient.getAreasList()).thenReturn(success(createAreasDtoFromMap(input)));
    translationsMock.setTranslation("area.12", 1, "RU", region12);
    translationsMock.setTranslation("area.1", 1, "RU", region1);
    HHSessionContext sessionContext = mock(HHSessionContext.class);
    when(sessionContext.getLocalization()).thenReturn(new HhLocale(1, "RU", null, null, 0, 0, null,
        null, false, false, false, 0, null, null, null, null)
    );
    RegionsService regionsService = new RegionsService(this.hhAreasClient, translationsMock, sessionContext);
    Map<Integer, String> translatedRegions = regionsService.getTranslatedRegions(input.get(113));
    assertEquals(region1, translatedRegions.get(1), "Translated region is not equals to expected");
    assertEquals(region12, translatedRegions.get(12), "Translated region is not equals to expected");
  }

  @NotNull
  private AreasDto createAreasDtoFromMap(Map<Integer, List<Integer>> input) {
    return new AreasDto(0, input.entrySet().stream()
        .map(entry -> new AreasDto(entry.getKey(), entry.getValue().stream()
            .map(AreasDto::new).collect(Collectors.toList())))
        .collect(Collectors.toList())
    );
  }
}
