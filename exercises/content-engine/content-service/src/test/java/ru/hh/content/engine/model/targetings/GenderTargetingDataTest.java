package ru.hh.content.engine.model.targetings;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.model.enums.TargetingGender;
import ru.hh.content.engine.targetings.UserDataForTargeting;

class GenderTargetingDataTest extends AppTestBase {
  @ParameterizedTest
  @CsvSource(
      value = {
          "MALE,MALE,true",
          "FEMALE,MALE,false",
          "MALE,FEMALE,false",
          "FEMALE,FEMALE,true",
          "MALE,null,false",
          "FEMALE,null,false",
      },
      nullValues = "null")
  public void matchGenderTargetingTest(TargetingGender targetingGender, TargetingGender userGender, boolean isMatch) {
    GenderTargetingData genderTargetingData = new GenderTargetingData(targetingGender);

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setGender(userGender);

    assertEquals(isMatch, genderTargetingData.match(userDataForTargeting), "Gender targeting wrong match");
  }
}
