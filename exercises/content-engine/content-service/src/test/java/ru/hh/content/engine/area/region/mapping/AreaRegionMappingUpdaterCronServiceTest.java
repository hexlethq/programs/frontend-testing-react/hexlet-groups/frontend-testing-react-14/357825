package ru.hh.content.engine.area.region.mapping;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.clients.xmlback.HHAreasClient;
import ru.hh.content.engine.clients.xmlback.dto.AreasDto;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.utils.JClientMockUtils.success;

class AreaRegionMappingUpdaterCronServiceTest extends AppTestBase {
  @Inject
  private AreaRegionMappingUpdaterCronService areaRegionMappingUpdaterCronService;

  @Inject
  private AreaRegionMappingDao areaRegionMappingDao;

  @Inject
  private HHAreasClient hhAreasClient;

  @Test
  public void testSingleCoreRegion() {
    int coreRegion = nextId();
    getSettingsTestClient().addSetting(AreaRegionMappingUpdaterCronService.CORE_REGIONS_SETTING_NAME, String.format("%s", coreRegion));

    int subRegionOne = nextId();
    int subRegionTwo = nextId();

    when(hhAreasClient.getAreasList()).thenReturn(success(createAreasDtoFromMap(Map.of(coreRegion, List.of(subRegionOne, subRegionTwo)))));

    areaRegionMappingUpdaterCronService.updateAreaRegionMapping(nextId(), nextId());

    assertEquals(coreRegion, areaRegionMappingDao.getRegionByArea(subRegionOne).get(), "Wrong region for sub region");
    assertEquals(coreRegion, areaRegionMappingDao.getRegionByArea(subRegionTwo).get(), "Wrong region for sub region");
  }

  @Test
  public void testTwoCoreRegions() {
    int coreRegionOne = nextId();
    int coreRegionTwo = nextId();
    getSettingsTestClient().addSetting(
        AreaRegionMappingUpdaterCronService.CORE_REGIONS_SETTING_NAME, String.format("%s,%s", coreRegionOne, coreRegionTwo)
    );

    int subRegionOne = nextId();
    int subRegionTwo = nextId();

    AreasDto areasDtoFromMap = createAreasDtoFromMap(
        Map.of(
            coreRegionOne, List.of(subRegionOne),
            coreRegionTwo, List.of(subRegionTwo))
    );
    when(hhAreasClient.getAreasList()).thenReturn(success(areasDtoFromMap
    ));

    areaRegionMappingUpdaterCronService.updateAreaRegionMapping(nextId(), nextId());

    assertEquals(coreRegionOne, areaRegionMappingDao.getRegionByArea(subRegionOne).get(), "Wrong region for sub region");
    assertEquals(coreRegionTwo, areaRegionMappingDao.getRegionByArea(subRegionTwo).get(), "Wrong region for sub region");
  }

  @NotNull
  private AreasDto createAreasDtoFromMap(Map<Integer, List<Integer>> input) {
    return new AreasDto(0, input.entrySet().stream()
        .map(entry -> new AreasDto(entry.getKey(), entry.getValue().stream()
            .map(AreasDto::new).collect(Collectors.toList())))
        .collect(Collectors.toList())
    );
  }
}
