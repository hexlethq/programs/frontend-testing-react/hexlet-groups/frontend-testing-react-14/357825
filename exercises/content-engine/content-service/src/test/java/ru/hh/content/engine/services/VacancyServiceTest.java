package ru.hh.content.engine.services;

import java.util.List;
import javax.inject.Inject;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.clients.xmlback.HHVacancyClient;
import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyEmployerDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyListDto;
import static ru.hh.content.engine.utils.JClientMockUtils.serverError;
import static ru.hh.content.engine.utils.JClientMockUtils.success;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyDataDto;

class VacancyServiceTest extends AppTestBase {

  @Inject
  private HHVacancyClient hhVacancyClient;
  @Inject
  private VacancyService vacancyService;

  @Test
  public void getVacancySuccessTest() {
    VacancyDto vacancyDto = new VacancyDto(nextInt(), "Название вакансии");
    when(hhVacancyClient.getVacancy(vacancyDto.getVacancyId())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(List.of(vacancyDto.getVacancyId()))).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    VacancyDataDto vacancy = vacancyService.getFrontVacancy(vacancyDto.getVacancyId());
    assertEquals(vacancyDto.getVacancyId(), vacancy.getId(), "Vacancy id should be the same");
    assertEquals(vacancyDto.getName(), vacancy.getName(), "Vacancy name should be the same");
  }

  @Test
  public void getVacancyEmptyTest() {
    when(hhVacancyClient.getVacancy(anyInt())).thenReturn(success(null));
    assertThrows(
        RuntimeException.class,
        () -> vacancyService.getFrontVacancy(nextInt()),
        "Should raise error if xmlback return empty"
    );
  }

  @Test
  public void getVacancyServerErrorTest() {
    when(hhVacancyClient.getVacancy(anyInt())).thenReturn(serverError(null));
    assertThrows(
        RuntimeException.class,
        () -> vacancyService.getFrontVacancy(nextInt()),
        "Should raise error if xmlback return 5xx"
    );
  }
}
