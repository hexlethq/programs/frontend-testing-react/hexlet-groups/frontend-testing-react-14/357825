package ru.hh.content.engine.work.in.company.service;

import java.net.URI;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.ws.rs.core.UriInfo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.clients.xmlback.WorkInCompanyClient;
import ru.hh.content.engine.clients.xmlback.WorkInCompanyDisplayInfo;
import ru.hh.content.engine.clients.xmlback.WorkInCompanyDisplayInfos;
import ru.hh.content.engine.dto.ImpressionRequestDto;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.targetings.UserDataForTargeting;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.utils.CommonTestUtils.nextMonday;
import static ru.hh.content.engine.utils.JClientMockUtils.success;
import static ru.hh.content.engine.work.in.company.WorkInCompanyConstants.MAX_COMPANIES_ON_PAGE_SETTING_NAME;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyFormDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyViewDto;

class WorkInCompanyViewServiceTest extends AppTestBase {
  @Inject
  private WorkInCompanyViewService workInCompanyViewService;
  @Inject
  private WorkInCompanyService workInCompanyService;
  @Inject
  private WorkInCompanyClient workInCompanyClient;

  @Test
  public void getOneCompany() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";

    HashMap<Integer, WorkInCompanyDisplayInfo> workInCompanyDisplayInfoMap = new HashMap<>();
    workInCompanyDisplayInfoMap.put(employerId, new WorkInCompanyDisplayInfo());
    when(workInCompanyClient.getWorkInCompanyDisplayInfos(any()))
        .thenReturn(success(new WorkInCompanyDisplayInfos(workInCompanyDisplayInfoMap)));
    when(workInCompanyClient.getWorkInCompanyStubs(anyString(), anyInt())).thenReturn(success(new Integer[0]));

    runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            null,
            Set.of(TargetingDeviceType.DESKTOP))
    ));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();
    
    List<WorkInCompanyViewDto> workInCompanyToView =
        runAsBackoffice(
            () -> workInCompanyViewService.getWorkInCompanyToView(dateStart, impressionRequestDto),
            nextId(),
            regionId
        );

    assertEquals(1, workInCompanyToView.size(), "Wrong count of employers");
    assertEquals(employerId, workInCompanyToView.get(0).getEmployerId(), "Wrong employer id");
    assertEquals(displayName, workInCompanyToView.get(0).getDisplayName(), "Wrong display name");
  }

  @Test
  public void getOneCompanyAndOneStub() throws Exception {
    getSettingsTestClient().addSetting(MAX_COMPANIES_ON_PAGE_SETTING_NAME, "2");

    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";

    HashMap<Integer, WorkInCompanyDisplayInfo> workInCompanyDisplayInfoMap = new HashMap<>();
    int employerIdStub = nextId();
    workInCompanyDisplayInfoMap.put(employerId, new WorkInCompanyDisplayInfo());
    String stubName = "stub";
    workInCompanyDisplayInfoMap.put(employerIdStub, new WorkInCompanyDisplayInfo(stubName));
    when(workInCompanyClient.getWorkInCompanyDisplayInfos(any()))
        .thenReturn(success(new WorkInCompanyDisplayInfos(workInCompanyDisplayInfoMap)));
    Integer[] employerIds = new Integer[]{employerIdStub};
    when(workInCompanyClient.getWorkInCompanyStubs(anyString(), anyInt())).thenReturn(success(employerIds));

    runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            null,
            Set.of(TargetingDeviceType.DESKTOP))
    ));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));

    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<WorkInCompanyViewDto> workInCompanyToView =
        runAsBackoffice(
            () -> workInCompanyViewService.getWorkInCompanyToView(dateStart, impressionRequestDto),
            nextId(),
            regionId
        );

    assertEquals(2, workInCompanyToView.size(), "Wrong count of employers");
    for (WorkInCompanyViewDto workInCompanyViewDto : workInCompanyToView) {
      if (workInCompanyViewDto.getEmployerId() == employerId) {
        assertEquals(displayName, workInCompanyViewDto.getDisplayName(), "Wrong display name for reservation");
      } else {
        assertEquals(stubName, workInCompanyViewDto.getDisplayName(), "Wrong display name for stub");
      }
    }
  }

  @Test
  public void getOneCompanyForCity() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";

    HashMap<Integer, WorkInCompanyDisplayInfo> workInCompanyDisplayInfoMap = new HashMap<>();
    workInCompanyDisplayInfoMap.put(employerId, new WorkInCompanyDisplayInfo());
    when(workInCompanyClient.getWorkInCompanyDisplayInfos(any()))
        .thenReturn(success(new WorkInCompanyDisplayInfos(workInCompanyDisplayInfoMap)));
    when(workInCompanyClient.getWorkInCompanyStubs(anyString(), anyInt())).thenReturn(success(new Integer[0]));

    runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            null,
            Set.of(TargetingDeviceType.DESKTOP))
    ));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();
    
    List<WorkInCompanyViewDto> workInCompanyToView =
        runAsBackoffice(
            () -> workInCompanyViewService.getWorkInCompanyToView(dateStart, impressionRequestDto),
            nextId(),
            regionId
        );

    assertEquals(1, workInCompanyToView.size(), "Wrong count of employers");
    assertEquals(employerId, workInCompanyToView.get(0).getEmployerId(), "Wrong employer id");
    assertEquals(displayName, workInCompanyToView.get(0).getDisplayName(), "Wrong display name");
  }

  @Test
  public void getOneCompanyWrongTargeting() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";
    when(workInCompanyClient.getWorkInCompanyDisplayInfos(any())).thenReturn(success(new WorkInCompanyDisplayInfos()));
    when(workInCompanyClient.getWorkInCompanyStubs(anyString(), anyInt())).thenReturn(success(new Integer[0]));

    runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            null,
            Set.of(TargetingDeviceType.DESKTOP))
    ));
    
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<WorkInCompanyViewDto> workInCompanyToView =
        runAsBackoffice(
            () -> workInCompanyViewService.getWorkInCompanyToView(dateStart, impressionRequestDto),
            nextId(),
            nextId()
        );

    assertEquals(0, workInCompanyToView.size(), "Wrong count of employers");
  }

  @Test
  public void getOneCompanyWrongDate() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";
    when(workInCompanyClient.getWorkInCompanyDisplayInfos(any())).thenReturn(success(new WorkInCompanyDisplayInfos()));
    when(workInCompanyClient.getWorkInCompanyStubs(anyString(), anyInt())).thenReturn(success(new Integer[0]));

    runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            null,
            Set.of(TargetingDeviceType.DESKTOP))
    ));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();


    List<WorkInCompanyViewDto> workInCompanyToView =
        runAsBackoffice(
            () -> workInCompanyViewService.getWorkInCompanyToView(dateEnd.plusDays(1), impressionRequestDto),
            nextId(),
            regionId
        );

    assertEquals(0, workInCompanyToView.size(), "Wrong count of employers");
  }

  @Test
  public void getOneCompanyExcludedRegion() throws Exception {
    int regionId = nextId();
    int excludedRegionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int employerId = nextId();
    String displayName = "Cute name";
    when(workInCompanyClient.getWorkInCompanyDisplayInfos(any())).thenReturn(success(new WorkInCompanyDisplayInfos()));
    when(workInCompanyClient.getWorkInCompanyStubs(anyString(), anyInt())).thenReturn(success(new Integer[0]));

    runAsBackoffice(() -> workInCompanyService.createWorkInCompany(
        new WorkInCompanyFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            displayName,
            employerId,
            null,
            Set.of(TargetingDeviceType.DESKTOP),
            true,
            Set.of(excludedRegionId)
            )
    ));

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));

    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<WorkInCompanyViewDto> workInCompanyToView =
        runAsBackoffice(
            () -> workInCompanyViewService.getWorkInCompanyToView(dateEnd.plusDays(1), impressionRequestDto),
            nextId(),
            excludedRegionId
        );

    assertEquals(0, workInCompanyToView.size(), "Wrong count of employers");
  }
  
  private ImpressionRequestDto generateImpressionRequest() {
    ImpressionRequestDto impressionRequestDto = new ImpressionRequestDto();

    impressionRequestDto.setUserAgent(getEntityGenerators().getStringGenerator().generate(10));
    impressionRequestDto.setRemoteHostIp(getEntityGenerators().getStringGenerator().generate(10));

    UriInfo uriInfo = Mockito.mock(UriInfo.class);
    Mockito.when(uriInfo.getRequestUri())
        .thenReturn(URI.create("http://hh.ru"));
    
    impressionRequestDto.setUriInfo(uriInfo);
    return impressionRequestDto;
  }
}
