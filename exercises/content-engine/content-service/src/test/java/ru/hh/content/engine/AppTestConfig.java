package ru.hh.content.engine;

import com.datastax.oss.driver.api.core.CqlSession;
import org.mockito.Mockito;
import static org.mockito.Mockito.RETURNS_MOCKS;
import static org.mockito.Mockito.mock;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.PropertiesFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import ru.hh.content.engine.config.CommonConfig;
import ru.hh.content.engine.resources.filter.HHSessionFilter;
import ru.hh.content.engine.utils.TranslationsMock;
import ru.hh.memcached.HHMemcachedTestClient;
import ru.hh.nab.testbase.NabTestConfig;
import ru.hh.nab.testbase.hibernate.NabHibernateTestBaseConfig;
import ru.hh.settings.SettingsClient;
import ru.hh.settings.SettingsTestClient;
import ru.hh.mailer.utils.MailService;

@Configuration
@Import({
    NabTestConfig.class,
    NabHibernateTestBaseConfig.class,
    ClientsMocksConfig.class,

    CommonConfig.class,
    EntityGenerators.class,
    HHMemcachedTestClient.class,
    HHSessionFilter.class,
})
public class AppTestConfig {
  @Bean
  PropertiesFactoryBean serviceProperties() {
    PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
    propertiesFactoryBean.setLocation(new ClassPathResource("service-test.properties"));
    return propertiesFactoryBean;
  }

  @Bean
  CqlSession cassandraSession() {
    return mock(CqlSession.class, RETURNS_MOCKS);
  }

  @Bean
  String datacenter() {
    return "test";
  }

  @Bean
  public SettingsClient getSettingsClient() {
    return new SettingsTestClient();
  }

  @Bean
  TranslationsMock translations() {
    return new TranslationsMock();
  }
  
  @Bean
  @Qualifier("mock")
  public MailService mailService() {
    return Mockito.mock(MailService.class);
  }
}
