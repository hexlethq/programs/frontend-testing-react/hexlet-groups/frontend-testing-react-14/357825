package ru.hh.content.engine.vacancy.of.the.day.service;

import java.net.URI;
import java.time.LocalDate;
import static java.util.Collections.emptySet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import org.mockito.Mockito;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.clients.banner.BannerClient;
import ru.hh.content.engine.clients.banner.dto.ClickmeVacancyDto;
import ru.hh.content.engine.clients.logic.LogicClient;
import ru.hh.content.engine.clients.logic.dto.ScoredVacanciesDto;
import ru.hh.content.engine.clients.xmlback.HHVacancyClient;
import ru.hh.content.engine.clients.xmlback.dto.AreaDto;
import ru.hh.content.engine.clients.xmlback.dto.CompensationDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyEmployerDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyId;
import ru.hh.content.engine.clients.xmlback.dto.VacancyIdsList;
import ru.hh.content.engine.clients.xmlback.dto.VacancyListDto;
import ru.hh.content.engine.dto.ImpressionRequestDto;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.targetings.UserDataForTargeting;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.utils.CommonTestUtils.nextMonday;
import static ru.hh.content.engine.utils.JClientMockUtils.status;
import static ru.hh.content.engine.utils.JClientMockUtils.success;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayFormDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayFromPlace;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayViewDto;
import static ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayViewService.AB_TEST_NAME_SETTING;
import static ru.hh.content.engine.work.in.company.WorkInCompanyConstants.MAX_COMPANIES_ON_PAGE_SETTING_NAME;

class VacancyOfTheDayViewServiceTest extends AppTestBase {
  @Inject
  private VacancyOfTheDayViewService vacancyOfTheDayViewService;
  @Inject
  private VacancyOfTheDayService vacancyOfTheDayService;
  @Inject
  private HHVacancyClient hhVacancyClient;
  @Inject
  private BannerClient bannerClient;
  @Inject
  private LogicClient logicClient;

  @Test
  public void getOneCompany() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();

    String vacancyName = "Название вакансии";
    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any())).thenReturn(success(new VacancyIdsList(List.of())));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateStart, impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            regionId
        );

    assertEquals(1, vacancyOfTheDayView.size(), "Wrong count of vacancies");
    assertEquals(vacancyId, vacancyOfTheDayView.get(0).getVacancyId(), "Wrong vacancy id");
    assertEquals(vacancyName, vacancyOfTheDayView.get(0).getName(), "Wrong display name");
  }

  @Test
  public void getOneCompanyAndOneStub() throws Exception {
    getSettingsTestClient().addSetting(MAX_COMPANIES_ON_PAGE_SETTING_NAME, "2");

    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    int vacancyIdStub = nextId();

    String vacancyName = "Название вакансии";
    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);

    String vacancyStubName = "Название вакансии-Заглушки";
    VacancyDto vacancyDtoStub = new VacancyDto(
        vacancyIdStub,
        vacancyStubName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyStubDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyStubDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto, vacancyDtoStub))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any()))
        .thenReturn(success(new VacancyIdsList(List.of(new VacancyId(vacancyIdStub)))));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));

    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateStart, impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            regionId
        );

    assertEquals(2, vacancyOfTheDayView.size(), "Wrong count of vacancies");
    for (VacancyOfTheDayViewDto vacancyOfTheDayViewDto : vacancyOfTheDayView) {
      if (vacancyOfTheDayViewDto.getVacancyId() == vacancyId) {
        assertEquals(vacancyName, vacancyOfTheDayViewDto.getName(), "Wrong display name for reservation");
      } else {
        assertEquals(vacancyStubName, vacancyOfTheDayViewDto.getName(), "Wrong display name for stub");
      }
    }
  }

  @Test
  public void getOneCompanyForCity() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();

    String vacancyName = "Название вакансии";
    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any())).thenReturn(success(new VacancyIdsList(List.of())));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateStart, impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            regionId
        );

    assertEquals(1, vacancyOfTheDayView.size(), "Wrong count of vacancies");
    assertEquals(vacancyId, vacancyOfTheDayView.get(0).getVacancyId(), "Wrong vacancy id");
    assertEquals(vacancyName, vacancyOfTheDayView.get(0).getName(), "Wrong display name");
  }

  @Test
  public void getOneCompanyWrongTargeting() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    String vacancyName = "Название вакансии";

    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any())).thenReturn(success(new VacancyIdsList(List.of())));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateStart, impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            nextId()
        );

    assertEquals(0, vacancyOfTheDayView.size(), "Wrong count of vacancies");
  }

  @Test
  public void getOneCompanyWrongDate() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    String vacancyName = "Название вакансии";

    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any())).thenReturn(success(new VacancyIdsList(List.of())));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));

    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();


    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateEnd.plusDays(1), impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            regionId
        );

    assertEquals(0, vacancyOfTheDayView.size(), "Wrong count of vacancies");
  }
  
  @Test
  public void getClickmeVacanciesInABTest() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    int clickmeVacancyId = nextId();
    getSettingsTestClient().addSetting(AB_TEST_NAME_SETTING, "abTestName");

    String vacancyName = "Название вакансии";
    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    
    VacancyDto clickmeVacancyDto = new VacancyDto(
        clickmeVacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(List.of(vacancyId))).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    when(hhVacancyClient.getVacancies(List.of(clickmeVacancyId))).thenReturn(success(new VacancyListDto(List.of(clickmeVacancyDto))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any())).thenReturn(success(new VacancyIdsList(List.of())));
    
    ClickmeVacancyDto[] clickmeVacancy = {new ClickmeVacancyDto(String.format("https://hh.ru/vacancy/%s", clickmeVacancyId))};
    when(bannerClient.getClickmeVacancies()).thenReturn(success(clickmeVacancy));
    
    when(logicClient.scoreVacancies(any(), anyInt())).thenReturn(success(new ScoredVacanciesDto(List.of(clickmeVacancyId))));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateStart, impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            regionId,
            Set.of(vacancyOfTheDayViewService.getAbTestName()),
            emptySet()
        );

    assertEquals(1, vacancyOfTheDayView.size(), "Wrong count of vacancies");
    assertEquals(clickmeVacancyId, vacancyOfTheDayView.get(0).getVacancyId(), "Wrong vacancy id");
    assertEquals(vacancyName, vacancyOfTheDayView.get(0).getName(), "Wrong display name");
  }
  
  @Test
  public void getClickmeVacanciesInControlGroup() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    getSettingsTestClient().addSetting(AB_TEST_NAME_SETTING, "abTestName");

    String vacancyName = "Название вакансии";
    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any())).thenReturn(success(new VacancyIdsList(List.of())));
    
    int clickmeVacancyId = nextId();
    ClickmeVacancyDto[] clickmeVacancy = {new ClickmeVacancyDto(String.format("https://hh.ru/vacancy/%s", clickmeVacancyId))};
    when(bannerClient.getClickmeVacancies()).thenReturn(success(clickmeVacancy));
    
    when(logicClient.scoreVacancies(any(), anyInt())).thenReturn(success(new ScoredVacanciesDto(List.of(clickmeVacancyId))));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateStart, impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            regionId,
            emptySet(),
            Set.of(vacancyOfTheDayViewService.getAbTestName())
        );

    assertEquals(1, vacancyOfTheDayView.size(), "Wrong count of vacancies");
    assertEquals(vacancyId, vacancyOfTheDayView.get(0).getVacancyId(), "Wrong vacancy id");
    assertEquals(vacancyName, vacancyOfTheDayView.get(0).getName(), "Wrong display name");
  }
  
  @Test
  public void getClickmeVacanciesInABTimeoutException() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    getSettingsTestClient().addSetting(AB_TEST_NAME_SETTING, "abTestName");

    String vacancyName = "Название вакансии";
    VacancyDto vacancyDto = new VacancyDto(
        vacancyId,
        vacancyName,
        new VacancyEmployerDto(),
        new CompensationDto(),
        new AreaDto(nextId())
    );
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    when(hhVacancyClient.getVacanciesOfTheDay(anyInt(), anyInt(), any())).thenReturn(success(new VacancyIdsList(List.of())));
    
    int clickmeVacancyId = nextId();
    ClickmeVacancyDto[] clickmeVacancy = {new ClickmeVacancyDto(String.format("https://hh.ru/vacancy/%s", clickmeVacancyId))};
    when(bannerClient.getClickmeVacancies()).thenReturn(success(clickmeVacancy));
    
    when(logicClient.scoreVacancies(any(), anyInt())).thenReturn(status(Response.Status.REQUEST_TIMEOUT));

    Long vacancyOfTheDayId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            "comment",
            "crmUrl",
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    runInTransaction(session -> session.createQuery(
        "UPDATE VacancyOfTheDay set status = :status where vacancyOfTheDayId = :vacancyOfTheDayId"
    )
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("status", VacancyOfTheDayStatus.APPROVED)
        .executeUpdate());

    UserDataForTargeting userDataForTargeting = new UserDataForTargeting();
    userDataForTargeting.setRegionIds(Set.of(regionId));
    ImpressionRequestDto impressionRequestDto = generateImpressionRequest();

    List<VacancyOfTheDayViewDto> vacancyOfTheDayView =
        runAsBackoffice(
            () -> vacancyOfTheDayViewService.getVacanciesToView(dateStart, impressionRequestDto, VacancyOfTheDayFromPlace.MAIN_PAGE),
            nextId(),
            regionId,
            emptySet(),
            Set.of(vacancyOfTheDayViewService.getAbTestName())
        );

    assertEquals(1, vacancyOfTheDayView.size(), "Wrong count of vacancies");
    assertEquals(vacancyId, vacancyOfTheDayView.get(0).getVacancyId(), "Wrong vacancy id");
    assertEquals(vacancyName, vacancyOfTheDayView.get(0).getName(), "Wrong display name");
  }

  private ImpressionRequestDto generateImpressionRequest() {
    ImpressionRequestDto impressionRequestDto = new ImpressionRequestDto();

    impressionRequestDto.setUserAgent(getEntityGenerators().getStringGenerator().generate(10));
    impressionRequestDto.setRemoteHostIp(getEntityGenerators().getStringGenerator().generate(10));

    UriInfo uriInfo = Mockito.mock(UriInfo.class);
    Mockito.when(uriInfo.getRequestUri())
        .thenReturn(URI.create("http://hh.ru"));

    impressionRequestDto.setUriInfo(uriInfo);
    return impressionRequestDto;
  }
}
