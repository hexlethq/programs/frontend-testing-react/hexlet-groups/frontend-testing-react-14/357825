package ru.hh.content.engine.work.in.company.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.services.RegionsService;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyRegionDto;

class WorkInCompanyRegionsServiceTest extends AppTestBase {

  @Test
  public void getRegionsForWorkInCompanyTest() {
    RegionsService regionsService = mock(RegionsService.class);
    when(regionsService.getCountryRegionsMap()).thenReturn(Map.of(113, List.of(12, 1)));
    Map<Integer, String> translations = Map.of(113, "Россия", 12, "Благовещенск", 1, "Москва");
    when(regionsService.getTranslatedAllRegions()).thenReturn(translations);
    WorkInCompanyRegionsService workInCompanyRegionsService = new WorkInCompanyRegionsService(regionsService);
    List<WorkInCompanyRegionDto> regionsForWorkInCompany = workInCompanyRegionsService.getRegionsForWorkInCompany();
    assertEquals(translations.size(), regionsForWorkInCompany.size(), "work in company regions list is not equals to expected");
    translations.forEach((id, value) -> {
      Optional<WorkInCompanyRegionDto> dto = regionsForWorkInCompany.stream().filter(it -> it.getId().equals(id.toString())).findFirst();
      assertTrue(dto.isPresent(), "expected id is not present in work in company regions list");
      assertEquals(value, dto.get().getText(), "translation value is not equals to expected");
    });
  }
}
