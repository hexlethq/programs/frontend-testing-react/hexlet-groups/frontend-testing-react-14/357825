package ru.hh.content.engine.utils;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.concurrent.atomic.AtomicLong;

public class CommonTestUtils {

  public static final LocalDateTime JESUS_BIRTHDAY = LocalDateTime.of(1, 1, 1, 0, 0);

  private CommonTestUtils() {

  }

  private static final AtomicLong idSequencer = new AtomicLong(1000);
  private static final AtomicLong dateSequencer = new AtomicLong();

  public static int nextId() {
    return Long.valueOf(idSequencer.incrementAndGet()).intValue();
  }

  public static long nextLongId() {
    return idSequencer.incrementAndGet();
  }

  public static LocalDateTime nextDateByDay() {
    return JESUS_BIRTHDAY.plusDays(dateSequencer.incrementAndGet());
  }

  public static LocalDateTime nextMonday() {
    while (JESUS_BIRTHDAY.plusDays(dateSequencer.incrementAndGet()).getDayOfWeek() != DayOfWeek.MONDAY) {

    }

    return JESUS_BIRTHDAY.plusDays(dateSequencer.get());
  }

  public static LocalDateTime nextDateByMonth() {
    return JESUS_BIRTHDAY.plusDays(dateSequencer.addAndGet(31));
  }
}
