package ru.hh.content.engine.vacancy.of.the.day.service;

import java.time.LocalDate;
import static java.util.Collections.emptySet;
import java.util.List;
import java.util.Set;
import javax.inject.Inject;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import ru.hh.content.engine.AppTestBase;
import ru.hh.content.engine.clients.xmlback.HHVacancyClient;
import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyEmployerDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyListDto;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDayReservation;
import static ru.hh.content.engine.utils.CommonTestUtils.nextId;
import static ru.hh.content.engine.utils.CommonTestUtils.nextMonday;
import static ru.hh.content.engine.utils.JClientMockUtils.success;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayDao;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyFrontDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayFormDto;

class VacancyOfTheDayServiceTest extends AppTestBase {
  @Inject
  private VacancyOfTheDayService vacancyOfTheDayService;
  @Inject
  VacancyOfTheDayDao vacancyOfTheDayDao;
  @Inject
  private HHVacancyClient hhVacancyClient;

  @Test
  public void createVacancyOfTheDay() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    String comment = "comment";
    String crmUrl = "crm_url";

    VacancyDto vacancyDto = new VacancyDto(vacancyId, "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));

    long returnedId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            comment,
            crmUrl,
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ));

    VacancyOfTheDay vacancyOfTheDayFromDb = getInTransaction(session -> session.get(VacancyOfTheDay.class, returnedId));
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getInTransaction(session ->
        session.createQuery(
            "from VacancyOfTheDayReservation where vacancyOfTheDayId = :vacancyOfTheDayId",
            VacancyOfTheDayReservation.class)
            .setParameter("vacancyOfTheDayId", vacancyOfTheDayFromDb.getVacancyOfTheDayId())
            .uniqueResult()
    );

    assertEquals(vacancyId, vacancyOfTheDayFromDb.getVacancyId(), "Wrong vacancy id");
    assertEquals(comment, vacancyOfTheDayFromDb.getComment(), "Wrong comment");
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDayFromDb.getStatus(), "Wrong status");
    assertEquals(VacancyOfTheDayPayedStatus.NOT_PAYED, vacancyOfTheDayFromDb.getPayedStatus(), "Wrong payed status");

    assertNotNull(vacancyOfTheDayReservation, "Reservation must not be null");
    assertEquals(0, vacancyOfTheDayReservation.getReservationNumber(), "Wrong reservation number");
  }

  @Test
  public void createVacancyOfTheDayWithRussiaPacket() throws Exception {
    int regionId = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    String comment = "comment";
    String crmUrl = "crm_url";

    getEntityGenerators().createPacketToRegion(regionId, PacketType.RUSSIA);

    VacancyDto vacancyDto = new VacancyDto(vacancyId, "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));

    long returnedId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            comment,
            crmUrl,
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.RUSSIA)
    ));

    VacancyOfTheDay vacancyOfTheDayFromDb = getInTransaction(session -> session.get(VacancyOfTheDay.class, returnedId));
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getInTransaction(session ->
        session.createQuery(
            "from VacancyOfTheDayReservation where vacancyOfTheDayId = :vacancyOfTheDayId",
            VacancyOfTheDayReservation.class)
            .setParameter("vacancyOfTheDayId", vacancyOfTheDayFromDb.getVacancyOfTheDayId())
            .uniqueResult()
    );

    assertEquals(vacancyId, vacancyOfTheDayFromDb.getVacancyId(), "Wrong vacancy id");
    assertEquals(comment, vacancyOfTheDayFromDb.getComment(), "Wrong comment");
    assertEquals(VacancyOfTheDayStatus.NEW, vacancyOfTheDayFromDb.getStatus(), "Wrong status");
    assertEquals(VacancyOfTheDayPayedStatus.NOT_PAYED, vacancyOfTheDayFromDb.getPayedStatus(), "Wrong payed status");

    assertNotNull(vacancyOfTheDayReservation, "Reservation must not be null");
    assertEquals(0, vacancyOfTheDayReservation.getReservationNumber(), "Wrong reservation number");
    assertEquals(regionId, vacancyOfTheDayReservation.getRegionId(), "Wrong region id");
  }

  @Test
  public void editVacancyOfTheDay() throws Exception {
    int regionId = nextId();
    int regionIdAfterEdit = nextId();
    LocalDate dateStart = nextMonday().toLocalDate();
    LocalDate dateEnd = dateStart.plusDays(6);
    int vacancyId = nextId();
    int vacancyIdAfterEdit = nextId();
    String comment = "comment";
    String commentAfterEdit = "edited comment";
    String crmUrl = "crm_url";
    String crmUrlAfterEdit = "another_crm_url";

    int creatorUserId = nextId();

    VacancyDto vacancyDto = new VacancyDto(vacancyId, "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));

    long returnedId = runAsBackoffice(() -> vacancyOfTheDayService.createVacancyOfTheDay(
        new VacancyOfTheDayFormDto(
            Set.of(regionId),
            dateStart,
            dateEnd,
            comment,
            crmUrl,
            vacancyId,
            Set.of(TargetingDeviceType.DESKTOP),
            VacancyOfTheDayStatus.NEW,
            VacancyOfTheDayPayedStatus.NOT_PAYED,
            VacancyOfTheDayCampaignType.COMMERCIAL,
            PacketType.CUSTOM)
    ), creatorUserId);

    runAsBackoffice(() -> {
          vacancyOfTheDayService.editVacancyOfTheDay(
              new VacancyOfTheDayFormDto(
                  Set.of(regionIdAfterEdit),
                  dateStart,
                  dateEnd,
                  commentAfterEdit,
                  crmUrlAfterEdit,
                  vacancyIdAfterEdit,
                  Set.of(TargetingDeviceType.DESKTOP),
                  VacancyOfTheDayStatus.NEW,
                  VacancyOfTheDayPayedStatus.NOT_PAYED,
                  VacancyOfTheDayCampaignType.COMMERCIAL,
                  PacketType.CUSTOM),
              returnedId
          );
          return null;
        }, creatorUserId
    );

    VacancyOfTheDay vacancyOfTheDayFromDbAfterEdit = getInTransaction(session -> session.get(VacancyOfTheDay.class, returnedId));
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getInTransaction(session ->
        session.createQuery(
            "from VacancyOfTheDayReservation where vacancyOfTheDayId = :vacancyOfTheDayId",
            VacancyOfTheDayReservation.class)
            .setParameter("vacancyOfTheDayId", vacancyOfTheDayFromDbAfterEdit.getVacancyOfTheDayId())
            .uniqueResult()
    );

    assertEquals(vacancyIdAfterEdit, vacancyOfTheDayFromDbAfterEdit.getVacancyId(), "Wrong vacancy id");
    assertEquals(crmUrlAfterEdit, vacancyOfTheDayFromDbAfterEdit.getCrmUrl(), "Wrong display name");
    assertEquals(commentAfterEdit, vacancyOfTheDayFromDbAfterEdit.getComment(), "Wrong comment");

    assertNotNull(vacancyOfTheDayReservation, "Reservation must not be null");
    assertEquals(0, vacancyOfTheDayReservation.getReservationNumber(), "Wrong reservation number");
  }
  
  @Test
  public void getVacancyOfTheDayListTest() {
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getEntityGenerators().createVacancyOfTheDayReservation(it -> {
      it.setDateStart(LocalDate.now().minusDays(10));
      it.setDateEnd(LocalDate.now().minusDays(2));
      it.setRegionId(nextId());
    });
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayReservation.getVacancyOfTheDayId());
    
    VacancyDto vacancyDto = new VacancyDto(vacancyOfTheDay.getVacancyId(), "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    
    List<VacancyFrontDto> vacancyFrontDtoList = vacancyOfTheDayService.getVacancyOfTheDayList(
        vacancyOfTheDay.getVacancyOfTheDayId(),
        vacancyOfTheDay.getVacancyId(),
        vacancyDto.getName(),
        vacancyOfTheDay.getEmployerId(),
        vacancyOfTheDay.getEmployerName(),
        vacancyOfTheDay.getManagerUserId(),
        vacancyOfTheDay.getManagerName(),
        vacancyOfTheDay.getCrmUrl(),
        LocalDate.of(1, 1, 1),
        LocalDate.now(),
        Set.of(vacancyOfTheDayReservation.getRegionId()),
        true,
        Set.of(vacancyOfTheDayReservation.getDeviceType()),
        Set.of(vacancyOfTheDay.getStatus()),
        Set.of(vacancyOfTheDay.getPayedStatus()),
        Set.of(vacancyOfTheDay.getCampaignType()),
        Set.of(vacancyOfTheDay.getPacketType())
    );
    assertEquals(1, vacancyFrontDtoList.size(), "Wrong found");
    
    List<VacancyFrontDto> vacancyFrontDtoList2 = vacancyOfTheDayService.getVacancyOfTheDayList(
        vacancyOfTheDay.getVacancyOfTheDayId(),
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        LocalDate.of(1, 1, 1),
        LocalDate.now(),
        emptySet(),
        false,
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet()
    );
    assertEquals(1, vacancyFrontDtoList2.size(), "Wrong found");
  }
  
  @Test
  public void getVacancyOfTheDayById() {
    LocalDate from = LocalDate.now().minusDays(10);
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getEntityGenerators().createVacancyOfTheDayReservation(it -> {
      it.setDateStart(from);
      it.setDateEnd(from.plusDays(6));
      it.setRegionId(nextId());
    });
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayReservation.getVacancyOfTheDayId());
    
    VacancyDto vacancyDto = new VacancyDto(vacancyOfTheDay.getVacancyId(), "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    
    List<VacancyFrontDto> vacancyFrontDtoList = vacancyOfTheDayService.getVacancyOfTheDayList(
        vacancyOfTheDay.getVacancyOfTheDayId(),
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        from,
        from.plusDays(6),
        emptySet(),
        false,
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet()
    );
    
    assertEquals(1, vacancyFrontDtoList.size(), "vacancy of the day list size must be 1");
    assertEquals(vacancyOfTheDay.getVacancyOfTheDayId(), vacancyFrontDtoList.get(0).getVacancyOfTheDayId(), "Wrong vacancy of the day id");
    assertEquals(from, vacancyFrontDtoList.get(0).getDateFrom(), "Wrong date from");
    assertEquals(from.plusDays(6), vacancyFrontDtoList.get(0).getDateTo(), "Wrong date to");
    assertEquals(vacancyOfTheDay.getEmployerId(), vacancyFrontDtoList.get(0).getEmployer().getId(), "Wrong employer id");
    assertEquals(vacancyOfTheDay.getEmployerName(), vacancyFrontDtoList.get(0).getEmployer().getName(), "Wrong employer name");
  }
  
  @Test
  @DisplayName("Get vacancy of the day list for period")
  public void testTen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getEntityGenerators().createVacancyOfTheDayReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayReservation.getVacancyOfTheDayId());
    
    VacancyDto vacancyDto = new VacancyDto(vacancyOfTheDay.getVacancyId(), "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    
    List<VacancyFrontDto> vacancyFrontDtoList = vacancyOfTheDayService.getVacancyOfTheDayList(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        localDateTime,
        localDateTime.plusWeeks(2),
        emptySet(),
        false,
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet()
    );
    
    assertEquals(1, vacancyFrontDtoList.size(), "VacancyOfTheDay list size must be 1");
    assertEquals(vacancyOfTheDay.getVacancyOfTheDayId(), vacancyFrontDtoList.get(0).getVacancyOfTheDayId(), "Wrong vacancy of the day id");
    assertEquals(localDateTime, vacancyFrontDtoList.get(0).getDateFrom(), "Wrong date from");
    assertEquals(localDateTime.plusDays(6), vacancyFrontDtoList.get(0).getDateTo(), "Wrong date to");
    assertEquals(vacancyOfTheDay.getEmployerId(), vacancyFrontDtoList.get(0).getEmployer().getId(), "Wrong employer id");
    assertEquals(vacancyOfTheDay.getEmployerName(), vacancyFrontDtoList.get(0).getEmployer().getName(), "Wrong employer name");
  }
  
  @Test
  @DisplayName("Get vacancy of the day list for period from")
  public void testEleven() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getEntityGenerators().createVacancyOfTheDayReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayReservation.getVacancyOfTheDayId());
    
    VacancyDto vacancyDto = new VacancyDto(vacancyOfTheDay.getVacancyId(), "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    
    List<VacancyFrontDto> vacancyFrontDtoList = vacancyOfTheDayService.getVacancyOfTheDayList(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        localDateTime,
        null,
        emptySet(),
        false,
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet()
    );
    
    assertTrue(vacancyFrontDtoList.size() > 1, "VacancyOfTheDay list size must be bigger than 1");
  }
  
  @Test
  @DisplayName("Get vacancy of the day list for period to")
  public void testTwelve() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getEntityGenerators().createVacancyOfTheDayReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayReservation.getVacancyOfTheDayId());
    
    VacancyDto vacancyDto = new VacancyDto(vacancyOfTheDay.getVacancyId(), "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    
    List<VacancyFrontDto> vacancyFrontDtoList = vacancyOfTheDayService.getVacancyOfTheDayList(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        localDateTime.plusWeeks(2),
        emptySet(),
        false,
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet()
    );
    
    assertTrue(vacancyFrontDtoList.size() > 1, "VacancyOfTheDay list size must be bigger than 1");
  }
  
  @Test
  @DisplayName("Get work in company list for period wrong employer")
  public void testThirteen() {
    LocalDate localDateTime = nextMonday().toLocalDate();
    int regionId = nextId();
    
    VacancyOfTheDayReservation vacancyOfTheDayReservation = getEntityGenerators().createVacancyOfTheDayReservation(it -> {
      it.setDateStart(localDateTime);
      it.setDateEnd(localDateTime.plusDays(6));
      it.setRegionId(regionId);
    });
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayReservation.getVacancyOfTheDayId());
    
    VacancyDto vacancyDto = new VacancyDto(vacancyOfTheDay.getVacancyId(), "Название вакансии");
    VacancyEmployerDto companyDto = new VacancyEmployerDto(nextInt(), "Название емплоера");
    vacancyDto.setVacancyEmployerDto(companyDto);
    when(hhVacancyClient.getVacancies(any())).thenReturn(success(new VacancyListDto(List.of(vacancyDto))));
    
    List<VacancyFrontDto> vacancyFrontDtoList = vacancyOfTheDayService.getVacancyOfTheDayList(
        null,
        null,
        null,
        nextId(),
        null,
        null,
        null,
        null,
        localDateTime,
        localDateTime.plusWeeks(2),
        emptySet(),
        false,
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet(),
        emptySet()
    );
    
    assertEquals(0, vacancyFrontDtoList.size(), "vacancy of the day list size must be 0");
  }
}
