package ru.hh.content.engine.utils.cache;

import java.util.Optional;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import ru.hh.content.engine.AppTestBase;

class CacheServiceTest extends AppTestBase {
  public static final String TEST_TTL = "test.ttl";

  private CacheService<String> cacheService;

  @BeforeEach
  public void setUpClass() {
    cacheService = new CacheService<>(getSettingsTestClient(), TEST_TTL);
  }

  @Test
  public void testGetFromCache() {
    String cachedString = "I AM CACHE";
    String key = "key";

    Optional<String> fromCache = cacheService.getFromCache(key);

    assertTrue(fromCache.isEmpty(), "Cache must be empty");

    getSettingsTestClient().addSetting(TEST_TTL, "10000000");

    cacheService.putInCache(key, cachedString);

    fromCache = cacheService.getFromCache(key);

    assertTrue(fromCache.isPresent(), "Cache must be not empty");
    assertEquals(cachedString, fromCache.get(), "Cache was wrong");
  }

  @Test
  public void testGetFromCacheAfterTTL() {
    String cachedString = "I AM CACHE";
    String key = "key";

    Optional<String> fromCache = cacheService.getFromCache(key);

    assertTrue(fromCache.isEmpty(), "Cache must be empty");

    getSettingsTestClient().addSetting(TEST_TTL, "0");

    cacheService.putInCache(key, cachedString);

    fromCache = cacheService.getFromCache(key);

    assertTrue(fromCache.isEmpty(), "Cache must be empty after ttl");
  }

  @Test
  public void testGetFromCacheAfterTTLByRemoveOldCache() {
    String cachedString = "I AM CACHE";
    String key = "key";

    getSettingsTestClient().addSetting(TEST_TTL, "0");

    cacheService.putInCache(key, cachedString);

    cacheService.removeOldCache();

    CacheItem<String> fromCache = cacheService.getCacheItemMap().get(key);

    assertNull(fromCache, "Cache must be empty after ttl");
  }
}
