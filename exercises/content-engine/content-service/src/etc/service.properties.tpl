serviceName=content-engine
datacenter=test
allowCrossDCRequests=false

jetty.port=${docker/content-engine/application_port}
jetty.minThreads=8
jetty.maxThreads=8
jetty.selectors=1

invoker.jclient.host=http://${docker/hh-invoker/application_host}:${docker/hh-invoker/application_port}

memcached.operation.timeout.millis=25

memcached.servers=${haproxy/memcached-hhid/application_host}:${haproxy/memcached-hhid/application_port}
memcached.maxReconnectDelay=2
memcached.opTimeoutMs=200
memcached.timeoutExceptionThreshold=20
memcached.opQueueMaxBlockTime=0
memcached.opQueueCapacity=200
memcached.writeOpQueueCapacity=100
memcached.readOpQueueCapacity=100
memcached.failureMode=Cancel
memcached.sendStats=false
memcached.sendQueuesStats=false
memcached.protocol=BINARY

master.jdbcUrl=jdbc:postgresql://${docker/postgresql/application_host}:${docker/postgresql/application_port}/content_engine?stringtype=unspecified
master.user=hh
master.password=123

master.pool.maxLifetime=300000
master.pool.idleTimeout=0
master.pool.connectionTimeout=5000
master.pool.leakDetectionThreshold=10000

master.monitoring.sendStats=false
master.monitoring.sendSampledStats=false
master.monitoring.longConnectionUsageMs=2000

readonly.jdbcUrl=jdbc:postgresql://${docker/postgresql/application_host}:${docker/postgresql/application_port}/content_engine?stringtype=unspecified
readonly.user=hh
readonly.password=123

readonly.pool.maxLifetime=300000
readonly.pool.idleTimeout=0
readonly.pool.connectionTimeout=5000
readonly.pool.leakDetectionThreshold=10000

readonly.monitoring.sendStats=false
readonly.monitoring.sendSampledStats=false
readonly.monitoring.longConnectionUsageMs=2000

slow.jdbcUrl=jdbc:postgresql://${docker/postgresql/application_host}:${docker/postgresql/application_port}/content_engine?stringtype=unspecified
slow.user=hh
slow.password=123

slow.pool.maxLifetime=300000
slow.pool.idleTimeout=0
slow.pool.connectionTimeout=5000
slow.pool.leakDetectionThreshold=10000

slow.monitoring.sendStats=false
slow.monitoring.sendSampledStats=false
slow.monitoring.longConnectionUsageMs=2000

jclient.connectionTimeoutMs=2000
jclient.requestTimeoutMs=5000
jclient.readTimeoutMs=5000
jclient.hostsWithSession=http://localhost

cassandra.main.nodes=${docker/cassandra-site/application_host}
cassandra.main.connectTimeoutMs=2000
cassandra.main.readTimeoutPerNodeMs=5000
cassandra.main.heartbeatIntervalSeconds=30
cassandra.main.reconnectBaseDelayMs=1000
cassandra.main.reconnectMaxDelayMs=10000
cassandra.main.logStatements=true
cassandra.main.sendStats=false

kafka.banner.common.bootstrap.servers=${haproxy/kafka-banner/application_host}:${haproxy/kafka-banner/application_port}

kafka.banner.consumer.default.auto.offset.reset=latest
kafka.banner.consumer.default.fetch.max.wait.ms=10000
kafka.banner.consumer.default.fetch.min.bytes=10000000
kafka.banner.consumer.default.max.poll.interval.ms=500000
kafka.banner.consumer.default.max.poll.records=100000
kafka.banner.consumer.default.enable.auto.commit=false
kafka.banner.consumer.default.heartbeat.interval.ms=10000
kafka.banner.consumer.default.session.timeout.ms=200000
kafka.banner.consumer.default.client.rack=test

clickhouse.banner.client.jdbcUrl=jdbc:clickhouse://dev.pyn.ru:11510/clickme
clickhouse.banner.client.user=default
clickhouse.banner.client.password=123
clickhouse.banner.client.dataTransferTimeout=20000
clickhouse.banner.client.keepAliveTimeout=60000
clickhouse.banner.client.timeToLiveMillis=120000

clickhouse.banner.pool.maximumPoolSize=3
clickhouse.banner.pool.connectionTimeout=100000
clickhouse.banner.pool.maxLifetime=300000
clickhouse.banner.pool.idleTimeout=0
clickhouse.banner.pool.leakDetectionThreshold=150000

jclient.hh-session.internalEndpointUrl=http://${haproxy/hh-session/application_host}:${haproxy/hh-session/application_port}
jclient.hh-session.accountSessionsMinBatchSize=5
jclient.hh-session.accountSessionsMaxBatchSize=7
jclient.hh-session.batchRequestTimeout=200
jclient.hh-session.connectionTimeoutMs=1000
jclient.hh-session.requestTimeoutMs=2000
jclient.hh-session.readTimeoutMs=-1
jclient.hh-session.sourceName=hh-session
jclient.hh-session.timeoutMultiplier=2
jclient.hh-session.maxRequestRetry=0
jclient.hh-session.threadpool.name=hh-session
jclient.hh-session.threadpool.minSize=1
jclient.hh-session.threadpool.maxSize=4
jclient.hh-session.threadpool.queueSize=20
jclient.hh-session.threadpool.keepAliveTimeSec=60

jclient.hh-xmlback.internalEndpointUrl=http://${haproxy/hh-xmlback/application_host}:${haproxy/hh-xmlback/application_port}
jclient.hh-xmlback.accountSessionsMinBatchSize=5
jclient.hh-xmlback.accountSessionsMaxBatchSize=7
jclient.hh-xmlback.batchRequestTimeout=200
jclient.hh-xmlback.connectionTimeoutMs=1000
jclient.hh-xmlback.requestTimeoutMs=2000
jclient.hh-xmlback.readTimeoutMs=-1
jclient.hh-xmlback.sourceName=hh-xmlback
jclient.hh-xmlback.timeoutMultiplier=2
jclient.hh-xmlback.maxRequestRetry=0
jclient.hh-xmlback.threadpool.name=hh-xmlback
jclient.hh-xmlback.threadpool.minSize=1
jclient.hh-xmlback.threadpool.maxSize=4
jclient.hh-xmlback.threadpool.queueSize=20
jclient.hh-xmlback.threadpool.keepAliveTimeSec=60

jclient.hh-xmlback.areas.timeout=10000
jclient.hh-xmlback.vacancy.timeout=10000

analytics.jclient.host=http://${haproxy/hh-analytics/application_host}:${haproxy/hh-analytics/application_port}

banner-engine.jclient.host=http://${haproxy/banner-engine/application_host}:${haproxy/banner-engine/application_port}

logic.jclient.host=http://${haproxy/logic/application_host}:${haproxy/logic/application_port}
logic.jclient.requestTimeout=100

jclient.hhid.internalEndpointUrl=http://${haproxy/hhid/application_host}:${haproxy/hhid/application_port}
jclient.hhid.connectionAcquisitionTimeout=100
jclient.hhid.connectionPoolSize=8
jclient.hhid.connectTimeout=200
jclient.hhid.readTimeout=-1
jclient.hhid.readTimeout.account=-1
jclient.hhid.readTimeout.session=-1
jclient.hhid.tcpNoDelay=true

webdav.filestorage.storageaddresses=${haproxy/webdav-put/application_host}:${haproxy/webdav-put/application_port}
webdav.filestorage.balanceraddresses=${haproxy/webdav-get/application_host}:${haproxy/webdav-get/application_port}
webdav.filestorage.timeout=20000
webdav.filestorage.maxPoolSize=20
webdav.filestorage.request.timeout.millis=20000

webdav.timeout_ms=5000
webdav.public.file.baseUrl=https://hhcdn.ru

mail.client.hosts=${haproxy/queue-mailer-ext/application_host}
mail.client.port=${haproxy/queue-mailer-ext/application_port}
mail.client.username=guest
mail.client.password=guest
mail.client.virtualhost=/
mail.client.heartbit.interval.seconds=5
mail.client.publisher.name=mailer
mail.client.publisher.innerqueue.size=1000
mail.client.publisher.reconnection.delay.millis=10000
mail.client.publisher.mandatory=true
mail.client.publisher.use.mdc=true
mail.client.queue.base=mail.active
mail.client.send.timeout=1000
mail.client.send.timeout.unit=MILLISECONDS
mail.client.send.batch.timeout=10
mail.client.send.batch.timeout.unit=MINUTES
mail.client.sender=hh

log.dir=logs
log.immediate.flush=true
log.toConsole=true

is.test.environment=true

jdebug.enabled=true
jdebug.logFullQuery=true

translations.path=${translations}
translations.check.interval.ms=5000

consul.enabled=false
consul.http.port=13100
consul.check.timeout=5s
consul.check.interval=5s
consul.tags=
