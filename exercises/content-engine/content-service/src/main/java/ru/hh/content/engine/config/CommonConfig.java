package ru.hh.content.engine.config;

import java.util.List;
import javax.inject.Inject;
import javax.sql.DataSource;
import javax.xml.bind.JAXBException;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.content.engine.area.region.mapping.AreaRegionMappingConfig;
import ru.hh.content.engine.limits.ContentLimitConfig;
import ru.hh.content.engine.packets.PacketConfig;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.BannerService;
import ru.hh.content.engine.services.EmployerService;
import ru.hh.content.engine.services.ExperimentService;
import ru.hh.content.engine.services.FrontRegionService;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.content.engine.services.ImageLoaderService;
import ru.hh.content.engine.services.LogicService;
import ru.hh.content.engine.services.NotificationService;
import ru.hh.content.engine.services.RegionsService;
import ru.hh.content.engine.services.Translations;
import ru.hh.content.engine.services.VacancyService;
import ru.hh.content.engine.targetings.TargetingConfig;
import ru.hh.content.engine.utils.GenericDao;
import ru.hh.content.engine.vacancy.of.the.day.VacancyOfTheDayConfig;
import ru.hh.content.engine.vacancy.of.the.day.service.ScoredVacancyService;
import ru.hh.content.engine.work.in.company.WorkInCompanyConfig;
import ru.hh.jdebug.jersey2.Jersey2DebugRecorder;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.datasource.DataSourceFactory;
import ru.hh.nab.datasource.DataSourceType;
import ru.hh.nab.hibernate.MappingConfig;
import ru.hh.nab.hibernate.NabHibernateCommonConfig;
import ru.hh.nab.hibernate.datasource.RoutingDataSource;
import ru.hh.nab.metrics.StatsDSender;
import ru.hh.nab.starter.AppMetadata;

@Configuration
@Import({
    NabHibernateCommonConfig.class,
    ResourcesConfig.class,
    KafkaConfig.class,
    ClickhouseConfig.class,

    TargetingConfig.class,
    ContentLimitConfig.class,
    WorkInCompanyConfig.class,
    AreaRegionMappingConfig.class,
    VacancyOfTheDayConfig.class,
    PacketConfig.class,

    GenericDao.class,
    HHSessionContext.class,
    ImageLoaderService.class,

    RegionsService.class,
    FrontRegionService.class,
    VacancyService.class,
    EmployerService.class,
    NotificationService.class,
    BannerService.class,
    LogicService.class,
    ScoredVacancyService.class,
    ExperimentService.class,
    SecurityContext.class,
})
public class CommonConfig {
  private final FileSettings fileSettings;

  @Inject
  public CommonConfig(FileSettings fileSettings, String serviceName, AppMetadata appMetadata) throws JAXBException {
    this.fileSettings = fileSettings;

    if (Boolean.TRUE.equals(fileSettings.getBoolean("jdebug.enabled"))) {
      Jersey2DebugRecorder.init(serviceName, appMetadata.getVersion(), false, List.of());
    }
  }

  @Bean
  DataSource routingDataSource(DataSource contentMasterDataSource,
                               DataSource contentReadonlyDataSource,
                               DataSource contentSlowDataSource) {
    RoutingDataSource routingDataSource = new RoutingDataSource(contentMasterDataSource);
    routingDataSource.addDataSource(DataSourceType.READONLY, contentReadonlyDataSource);
    routingDataSource.addDataSource(DataSourceType.SLOW, contentSlowDataSource);
    return routingDataSource;
  }

  @Bean
  public DataSource contentMasterDataSource(DataSourceFactory dataSourceFactory) {
    return dataSourceFactory.create(DataSourceType.MASTER, false, fileSettings);
  }

  @Bean
  DataSource contentSlowDataSource(DataSourceFactory dataSourceFactory) {
    return dataSourceFactory.create(DataSourceType.SLOW, false, fileSettings);
  }

  @Bean
  public DataSource contentReadonlyDataSource(DataSourceFactory dataSourceFactory) {
    return dataSourceFactory.create(DataSourceType.READONLY, true, fileSettings);
  }

  @Bean
  MappingConfig mappingConfig() {
    MappingConfig mappingConfig = new MappingConfig();
    mappingConfig.addPackagesToScan("ru.hh.content.engine.model");

    return mappingConfig;
  }

  @Bean
  Translations translations(FileSettings fileSettings,
                            String serviceName,
                            StatsDSender statsDSender) {
    return new Translations(
        fileSettings.getString("translations.path"),
        fileSettings.getInteger("translations.check.interval.ms"),
        serviceName,
        statsDSender
    );
  }
}
