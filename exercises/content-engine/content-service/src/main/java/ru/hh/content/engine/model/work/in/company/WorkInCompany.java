package ru.hh.content.engine.model.work.in.company;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.WorkInCompanyStatus;
import static ru.hh.content.engine.model.enums.WorkInCompanyStatus.ACTIVE;

@Entity
@Table(name = "work_in_company")
public class WorkInCompany {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "work_in_company_id", nullable = false)
  private long workInCompanyId;

  @Column(name = "display_name", nullable = false)
  private String displayName;

  @Column(name = "comment")
  private String comment;

  @Column(name = "employer_id", nullable = false)
  private Integer employerId;

  @Column(name = "manager_user_id", nullable = false)
  private Integer managerUserId;

  @Column(name = "manager_name", nullable = false)
  private String managerName;

  @Column(name = "work_in_company_logo_id")
  private Integer workInCompanyLogoId;

  @JoinColumn(name = "work_in_company_logo_id", insertable = false, updatable = false)
  @ManyToOne(fetch = FetchType.LAZY)
  private WorkInCompanyLogo workInCompanyLogo;

  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private WorkInCompanyStatus status;

  @OneToMany(mappedBy = "workInCompanyId", fetch = FetchType.LAZY)
  private List<WorkInCompanyReservation> workInCompanyReservations;

  public WorkInCompany() {
    this.status = ACTIVE;
  }

  public WorkInCompany(String displayName,
                       Integer employerId,
                       Integer workInCompanyLogoId,
                       Integer managerUserId,
                       String managerName) {
    this();

    this.displayName = displayName;
    this.employerId = employerId;
    this.workInCompanyLogoId = workInCompanyLogoId;
    this.managerUserId = managerUserId;
    this.managerName = managerName;
  }

  public WorkInCompany(String displayName,
                       Integer employerId,
                       Integer workInCompanyLogoId,
                       Integer managerUserId,
                       String managerName,
                       String comment) {
    this();

    this.displayName = displayName;
    this.employerId = employerId;
    this.workInCompanyLogoId = workInCompanyLogoId;
    this.managerUserId = managerUserId;
    this.managerName = managerName;
    this.comment = comment;
  }

  public long getWorkInCompanyId() {
    return workInCompanyId;
  }

  public void setWorkInCompanyId(int employerOfTheDayId) {
    this.workInCompanyId = employerOfTheDayId;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public Integer getEmployerId() {
    return employerId;
  }

  public void setEmployerId(Integer employerId) {
    this.employerId = employerId;
  }

  public Integer getWorkInCompanyLogoId() {
    return workInCompanyLogoId;
  }

  public void setWorkInCompanyLogoId(Integer workInCompanyLogoId) {
    this.workInCompanyLogoId = workInCompanyLogoId;
  }

  public Integer getManagerUserId() {
    return managerUserId;
  }

  public void setManagerUserId(Integer managerId) {
    this.managerUserId = managerId;
  }

  public String getManagerName() {
    return managerName;
  }

  public void setManagerName(String managerName) {
    this.managerName = managerName;
  }

  public void setWorkInCompanyId(long workInCompanyId) {
    this.workInCompanyId = workInCompanyId;
  }

  public List<WorkInCompanyReservation> getWorkInCompanyReservations() {
    return workInCompanyReservations;
  }

  public void setWorkInCompanyReservations(List<WorkInCompanyReservation> workInCompanyReservations) {
    this.workInCompanyReservations = workInCompanyReservations;
  }

  public LocalDate getDateStartFromReservations() {
    return workInCompanyReservations.stream().map(WorkInCompanyReservation::getDateStart).min(LocalDate::compareTo).get();
  }

  public LocalDate getDateEndFromReservations() {
    return workInCompanyReservations.stream().map(WorkInCompanyReservation::getDateEnd).max(LocalDate::compareTo).get();
  }

  public Set<Integer> getNotExcludedRegionIdsFromReservations() {
    return workInCompanyReservations.stream()
        .filter(workInCompanyReservation -> !workInCompanyReservation.getExclusion())
        .map(WorkInCompanyReservation::getRegionId)
        .filter(it -> it != 113)
        .collect(Collectors.toSet());
  }

  public Set<Integer> getExcludedRegionIdsFromReservations() {
    return workInCompanyReservations.stream()
        .filter(WorkInCompanyReservation::getExclusion)
        .map(WorkInCompanyReservation::getRegionId)
        .collect(Collectors.toSet());
  }

  public boolean isRussianFromReservations() {
    return workInCompanyReservations.stream().map(WorkInCompanyReservation::getRegionId).anyMatch(it -> it == 113);
  }

  public Set<TargetingDeviceType> getDeviceTypesFromReservations() {
    return workInCompanyReservations.stream().map(WorkInCompanyReservation::getDeviceType).collect(Collectors.toSet());
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public WorkInCompanyStatus getStatus() {
    return status;
  }

  public void setStatus(WorkInCompanyStatus status) {
    this.status = status;
  }

  public WorkInCompanyLogo getWorkInCompanyLogo() {
    return workInCompanyLogo;
  }

  public void setWorkInCompanyLogo(WorkInCompanyLogo workInCompanyLogo) {
    this.workInCompanyLogo = workInCompanyLogo;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WorkInCompany that = (WorkInCompany) o;
    return workInCompanyId == that.workInCompanyId &&
        Objects.equals(displayName, that.displayName) &&
        Objects.equals(comment, that.comment) &&
        Objects.equals(employerId, that.employerId) &&
        Objects.equals(managerUserId, that.managerUserId) &&
        Objects.equals(managerName, that.managerName) &&
        Objects.equals(workInCompanyLogoId, that.workInCompanyLogoId) &&
        status == that.status &&
        Objects.equals(workInCompanyReservations, that.workInCompanyReservations);
  }

  @Override
  public int hashCode() {
    return Objects.hash(workInCompanyId,
        displayName,
        comment,
        employerId,
        managerUserId,
        managerName,
        workInCompanyLogoId,
        status,
        workInCompanyReservations);
  }

  @Override
  public String toString() {
    return "WorkInCompany{" +
        "workInCompanyId=" + workInCompanyId +
        ", displayName='" + displayName + '\'' +
        ", comment='" + comment + '\'' +
        ", employerId=" + employerId +
        ", managerUserId=" + managerUserId +
        ", managerName='" + managerName + '\'' +
        ", workInCompanyLogoId=" + workInCompanyLogoId +
        ", status=" + status +
        ", workInCompanyReservations=" + workInCompanyReservations +
        '}';
  }
}
