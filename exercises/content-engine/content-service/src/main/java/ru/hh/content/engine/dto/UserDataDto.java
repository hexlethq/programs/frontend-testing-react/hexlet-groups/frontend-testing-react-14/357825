package ru.hh.content.engine.dto;

public class UserDataDto {
  private int id;
  private String name;
  private boolean superAdmin;

  public UserDataDto() {
  }

  public UserDataDto(int id, String name, boolean superAdmin) {
    this.id = id;
    this.name = name;
    this.superAdmin = superAdmin;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isSuperAdmin() {
    return superAdmin;
  }

  public void setSuperAdmin(boolean superAdmin) {
    this.superAdmin = superAdmin;
  }
}
