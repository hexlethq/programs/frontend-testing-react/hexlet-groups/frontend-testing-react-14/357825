package ru.hh.content.engine.targetings;

import java.util.List;
import javax.inject.Inject;
import org.hibernate.SessionFactory;
import ru.hh.content.engine.model.enums.Targeting;
import ru.hh.content.engine.utils.GenericDao;

public class TargetingDao {
  public final static String TARGETING_ID_SEQ = "targeting_id_seq";

  private final SessionFactory sessionFactory;
  private final GenericDao genericDao;

  @Inject
  public TargetingDao(SessionFactory sessionFactory,
                      GenericDao genericDao) {
    this.sessionFactory = sessionFactory;
    this.genericDao = genericDao;
  }

  public List<Targeting> getTargetings(long targetingId) {
    return sessionFactory.getCurrentSession().createQuery("from Targeting where targetingId = :targetingId", Targeting.class)
        .setParameter("targetingId", targetingId)
        .list();
  }

  public long getNewTargetingId() {
    return genericDao.nextSequenceValue(TARGETING_ID_SEQ);
  }

  public void saveTargetings(List<Targeting> targetings) {
    genericDao.bulkInsert(targetings);
  }
}
