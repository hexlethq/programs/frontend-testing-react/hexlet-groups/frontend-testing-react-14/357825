package ru.hh.content.engine.clients.xmlback;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import ru.hh.content.engine.clients.converter.Json2Converter;
import static ru.hh.content.engine.utils.CommonUtils.createObjectMapper;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.JClientBase;
import ru.hh.jclient.common.Param;
import ru.hh.jclient.common.RequestBuilder;
import ru.hh.jclient.common.ResultWithStatus;
import ru.hh.nab.common.properties.FileSettings;

public class WorkInCompanyClient extends JClientBase {
  private static final String GET_DISPLAY_INFO_URL = "/rs/content_engine/work_in_company/display_info";
  private static final String GET_STUBS_URL = "/rs/content_engine/work_in_company/company_stubs";

  private final ObjectMapper objectMapper;

  @Inject
  public WorkInCompanyClient(FileSettings fileSettings,
                             HttpClientFactory xmlBackHttpClientFactory) {
    super(fileSettings.getString("jclient.hh-xmlback.internalEndpointUrl"), xmlBackHttpClientFactory);
    this.objectMapper = createObjectMapper();
  }

  public CompletableFuture<ResultWithStatus<WorkInCompanyDisplayInfos>> getWorkInCompanyDisplayInfos(List<Integer> employerIds) {
    if (employerIds.isEmpty()) {
      return success(new WorkInCompanyDisplayInfos());
    }

    RequestBuilder request = get(url(GET_DISPLAY_INFO_URL))
        .setQueryParams(employerIds.stream().map(employerId -> Param.of("employerId", employerId)).collect(Collectors.toList()));
    return http.with(request.build())
        .expect(new Json2Converter<>(objectMapper, WorkInCompanyDisplayInfos.class))
        .resultWithStatus();
  }

  public CompletableFuture<ResultWithStatus<Integer[]>> getWorkInCompanyStubs(String areaPath, int limit) {
    RequestBuilder request = get(url(GET_STUBS_URL))
        .addQueryParam("areaPath", areaPath)
        .addQueryParam("limit", String.valueOf(limit));
    return http.with(request.build())
        .expect(new Json2Converter<>(objectMapper, Integer[].class))
        .resultWithStatus();
  }

  private static <T> CompletableFuture<ResultWithStatus<T>> success(T value) {
    return CompletableFuture.completedFuture(new ResultWithStatus<>(value, Response.Status.OK.getStatusCode()));
  }
}
