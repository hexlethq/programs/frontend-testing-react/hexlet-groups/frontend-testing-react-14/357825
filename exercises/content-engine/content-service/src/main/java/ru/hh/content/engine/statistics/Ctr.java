package ru.hh.content.engine.statistics;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import ru.hh.content.engine.utils.CommonUtils;

public class Ctr {
  @JsonSerialize(using = CommonUtils.FloatTwoDigitSerializer.class)
  private Float mobile;
  @JsonSerialize(using = CommonUtils.FloatTwoDigitSerializer.class)
  private Float desktop;
  @JsonSerialize(using = CommonUtils.FloatTwoDigitSerializer.class)
  private Float total;

  public Ctr() {
  }

  public Ctr(Float mobile, Float desktop, Float total) {
    this.mobile = mobile;
    this.desktop = desktop;
    this.total = total;
  }

  public Float getMobile() {
    return mobile;
  }

  public void setMobile(Float mobile) {
    this.mobile = mobile;
  }

  public Float getDesktop() {
    return desktop;
  }

  public void setDesktop(Float desktop) {
    this.desktop = desktop;
  }

  public Float getTotal() {
    return total;
  }

  public void setTotal(Float total) {
    this.total = total;
  }
}
