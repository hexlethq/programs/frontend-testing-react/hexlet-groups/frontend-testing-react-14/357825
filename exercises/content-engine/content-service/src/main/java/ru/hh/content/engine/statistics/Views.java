package ru.hh.content.engine.statistics;

public class Views {
  private Long mobile;
  private Long desktop;
  private Long total;
  private Percent percent;

  public Views() {
  }

  public Views(Long mobile, Long desktop, Long total, Percent percent) {
    this.mobile = mobile;
    this.desktop = desktop;
    this.total = total;
    this.percent = percent;
  }

  public Long getMobile() {
    return mobile;
  }

  public void setMobile(Long mobile) {
    this.mobile = mobile;
  }

  public Long getDesktop() {
    return desktop;
  }

  public void setDesktop(Long desktop) {
    this.desktop = desktop;
  }

  public Long getTotal() {
    return total;
  }

  public void setTotal(Long total) {
    this.total = total;
  }

  public Percent getPercent() {
    return percent;
  }

  public void setPercent(Percent percent) {
    this.percent = percent;
  }
}
