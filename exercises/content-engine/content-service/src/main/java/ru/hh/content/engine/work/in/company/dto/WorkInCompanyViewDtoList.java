package ru.hh.content.engine.work.in.company.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "employersList")
@XmlAccessorType(XmlAccessType.FIELD)
public class WorkInCompanyViewDtoList {
  @XmlElement(name = "employer")
  private List<WorkInCompanyViewDto> workInCompanyViewDtos;

  public WorkInCompanyViewDtoList() {
  }

  public WorkInCompanyViewDtoList(List<WorkInCompanyViewDto> workInCompanyViewDtos) {
    this.workInCompanyViewDtos = workInCompanyViewDtos;
  }

  public List<WorkInCompanyViewDto> getWorkInCompanyViewDtos() {
    return workInCompanyViewDtos;
  }

  public void setWorkInCompanyViewDtos(List<WorkInCompanyViewDto> workInCompanyViewDtos) {
    this.workInCompanyViewDtos = workInCompanyViewDtos;
  }
}
