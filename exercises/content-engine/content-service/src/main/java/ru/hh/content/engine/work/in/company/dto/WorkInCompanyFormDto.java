package ru.hh.content.engine.work.in.company.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import static ru.hh.content.api.CommonApiConstants.COMMON_DATE_FORMAT;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.WorkInCompanyStatus;

public class WorkInCompanyFormDto {
  @NotEmpty
  @NotNull
  private Set<Integer> regionIds;
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateStart;
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateEnd;
  @NotNull
  private String displayName;
  private String comment;
  @NotNull
  private Integer employerId;
  private Integer workInCompanyLogoId;
  private String workInCompanyLogoUrl;
  @NotNull
  @NotEmpty
  private Set<TargetingDeviceType> deviceTypes;
  private WorkInCompanyStatus workInCompanyStatus;
  private boolean isRussia;
  private Set<Integer> excludedRegions;

  public WorkInCompanyFormDto() {
  }

  public WorkInCompanyFormDto(Set<Integer> regionIds,
                              LocalDate dateStart,
                              LocalDate dateEnd,
                              String displayName,
                              Integer employerId,
                              Integer workInCompanyLogoId,
                              Set<TargetingDeviceType> deviceTypes) {
    this.regionIds = regionIds;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.displayName = displayName;
    this.employerId = employerId;
    this.workInCompanyLogoId = workInCompanyLogoId;
    this.deviceTypes = deviceTypes;
  }

  public WorkInCompanyFormDto(Set<Integer> regionIds,
                              LocalDate dateStart,
                              LocalDate dateEnd,
                              String displayName,
                              Integer employerId,
                              Integer workInCompanyLogoId,
                              Set<TargetingDeviceType> deviceTypes,
                              String comment) {
    this.regionIds = regionIds;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.displayName = displayName;
    this.employerId = employerId;
    this.workInCompanyLogoId = workInCompanyLogoId;
    this.deviceTypes = deviceTypes;
    this.comment = comment;
  }

  public WorkInCompanyFormDto(Set<Integer> regionIds,
                              LocalDate dateStart,
                              LocalDate dateEnd,
                              String displayName,
                              Integer employerId,
                              Integer workInCompanyLogoId,
                              Set<TargetingDeviceType> deviceTypes,
                              boolean isRussia,
                              Set<Integer> excludedRegions) {
    this.regionIds = regionIds;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.displayName = displayName;
    this.employerId = employerId;
    this.workInCompanyLogoId = workInCompanyLogoId;
    this.deviceTypes = deviceTypes;
    this.isRussia = isRussia;
    this.excludedRegions = excludedRegions;
  }

  public WorkInCompanyFormDto(Set<Integer> regionIds,
                              Set<Integer> excludedRegionIds,
                              Boolean isRussia,
                              LocalDate dateStart,
                              LocalDate dateEnd,
                              String displayName,
                              Integer employerId,
                              Integer workInCompanyLogoId,
                              Set<TargetingDeviceType> deviceTypes,
                              String comment,
                              WorkInCompanyStatus workInCompanyStatus) {
    this.regionIds = regionIds;
    this.excludedRegions = excludedRegionIds;
    this.isRussia = isRussia;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.displayName = displayName;
    this.comment = comment;
    this.employerId = employerId;
    this.workInCompanyLogoId = workInCompanyLogoId;
    this.deviceTypes = deviceTypes;
    this.workInCompanyStatus = workInCompanyStatus;
  }

  public Set<Integer> getRegionIds() {
    return regionIds;
  }

  public void setRegionIds(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  public LocalDate getDateStart() {
    return dateStart;
  }

  public void setDateStart(LocalDate dateStart) {
    this.dateStart = dateStart;
  }

  public LocalDate getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(LocalDate dateEnd) {
    this.dateEnd = dateEnd;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public Integer getEmployerId() {
    return employerId;
  }

  public void setEmployerId(Integer employerId) {
    this.employerId = employerId;
  }

  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }
  
  public Integer getWorkInCompanyLogoId() {
    return workInCompanyLogoId;
  }
  
  public void setWorkInCompanyLogoId(Integer workInCompanyLogoId) {
    this.workInCompanyLogoId = workInCompanyLogoId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public WorkInCompanyStatus getWorkInCompanyStatus() {
    return workInCompanyStatus;
  }

  public void setWorkInCompanyStatus(WorkInCompanyStatus workInCompanyStatus) {
    this.workInCompanyStatus = workInCompanyStatus;
  }

  public boolean getIsRussia() {
    return isRussia;
  }

  public void setIsRussia(boolean isRussia) {
    this.isRussia = isRussia;
  }

  public Set<Integer> getExcludedRegions() {
    return excludedRegions;
  }

  public void setExcludedRegions(Set<Integer> excludedRegions) {
    this.excludedRegions = excludedRegions;
  }

  public String getWorkInCompanyLogoUrl() {
    return workInCompanyLogoUrl;
  }

  public void setWorkInCompanyLogoUrl(String workInCompanyLogoUrl) {
    this.workInCompanyLogoUrl = workInCompanyLogoUrl;
  }
}
