package ru.hh.content.engine.limits;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.limits.ContentLimit;
import ru.hh.content.engine.utils.GenericDao;

public class ContentLimitDao {
  public static final String CONTENT_LIMIT_ID_SEQ_NAME = "content_limit_id_seq";

  private final SessionFactory sessionFactory;
  private final GenericDao genericDao;

  public ContentLimitDao(SessionFactory sessionFactory, GenericDao genericDao) {
    this.sessionFactory = sessionFactory;
    this.genericDao = genericDao;
  }

  @Transactional(readOnly = true)
  public List<ContentLimit> getContentLimits(long contentLimitId) {
    return sessionFactory.getCurrentSession().createQuery("from ContentLimit where contentLimitId = :contentLimitId", ContentLimit.class)
        .setParameter("contentLimitId", contentLimitId)
        .list();
  }

  @Transactional
  public void saveContentLimits(List<ContentLimit> contentLimits) {
    genericDao.bulkInsert(contentLimits);
  }

  @Transactional
  public void getNextContentLimitId() {
    genericDao.nextSequenceValue(CONTENT_LIMIT_ID_SEQ_NAME);
  }
}
