package ru.hh.content.engine.work.in.company;

import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.content.engine.utils.cache.CacheService;
import ru.hh.content.engine.work.in.company.dao.WorkInCompanyDao;
import ru.hh.content.engine.work.in.company.dao.WorkInCompanyReservationDao;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyViewDto;
import ru.hh.content.engine.work.in.company.resource.WorkInCompanyEmployerResource;
import ru.hh.content.engine.work.in.company.resource.WorkInCompanyRegionsResource;
import ru.hh.content.engine.work.in.company.resource.WorkInCompanyResource;
import ru.hh.content.engine.work.in.company.resource.WorkInCompanyUserResource;
import ru.hh.content.engine.work.in.company.service.WorkInCompanyRegionsService;
import ru.hh.content.engine.work.in.company.service.WorkInCompanyService;
import ru.hh.content.engine.work.in.company.service.WorkInCompanyViewService;
import ru.hh.settings.SettingsClient;

@Configuration
@Import({
    WorkInCompanyService.class,
    WorkInCompanyDao.class,

    WorkInCompanyResource.class,
    WorkInCompanyUserResource.class,
    WorkInCompanyRegionsResource.class,

    WorkInCompanyViewService.class,
    WorkInCompanyRegionsService.class,

    WorkInCompanyReservationDao.class,

    WorkInCompanyEmployerResource.class,
})
public class WorkInCompanyConfig {
  public static final String WORK_IN_COMPANY_CACHE_TTL_SETTING_NAME = "work_in_company.cache.ttl.ms";

  @Bean
  public CacheService<List<WorkInCompanyViewDto>> workInCompanyViewServiceCacheService(SettingsClient settingsClient) {
    return new CacheService<>(settingsClient, WORK_IN_COMPANY_CACHE_TTL_SETTING_NAME);
  }
}
