package ru.hh.content.engine.limits;

import static java.lang.String.valueOf;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import static java.util.stream.Collectors.toMap;
import ru.hh.content.engine.model.limits.ContentLimit;
import ru.hh.content.engine.model.enums.ContentLimitType;
import ru.hh.memcached.HHMemcachedClient;
import ru.hh.settings.SettingsClient;

public class ContentLimitService {
  static final String MEMCACHED_REGION = "content_engine.content_limit_is_reached";
  public static final String LIMITS_CACHE_EXPIRE_TIME_SETTING_NAME = "limits.cache_expire_time_ms";

  private final Map<ContentLimitType, ContentLimitCheckerService> limitTypeToContentLimitCheckerService;
  private final ContentLimitDao contentLimitDao;
  private final HHMemcachedClient memcachedClient;
  private final SettingsClient settingsClient;

  public ContentLimitService(List<ContentLimitCheckerService> contentLimitCheckerServices,
                             ContentLimitDao contentLimitDao,
                             HHMemcachedClient memcachedClient,
                             SettingsClient settingsClient) {
    this.limitTypeToContentLimitCheckerService = contentLimitCheckerServices
        .stream().collect(toMap(ContentLimitCheckerService::getContentLimitType, Function.identity()));
    this.contentLimitDao = contentLimitDao;
    this.memcachedClient = memcachedClient;
    this.settingsClient = settingsClient;
  }

  public boolean isReached(long contentLimitId) {
    List<ContentLimit> contentLimits = contentLimitDao.getContentLimits(contentLimitId);

    Object isReachedCache = memcachedClient.get(MEMCACHED_REGION, valueOf(contentLimitId));

    if (isReachedCache != null) {
      return (boolean) isReachedCache;
    }

    boolean isReached = contentLimits.stream()
        .anyMatch(
            contentLimit -> limitTypeToContentLimitCheckerService.get(contentLimit.getContentLimitType()).isReached(contentLimitId)
        );
    putCapacityIntoCache(contentLimitId, isReached);
    return isReached;
  }

  private void putCapacityIntoCache(long contentLimitId, boolean isCapacityReached) {
    memcachedClient.set(
        MEMCACHED_REGION,
        valueOf(contentLimitId),
        settingsClient.getInteger(LIMITS_CACHE_EXPIRE_TIME_SETTING_NAME, 1),
        isCapacityReached
    );
  }
}
