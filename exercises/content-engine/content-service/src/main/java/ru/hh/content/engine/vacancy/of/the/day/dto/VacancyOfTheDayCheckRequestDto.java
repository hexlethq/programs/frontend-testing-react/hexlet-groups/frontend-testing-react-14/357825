package ru.hh.content.engine.vacancy.of.the.day.dto;

import java.time.LocalDate;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;

public class VacancyOfTheDayCheckRequestDto {
  @NotNull
  private LocalDate dateFrom;
  @NotNull
  private LocalDate dateTo;
  private Set<Integer> regionIds;
  private PacketType packet;
  @NotNull
  @NotEmpty
  private Set<TargetingDeviceType> deviceTypes;

  public VacancyOfTheDayCheckRequestDto() {
  }

  public VacancyOfTheDayCheckRequestDto(@NotNull LocalDate dateFrom,
                                        @NotNull LocalDate dateTo,
                                        Set<Integer> regionIds,
                                        PacketType packet,
                                        @NotNull @NotEmpty Set<TargetingDeviceType> deviceTypes) {
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.regionIds = regionIds;
    this.packet = packet;
    this.deviceTypes = deviceTypes;
  }

  public LocalDate getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(LocalDate dateFrom) {
    this.dateFrom = dateFrom;
  }

  public LocalDate getDateTo() {
    return dateTo;
  }

  public void setDateTo(LocalDate dateTo) {
    this.dateTo = dateTo;
  }

  public Set<Integer> getRegionIds() {
    return regionIds;
  }

  public void setRegionIds(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }

  public PacketType getPacket() {
    return packet;
  }

  public void setPacket(PacketType packet) {
    this.packet = packet;
  }
}
