package ru.hh.content.engine.vacancy.of.the.day.resource;

import java.time.LocalDate;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.HttpHeaders.CONTENT_DISPOSITION;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.apache.poi.ss.usermodel.Workbook;
import static ru.hh.content.api.dto.ContentType.VACANCY_OF_THE_DAY;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.AGGREGATED_STATISTIC;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATISTIC_BY_DATE;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATISTIC_BY_DATE_XLS;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATISTIC_BY_REGION;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATISTIC_BY_REGION_XLS;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATISTIC_ROOT;
import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.services.VacancyService;
import ru.hh.content.engine.statistics.CHStatisticsDao;
import ru.hh.content.engine.statistics.StatisticXlsService;
import ru.hh.content.engine.statistics.SumStatisticByDate;
import ru.hh.content.engine.statistics.SumStatisticByRegion;
import ru.hh.content.engine.utils.ResourceUtils;
import static ru.hh.content.engine.utils.ResourceUtils.assertDateFromIsBeforeDateTo;
import static ru.hh.content.engine.utils.ResourceUtils.parseLocalDate;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayDao;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyDataDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayStatisticByDateResponseDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayStatisticByRegionResponseDto;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayStatisticService;
import ru.hh.content.engine.work.in.company.dto.EmployerInfo;

@Path(STATISTIC_ROOT)
public class VacancyOfTheDayStatisticResource {
  public static final String XLS_MEDIA_TYPE = "application/vnd.ms-excel";
  public static final String XLSX_EXT = "xlsx";
  private final CHStatisticsDao statisticsDao;
  private final StatisticXlsService statisticXlsService;
  private final VacancyOfTheDayDao vacancyOfTheDayDao;
  private final VacancyService vacancyService;
  private final VacancyOfTheDayStatisticService vacancyOfTheDayStatisticService;

  public VacancyOfTheDayStatisticResource(CHStatisticsDao statisticsDao,
                                          StatisticXlsService statisticXlsService,
                                          VacancyOfTheDayDao vacancyOfTheDayDao,
                                          VacancyService vacancyService,
                                          VacancyOfTheDayStatisticService vacancyOfTheDayStatisticService) {
    this.statisticsDao = statisticsDao;
    this.statisticXlsService = statisticXlsService;
    this.vacancyOfTheDayDao = vacancyOfTheDayDao;
    this.vacancyService = vacancyService;
    this.vacancyOfTheDayStatisticService = vacancyOfTheDayStatisticService;
  }

  @GET
  @Path(STATISTIC_BY_DATE)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByDate(@QueryParam("fromDate") String dateFrom,
                                  @QueryParam("toDate") String dateTo,
                                  @NotNull @QueryParam("vacancyOfTheDayId") Integer vacancyOfTheDayId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);
    SumStatisticByDate statisticsByDate = statisticsDao.getStatisticsByDate(
        parsedDateFrom, parsedDateTo, vacancyOfTheDayId, VACANCY_OF_THE_DAY
    );
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayId);
    VacancyDto vacancyInfo = vacancyService.getVacancy(vacancyOfTheDay.getVacancyId());
    EmployerInfo employerInfo = new EmployerInfo(vacancyOfTheDay.getEmployerId(), vacancyOfTheDay.getEmployerName());
    statisticsByDate.setEmployerInfo(employerInfo);
    return Response.ok(new VacancyOfTheDayStatisticByDateResponseDto(statisticsByDate, new VacancyDataDto(vacancyInfo))).build();
  }

  @GET
  @Path(STATISTIC_BY_DATE_XLS)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByDateXls(@QueryParam("fromDate") String dateFrom,
                                     @QueryParam("toDate") String dateTo,
                                     @NotNull @QueryParam("vacancyOfTheDayId") Integer vacancyOfTheDayId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);

    Workbook workbook = statisticXlsService.getStatisticByDateXls(parsedDateFrom, parsedDateTo, vacancyOfTheDayId, VACANCY_OF_THE_DAY);
    StreamingOutput stream = workbook::write;
    VacancyDto vacancyInfo = vacancyService.getVacancy(vacancyOfTheDayDao.get(vacancyOfTheDayId).getVacancyId());
    String fileName = String.format("votd_id%s_%s_%s_%s_by_date", vacancyOfTheDayId, vacancyInfo.getName(), dateFrom, dateTo);
    return Response.ok(stream, XLS_MEDIA_TYPE)
        .header(CONTENT_DISPOSITION, String.format("inline; filename=\"%s.%s\"", fileName, XLSX_EXT))
        .build();
  }

  @GET
  @Path(STATISTIC_BY_REGION_XLS)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByRegionXls(@QueryParam("fromDate") String dateFrom,
                                       @QueryParam("toDate") String dateTo,
                                       @NotNull @QueryParam("vacancyOfTheDayId") Integer vacancyOfTheDayId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);

    Workbook workbook = statisticXlsService.getStatisticByRegionXls(parsedDateFrom, parsedDateTo, vacancyOfTheDayId, VACANCY_OF_THE_DAY);
    StreamingOutput stream = workbook::write;
    VacancyDto vacancyInfo = vacancyService.getVacancy(vacancyOfTheDayDao.get(vacancyOfTheDayId).getVacancyId());
    String fileName = String.format("votd_id%s_%s_%s_%s_by_region", vacancyOfTheDayId, vacancyInfo.getName(), dateFrom, dateTo);
    return Response.ok(stream, XLS_MEDIA_TYPE)
        .header(CONTENT_DISPOSITION, String.format("inline; filename=\"%s.%s\"", fileName, XLSX_EXT))
        .build();
  }

  @GET
  @Path(STATISTIC_BY_REGION)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByRegion(@QueryParam("fromDate") String dateFrom,
                                    @QueryParam("toDate") String dateTo,
                                    @NotNull @QueryParam("vacancyOfTheDayId") Integer vacancyOfTheDayId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);
    SumStatisticByRegion statisticsByRegion = statisticsDao.getStatisticsByRegion(
        parsedDateFrom, parsedDateTo, vacancyOfTheDayId, VACANCY_OF_THE_DAY
    );
    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayId);
    VacancyDto vacancyInfo = vacancyService.getVacancy(vacancyOfTheDay.getVacancyId());
    EmployerInfo employerInfo = new EmployerInfo(vacancyOfTheDay.getEmployerId(), vacancyOfTheDay.getEmployerName());
    statisticsByRegion.setEmployerInfo(employerInfo);

    return Response.ok(new VacancyOfTheDayStatisticByRegionResponseDto(statisticsByRegion, new VacancyDataDto(vacancyInfo))).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response commonStatistic(@QueryParam("vacancyOfTheDayId") Long vacancyOfTheDayId,
                                  @QueryParam("vacancyId") Integer vacancyId,
                                  @QueryParam("vacancyName") String vacancyName,
                                  @QueryParam("employerId") Integer employerId,
                                  @QueryParam("employerName") String employerName,
                                  @QueryParam("creatorId") Integer creatorId,
                                  @QueryParam("creatorName") String creatorName,
                                  @QueryParam("crmUrl") String crmUrl,
                                  @QueryParam("dateFrom") String dateFrom,
                                  @QueryParam("dateTo") String dateTo,
                                  @QueryParam("regionIds") Set<Integer> regionIds,
                                  @QueryParam("exactlyRegionFilter") boolean exactlyRegionFilter,
                                  @QueryParam("deviceTypes") Set<TargetingDeviceType> deviceTypes,
                                  @QueryParam("campaignStatuses") Set<VacancyOfTheDayStatus> campaignStatuses,
                                  @QueryParam("paymentStatuses") Set<VacancyOfTheDayPayedStatus> paymentStatuses,
                                  @QueryParam("campaignTypes") Set<VacancyOfTheDayCampaignType> campaignTypes,
                                  @QueryParam("packets") Set<PacketType> packetTypes
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);

    return Response.ok(vacancyOfTheDayStatisticService.getCommonStatistic(
        vacancyId,
        vacancyOfTheDayId,
        vacancyName,
        employerId,
        employerName,
        creatorId,
        creatorName,
        parseLocalDate(dateFrom),
        parseLocalDate(dateTo),
        regionIds,
        exactlyRegionFilter,
        deviceTypes,
        campaignStatuses,
        campaignTypes,
        packetTypes
    )).build();
  }

  @GET
  @Path(AGGREGATED_STATISTIC)
  @Produces(MediaType.APPLICATION_JSON)
  public Response commonAggregatedStatistic(
      @NotNull @QueryParam("dateFrom") String dateFrom,
      @NotNull @QueryParam("dateTo") String dateTo,
      @QueryParam("regionIds") Set<Integer> regionIds,
      @NotNull @NotEmpty @QueryParam("deviceTypes") Set<TargetingDeviceType> deviceTypes,
      @NotNull @QueryParam("packet") PacketType packetType
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);

    return Response.ok(
        vacancyOfTheDayStatisticService.getCommonAggregatedStatistic(parsedDateFrom, parsedDateTo, packetType, regionIds, deviceTypes)
    ).build();
  }
}
