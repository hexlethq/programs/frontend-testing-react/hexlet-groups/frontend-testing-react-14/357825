package ru.hh.content.engine.work.in.company.dto;

import java.time.LocalDate;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import ru.hh.content.engine.model.enums.TargetingDeviceType;

public class WorkInCompanyCheckRequestDto {
  @NotNull
  private LocalDate dateFrom;
  @NotNull
  private LocalDate dateTo;
  @NotNull
  @NotEmpty
  private Set<Integer> regionIds;
  @NotNull
  @NotEmpty
  private Set<TargetingDeviceType> deviceTypes;

  public WorkInCompanyCheckRequestDto() {
  }

  public WorkInCompanyCheckRequestDto(LocalDate dateFrom, LocalDate dateTo, Set<Integer> regionIds, Set<TargetingDeviceType> deviceTypes) {
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.regionIds = regionIds;
    this.deviceTypes = deviceTypes;
  }

  public LocalDate getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(LocalDate dateFrom) {
    this.dateFrom = dateFrom;
  }

  public LocalDate getDateTo() {
    return dateTo;
  }

  public void setDateTo(LocalDate dateTo) {
    this.dateTo = dateTo;
  }

  public Set<Integer> getRegionIds() {
    return regionIds;
  }

  public void setRegionIds(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }
}
