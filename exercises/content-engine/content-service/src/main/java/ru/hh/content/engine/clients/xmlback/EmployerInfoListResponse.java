package ru.hh.content.engine.clients.xmlback;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Collection;

public class EmployerInfoListResponse {
  @JsonProperty("item")
  public Collection<EmployerInfoResponse> items;

  public EmployerInfoListResponse() {
  }

  public Collection<EmployerInfoResponse> getItems() {
    return items;
  }

  public void setItems(Collection<EmployerInfoResponse> items) {
    this.items = items;
  }
}
