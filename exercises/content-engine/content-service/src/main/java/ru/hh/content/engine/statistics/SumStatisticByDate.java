package ru.hh.content.engine.statistics;

import java.util.List;
import ru.hh.content.engine.work.in.company.dto.EmployerInfo;

public class SumStatisticByDate {
  private List<StatisticsByDate> statisticsByDates;
  private StatisticRow totalStatistic;
  private EmployerInfo employerInfo;

  public SumStatisticByDate() {
  }

  public SumStatisticByDate(List<StatisticsByDate> statisticsByDates, StatisticRow totalStatistic) {
    this.statisticsByDates = statisticsByDates;
    this.totalStatistic = totalStatistic;
  }

  public List<StatisticsByDate> getStatisticsByDates() {
    return statisticsByDates;
  }

  public void setStatisticsByDates(List<StatisticsByDate> statisticsByDates) {
    this.statisticsByDates = statisticsByDates;
  }

  public StatisticRow getTotalStatistic() {
    return totalStatistic;
  }

  public void setTotalStatistic(StatisticRow totalStatistic) {
    this.totalStatistic = totalStatistic;
  }

  public EmployerInfo getEmployerInfo() {
    return employerInfo;
  }

  public void setEmployerInfo(EmployerInfo employerInfo) {
    this.employerInfo = employerInfo;
  }
}
