package ru.hh.content.engine.work.in.company.dto;

public class WorkInCompaniesListEmployerInfo {
  private Integer employerId;
  private String employerName;
  private String displayName;

  public WorkInCompaniesListEmployerInfo() {
  }

  public WorkInCompaniesListEmployerInfo(Integer employerId, String displayName) {
    this.employerId = employerId;
    this.displayName = displayName;
  }

  public Integer getEmployerId() {
    return employerId;
  }

  public void setEmployerId(Integer employerId) {
    this.employerId = employerId;
  }

  public String getEmployerName() {
    return employerName;
  }

  public void setEmployerName(String employerName) {
    this.employerName = employerName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }
}
