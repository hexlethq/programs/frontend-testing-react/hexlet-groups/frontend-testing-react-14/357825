package ru.hh.content.engine.services;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import static java.util.stream.Collectors.toList;
import javax.ws.rs.core.Response;
import ru.hh.content.engine.clients.xmlback.EmployerClient;
import ru.hh.content.engine.clients.xmlback.EmployerInfoListResponse;
import ru.hh.content.engine.clients.xmlback.EmployerInfoResponse;
import ru.hh.content.engine.utils.ExceptionUtils;
import ru.hh.content.engine.work.in.company.dto.EmployerInfo;
import ru.hh.errors.common.Errors;
import ru.hh.jclient.common.ResultWithStatus;

public class EmployerService {
  private final EmployerClient employerClient;

  public EmployerService(EmployerClient employerClient) {
    this.employerClient = employerClient;
  }

  public EmployerInfo getEmployerInfo(int employerId) {
    ResultWithStatus<EmployerInfoResponse> employerInfoResponseResultWithStatus =
        ExceptionUtils.getWithExceptionsToRuntimeWrapper(() -> employerClient.getEmployerInfo(employerId).get());

    if (employerInfoResponseResultWithStatus.getStatusCode() == Response.Status.NOT_FOUND.getStatusCode()) {
      throw new Errors(Response.Status.NOT_FOUND, "employer_not_found", null).toWebApplicationException();
    }

    if (!employerInfoResponseResultWithStatus.isSuccess()) {
      throw new Errors(Response.Status.BAD_GATEWAY, "xmlback_error", null).toWebApplicationException();
    }

    Optional<EmployerInfoResponse> employerInfoResponseOptional = employerInfoResponseResultWithStatus.get();

    if (employerInfoResponseOptional.isEmpty()) {
      throw new Errors(Response.Status.BAD_GATEWAY, "xmlback_error", null).toWebApplicationException();
    }

    EmployerInfoResponse employerInfoResponse = employerInfoResponseOptional.get();

    return new EmployerInfo(employerInfoResponse.getId(), employerInfoResponse.getName());
  }

  public List<EmployerInfo> getEmployerInfoList(Collection<Integer> employerIds) {
    if (employerIds == null || employerIds.isEmpty()) {
      return Collections.emptyList();
    }
    ResultWithStatus<EmployerInfoListResponse> employerInfoListResponseResultWithStatus =
        ExceptionUtils.getWithExceptionsToRuntimeWrapper(() -> employerClient.getEmployerListByIds(employerIds).get());

    if (employerInfoListResponseResultWithStatus.getStatusCode() == Response.Status.NOT_FOUND.getStatusCode()) {
      throw new Errors(Response.Status.NOT_FOUND, "employers_not_found", null).toWebApplicationException();
    }

    if (!employerInfoListResponseResultWithStatus.isSuccess()) {
      throw new Errors(Response.Status.BAD_GATEWAY, "xmlback_error", null).toWebApplicationException();
    }

    Optional<EmployerInfoListResponse> employerInfoListResponseOptional = employerInfoListResponseResultWithStatus.get();

    if (employerInfoListResponseOptional.isEmpty()) {
      throw new Errors(Response.Status.BAD_GATEWAY, "xmlback_error", null).toWebApplicationException();
    }

    EmployerInfoListResponse employerInfoListResponse = employerInfoListResponseOptional.get();

    Collection<EmployerInfoResponse> items = employerInfoListResponse.getItems();
    if (items == null || items.isEmpty()) {
      return Collections.emptyList();
    }
    return items.stream()
        .map(employerInfoResponse -> new EmployerInfo(employerInfoResponse.getId(), employerInfoResponse.getName()))
        .collect(toList());
  }
}
