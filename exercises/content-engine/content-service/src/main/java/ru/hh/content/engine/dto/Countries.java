package ru.hh.content.engine.dto;

public enum Countries {
  RUSSIA(113),
  UKRAINE(5),
  BELARUS(16),
  ;
  private final int countryId;

  Countries(int countryId) {
    this.countryId = countryId;
  }

  public int getCountryId() {
    return countryId;
  }
}
