package ru.hh.content.engine.clients.xmlback;

import java.util.Map;

public class WorkInCompanyDisplayInfos {
  private Map<Integer, WorkInCompanyDisplayInfo> workInCompanyDisplayInfoMap;

  public WorkInCompanyDisplayInfos() {
  }

  public WorkInCompanyDisplayInfos(Map<Integer, WorkInCompanyDisplayInfo> workInCompanyDisplayInfoMap) {
    this.workInCompanyDisplayInfoMap = workInCompanyDisplayInfoMap;
  }

  public Map<Integer, WorkInCompanyDisplayInfo> getWorkInCompanyDisplayInfoMap() {
    return workInCompanyDisplayInfoMap;
  }

  public void setWorkInCompanyDisplayInfoMap(Map<Integer, WorkInCompanyDisplayInfo> workInCompanyDisplayInfoMap) {
    this.workInCompanyDisplayInfoMap = workInCompanyDisplayInfoMap;
  }
}
