package ru.hh.content.engine.vacancy.of.the.day.resource;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.content.api.dto.ContentEvent;
import ru.hh.content.api.dto.ContentType;
import ru.hh.content.api.dto.EventType;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.CLICK_RESOURCE;
import ru.hh.content.engine.area.region.mapping.AreaRegionMappingDao;
import ru.hh.content.engine.kafka.banner.KafkaBannerPublisher;
import ru.hh.content.engine.services.HHSessionContext;
import static ru.hh.content.engine.utils.CommonUtils.getPlatformBySession;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayClickDto;

@Path(CLICK_RESOURCE)
public class VacancyOfTheDayClickResource {
  public static final Logger LOGGER = LoggerFactory.getLogger(VacancyOfTheDayClickResource.class);

  private final KafkaBannerPublisher kafkaBannerPublisher;
  private final HHSessionContext sessionContext;
  private final AreaRegionMappingDao areaRegionMappingDao;

  public VacancyOfTheDayClickResource(KafkaBannerPublisher kafkaBannerPublisher,
                                      HHSessionContext sessionContext,
                                      AreaRegionMappingDao areaRegionMappingDao) {
    this.kafkaBannerPublisher = kafkaBannerPublisher;
    this.sessionContext = sessionContext;
    this.areaRegionMappingDao = areaRegionMappingDao;
  }

  @GET
  public Response click(@BeanParam VacancyOfTheDayClickDto clickRequestDto) {
    kafkaBannerPublisher.sendKafkaMessage("content_events", fillContentClickEvent(clickRequestDto));
    String vacancyPath = String.format("%s/vacancy/%s", clickRequestDto.getHost(), clickRequestDto.getVacancyId());
  
    Map<String, String> params = new HashMap<>();
    params.put("from", clickRequestDto.getFrom());
    params.put("utm_source", clickRequestDto.getUtmSource());
    params.put("utm_campaign", clickRequestDto.getUtmCampaign());
    params.put("utm_local_campaign", clickRequestDto.getUtmLocalCampaign());
    params.put("utm_content", clickRequestDto.getUtmContent());
    params.put("utm_medium", clickRequestDto.getUtmMedium());
  
    StringBuilder clickParams = new StringBuilder();
    for (String param : params.keySet()) {
      if (params.get(param) != null && !params.get(param).isEmpty()) {
        if (clickParams.toString().isBlank()) {
          clickParams = new StringBuilder(String.format("?%s=%s", param, params.get(param)));
        } else {
          clickParams.append(String.format("&%s=%s", param, params.get(param)));
        }
      }
    }
    
    return Response.temporaryRedirect(
        URI.create(vacancyPath + clickParams.toString())
    ).build();
  }

  private ContentEvent fillContentClickEvent(VacancyOfTheDayClickDto requestDto) {
    Optional<Integer> regionByArea = areaRegionMappingDao.getRegionByArea(requestDto.getDomainAreaId());
    if (regionByArea.isEmpty()) {
      LOGGER.error("User with hhuid {} has null region", requestDto.getHhuid());
    }

    return ContentEvent.newBuilder()
        .contentType(ContentType.VACANCY_OF_THE_DAY)
        .eventType(EventType.CLICK)
        .contentId(requestDto.getContentId())
        .contentPlaceId(requestDto.getPlaceId())
        .hhuid(sessionContext.getSession().uid)
        .hhid(Optional.ofNullable(requestDto.getHhid()).orElse(0L))
        .domainAreaId(regionByArea.orElse(0))
        .remoteHostIp(requestDto.getRemoteHostIp())
        .platform(getPlatformBySession(sessionContext))
        .requestPath(requestDto.getUriInfo().getRequestUri().toString())
        .userAgent(requestDto.getUserAgent())
        .build();
  }
}
