package ru.hh.content.engine.vacancy.of.the.day.resource;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static org.apache.commons.io.FileUtils.ONE_MB;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.web.bind.annotation.RequestBody;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.CHECK_BUSY;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.DELETE;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.EDIT;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.PAYMENT_STATUS;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.SCREENSHOT;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATUS_ACTIVE;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATUS_APPROVE;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATUS_PAUSE;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATUS_REJECT;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.STATUS_TO_MODERATION;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.VACANCY;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.VACANCY_OF_THE_DAY_LIST;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.VACANCY_OF_THE_DAY_ROOT;
import ru.hh.content.engine.model.enums.ImageType;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.VacancyService;
import ru.hh.content.engine.utils.ContentImage;
import static ru.hh.content.engine.utils.ResourceUtils.parseLocalDate;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayCheckRequestDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayFormDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyScreenshotDto;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayActionsService;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayScreenshotService;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayService;
import ru.hh.errors.common.Errors;

@Path(VACANCY_OF_THE_DAY_ROOT)
public class VacancyOfTheDayResource {
  public static final int MAX_SCREEN_FILE_SIZE = Math.toIntExact(100 * ONE_MB) + 1;

  private final SecurityContext securityContext;
  private final VacancyOfTheDayService vacancyOfTheDayService;
  private final VacancyOfTheDayActionsService vacancyOfTheDayActionsService;
  private final VacancyOfTheDayScreenshotService vacancyOfTheDayScreenshotService;
  private final VacancyService vacancyService;

  public VacancyOfTheDayResource(SecurityContext securityContext,
                                 VacancyOfTheDayService vacancyOfTheDayService,
                                 VacancyOfTheDayActionsService vacancyOfTheDayActionsService,
                                 VacancyOfTheDayScreenshotService vacancyOfTheDayScreenshotService,
                                 VacancyService vacancyService) {
    this.securityContext = securityContext;
    this.vacancyOfTheDayService = vacancyOfTheDayService;
    this.vacancyOfTheDayActionsService = vacancyOfTheDayActionsService;
    this.vacancyOfTheDayScreenshotService = vacancyOfTheDayScreenshotService;
    this.vacancyService = vacancyService;
  }

  @POST
  public Response create(@RequestBody @Valid VacancyOfTheDayFormDto vacancyOfTheDayFormDto) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(vacancyOfTheDayService.createVacancyOfTheDay(vacancyOfTheDayFormDto)).build();
  }

  @PUT
  @Path(EDIT)
  public Response edit(@RequestBody @Valid VacancyOfTheDayFormDto vacancyOfTheDayFormDto,
                       @PathParam(value = "vacancyOfTheDayId") Long vacancyOfTheDayId) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayService.editVacancyOfTheDay(vacancyOfTheDayFormDto, vacancyOfTheDayId);
    return Response.ok().build();
  }

  @GET
  @Path(VACANCY)
  @Produces(MediaType.APPLICATION_JSON)
  public Response getVacancy(@QueryParam("vacancyId") int vacancyId) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(vacancyService.getFrontVacancy(vacancyId)).build();
  }

  @PUT
  @Path(SCREENSHOT)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public Response uploadScreenshot(@QueryParam("vacancyOfTheDayId") long vacancyOfTheDayId,
                                   @FormDataParam("file") InputStream fileInputStream,
                                   @FormDataParam("file") FormDataContentDisposition fileMetaData) {
    securityContext.checkCurrentUserIsBackOffice();
    try {
      byte[] bytes = IOUtils.toByteArray(new BoundedInputStream(fileInputStream, MAX_SCREEN_FILE_SIZE));
      VacancyScreenshotDto vacancyScreenShotDto = vacancyOfTheDayScreenshotService.createVacancyScreenshot(
          vacancyOfTheDayId, ImageType.SCREEN, ContentImage.fromBytes(bytes)
      );
      return Response.ok(vacancyScreenShotDto).build();
    } catch (IOException e) {
      throw new Errors(500, "wrong_image", "Something wrong with image").toWebApplicationException();
    }
  }

  @GET
  @Path(SCREENSHOT)
  @Produces(MediaType.APPLICATION_JSON)
  public Response getScreenshots(@QueryParam("vacancyOfTheDayId") long vacancyOfTheDayId) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(vacancyOfTheDayScreenshotService.getVacancyScreenshots(vacancyOfTheDayId)).build();
  }

  @DELETE
  @Path(SCREENSHOT)
  @Produces(MediaType.APPLICATION_JSON)
  public Response deleteScreenshot(@QueryParam("screenId") int screenId) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayScreenshotService.deleteScreenshot(screenId);
    return Response.ok().build();
  }

  @PUT
  @Path(PAYMENT_STATUS)
  @Produces(MediaType.APPLICATION_JSON)
  public Response setPaymentStatus(@PathParam("vacancyOfTheDayId") long vacancyOfTheDayId,
                                   @QueryParam("status") VacancyOfTheDayPayedStatus status) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayActionsService.setPaymentStatus(vacancyOfTheDayId, status);
    return Response.ok().build();
  }

  @PUT
  @Path(STATUS_APPROVE)
  @Produces(MediaType.APPLICATION_JSON)
  public Response setStatusApprove(@QueryParam("vacancyOfTheDayId") List<Long> vacancyOfTheDayIdList) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(vacancyOfTheDayActionsService.setStatusApprove(vacancyOfTheDayIdList)).build();
  }

  @PUT
  @Path(STATUS_TO_MODERATION)
  @Produces(MediaType.APPLICATION_JSON)
  public Response setStatusToModeration(@QueryParam("vacancyOfTheDayId") List<Long> vacancyOfTheDayIdList) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayActionsService.setStatusToModeration(vacancyOfTheDayIdList);
    return Response.ok().build();
  }

  @PUT
  @Path(STATUS_REJECT)
  @Produces(MediaType.APPLICATION_JSON)
  public Response setStatusToReject(@QueryParam("vacancyOfTheDayId") List<Long> vacancyOfTheDayIdList,
                                    @QueryParam("reason") String reason) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayActionsService.setStatusNotApproved(vacancyOfTheDayIdList, reason);
    return Response.ok().build();
  }

  @PUT
  @Path(STATUS_ACTIVE)
  @Produces(MediaType.APPLICATION_JSON)
  public Response activateVacancyOfTheDay(@QueryParam("vacancyOfTheDayId") Long vacancyOfTheDayId) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayActionsService.setStatusActive(vacancyOfTheDayId);
    return Response.ok().build();
  }

  @PUT
  @Path(STATUS_PAUSE)
  @Produces(MediaType.APPLICATION_JSON)
  public Response pauseVacancyOfTheDay(@QueryParam("vacancyOfTheDayId") Long vacancyOfTheDayId) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayActionsService.setStatusPause(vacancyOfTheDayId);
    return Response.ok().build();
  }

  @DELETE
  @Path(DELETE)
  @Produces(MediaType.APPLICATION_JSON)
  public Response deleteReservation(@PathParam("vacancyOfTheDayId") long vacancyOfTheDayId) {
    securityContext.checkCurrentUserIsBackOffice();
    vacancyOfTheDayActionsService.delete(vacancyOfTheDayId);
    return Response.ok().build();
  }

  @PUT
  @Path(CHECK_BUSY)
  @Produces(MediaType.APPLICATION_JSON)
  public Response checkBusy(@RequestBody @Valid VacancyOfTheDayCheckRequestDto vacancyOfTheDayCheckRequestDto) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(vacancyOfTheDayService.checkBusy(vacancyOfTheDayCheckRequestDto)).build();
  }

  @GET
  @Path(VACANCY_OF_THE_DAY_LIST)
  @Produces(MediaType.APPLICATION_JSON)
  public Response getVacancyOfTheDayList(@QueryParam("vacancyOfTheDayId") Long vacancyOfTheDayId,
                                         @QueryParam("vacancyId") Integer vacancyId,
                                         @QueryParam("vacancyName") String vacancyName,
                                         @QueryParam("employerId") Integer employerId,
                                         @QueryParam("employerName") String employerName,
                                         @QueryParam("creatorId") Integer creatorId,
                                         @QueryParam("creatorName") String creatorName,
                                         @QueryParam("crmUrl") String crmUrl,
                                         @QueryParam("dateFrom") String dateFrom,
                                         @QueryParam("dateTo") String dateTo,
                                         @QueryParam("regionIds") Set<Integer> regionIds,
                                         @QueryParam("exactlyRegionFilter") boolean exactlyRegionFilter,
                                         @QueryParam("deviceTypes") Set<TargetingDeviceType> deviceTypes,
                                         @QueryParam("campaignStatuses") Set<VacancyOfTheDayStatus> campaignStatuses,
                                         @QueryParam("paymentStatuses") Set<VacancyOfTheDayPayedStatus> paymentStatuses,
                                         @QueryParam("campaignTypes") Set<VacancyOfTheDayCampaignType> campaignTypes,
                                         @QueryParam("packets") Set<PacketType> packetTypes
  ) {

    securityContext.checkCurrentUserIsBackOffice();
    LocalDate parsedDateFrom = null;
    LocalDate parsedDateTo = null;
    if (dateFrom != null) {
      parsedDateFrom = parseLocalDate(dateFrom);
    }
    if (dateTo != null) {
      parsedDateTo = parseLocalDate(dateTo);
    }

    return Response.ok(vacancyOfTheDayService.getVacancyOfTheDayList(vacancyOfTheDayId,
        vacancyId,
        vacancyName,
        employerId,
        employerName,
        creatorId,
        creatorName,
        crmUrl,
        parsedDateFrom,
        parsedDateTo,
        regionIds,
        exactlyRegionFilter,
        deviceTypes,
        campaignStatuses,
        paymentStatuses,
        campaignTypes,
        packetTypes
    )).build();
  }
}
