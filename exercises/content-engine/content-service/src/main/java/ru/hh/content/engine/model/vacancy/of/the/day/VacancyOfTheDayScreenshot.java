package ru.hh.content.engine.model.vacancy.of.the.day;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vacancy_of_the_day_screenshot")
public class VacancyOfTheDayScreenshot {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "vacancy_of_the_day_screenshot_id", nullable = false)
  private int vacancyOfTheDayScreenshotId;

  @Column(name = "url", nullable = false)
  private String url;

  @Column(name = "vacancy_of_the_day_id", nullable = false)
  private Long vacancyOfTheDayId;
  
  public VacancyOfTheDayScreenshot() {
  }
  
  public VacancyOfTheDayScreenshot(String url, Long vacancyOfTheDayId) {
    this.url = url;
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }
  
  public int getVacancyOfTheDayScreenshotId() {
    return vacancyOfTheDayScreenshotId;
  }

  public void setVacancyOfTheDayScreenshotId(int vacancyOfTheDayScreenshotId) {
    this.vacancyOfTheDayScreenshotId = vacancyOfTheDayScreenshotId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Long getVacancyOfTheDayId() {
    return vacancyOfTheDayId;
  }

  public void setVacancyOfTheDayId(Long vacancyOfTheDayId) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VacancyOfTheDayScreenshot that = (VacancyOfTheDayScreenshot) o;
    return vacancyOfTheDayScreenshotId == that.vacancyOfTheDayScreenshotId &&
        Objects.equals(url, that.url) &&
        Objects.equals(vacancyOfTheDayId, that.vacancyOfTheDayId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(vacancyOfTheDayScreenshotId, url, vacancyOfTheDayId);
  }

  @Override
  public String toString() {
    return "VacancyOfTheDayScreenshot{" +
        "vacancyOfTheDayScreenshotId=" + vacancyOfTheDayScreenshotId +
        ", url='" + url + '\'' +
        ", vacancyOfTheDayId=" + vacancyOfTheDayId +
        '}';
  }
}
