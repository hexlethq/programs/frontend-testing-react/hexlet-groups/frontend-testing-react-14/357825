package ru.hh.content.engine.dto;

import java.util.stream.Stream;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.CookieParam;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import ru.hh.content.engine.model.enums.TargetingDeviceType;

public class ImpressionRequestDto {
  private String hhuid;

  @HeaderParam("X-Real-IP")
  private String remoteHostIp;

  @QueryParam("userAgent")
  private String userAgent;

  @Context
  private UriInfo uriInfo;

  @Nullable
  @CookieParam("hhuid")
  private String cookieHhuid;

  @Nullable
  @HeaderParam("hhuid")
  private String headerHhuid;

  @Nullable
  @QueryParam("hhuid")
  private String queryHhuid;

  @Nullable
  @HeaderParam("hhid")
  private Long hhid;

  @Nonnull
  @QueryParam("contentId")
  private Long contentId;

  @Nonnull
  @QueryParam("contentPlaceId")
  private Long contentPlaceId;

  @Nonnull
  @QueryParam("domainAreaId")
  private Integer domainAreaId;

  @Nonnull
  @QueryParam("targetingDeviceType")
  private TargetingDeviceType targetingDeviceType;

  @Nonnull
  @QueryParam("areaPath")
  private String areaPath;

  public String getRemoteHostIp() {
    return remoteHostIp;
  }

  public void setRemoteHostIp(String remoteHostIp) {
    this.remoteHostIp = remoteHostIp;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public UriInfo getUriInfo() {
    return uriInfo;
  }

  public void setUriInfo(UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }

  public String getHhuid() {
    return Stream.of(hhuid, cookieHhuid, headerHhuid, queryHhuid)
        .filter(it -> !it.isBlank())
        .findFirst()
        .get();
  }

  @Nullable
  public Long getHhid() {
    return hhid;
  }

  public void setHhid(@Nullable Long hhid) {
    this.hhid = hhid;
  }

  public int getDomainAreaId() {
    return domainAreaId;
  }

  public void setDomainAreaId(int domainAreaId) {
    this.domainAreaId = domainAreaId;
  }
  
  @Nonnull
  public TargetingDeviceType getTargetingDeviceType() {
    return targetingDeviceType;
  }
  
  public void setTargetingDeviceType(@Nonnull TargetingDeviceType targetingDeviceType) {
    this.targetingDeviceType = targetingDeviceType;
  }
  
  @Nonnull
  public String getAreaPath() {
    return areaPath;
  }
  
  public void setAreaPath(@Nonnull String areaPath) {
    this.areaPath = areaPath;
  }
  
  public void setHhuid(String hhuid) {
    this.hhuid = hhuid;
  }
  
  public void setCookieHhuid(@Nullable String cookieHhuid) {
    this.cookieHhuid = cookieHhuid;
  }
  
  public void setHeaderHhuid(@Nullable String headerHhuid) {
    this.headerHhuid = headerHhuid;
  }
  
  public void setQueryHhuid(@Nullable String queryHhuid) {
    this.queryHhuid = queryHhuid;
  }
  
  public void setDomainAreaId(@Nonnull Integer domainAreaId) {
    this.domainAreaId = domainAreaId;
  }
  
  @Nonnull
  public Long getContentId() {
    return contentId;
  }
  
  public void setContentId(@Nonnull Long contentId) {
    this.contentId = contentId;
  }
  
  @Nonnull
  public Long getContentPlaceId() {
    return contentPlaceId;
  }
  
  public void setContentPlaceId(@Nonnull Long contentPlaceId) {
    this.contentPlaceId = contentPlaceId;
  }
}
