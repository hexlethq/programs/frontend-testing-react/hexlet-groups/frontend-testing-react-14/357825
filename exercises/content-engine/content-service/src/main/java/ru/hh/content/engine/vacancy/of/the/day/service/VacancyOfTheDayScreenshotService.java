package ru.hh.content.engine.vacancy.of.the.day.service;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.ImageType;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDayScreenshot;
import ru.hh.content.engine.services.ImageLoaderService;
import ru.hh.content.engine.utils.ContentImage;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayScreenshotDao;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyScreenshotDto;

public class VacancyOfTheDayScreenshotService {
  private final VacancyOfTheDayScreenshotDao vacancyOfTheDayScreenshotDao;
  private final ImageLoaderService imageLoaderService;

  public VacancyOfTheDayScreenshotService(VacancyOfTheDayScreenshotDao vacancyOfTheDayScreenshotDao,
                                          ImageLoaderService imageLoaderService) {

    this.vacancyOfTheDayScreenshotDao = vacancyOfTheDayScreenshotDao;
    this.imageLoaderService = imageLoaderService;
  }

  @Transactional
  public VacancyScreenshotDto createVacancyScreenshot(long vacancyOfTheDayId, ImageType imageType, ContentImage contentImage) {
    String url = imageLoaderService.uploadImageAndGetFileName(contentImage, imageType);
    VacancyOfTheDayScreenshot vacancyOfTheDayScreenshot = new VacancyOfTheDayScreenshot(url, vacancyOfTheDayId);
    vacancyOfTheDayScreenshotDao.save(vacancyOfTheDayScreenshot);
    return new VacancyScreenshotDto(imageLoaderService.getFullFileUrl(url), vacancyOfTheDayScreenshot.getVacancyOfTheDayScreenshotId());
  }

  public List<VacancyScreenshotDto> getVacancyScreenshots(long vacancyOfTheDayId) {
    return vacancyOfTheDayScreenshotDao.getScreenshots(vacancyOfTheDayId)
        .stream()
        .map(VacancyScreenshotDto::new)
        .peek(item -> item.setSrc(imageLoaderService.getFullFileUrl(item.getSrc())))
        .collect(Collectors.toList());
  }

  public boolean deleteScreenshot(int screenId) {
    vacancyOfTheDayScreenshotDao.deleteScreenshot(screenId);
    return true;
  }
}
