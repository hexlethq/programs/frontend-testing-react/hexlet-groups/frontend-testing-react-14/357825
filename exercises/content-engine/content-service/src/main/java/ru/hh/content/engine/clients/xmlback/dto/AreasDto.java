package ru.hh.content.engine.clients.xmlback.dto;

import java.util.List;

public class AreasDto {
  private int id;
  private List<AreasDto> children;

  public AreasDto() {
  }

  public AreasDto(int id, List<AreasDto> children) {
    this.id = id;
    this.children = children;
  }

  public AreasDto(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public List<AreasDto> getChildren() {
    return children;
  }

  public void setChildren(List<AreasDto> children) {
    this.children = children;
  }
}
