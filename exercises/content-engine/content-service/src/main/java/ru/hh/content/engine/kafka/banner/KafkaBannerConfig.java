package ru.hh.content.engine.kafka.banner;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import static ru.hh.content.engine.utils.CommonUtils.createObjectMapper;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.kafka.consumer.DefaultConsumerFactory;
import ru.hh.nab.kafka.consumer.KafkaConsumerFactory;
import ru.hh.nab.kafka.producer.KafkaProducer;
import ru.hh.nab.kafka.producer.KafkaProducerFactory;
import ru.hh.nab.kafka.serialization.JacksonDeserializerSupplier;
import ru.hh.nab.kafka.serialization.JacksonSerializerSupplier;
import ru.hh.nab.kafka.util.ConfigProvider;
import ru.hh.nab.metrics.StatsDSender;

@Configuration
@Import({
    KafkaBannerPublisher.class,
})
public class KafkaBannerConfig {

  @Bean
  public ConfigProvider bannerKafkaConfigProvider(String serviceName,
                                                  FileSettings fileSettings) {
    return new ConfigProvider(serviceName, "kafka.banner", fileSettings);
  }

  @Bean
  public KafkaProducer kafkaBannerProducer(ConfigProvider bannerKafkaConfigProvider) {
    return new KafkaProducerFactory(bannerKafkaConfigProvider, new JacksonSerializerSupplier(createObjectMapper())).createDefaultProducer();
  }

  @Bean
  public KafkaConsumerFactory kafkaBannerConsumerFactory(ConfigProvider bannerKafkaConfigProvider, StatsDSender statsDSender) {
    return new DefaultConsumerFactory(bannerKafkaConfigProvider, new JacksonDeserializerSupplier(new ObjectMapper()
        .findAndRegisterModules()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)), statsDSender);
  }
}
