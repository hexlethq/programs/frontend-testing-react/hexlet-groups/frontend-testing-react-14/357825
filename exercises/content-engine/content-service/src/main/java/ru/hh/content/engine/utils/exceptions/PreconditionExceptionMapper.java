package ru.hh.content.engine.utils.exceptions;

import static javax.ws.rs.core.Response.Status.PRECONDITION_FAILED;
import javax.ws.rs.ext.Provider;
import ru.hh.nab.starter.exceptions.NabExceptionMapper;

@Provider
public class PreconditionExceptionMapper extends NabExceptionMapper<PreconditionException> {
  public PreconditionExceptionMapper() {
    super(PRECONDITION_FAILED, LoggingLevel.INFO_WITHOUT_STACK_TRACE);
  }
}
