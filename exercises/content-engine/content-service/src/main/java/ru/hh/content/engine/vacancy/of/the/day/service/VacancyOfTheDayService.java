package ru.hh.content.engine.vacancy.of.the.day.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDayReservation;
import ru.hh.content.engine.packets.PacketService;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.content.engine.services.VacancyService;
import ru.hh.content.engine.utils.exceptions.VacancyOfTheDayReservationException;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayDao;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayReservationDao;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyDataDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyFrontDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayCheckRequestDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayFormDto;
import ru.hh.content.engine.work.in.company.dto.ReserveNextFreePeriod;
import ru.hh.content.engine.work.in.company.dto.ReserveStatus;
import ru.hh.nab.datasource.DataSourceType;
import ru.hh.nab.hibernate.transaction.ExecuteOnDataSource;

public class VacancyOfTheDayService {
  static final int MAX_VACANCIES_ON_PAGE = 14;
  private static final int DAYS_BETWEEN_TWO_WEEKS = 6;

  private final VacancyOfTheDayDao vacancyOfTheDayDao;
  private final HHSessionContext hhSessionContext;
  private final VacancyOfTheDayReservationDao vacancyOfTheDayReservationDao;
  private final PacketService packetService;
  private final VacancyService vacancyService;
  private final SecurityContext securityContext;

  public VacancyOfTheDayService(VacancyOfTheDayDao vacancyOfTheDayDao,
                                HHSessionContext hhSessionContext,
                                VacancyOfTheDayReservationDao vacancyOfTheDayReservationDao,
                                PacketService packetService,
                                SecurityContext securityContext,
                                VacancyService vacancyService) {
    this.vacancyOfTheDayDao = vacancyOfTheDayDao;
    this.hhSessionContext = hhSessionContext;
    this.vacancyOfTheDayReservationDao = vacancyOfTheDayReservationDao;
    this.packetService = packetService;
    this.vacancyService = vacancyService;
    this.securityContext = securityContext;
  }

  @ExecuteOnDataSource(dataSourceType = DataSourceType.READONLY)
  public List<VacancyFrontDto> getVacancyOfTheDayList(Long vacancyOfTheDayId,
                                                      Integer vacancyId,
                                                      String vacancyName,
                                                      Integer employerId,
                                                      String employerName,
                                                      Integer creatorId,
                                                      String creatorName,
                                                      String crmUrl,
                                                      LocalDate dateFrom,
                                                      LocalDate dateTo,
                                                      Set<Integer> regionIds,
                                                      boolean exactlyRegionFilter,
                                                      Set<TargetingDeviceType> deviceTypes,
                                                      Set<VacancyOfTheDayStatus> campaignStatuses,
                                                      Set<VacancyOfTheDayPayedStatus> paymentStatuses,
                                                      Set<VacancyOfTheDayCampaignType> campaignTypes,
                                                      Set<PacketType> packetTypes
  ) {

    List<VacancyOfTheDay> vacancyOfTheDayList = vacancyOfTheDayDao.getVacanciesOfTheDayWithDatesByPeriodAndEmployer(
        vacancyOfTheDayId,
        vacancyId,
        employerId,
        employerName,
        creatorId,
        creatorName,
        crmUrl,
        dateFrom,
        dateTo,
        deviceTypes,
        campaignStatuses,
        paymentStatuses,
        campaignTypes,
        packetTypes
    );

    Set<Integer> vacancyIds = vacancyOfTheDayList.stream().map(VacancyOfTheDay::getVacancyId).filter(Objects::nonNull).collect(Collectors.toSet());
    List<VacancyDto> vacancyDtoList = vacancyService.getVacancies(vacancyIds);
    Map<Integer, VacancyDto> vacancyDtoMap = vacancyDtoList
        .stream()
        .collect(Collectors.toMap(VacancyDto::getVacancyId, Function.identity()));

    return vacancyOfTheDayList.stream()
        .map(VacancyFrontDto::new)
        .peek(item -> {
              if (item.getVacancy() != null) {
                item.getVacancy().setName(
                    vacancyDtoMap.getOrDefault(
                        item.getVacancy().getId(),
                        new VacancyDto(item.getVacancy().getId(), "")
                    ).getName()
                );
              } else {
                item.setVacancy(new VacancyDataDto(0, "Не указана"));
              }
            }
        )
        .filter(item -> regionFilter(regionIds, exactlyRegionFilter, item))
        .filter(item -> like(vacancyName, item))
        .collect(Collectors.toList());
  }

  @Transactional(readOnly = true)
  public List<ReserveNextFreePeriod> checkBusy(VacancyOfTheDayCheckRequestDto vacancyOfTheDayCheckRequestDto) {
    validateVacancyOfTheDayPeriod(vacancyOfTheDayCheckRequestDto.getDateFrom(), vacancyOfTheDayCheckRequestDto.getDateTo());

    int duration = (int) vacancyOfTheDayCheckRequestDto.getDateFrom().until(vacancyOfTheDayCheckRequestDto.getDateTo(), DAYS) + 1;

    List<ReserveNextFreePeriod> reserveNextFreePeriods = new ArrayList<>();

    Set<Integer> regionIds = getRegionIdsFromPacketOrField(vacancyOfTheDayCheckRequestDto.getPacket(), vacancyOfTheDayCheckRequestDto.getRegionIds());

    List<Integer> busyRegions = vacancyOfTheDayReservationDao.getBusyRegionsForPeriod(vacancyOfTheDayCheckRequestDto.getDeviceTypes(),
        regionIds,
        vacancyOfTheDayCheckRequestDto.getDateFrom(),
        vacancyOfTheDayCheckRequestDto.getDateTo(),
        MAX_VACANCIES_ON_PAGE
    );

    Map<Integer, LocalDate> nextFreeDatesByRegions = vacancyOfTheDayReservationDao.getNextFreeDatesByRegions(
        vacancyOfTheDayCheckRequestDto.getDeviceTypes(),
        duration,
        busyRegions,
        vacancyOfTheDayCheckRequestDto.getDateFrom(),
        MAX_VACANCIES_ON_PAGE
    );

    for (Integer regionId : regionIds) {
      if (nextFreeDatesByRegions.containsKey(regionId)) {
        LocalDate nextFree = nextFreeDatesByRegions.get(regionId);
        reserveNextFreePeriods.add(new ReserveNextFreePeriod(
            regionId, ReserveStatus.BUSY, nextFree.plusWeeks(1), nextFree.plusWeeks(1).plusDays(duration - 1))
        );
      } else {
        reserveNextFreePeriods.add(new ReserveNextFreePeriod(regionId, ReserveStatus.FREE));
      }
    }

    return reserveNextFreePeriods;
  }

  @Transactional
  public long createVacancyOfTheDay(VacancyOfTheDayFormDto vacancyOfTheDayFormDto) {
    validateVacancyOfTheDayPeriod(vacancyOfTheDayFormDto.getDateStart(), vacancyOfTheDayFormDto.getDateEnd());

    VacancyOfTheDay vacancyOfTheDay = new VacancyOfTheDay(
        vacancyOfTheDayFormDto.getComment(),
        vacancyOfTheDayFormDto.getCrmUrl(),
        vacancyOfTheDayFormDto.getVacancyId(),
        hhSessionContext.getAccount().userId,
        hhSessionContext.getUserFullName(),
        hhSessionContext.getAccount().email,
        VacancyOfTheDayStatus.NEW,
        VacancyOfTheDayPayedStatus.NOT_PAYED,
        vacancyOfTheDayFormDto.getPacketType(),
        vacancyOfTheDayFormDto.getCampaignType()
    );

    if (vacancyOfTheDayFormDto.getVacancyId() != null) {
      fillEmployerInfo(vacancyOfTheDayFormDto, vacancyOfTheDay);
    }

    vacancyOfTheDayDao.save(vacancyOfTheDay);

    saveReservations(vacancyOfTheDayFormDto, vacancyOfTheDay);

    return vacancyOfTheDay.getVacancyOfTheDayId();
  }

  @Transactional
  public void editVacancyOfTheDay(VacancyOfTheDayFormDto vacancyOfTheDayFormDto, long vacancyOfTheDayId) {
    validateVacancyOfTheDayPeriod(vacancyOfTheDayFormDto.getDateStart(), vacancyOfTheDayFormDto.getDateEnd());

    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayId);

    securityContext.checkUserIsCurrentOrAdmin(vacancyOfTheDay.getManagerUserId());

    vacancyOfTheDay.setComment(vacancyOfTheDayFormDto.getComment());
    vacancyOfTheDay.setCrmUrl(vacancyOfTheDayFormDto.getCrmUrl());
    vacancyOfTheDay.setPacketType(vacancyOfTheDayFormDto.getPacketType());
    vacancyOfTheDay.setVacancyId(vacancyOfTheDayFormDto.getVacancyId());
    vacancyOfTheDay.setCampaignType(vacancyOfTheDayFormDto.getCampaignType());

    if (vacancyOfTheDayFormDto.getVacancyId() != null) {
      fillEmployerInfo(vacancyOfTheDayFormDto, vacancyOfTheDay);
    }

    vacancyOfTheDay = vacancyOfTheDayDao.update(vacancyOfTheDay);

    vacancyOfTheDayReservationDao.deleteReservations(vacancyOfTheDayId);
    saveReservations(vacancyOfTheDayFormDto, vacancyOfTheDay);
  }

  private void fillEmployerInfo(VacancyOfTheDayFormDto vacancyOfTheDayFormDto, VacancyOfTheDay vacancyOfTheDay) {
    VacancyDto vacancy = vacancyService.getVacancy(vacancyOfTheDayFormDto.getVacancyId());
    vacancyOfTheDay.setEmployerId(vacancy.getVacancyEmployerDto().getEmployerId());
    vacancyOfTheDay.setEmployerName(vacancy.getVacancyEmployerDto().getEmployerName());
  }

  private void saveReservations(VacancyOfTheDayFormDto vacancyOfTheDayFormDto, VacancyOfTheDay vacancyOfTheDay) {
    List<VacancyOfTheDayReservation> vacancyOfTheDayReservationList = new ArrayList<>();

    vacancyOfTheDayFormDto.getDateStart().datesUntil(vacancyOfTheDayFormDto.getDateEnd(), Period.ofWeeks(1)).forEach(dateStart ->
        processWeek(vacancyOfTheDayFormDto, vacancyOfTheDay, vacancyOfTheDayReservationList, dateStart)
    );

    vacancyOfTheDayReservationDao.saveReservations(vacancyOfTheDayReservationList);
  }

  private void validateVacancyOfTheDayPeriod(LocalDate dateStart, LocalDate dateEnd) {
    if (!dateStart.isBefore(dateEnd)) {
      throw new IllegalArgumentException("Date end before date start");
    }

    if (dateStart.getDayOfWeek() != DayOfWeek.MONDAY) {
      throw new IllegalArgumentException("Start day must be monday");
    }

    if (dateEnd.getDayOfWeek() != DayOfWeek.SUNDAY) {
      throw new IllegalArgumentException("End day must be sunday");
    }
  }

  private void processWeek(VacancyOfTheDayFormDto vacancyOfTheDayFormDto,
                           VacancyOfTheDay vacancyOfTheDay,
                           List<VacancyOfTheDayReservation> vacancyOfTheDayReservationList,
                           LocalDate dateStart) {
    for (TargetingDeviceType deviceType : vacancyOfTheDayFormDto.getDeviceTypes()) {
      Set<Integer> regionIds = getRegionIdsFromPacketOrField(vacancyOfTheDayFormDto.getPacketType(), vacancyOfTheDayFormDto.getRegionIds());

      Map<Integer, Set<Integer>> regionToReservationNumbers = vacancyOfTheDayReservationDao.getRegionToReservationNumbers(
          dateStart,
          deviceType,
          List.copyOf(regionIds)
      );

      Map<Integer, Integer> minFreeReservationNumbersForRegions = calculateMinFreeReservationNumbersForRegions(
          regionToReservationNumbers, regionIds
      );

      if (minFreeReservationNumbersForRegions.size() != regionIds.size()) {
        throw new VacancyOfTheDayReservationException();
      }

      for (Integer regionId : regionIds) {
        vacancyOfTheDayReservationList.add(new VacancyOfTheDayReservation(
            dateStart,
            dateStart.plusDays(DAYS_BETWEEN_TWO_WEEKS),
            vacancyOfTheDay.getVacancyOfTheDayId(),
            deviceType,
            regionId,
            minFreeReservationNumbersForRegions.get(regionId))
        );
      }
    }
  }

  private Set<Integer> getRegionIdsFromPacketOrField(PacketType packetType, Set<Integer> regionIdsFromFront) {
    Set<Integer> regionIds;
    if (packetType.getCanChooseRegions()) {
      if (regionIdsFromFront == null || regionIdsFromFront.isEmpty()) {
        throw new IllegalArgumentException();
      }

      regionIds = new HashSet<>(regionIdsFromFront);
    } else {
      regionIds = packetService.getRegionsForPacket(packetType);
    }
    return regionIds;
  }

  private Map<Integer, Integer> calculateMinFreeReservationNumbersForRegions(Map<Integer, Set<Integer>> regionToReservationNumbers,
                                                                             Set<Integer> regionIds) {
    Map<Integer, Integer> regionToFreeReservationNumber = new HashMap<>();

    for (Integer regionId : regionIds) {
      if (regionToReservationNumbers.containsKey(regionId)) {
        for (int i = 0; i < MAX_VACANCIES_ON_PAGE; i++) {
          if (!regionToReservationNumbers.get(regionId).contains(i)) {
            regionToFreeReservationNumber.put(regionId, i);
            break;
          }
        }
      } else {
        regionToFreeReservationNumber.put(regionId, 0);
      }
    }

    return regionToFreeReservationNumber;
  }

  private boolean regionFilter(Set<Integer> regionIds, boolean exactlyRegionFilter, VacancyFrontDto it) {
    if (regionIds != null) {
      if (exactlyRegionFilter) {
        return it.getRegionList().equals(regionIds);
      } else {
        return it.getRegionList().containsAll(regionIds);
      }
    } else {
      return true;
    }
  }

  private boolean like(String vacancyName, VacancyFrontDto it) {
    return (vacancyName == null ||
        (it.getVacancy() != null && it.getVacancy().getName().toLowerCase().contains(vacancyName.toLowerCase()))
    );
  }
}
