package ru.hh.content.engine.model.vacancy.of.the.day;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;

@Entity
@Table(name = "vacancy_of_the_day")
public class VacancyOfTheDay {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "vacancy_of_the_day_id", nullable = false)
  private long vacancyOfTheDayId;

  @Column(name = "comment")
  private String comment;

  @Column(name = "crm_url")
  private String crmUrl;

  @Column(name = "vacancy_id")
  private Integer vacancyId;

  @Column(name = "employer_id")
  private Integer employerId;

  @Column(name = "employer_name")
  private String employerName;

  @Column(name = "manager_user_id", nullable = false)
  private Integer managerUserId;

  @Column(name = "manager_name", nullable = false)
  private String managerName;

  @Column(name = "manager_email", nullable = false)
  private String managerEmail;

  @Column(name = "status")
  @Enumerated(EnumType.STRING)
  private VacancyOfTheDayStatus status;

  @Column(name = "payed_status")
  @Enumerated(EnumType.STRING)
  private VacancyOfTheDayPayedStatus payedStatus;

  @Column(name = "packet_type")
  @Enumerated(EnumType.STRING)
  private PacketType packetType;

  @Column(name = "campaign_type")
  @Enumerated(EnumType.STRING)
  private VacancyOfTheDayCampaignType campaignType;

  @OneToMany(mappedBy = "vacancyOfTheDayId", fetch = FetchType.LAZY)
  private List<VacancyOfTheDayReservation> vacancyOfTheDayReservations;

  public VacancyOfTheDay() {
  }

  public VacancyOfTheDay(String comment,
                         String crmUrl,
                         Integer vacancyId,
                         Integer managerUserId,
                         String managerName,
                         String managerEmail,
                         VacancyOfTheDayStatus status,
                         VacancyOfTheDayPayedStatus payedStatus,
                         PacketType packetType,
                         VacancyOfTheDayCampaignType campaignType) {
    this.comment = comment;
    this.crmUrl = crmUrl;
    this.vacancyId = vacancyId;
    this.managerUserId = managerUserId;
    this.managerName = managerName;
    this.managerEmail = managerEmail;
    this.status = status;
    this.payedStatus = payedStatus;
    this.packetType = packetType;
    this.campaignType = campaignType;
  }

  public long getVacancyOfTheDayId() {
    return vacancyOfTheDayId;
  }

  public void setVacancyOfTheDayId(long vacancyOfTheDayId) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Integer getVacancyId() {
    return vacancyId;
  }

  public void setVacancyId(Integer vacancyId) {
    this.vacancyId = vacancyId;
  }

  public Integer getManagerUserId() {
    return managerUserId;
  }

  public void setManagerUserId(Integer managerUserId) {
    this.managerUserId = managerUserId;
  }

  public String getManagerName() {
    return managerName;
  }

  public void setManagerName(String managerName) {
    this.managerName = managerName;
  }

  public VacancyOfTheDayStatus getStatus() {
    return status;
  }

  public void setStatus(VacancyOfTheDayStatus status) {
    this.status = status;
  }

  public VacancyOfTheDayPayedStatus getPayedStatus() {
    return payedStatus;
  }

  public void setPayedStatus(VacancyOfTheDayPayedStatus payedStatus) {
    this.payedStatus = payedStatus;
  }

  public List<VacancyOfTheDayReservation> getVacancyOfTheDayReservations() {
    return vacancyOfTheDayReservations;
  }

  public void setVacancyOfTheDayReservations(List<VacancyOfTheDayReservation> vacancyOfTheDayReservations) {
    this.vacancyOfTheDayReservations = vacancyOfTheDayReservations;
  }

  public String getCrmUrl() {
    return crmUrl;
  }

  public void setCrmUrl(String crmUrl) {
    this.crmUrl = crmUrl;
  }

  public PacketType getPacketType() {
    return packetType;
  }

  public void setPacketType(PacketType packetType) {
    this.packetType = packetType;
  }

  public Integer getEmployerId() {
    return employerId;
  }

  public void setEmployerId(Integer employerId) {
    this.employerId = employerId;
  }

  public String getEmployerName() {
    return employerName;
  }

  public void setEmployerName(String employerName) {
    this.employerName = employerName;
  }
  
  public String getManagerEmail() {
    return managerEmail;
  }
  
  public void setManagerEmail(String managerEmail) {
    this.managerEmail = managerEmail;
  }
  
  public VacancyOfTheDayCampaignType getCampaignType() {
    return campaignType;
  }

  public void setCampaignType(VacancyOfTheDayCampaignType campaignType) {
    this.campaignType = campaignType;
  }

  public Set<TargetingDeviceType> getDeviceTypesFromReservations() {
    return vacancyOfTheDayReservations.stream().map(VacancyOfTheDayReservation::getDeviceType).collect(Collectors.toSet());
  }

  public LocalDate getDateStartFromReservations() {
    return vacancyOfTheDayReservations.stream().map(VacancyOfTheDayReservation::getDateStart).min(LocalDate::compareTo).get();
  }

  public LocalDate getDateEndFromReservations() {
    return vacancyOfTheDayReservations.stream().map(VacancyOfTheDayReservation::getDateEnd).max(LocalDate::compareTo).get();
  }

  public Set<Integer> getRegionIdsFromReservations() {
    return vacancyOfTheDayReservations.stream()
        .map(VacancyOfTheDayReservation::getRegionId)
        .collect(Collectors.toSet());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VacancyOfTheDay that = (VacancyOfTheDay) o;
    return vacancyOfTheDayId == that.vacancyOfTheDayId &&
        Objects.equals(comment, that.comment) &&
        Objects.equals(crmUrl, that.crmUrl) &&
        Objects.equals(vacancyId, that.vacancyId) &&
        Objects.equals(employerId, that.employerId) &&
        Objects.equals(employerName, that.employerName) &&
        Objects.equals(managerUserId, that.managerUserId) &&
        Objects.equals(managerName, that.managerName) &&
        status == that.status &&
        payedStatus == that.payedStatus &&
        packetType == that.packetType &&
        campaignType == that.campaignType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(vacancyOfTheDayId,
        comment,
        crmUrl,
        vacancyId,
        employerId,
        employerName,
        managerUserId,
        managerName,
        status,
        payedStatus,
        packetType,
        campaignType);
  }

  @Override
  public String toString() {
    return "VacancyOfTheDay{" +
        "vacancyOfTheDayId=" + vacancyOfTheDayId +
        ", comment='" + comment + '\'' +
        ", crmUrl='" + crmUrl + '\'' +
        ", vacancyId=" + vacancyId +
        ", employerId=" + employerId +
        ", employerName='" + employerName + '\'' +
        ", managerUserId=" + managerUserId +
        ", managerName='" + managerName + '\'' +
        ", status=" + status +
        ", payedStatus=" + payedStatus +
        ", packetType=" + packetType +
        ", campaignType=" + campaignType +
        ", vacancyOfTheDayReservations=" + vacancyOfTheDayReservations +
        '}';
  }
}
