package ru.hh.content.engine.dto;

import javax.annotation.Nonnull;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

public class ClickRequestDto {
  @HeaderParam("X-Real-IP")
  private String remoteHostIp;

  @HeaderParam("User-Agent")
  private String userAgent;

  @Context
  private UriInfo uriInfo;

  @Nonnull
  @QueryParam("hhuid")
  private String hhuid;

  @QueryParam("hhid")
  private Long hhid;

  @Nonnull
  @QueryParam("host")
  private String host;

  @Nonnull
  @QueryParam("domainAreaId")
  private Integer domainAreaId;

  @Nonnull
  @QueryParam("contentId")
  private Long contentId;

  @Nonnull
  @QueryParam("employerId")
  private Long employerId;

  @Nonnull
  @QueryParam("placeId")
  private Long placeId;

  public String getRemoteHostIp() {
    return remoteHostIp;
  }

  public void setRemoteHostIp(String remoteHostIp) {
    this.remoteHostIp = remoteHostIp;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public UriInfo getUriInfo() {
    return uriInfo;
  }

  public void setUriInfo(UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }

  public long getContentId() {
    return contentId;
  }

  public void setContentId(long contentId) {
    this.contentId = contentId;
  }
  
  @Nonnull
  public String getHost() {
    return host;
  }
  
  public void setHost(@Nonnull String host) {
    this.host = host;
  }

  @Nonnull
  public Long getPlaceId() {
    return placeId;
  }

  public void setPlaceId(@Nonnull Long placeId) {
    this.placeId = placeId;
  }

  @Nonnull
  public Long getEmployerId() {
    return employerId;
  }

  public void setEmployerId(@Nonnull Long employerId) {
    this.employerId = employerId;
  }

  @Nonnull
  public String getHhuid() {
    return hhuid;
  }

  public void setHhuid(@Nonnull String hhuid) {
    this.hhuid = hhuid;
  }

  public Long getHhid() {
    return hhid;
  }

  public void setHhid(@Nonnull Long hhid) {
    this.hhid = hhid;
  }

  @Nonnull
  public Integer getDomainAreaId() {
    return domainAreaId;
  }

  public void setDomainAreaId(@Nonnull Integer domainAreaId) {
    this.domainAreaId = domainAreaId;
  }
}
