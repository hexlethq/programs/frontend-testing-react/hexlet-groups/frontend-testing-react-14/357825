package ru.hh.content.engine.utils.cache;

import static java.time.LocalDateTime.now;
import static java.time.temporal.ChronoUnit.MILLIS;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import org.springframework.scheduling.annotation.Scheduled;
import ru.hh.settings.SettingsClient;

public class CacheService<T> {
  private final Map<String, CacheItem<T>> cacheItemMap = new ConcurrentHashMap<>();
  private final SettingsClient settingsClient;
  private final String cacheTtlMsSettingsName;

  public CacheService(SettingsClient settingsClient, String cacheTtlMsSettingsName) {
    this.settingsClient = settingsClient;
    this.cacheTtlMsSettingsName = cacheTtlMsSettingsName;
  }

  public Optional<T> getFromCache(String key) {
    CacheItem<T> cacheItem = cacheItemMap.get(key);

    if (cacheItem == null) {
      return Optional.empty();
    }

    if (now().isAfter(cacheItem.getExpirationTime())) {
      cacheItemMap.remove(key);
      return Optional.empty();
    }

    return Optional.of(cacheItem.getCachedObject());
  }

  public void putInCache(String key, T object) {
    cacheItemMap.put(key, new CacheItem<>(now().plus(settingsClient.getInteger(cacheTtlMsSettingsName, 0), MILLIS), object));
  }

  @Scheduled(fixedRate = 1000 * 60 * 60)
  public void removeOldCache() {
    for (Map.Entry<String, CacheItem<T>> cacheItemEntry : cacheItemMap.entrySet()) {
      if (now().isAfter(cacheItemEntry.getValue().getExpirationTime())) {
        cacheItemMap.remove(cacheItemEntry.getKey());
      }
    }
  }

  protected Map<String, CacheItem<T>> getCacheItemMap() {
    return cacheItemMap;
  }
}
