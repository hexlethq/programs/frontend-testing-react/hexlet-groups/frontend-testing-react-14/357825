package ru.hh.content.engine.model.enums;

import ru.hh.content.engine.model.targetings.AgeTargetingData;
import ru.hh.content.engine.model.targetings.DeviceTypeTargetingData;
import ru.hh.content.engine.model.targetings.GenderTargetingData;
import ru.hh.content.engine.model.targetings.LanguageTargetingData;
import ru.hh.content.engine.model.targetings.ProfAreaTargetingData;
import ru.hh.content.engine.model.targetings.RegionTargetingData;
import ru.hh.content.engine.model.targetings.SalaryTargetingData;
import ru.hh.content.engine.model.targetings.SpecializationTargetingData;
import ru.hh.content.engine.model.targetings.TimeTargetingData;
import ru.hh.content.engine.model.targetings.UserTypeTargetingData;

public enum TargetingType {
  AGE(AgeTargetingData.class),
  REGION(RegionTargetingData.class),
  DEVICE_TYPE(DeviceTypeTargetingData.class),
  GENDER(GenderTargetingData.class),
  LANGUAGE(LanguageTargetingData.class),
  PROF_AREA(ProfAreaTargetingData.class),
  SALARY(SalaryTargetingData.class),
  SPECIALIZATION(SpecializationTargetingData.class),
  TIME(TimeTargetingData.class),
  USER_TYPE(UserTypeTargetingData.class),
  ;

  private final Class dataClass;

  TargetingType(Class dataClass) {
    this.dataClass = dataClass;
  }
}
