package ru.hh.content.engine.packets;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
    PacketResource.class,
    PacketService.class,
    PacketDao.class,
})
public class PacketConfig {
}
