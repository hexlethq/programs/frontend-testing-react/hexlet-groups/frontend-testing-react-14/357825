package ru.hh.content.engine.area.region.mapping;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.content.engine.clients.xmlback.HHAreasClient;
import ru.hh.content.engine.clients.xmlback.dto.AreasDto;
import ru.hh.content.engine.utils.ExceptionUtils;
import ru.hh.jclient.common.ResultWithStatus;
import ru.hh.settings.SettingsClient;

public class AreaRegionMappingUpdaterCronService {
  private static final Logger LOGGER = LoggerFactory.getLogger(AreaRegionMappingUpdaterCronService.class);
  public static final String CORE_REGIONS_SETTING_NAME = "core_regions";

  private final AreaRegionMappingUpdaterDao areaRegionMappingUpdaterDao;
  private final HHAreasClient hhAreasClient;
  private final SettingsClient settingsClient;

  @Inject
  public AreaRegionMappingUpdaterCronService(AreaRegionMappingUpdaterDao areaRegionMappingUpdaterDao,
                                             HHAreasClient hhAreasClient, SettingsClient settingsClient) {
    this.areaRegionMappingUpdaterDao = areaRegionMappingUpdaterDao;
    this.hhAreasClient = hhAreasClient;
    this.settingsClient = settingsClient;
  }

  public void updateAreaRegionMapping(int taskId, int launchId) {
    try {
      updateAreaRegionMappingInner();
    } catch (Exception e) {
      LOGGER.error("Fail to update area region mapping with exception", e);
    }
  }

  private void updateAreaRegionMappingInner() {
    AreasDto areasDto = getAreasDto();

    Set<Integer> coreRegions = Arrays.stream(settingsClient.getString(CORE_REGIONS_SETTING_NAME)
        .orElseThrow()
        .split(","))
        .map(Integer::valueOf)
        .collect(Collectors.toSet());

    processArea(areasDto, coreRegions);
  }

  private AreasDto getAreasDto() {
    ResultWithStatus<AreasDto> areasDtoResultWithStatus = ExceptionUtils.getWithExceptionsToRuntimeWrapper(() -> hhAreasClient.getAreasList().get());

    if (!areasDtoResultWithStatus.isSuccess()) {
      throw new RuntimeException(String.format("Fail to get areas dto result with status code %s", areasDtoResultWithStatus.getStatusCode()));
    }

    Optional<AreasDto> areasDtoOptional = areasDtoResultWithStatus.get();

    if (areasDtoOptional.isEmpty()) {
      throw new RuntimeException("Fail to get areas dto result with empty optional");
    }

    return areasDtoOptional.get();
  }

  private void processArea(AreasDto areasDto, Set<Integer> coreRegions) {
    for (AreasDto area : areasDto.getChildren()) {
      if (coreRegions.contains(area.getId())) {
        addChildrenToMapping(area, area.getId());
      } else {
        processArea(area, coreRegions);
      }
    }
  }

  private void addChildrenToMapping(AreasDto area, int coreRegionId) {
    areaRegionMappingUpdaterDao.updateAreaRegionMapping(area.getId(), coreRegionId);

    if (area.getChildren() != null) {
      for (AreasDto child : area.getChildren()) {
        addChildrenToMapping(child, coreRegionId);
      }
    }
  }
}
