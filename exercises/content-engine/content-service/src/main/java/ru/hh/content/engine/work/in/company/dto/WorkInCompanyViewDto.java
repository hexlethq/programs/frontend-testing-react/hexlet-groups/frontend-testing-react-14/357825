package ru.hh.content.engine.work.in.company.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

@XmlAccessorType(XmlAccessType.FIELD)
public class WorkInCompanyViewDto {
  @XmlElement(name = "companyName")
  private String displayName;
  @JsonInclude(JsonInclude.Include.NON_EMPTY)
  private String logoUrl;
  @XmlElement(name = "vacanciesNumber")
  private Long vacanciesCount;
  @XmlElement(name = "id")
  private Integer employerId;
  private Long workInCompanyId;
  private Integer placeId;

  public WorkInCompanyViewDto() {
  }

  public WorkInCompanyViewDto(String displayName, String logoUrl, Long vacanciesCount, Integer employerId) {
    this.displayName = displayName;
    this.logoUrl = logoUrl;
    this.vacanciesCount = vacanciesCount;
    this.employerId = employerId;
  }

  public WorkInCompanyViewDto(String displayName,
                              String logoUrl,
                              Long vacanciesCount,
                              Integer employerId,
                              Long workInCompanyId) {
    this.displayName = displayName;
    this.logoUrl = logoUrl;
    this.vacanciesCount = vacanciesCount;
    this.employerId = employerId;
    this.workInCompanyId = workInCompanyId;
  }

  public Integer getPlaceId() {
    return placeId;
  }

  public void setPlaceId(Integer placeId) {
    this.placeId = placeId;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public Integer getEmployerId() {
    return employerId;
  }

  public void setEmployerId(Integer employerId) {
    this.employerId = employerId;
  }

  public String getLogoUrl() {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }

  public Long getVacanciesCount() {
    return vacanciesCount;
  }

  public void setVacanciesCount(Long vacanciesCount) {
    this.vacanciesCount = vacanciesCount;
  }

  public Long getWorkInCompanyId() {
    return workInCompanyId;
  }

  public void setWorkInCompanyId(Long workInCompanyId) {
    this.workInCompanyId = workInCompanyId;
  }
}
