package ru.hh.content.engine.area.region.mapping;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.CronPaths.AREA_MAPPING_UPDATER_CRON;
import ru.hh.hhinvoker.client.InvokerClient;
import static ru.hh.hhinvoker.remote.RemoteTaskJob.runTaskJobAsync;

@Path(AREA_MAPPING_UPDATER_CRON)
public class AreaRegionMappingResource {
  private final InvokerClient invokerClient;
  private final AreaRegionMappingUpdaterCronService areaRegionMappingUpdaterCronService;

  public AreaRegionMappingResource(InvokerClient invokerClient, AreaRegionMappingUpdaterCronService areaRegionMappingUpdaterCronService) {
    this.invokerClient = invokerClient;
    this.areaRegionMappingUpdaterCronService = areaRegionMappingUpdaterCronService;
  }

  @POST
  public Response updater(@QueryParam("taskId") int taskId,
                          @QueryParam("launchId") int launchId) {
    runTaskJobAsync(invokerClient, taskId, launchId, taskJobContext ->
        areaRegionMappingUpdaterCronService.updateAreaRegionMapping(taskId, launchId)
    );
    return Response.status(Response.Status.NO_CONTENT).build();
  }
}
