package ru.hh.content.engine.vacancy.of.the.day.service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.content.engine.services.NotificationService;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayDao;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayReservationDao;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayScreenshotDao;
import static ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayApprovedErrorType.VACANCY_IS_NULL;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayApprovedResponse;

public class VacancyOfTheDayActionsService {

  private final VacancyOfTheDayDao vacancyOfTheDayDao;
  private final SecurityContext securityContext;
  private final HHSessionContext hhSessionContext;
  private final VacancyOfTheDayReservationDao vacancyOfTheDayReservationDao;
  private final VacancyOfTheDayScreenshotDao vacancyOfTheDayScreenshotDao;
  private final NotificationService notificationService;

  public VacancyOfTheDayActionsService(VacancyOfTheDayDao vacancyOfTheDayDao,
                                       SecurityContext securityContext,
                                       HHSessionContext hhSessionContext,
                                       VacancyOfTheDayReservationDao vacancyOfTheDayReservationDao,
                                       VacancyOfTheDayScreenshotDao vacancyOfTheDayScreenshotDao,
                                       NotificationService notificationService) {
    this.vacancyOfTheDayDao = vacancyOfTheDayDao;
    this.securityContext = securityContext;
    this.hhSessionContext = hhSessionContext;
    this.vacancyOfTheDayReservationDao = vacancyOfTheDayReservationDao;
    this.vacancyOfTheDayScreenshotDao = vacancyOfTheDayScreenshotDao;
    this.notificationService = notificationService;
  }
  
  public void setPaymentStatus(long vacancyOfTheDayId, VacancyOfTheDayPayedStatus status) {
    checkOwnerOrSuperAdmin(List.of(vacancyOfTheDayId));
    vacancyOfTheDayDao.setPaymentStatus(vacancyOfTheDayId, status);
  }
  
  public VacancyOfTheDayApprovedResponse setStatusApprove(List<Long> vacancyOfTheDayIdList) {
    checkSuperAdmin();

    Set<Long> vacancyOfTheDaysWithoutVacancy = vacancyOfTheDayDao.get(vacancyOfTheDayIdList)
        .stream()
        .filter(it -> it.getVacancyId() == null)
        .map(VacancyOfTheDay::getVacancyOfTheDayId)
        .collect(Collectors.toSet());

    vacancyOfTheDayIdList = vacancyOfTheDayIdList
        .stream()
        .filter(it -> !vacancyOfTheDaysWithoutVacancy.contains(it))
        .collect(Collectors.toList());

    setStatus(vacancyOfTheDayIdList, VacancyOfTheDayStatus.APPROVED);

    return new VacancyOfTheDayApprovedResponse(Map.of(VACANCY_IS_NULL, vacancyOfTheDaysWithoutVacancy));
  }
  
  public void setStatusToModeration(List<Long> vacancyOfTheDayIdList) {
    checkOwnerOrSuperAdmin(vacancyOfTheDayIdList);
    setStatus(vacancyOfTheDayIdList, VacancyOfTheDayStatus.ON_MODERATION);
  }
  
  public void setStatusNotApproved(List<Long> vacancyOfTheDayIdList, String reason) {
    checkSuperAdmin();
    setStatus(vacancyOfTheDayIdList, VacancyOfTheDayStatus.REJECTED);
    for (VacancyOfTheDay vacancyOfTheDay : vacancyOfTheDayDao.get(vacancyOfTheDayIdList)) {
      notificationService.sendEmail(vacancyOfTheDay, reason);
    }
  }

  public void setStatusActive(Long vacancyOfTheDayIdList) {
    checkSuperAdmin();

    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayIdList);
    if (vacancyOfTheDay.getStatus() != VacancyOfTheDayStatus.PAUSED) {
      throw new IllegalArgumentException("Status is not paused, activation is forbidden");
    }

    // Вообще тут должен быть именно статус ACTIVE, но пока его нет- использум APPROVE
    setStatus(List.of(vacancyOfTheDayIdList), VacancyOfTheDayStatus.APPROVED);
  }

  public void setStatusPause(Long vacancyOfTheDayIdList) {
    checkSuperAdmin();

    VacancyOfTheDay vacancyOfTheDay = vacancyOfTheDayDao.get(vacancyOfTheDayIdList);
    if (vacancyOfTheDay.getStatus() != VacancyOfTheDayStatus.APPROVED) {
      throw new IllegalArgumentException("Status is not active, pause is forbidden");
    }

    setStatus(List.of(vacancyOfTheDayIdList), VacancyOfTheDayStatus.PAUSED);
  }
  
  @Transactional
  public void delete(long vacancyOfTheDayId) {
    checkOwnerOrSuperAdmin(List.of(vacancyOfTheDayId));
    vacancyOfTheDayScreenshotDao.deleteAllScreenshots(vacancyOfTheDayId);
    vacancyOfTheDayReservationDao.deleteReservations(vacancyOfTheDayId);
    vacancyOfTheDayDao.delete(vacancyOfTheDayId);
  }
  
  private void setStatus(List<Long> vacancyOfTheDayIdList, VacancyOfTheDayStatus status) {
    vacancyOfTheDayDao.setStatus(vacancyOfTheDayIdList, status);
  }
  
  private void checkOwnerOrSuperAdmin(List<Long> vacancyOfTheDayIdList) {
    securityContext.checkCurrentUserIsBackOffice();
    List<Long> checked = vacancyOfTheDayDao.checkOwner(vacancyOfTheDayIdList, hhSessionContext.getAccount().userId);
    
    if ((checked.size() != vacancyOfTheDayIdList.size()) && !securityContext.currentUserIsSuperAdmin()) {
      throw new SecurityException(String.format("Current user doesn't have rights to change VacancyOfTheDay with ids %s", vacancyOfTheDayIdList));
    }
  }
  
  private void checkSuperAdmin() {
    securityContext.checkCurrentUserIsBackOffice();
    if (!securityContext.currentUserIsSuperAdmin()) {
      throw new SecurityException("Current user should be a super admin for this action, but he doesn't");
    }
  }
}
