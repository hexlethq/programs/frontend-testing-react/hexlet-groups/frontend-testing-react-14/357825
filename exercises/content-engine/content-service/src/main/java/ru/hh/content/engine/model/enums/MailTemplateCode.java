package ru.hh.content.engine.model.enums;

public enum MailTemplateCode {
  MODERATION("ContentVacancyOfTheDayModerationMailTemplate"),
  ;
  private String name;
  
  MailTemplateCode(String name) {
    this.name = name;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
}
