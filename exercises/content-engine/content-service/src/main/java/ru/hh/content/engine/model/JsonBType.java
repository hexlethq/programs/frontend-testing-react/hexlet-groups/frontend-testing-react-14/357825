package ru.hh.content.engine.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.internal.util.ReflectHelper;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

public class JsonBType<T> implements UserType, ParameterizedType, Serializable {
  private static final String JSON = "jsonClass";
  private static final ObjectMapper objectMapper = createObjectMapper();
  private Class<T> jsonClass;

  @Override
  public int[] sqlTypes() {
    return new int[]{Types.JAVA_OBJECT};
  }

  @Override
  public Class<T> returnedClass() {
    return jsonClass;
  }

  @Override
  public boolean equals(Object x, Object y) throws HibernateException {
    return false;
  }

  @Override
  public int hashCode(Object x) throws HibernateException {
    return 0;
  }

  @Override
  public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
      throws HibernateException, SQLException {
    final String cellContent = rs.getString(names[0]);
    if (cellContent == null) {
      return null;
    }
    try {
      return objectMapper.readValue(cellContent.getBytes(StandardCharsets.UTF_8), returnedClass());
    } catch (final Exception ex) {
      throw new RuntimeException("Failed to convert String to Json: " + ex.getMessage(), ex);
    }
  }

  @Override
  public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
      throws HibernateException, SQLException {
    if (value == null) {
      st.setNull(index, Types.OTHER);
      return;
    }
    try {
      final StringWriter w = new StringWriter();
      objectMapper.writeValue(w, value);
      w.flush();
      st.setObject(index, w.toString(), Types.OTHER);
    } catch (final Exception ex) {
      throw new RuntimeException("Failed to convert Json to String: " + ex.getMessage(), ex);
    }
  }

  @Override
  public Object deepCopy(Object value) throws HibernateException {
    try {
      ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
      ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
      objectOutputStream.writeObject(value);
      objectOutputStream.flush();
      objectOutputStream.close();
      byteArrayOutputStream.close();

      return new ObjectInputStream(new ByteArrayInputStream(byteArrayOutputStream.toByteArray())).readObject();
    } catch (ClassNotFoundException | IOException ex) {
      throw new HibernateException(ex);
    }
  }

  @Override
  public boolean isMutable() {
    return false;
  }

  @Override
  public Serializable disassemble(Object value) throws HibernateException {
    return null;
  }

  @Override
  public Object assemble(Serializable cached, Object owner) throws HibernateException {
    return null;
  }

  @Override
  public Object replace(Object original, Object target, Object owner) throws HibernateException {
    return null;
  }

  @Override
  public void setParameterValues(Properties properties) {
    String jsonClassName = properties.getProperty(JSON);
    try {
      jsonClass = ReflectHelper.classForName(jsonClassName, this.getClass());
    } catch (ClassNotFoundException exception) {
      throw new HibernateException("Json class not found", exception);
    }
  }

  private static ObjectMapper createObjectMapper() {
    return new ObjectMapper();
  }
}
