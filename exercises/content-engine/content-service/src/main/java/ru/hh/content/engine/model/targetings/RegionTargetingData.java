package ru.hh.content.engine.model.targetings;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class RegionTargetingData extends TargetingData {
  private Set<Integer> regionIds;

  public RegionTargetingData() {
  }

  public RegionTargetingData(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  public Set<Integer> getRegionIds() {
    return regionIds;
  }

  public void setRegionIds(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    if (regionIds.isEmpty()) {
      return true;
    }
    if (userDataForTargeting.getRegionIds() == null) {
      return false;
    }
    return !Collections.disjoint(regionIds, userDataForTargeting.getRegionIds());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    RegionTargetingData that = (RegionTargetingData) o;
    return Objects.equals(regionIds, that.regionIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(regionIds);
  }

  @Override
  public String toString() {
    return "RegionTargetingData{" +
        "regionIds=" + regionIds +
        '}';
  }
}
