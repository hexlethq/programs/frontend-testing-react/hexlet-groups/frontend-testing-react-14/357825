package ru.hh.content.engine.clients.banner;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import ru.hh.content.engine.clients.banner.dto.ClickmeVacancyDto;
import static ru.hh.content.engine.utils.CommonUtils.createObjectMapper;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.JClientBase;
import ru.hh.jclient.common.RequestBuilder;
import ru.hh.jclient.common.ResultWithStatus;

public class BannerClient extends JClientBase {
  private static final String BANNER_URL = "/api/clickmeVacancyBanners";
  private final ObjectMapper objectMapper;
  private final Integer requestTimeout;
  
  @Inject
  public BannerClient(String host, HttpClientFactory http, int requestTimeout) {
    super(host, http);
    this.objectMapper = createObjectMapper();
    this.requestTimeout = requestTimeout;
  }
  
  public CompletableFuture<ResultWithStatus<ClickmeVacancyDto[]>> getClickmeVacancies() {
    RequestBuilder request = get(jerseyUrl(BANNER_URL))
        .setRequestTimeout(requestTimeout);
    return http.with(request.build())
        .expectJson(objectMapper, ClickmeVacancyDto[].class)
        .resultWithStatus();
  }
}
