package ru.hh.content.engine.vacancy.of.the.day.dto;

import javax.annotation.Nonnull;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

public class VacancyOfTheDayClickDto {
  @HeaderParam("X-Real-IP")
  private String remoteHostIp;

  @HeaderParam("User-Agent")
  private String userAgent;

  @Context
  private UriInfo uriInfo;

  @Nonnull
  @QueryParam("hhuid")
  private String hhuid;

  @QueryParam("hhid")
  private Long hhid;

  @Nonnull
  @QueryParam("host")
  private String host;

  @Nonnull
  @QueryParam("domainAreaId")
  private Integer domainAreaId;

  @Nonnull
  @QueryParam("contentId")
  private Long contentId;

  @Nonnull
  @QueryParam("vacancyId")
  private Long vacancyId;

  @Nonnull
  @QueryParam("placeId")
  private Long placeId;

  @QueryParam("from")
  private String from;

  @QueryParam("utm_medium")
  private String utmMedium;

  @QueryParam("utm_source")
  private String utmSource;

  @QueryParam("utm_campaign")
  private String utmCampaign;

  @QueryParam("utm_local_campaign")
  private String utmLocalCampaign;

  @QueryParam("utm_content")
  private String utmContent;

  public String getRemoteHostIp() {
    return remoteHostIp;
  }

  public void setRemoteHostIp(String remoteHostIp) {
    this.remoteHostIp = remoteHostIp;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public UriInfo getUriInfo() {
    return uriInfo;
  }

  public void setUriInfo(UriInfo uriInfo) {
    this.uriInfo = uriInfo;
  }

  @Nonnull
  public String getHhuid() {
    return hhuid;
  }

  public void setHhuid(@Nonnull String hhuid) {
    this.hhuid = hhuid;
  }

  public Long getHhid() {
    return hhid;
  }

  public void setHhid(Long hhid) {
    this.hhid = hhid;
  }

  @Nonnull
  public String getHost() {
    return host;
  }

  public void setHost(@Nonnull String host) {
    this.host = host;
  }

  @Nonnull
  public Integer getDomainAreaId() {
    return domainAreaId;
  }

  public void setDomainAreaId(@Nonnull Integer domainAreaId) {
    this.domainAreaId = domainAreaId;
  }

  @Nonnull
  public Long getContentId() {
    return contentId;
  }

  public void setContentId(@Nonnull Long contentId) {
    this.contentId = contentId;
  }

  @Nonnull
  public Long getVacancyId() {
    return vacancyId;
  }

  public void setVacancyId(@Nonnull Long vacancyId) {
    this.vacancyId = vacancyId;
  }

  @Nonnull
  public Long getPlaceId() {
    return placeId;
  }

  public void setPlaceId(@Nonnull Long placeId) {
    this.placeId = placeId;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }
  
  public String getUtmMedium() {
    return utmMedium;
  }
  
  public void setUtmMedium(String utmMedium) {
    this.utmMedium = utmMedium;
  }
  
  public String getUtmSource() {
    return utmSource;
  }
  
  public void setUtmSource(String utmSource) {
    this.utmSource = utmSource;
  }
  
  public String getUtmCampaign() {
    return utmCampaign;
  }
  
  public void setUtmCampaign(String utmCampaign) {
    this.utmCampaign = utmCampaign;
  }
  
  public String getUtmLocalCampaign() {
    return utmLocalCampaign;
  }
  
  public void setUtmLocalCampaign(String utmLocalCampaign) {
    this.utmLocalCampaign = utmLocalCampaign;
  }
  
  public String getUtmContent() {
    return utmContent;
  }
  
  public void setUtmContent(String utmContent) {
    this.utmContent = utmContent;
  }
}
