package ru.hh.content.engine.clients.logic;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import ru.hh.content.engine.clients.logic.dto.AddVacancyParamDto;
import ru.hh.content.engine.clients.logic.dto.ScoredVacanciesDto;
import static ru.hh.content.engine.utils.CommonUtils.createObjectMapper;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.JClientBase;
import ru.hh.jclient.common.RequestBuilder;
import ru.hh.jclient.common.ResultWithStatus;

public class LogicClient extends JClientBase {
  private final static String LOGIC_URL = "/vacancy/search/filter";
  private final ObjectMapper objectMapper;
  private final Integer requestTimeout;
  
  @Inject
  public LogicClient(String host, HttpClientFactory http, int requestTimeout) {
    super(host, http);
    this.objectMapper = createObjectMapper();
    this.requestTimeout = requestTimeout;
  }
  
  public CompletableFuture<ResultWithStatus<ScoredVacanciesDto>> scoreVacancies(Set<Integer> vacancies, int limit) {
    RequestBuilder request = post(jerseyUrl(LOGIC_URL))
        .addQueryParam("limit", String.valueOf(limit))
        .setJsonBody(objectMapper, new AddVacancyParamDto(vacancies))
        .setRequestTimeout(requestTimeout);
    return http.with(request.build())
        .expectJson(objectMapper, ScoredVacanciesDto.class)
        .resultWithStatus();
  }
}
