package ru.hh.content.engine.vacancy.of.the.day.dto.view;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class VacancyAreaDto {
  @XmlAttribute
  private Integer id;

  public VacancyAreaDto() {
  }

  public VacancyAreaDto(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
