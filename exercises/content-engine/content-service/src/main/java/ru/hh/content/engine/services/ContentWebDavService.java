package ru.hh.content.engine.services;

import java.math.BigInteger;
import java.util.UUID;
import javax.inject.Inject;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.webdav.WebDavService;

public class ContentWebDavService {
  public static final String FILE_PREFIX = "content-engine/";
  public static final String PUBLIC_URL_SETTING = "webdav.public.file.baseUrl";

  private final WebDavService webDavService;
  private final FileSettings fileSettings;

  @Inject
  public ContentWebDavService(WebDavService webDavService, FileSettings fileSettings) {
    this.webDavService = webDavService;
    this.fileSettings = fileSettings;
  }

  public void saveFile(String filename, byte[] bytes) {
    webDavService.storeData(String.format("%s%s", FILE_PREFIX, filename), bytes);
  }
  
  public static BigInteger getRandomBigIntegerFromUUID() {
    return new BigInteger(
        UUID
            .randomUUID()
            .toString()
            .toUpperCase()
            .replace("-", ""), 16
    );
  }
  
  public String getFullFileUrl(String filename) {
    String cdnUrl = fileSettings.getString(PUBLIC_URL_SETTING);
    return cdnUrl + "/" + FILE_PREFIX + filename;
  }
}
