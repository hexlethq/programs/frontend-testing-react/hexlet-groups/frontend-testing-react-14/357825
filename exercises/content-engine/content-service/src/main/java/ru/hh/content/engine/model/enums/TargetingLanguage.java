package ru.hh.content.engine.model.enums;

public enum TargetingLanguage {
  ENGLISH,
  RUSSIAN,
  GERMAN,
  FRENCH,
  ;
}
