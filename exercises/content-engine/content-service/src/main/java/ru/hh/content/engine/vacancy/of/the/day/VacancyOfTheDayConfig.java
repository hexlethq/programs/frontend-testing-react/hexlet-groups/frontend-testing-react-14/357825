package ru.hh.content.engine.vacancy.of.the.day;

import java.util.List;
import java.util.Map;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.content.engine.clients.banner.dto.VacancyUrlDto;
import ru.hh.content.engine.packets.PacketService;
import ru.hh.content.engine.services.VacancyService;
import ru.hh.content.engine.statistics.CHStatisticsDao;
import ru.hh.content.engine.statistics.StatisticXlsService;
import ru.hh.content.engine.utils.TestEnvironmentCondition;
import ru.hh.content.engine.utils.cache.CacheService;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayDao;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayReservationDao;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayScreenshotDao;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayViewDto;
import ru.hh.content.engine.vacancy.of.the.day.resource.VacancyOfTheDayClickResource;
import ru.hh.content.engine.vacancy.of.the.day.resource.VacancyOfTheDayResource;
import ru.hh.content.engine.vacancy.of.the.day.resource.VacancyOfTheDayStatisticResource;
import ru.hh.content.engine.vacancy.of.the.day.resource.VacancyOfTheDayViewResource;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayActionsService;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayScreenshotService;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayService;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayStatisticService;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayViewService;
import ru.hh.settings.SettingsClient;

@Configuration
@Import({
    VacancyOfTheDayResource.class,
    VacancyOfTheDayViewResource.class,
    VacancyOfTheDayClickResource.class,

    VacancyOfTheDayService.class,
    VacancyOfTheDayActionsService.class,
    VacancyOfTheDayScreenshotService.class,
    VacancyOfTheDayViewService.class,

    VacancyOfTheDayDao.class,
    VacancyOfTheDayReservationDao.class,
    VacancyOfTheDayScreenshotDao.class,
})
public class VacancyOfTheDayConfig {
  public static final String VACANCY_OF_THE_DAY_CACHE_TTL_SETTING_NAME = "vacancy_of_the_day.cache.ttl.ms";
  public static final String CLICKME_VACANCY_BANNERS_CACHE_TTL_SETTING_NAME = "vacancy_of_the_day.clickme.vacancy.banners.cache.ttl.ms";

  @Bean
  public CacheService<List<VacancyOfTheDayViewDto>> vacancyOfTheDayViewServiceCacheService(SettingsClient settingsClient) {
    return new CacheService<>(settingsClient, VACANCY_OF_THE_DAY_CACHE_TTL_SETTING_NAME);
  }

  @Bean
  public CacheService<Map<Integer, VacancyUrlDto>> clickmeVacanciesCacheService(SettingsClient settingsClient) {
    return new CacheService<>(settingsClient, CLICKME_VACANCY_BANNERS_CACHE_TTL_SETTING_NAME);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public VacancyOfTheDayStatisticResource vacancyOfTheDayStatisticResource(
      CHStatisticsDao statisticsDao,
      StatisticXlsService statisticXlsService,
      VacancyOfTheDayDao vacancyOfTheDayDao,
      VacancyService vacancyService,
      VacancyOfTheDayStatisticService vacancyOfTheDayStatisticService
  ) {
    return new VacancyOfTheDayStatisticResource(
        statisticsDao,
        statisticXlsService,
        vacancyOfTheDayDao,
        vacancyService,
        vacancyOfTheDayStatisticService
    );
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public VacancyOfTheDayStatisticService vacancyOfTheDayStatisticService(
      CHStatisticsDao statisticsDao, VacancyOfTheDayService vacancyOfTheDayService, PacketService packetService) {
    return new VacancyOfTheDayStatisticService(statisticsDao, vacancyOfTheDayService, packetService);
  }
}
