package ru.hh.content.engine.work.in.company.dto;

public class WorkInCompanyUserData {
  private int id;
  private String name;
  private boolean superAdmin;

  public WorkInCompanyUserData() {
  }

  public WorkInCompanyUserData(int id, String name, boolean superAdmin) {
    this.id = id;
    this.name = name;
    this.superAdmin = superAdmin;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isSuperAdmin() {
    return superAdmin;
  }

  public void setSuperAdmin(boolean superAdmin) {
    this.superAdmin = superAdmin;
  }
}
