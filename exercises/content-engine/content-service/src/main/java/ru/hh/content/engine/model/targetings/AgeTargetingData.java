package ru.hh.content.engine.model.targetings;

import java.util.Objects;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class AgeTargetingData extends TargetingData {
  private Integer minAge;
  private Integer maxAge;
  private boolean includeAnonymousAge;

  public AgeTargetingData() {
  }

  public AgeTargetingData(Integer minAge, Integer maxAge, boolean includeAnonymousAge) {
    this.minAge = minAge;
    this.maxAge = maxAge;
    this.includeAnonymousAge = includeAnonymousAge;
  }

  public Integer getMinAge() {
    return minAge;
  }

  public void setMinAge(Integer minAge) {
    this.minAge = minAge;
  }

  public Integer getMaxAge() {
    return maxAge;
  }

  public void setMaxAge(Integer maxAge) {
    this.maxAge = maxAge;
  }

  public boolean isIncludeAnonymousAge() {
    return includeAnonymousAge;
  }

  public void setIncludeAnonymousAge(boolean includeAnonymousAge) {
    this.includeAnonymousAge = includeAnonymousAge;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    Integer age = userDataForTargeting.getAge();
    if (age == null) {
      return includeAnonymousAge;
    }
    return (minAge == null || age > minAge) && (maxAge == null || age < maxAge);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    AgeTargetingData that = (AgeTargetingData) o;
    return includeAnonymousAge == that.includeAnonymousAge &&
        Objects.equals(minAge, that.minAge) &&
        Objects.equals(maxAge, that.maxAge);
  }

  @Override
  public int hashCode() {
    return Objects.hash(minAge, maxAge, includeAnonymousAge);
  }

  @Override
  public String toString() {
    return "AgeTargetingData{" +
        "minAge=" + minAge +
        ", maxAge=" + maxAge +
        '}';
  }
}
