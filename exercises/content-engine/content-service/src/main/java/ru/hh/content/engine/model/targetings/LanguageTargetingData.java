package ru.hh.content.engine.model.targetings;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.model.enums.TargetingLanguage;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class LanguageTargetingData extends TargetingData {
  private Set<TargetingLanguage> languages;

  public LanguageTargetingData() {
  }

  public LanguageTargetingData(Set<TargetingLanguage> languages) {
    this.languages = languages;
  }

  public Set<TargetingLanguage> getLanguages() {
    return languages;
  }

  public void setLanguages(Set<TargetingLanguage> languages) {
    this.languages = languages;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    if (languages.isEmpty() || userDataForTargeting.getLanguages() == null) {
      return true;
    }

    return !Collections.disjoint(languages, userDataForTargeting.getLanguages());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    LanguageTargetingData that = (LanguageTargetingData) o;
    return Objects.equals(languages, that.languages);
  }

  @Override
  public int hashCode() {
    return Objects.hash(languages);
  }

  @Override
  public String toString() {
    return "LanguageTargetingData{" +
        "languages=" + languages +
        '}';
  }
}
