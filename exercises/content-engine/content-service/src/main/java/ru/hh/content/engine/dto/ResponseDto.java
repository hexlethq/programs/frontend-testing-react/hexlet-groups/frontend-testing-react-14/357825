package ru.hh.content.engine.dto;

public class ResponseDto {
  private String code;
  
  public ResponseDto(String code) {
    this.code = code;
  }
  
  public String getCode() {
    return code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
}
