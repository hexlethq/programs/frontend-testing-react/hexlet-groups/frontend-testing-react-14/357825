package ru.hh.content.engine.clients.xmlback;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import org.apache.commons.lang3.StringUtils;
import static ru.hh.content.engine.utils.CommonUtils.createObjectMapper;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.JClientBase;
import ru.hh.jclient.common.RequestBuilder;
import ru.hh.jclient.common.ResultWithStatus;
import ru.hh.nab.common.properties.FileSettings;

public class EmployerClient extends JClientBase {
  private static final String GET_EMPLOYER_INFO_URL = "/rs/employer/{employerId:[\\d]+}/short";
  private static final String GET_EMPLOYERS_LIST_INFO_URL = "/rs/employer/list/{employerIds:[\\d,]+}";
  
  private final ObjectMapper objectMapper;
  
  @Inject
  public EmployerClient(FileSettings fileSettings,
                        HttpClientFactory xmlBackHttpClientFactory) {
    super(fileSettings.getString("jclient.hh-xmlback.internalEndpointUrl"), xmlBackHttpClientFactory);
    this.objectMapper = createObjectMapper();
  }
  
  public CompletableFuture<ResultWithStatus<EmployerInfoResponse>> getEmployerInfo(int employerId) {
    RequestBuilder request = get(jerseyUrl(GET_EMPLOYER_INFO_URL, employerId));
    return http.with(request.build())
        .expectJson(objectMapper, EmployerInfoResponse.class)
        .resultWithStatus();
  }
  
  public CompletableFuture<ResultWithStatus<EmployerInfoListResponse>> getEmployerListByIds(Collection<Integer> employerIds) {
    RequestBuilder request = get(jerseyUrl(GET_EMPLOYERS_LIST_INFO_URL, StringUtils.join(employerIds, ",")));
    return http.with(request.build())
        .expectJson(objectMapper, EmployerInfoListResponse.class)
        .resultWithStatus();
  }
}
