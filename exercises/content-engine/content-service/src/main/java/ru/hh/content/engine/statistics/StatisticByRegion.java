package ru.hh.content.engine.statistics;

public class StatisticByRegion {
  private String regionName;
  private StatisticRow statisticRow;

  public StatisticByRegion() {
  }

  public StatisticByRegion(String regionName, StatisticRow statisticRow) {
    this.regionName = regionName;
    this.statisticRow = statisticRow;
  }

  public String getRegionName() {
    return regionName;
  }

  public void setRegionName(String regionName) {
    this.regionName = regionName;
  }

  public StatisticRow getStatisticRow() {
    return statisticRow;
  }

  public void setStatisticRow(StatisticRow statisticRow) {
    this.statisticRow = statisticRow;
  }
}
