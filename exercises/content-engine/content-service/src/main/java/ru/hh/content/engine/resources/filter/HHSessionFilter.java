package ru.hh.content.engine.resources.filter;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.hh.session.client.BadTokenException;
import ru.hh.hh.session.client.HhSessionClientFactory;
import ru.hh.hh.session.client.RequestDetails;
import ru.hh.hh.session.client.Session;

public class HHSessionFilter implements Filter {
  private static final Logger LOGGER = LoggerFactory.getLogger(HHSessionFilter.class);
  private final HhSessionClientFactory hhSessionClientFactory;
  private final HHSessionContext hhSessionContext;

  @Inject
  public HHSessionFilter(HhSessionClientFactory hhSessionClientFactory,
                         HHSessionContext hhSessionContext) {
    this.hhSessionClientFactory = hhSessionClientFactory;
    this.hhSessionContext = hhSessionContext;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    Session session = getSession(httpRequest);

    try {
      hhSessionContext.runInContext(
          session,
          () -> {
            chain.doFilter(request, response);
            return null;
          }
      );
    } catch (IOException | ServletException | RuntimeException e) {
      throw e;
    } catch (Exception e) {
      throw new ServletException(e);
    }
  }

  private Session getSession(HttpServletRequest httpRequest) {
    RequestDetails requestDetails = RequestDetails.of(httpRequest);
    Session session = null;
    try {
      session = hhSessionClientFactory.getNormalClient(requestDetails).session();
    } catch (BadTokenException | RuntimeException e) {
      LOGGER.warn("Failed to parse HhSession from header.", e);
    }
    return session;
  }
}
