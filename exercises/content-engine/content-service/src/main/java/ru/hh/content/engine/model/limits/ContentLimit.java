package ru.hh.content.engine.model.limits;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import ru.hh.content.engine.model.enums.ContentLimitType;

@Entity
@Table(name = "content_limit")
public class ContentLimit implements Serializable {
  @Id
  @Column(name = "content_limit_id", nullable = false)
  private Long contentLimitId;

  @Id
  @Column(name = "content_limit_type")
  @Enumerated(value = STRING)
  private ContentLimitType contentLimitType;

  public ContentLimit() {
  }

  public ContentLimit(Long contentLimitId, ContentLimitType contentLimitType) {
    this.contentLimitId = contentLimitId;
    this.contentLimitType = contentLimitType;
  }

  public Long getContentLimitId() {
    return contentLimitId;
  }

  public void setContentLimitId(Long contentLimitId) {
    this.contentLimitId = contentLimitId;
  }

  public ContentLimitType getContentLimitType() {
    return contentLimitType;
  }

  public void setContentLimitType(ContentLimitType contentLimitType) {
    this.contentLimitType = contentLimitType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ContentLimit that = (ContentLimit) o;
    return Objects.equals(contentLimitId, that.contentLimitId) &&
        contentLimitType == that.contentLimitType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(contentLimitId, contentLimitType);
  }

  @Override
  public String toString() {
    return "ContentLimit{" +
        "contentLimitId=" + contentLimitId +
        ", contentLimitType=" + contentLimitType +
        '}';
  }
}
