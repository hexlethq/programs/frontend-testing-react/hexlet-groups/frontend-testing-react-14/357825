package ru.hh.content.engine.services;

import java.text.MessageFormat;
import javax.inject.Inject;
import ru.hh.hh.session.client.HhLocale;
import ru.hh.nab.metrics.StatsDSender;
import ru.hh.translations2.Metrics;
import ru.hh.translations2.TranslationKey;
import static ru.hh.translations2.Translations.DEFAULT_LANG;
import static ru.hh.translations2.Translations.DEFAULT_SITE_ID;
import static ru.hh.translations2.Translations.getStub;

public class Translations {
  private final ru.hh.translations2.Translations translations;

  public Translations() {
    this.translations = null;
  }

  @Inject
  public Translations(String path,
                      int checkInterval,
                      String serviceName,
                      StatsDSender statsDSender) {
    this.translations = new ru.hh.translations2.Translations(path, checkInterval);
    Metrics.register(this.translations, serviceName, statsDSender);
  }

  public String getTranslation(String name, HhLocale locale) {
    return getTranslation(name, locale.siteId, locale.lang);
  }

  public String getTranslation(TranslationKey key) {
    return getTranslation(key.getName(), key.getSiteId(), key.getLang());
  }

  public String getTranslation(String name, int siteId, String lang) {
    String result = getTranslationWithoutFallback(name, siteId, lang);
    return result != null ? result : getStub(name, siteId, lang);
  }

  public String getTranslationWithoutFallback(String name, HhLocale locale) {
    return getTranslationWithoutFallback(name, locale.siteId, locale.lang);
  }

  public String getTranslationWithoutFallback(String name, int siteId, String lang) {
    return translations.getTranslation(name, siteId, lang);
  }

  public String getDefaultTranslation(String name) {
    return translations.getTranslation(name, DEFAULT_SITE_ID, DEFAULT_LANG);
  }

  public static String format(String value, Object... params) {
    return MessageFormat.format(value.replace("'", "''"), params);
  }
}
