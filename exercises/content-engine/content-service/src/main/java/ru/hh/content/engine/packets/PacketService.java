package ru.hh.content.engine.packets;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.PacketType;

public class PacketService {
  private final PacketDao packetDao;

  public PacketService(PacketDao packetDao) {
    this.packetDao = packetDao;
  }

  @Transactional(readOnly = true)
  public List<PacketDto> getPackets() {
    return Arrays.stream(PacketType.values())
        .map(packetType -> new PacketDto(packetType, packetType.getTranslation(), packetType.getCanChooseRegions()))
        .collect(Collectors.toList());
  }

  @Transactional(readOnly = true)
  public Set<Integer> getRegionsForPacket(PacketType packetType) {
    return new HashSet<>(packetDao.getRegionsByPacketType(packetType));
  }
}
