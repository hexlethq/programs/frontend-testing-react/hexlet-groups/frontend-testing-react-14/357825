package ru.hh.content.engine.clients.xmlback.dto;

public class EmployerDepartmentDto {
  private String name;
  private String code;

  public EmployerDepartmentDto() {
  }

  public EmployerDepartmentDto(String name, String code) {
    this.name = name;
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
