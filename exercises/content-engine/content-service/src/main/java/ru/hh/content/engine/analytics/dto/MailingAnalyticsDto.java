package ru.hh.content.engine.analytics.dto;

import java.time.LocalDate;

public class MailingAnalyticsDto {
  private long views;
  private LocalDate eventDate;
  private String utmCampaign;
  private String utmMedium;

  public MailingAnalyticsDto() {
  }

  public MailingAnalyticsDto(long views, LocalDate eventDate, String utmCampaign, String utmMedium) {
    this.views = views;
    this.eventDate = eventDate;
    this.utmCampaign = utmCampaign;
    this.utmMedium = utmMedium;
  }

  public long getViews() {
    return views;
  }

  public void setViews(long views) {
    this.views = views;
  }

  public LocalDate getEventDate() {
    return eventDate;
  }

  public void setEventDate(LocalDate eventDate) {
    this.eventDate = eventDate;
  }

  public String getUtmCampaign() {
    return utmCampaign;
  }

  public void setUtmCampaign(String utmCampaign) {
    this.utmCampaign = utmCampaign;
  }

  public String getUtmMedium() {
    return utmMedium;
  }

  public void setUtmMedium(String utmMedium) {
    this.utmMedium = utmMedium;
  }
}
