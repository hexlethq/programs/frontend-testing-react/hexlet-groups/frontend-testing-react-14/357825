package ru.hh.content.engine.clients.xmlback.dto;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "vacancies")
public class VacancyIdsList {

  @XmlElement(name = "vacancy")
  private List<VacancyId> vacancyIds;

  public VacancyIdsList() {
  }

  public VacancyIdsList(List<VacancyId> vacancyIds) {
    this.vacancyIds = vacancyIds;
  }

  public List<VacancyId> getVacancyIds() {
    return vacancyIds;
  }

  public void setVacancyIds(List<VacancyId> vacancyIds) {
    this.vacancyIds = vacancyIds;
  }
}
