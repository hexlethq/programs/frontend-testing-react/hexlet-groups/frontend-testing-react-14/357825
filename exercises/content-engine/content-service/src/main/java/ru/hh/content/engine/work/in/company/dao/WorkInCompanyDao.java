package ru.hh.content.engine.work.in.company.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.WorkInCompanyStatus;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;

public class WorkInCompanyDao {
  private final SessionFactory sessionFactory;

  public WorkInCompanyDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public WorkInCompany saveWorkInCompany(WorkInCompany workInCompany) {
    sessionFactory.getCurrentSession().save(workInCompany);
    return workInCompany;
  }

  public WorkInCompany updateWorkInCompany(WorkInCompany workInCompany) {
    sessionFactory.getCurrentSession().update(workInCompany);
    return workInCompany;
  }

  @Transactional(readOnly = true)
  public WorkInCompany getWorkInCompany(long workInCompanyId) {
    return sessionFactory.getCurrentSession().get(WorkInCompany.class, workInCompanyId);
  }

  @Transactional(readOnly = true)
  public List<WorkInCompany> getTodayEmployersWithLogos(LocalDate now, List<Integer> regionIds, TargetingDeviceType deviceType) {
    return sessionFactory.getCurrentSession()
        .createQuery("Select distinct wic from WorkInCompany wic left join fetch wic.workInCompanyLogo" +
                " join WorkInCompanyReservation reserv on wic.workInCompanyId = reserv.workInCompanyId " +
                " where :now between reserv.dateStart and reserv.dateEnd and reserv.regionId in :regionIds and reserv.deviceType = :deviceType " +
                " and wic.status = :activeStatus",
            WorkInCompany.class)
        .setParameter("now", now)
        .setParameter("regionIds", regionIds)
        .setParameter("deviceType", deviceType)
        .setParameter("activeStatus", WorkInCompanyStatus.ACTIVE)
        .list();
  }

  @Transactional(readOnly = true)
  public List<Long> getWorkInCompaniesExcludedForRegion(LocalDate now, List<Integer> regionIds) {
    return sessionFactory.getCurrentSession()
        .createQuery("Select distinct wic.workInCompanyId from WorkInCompany wic" +
                " join WorkInCompanyReservation reserv on wic.workInCompanyId = reserv.workInCompanyId " +
                " where :now between reserv.dateStart and reserv.dateEnd and reserv.regionId in :regionIds and reserv.isExclusion = true",
            Long.class)
        .setParameter("now", now)
        .setParameter("regionIds", regionIds)
        .list();
  }

  public List<WorkInCompany> getWorkInCompaniesWithDatesByPeriodAndEmployer(Integer employerId,
                                                                            LocalDate dateFrom,
                                                                            LocalDate dateTo,
                                                                            Set<TargetingDeviceType> deviceTypes,
                                                                            Set<WorkInCompanyStatus> statuses) {
    Query<WorkInCompany> query = sessionFactory.getCurrentSession()
        .createQuery("select wic " +
            "from WorkInCompanyReservation wicr join WorkInCompany wic on wic.workInCompanyId = wicr.workInCompanyId " +
            buildFilterSql(employerId, deviceTypes, statuses) +
            " group by wic having ((min(wicr.dateStart) >= :dateFrom and min(wicr.dateStart) <= :dateTo) " +
            "or (max(wicr.dateEnd) >= :dateFrom and max(wicr.dateEnd) <= :dateTo)" +
            " or (min(wicr.dateStart) <= :dateFrom and max(wicr.dateEnd) >= :dateTo))", WorkInCompany.class)
        .setParameter("dateFrom", dateFrom)
        .setParameter("dateTo", dateTo);

    query = addFilterParams(employerId, deviceTypes, statuses, query);

    return query.list();
  }

  private Query<WorkInCompany> addFilterParams(Integer employerId,
                                               Set<TargetingDeviceType> deviceTypes,
                                               Set<WorkInCompanyStatus> statuses,
                                               Query<WorkInCompany> query) {
    if (employerId != null) {
      query = query.setParameter("employerId", employerId);
    }

    if (deviceTypes != null && !deviceTypes.isEmpty()) {
      query = query.setParameter("deviceTypes", deviceTypes);
    }

    if (statuses != null && !statuses.isEmpty()) {
      query = query.setParameter("statuses", statuses);
    }
    return query;
  }

  private String buildFilterSql(Integer employerId, Set<TargetingDeviceType> deviceTypes, Set<WorkInCompanyStatus> statuses) {
    String employerFilter = "";
    String deviceTypesFilter = "";
    String statusesFilter = "";

    boolean notFirstConditionFlag = false;

    if (employerId != null) {
      employerFilter += " where wic.employerId = :employerId";
      notFirstConditionFlag = true;
    }

    if (deviceTypes != null && !deviceTypes.isEmpty()) {
      if (notFirstConditionFlag) {
        deviceTypesFilter = " and";
      } else {
        deviceTypesFilter = " where";
      }

      deviceTypesFilter += " wicr.deviceType in :deviceTypes";
      notFirstConditionFlag = true;
    }

    if (statuses != null && !statuses.isEmpty()) {
      if (notFirstConditionFlag) {
        statusesFilter = " and";
      } else {
        statusesFilter = " where";
      }

      statusesFilter += " wic.status in :statuses";
    }

    return employerFilter + deviceTypesFilter + statusesFilter;
  }

  public void setWorkInCompanyLogo(int workInCompanyId, int logoId) {
    sessionFactory.getCurrentSession()
        .createQuery("UPDATE WorkInCompany " +
            "SET workInCompanyLogoId=:logoId" +
            " where workInCompanyId=:workInCompanyId ")
        .setParameter("workInCompanyId", workInCompanyId)
        .setParameter("logoId", logoId)
        .executeUpdate();
  }

  public WorkInCompany getById(long workInCompanyId) {
    return sessionFactory.getCurrentSession().get(WorkInCompany.class, workInCompanyId);
  }
}
