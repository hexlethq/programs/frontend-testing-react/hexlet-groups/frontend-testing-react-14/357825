package ru.hh.content.engine.model.enums;

public enum TargetingUserType {
  ANONYMOUS,
  APPLICANT,
  EMPLOYER,
  AGENCY,
  ;
}
