package ru.hh.content.engine.security;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.NotAuthorizedException;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.hh.session.client.HhUser;
import ru.hh.hh.session.client.Session;
import ru.hh.hh.session.client.UserType;
import ru.hh.settings.SettingsClient;

public class SecurityContext {
  public static final String ADMIN_USER_IDS_SETTING_NAME = "admin.userIds";

  private final HHSessionContext sessionContext;
  private final SettingsClient settingsClient;

  @Inject
  public SecurityContext(HHSessionContext sessionContext, SettingsClient settingsClient) {
    this.sessionContext = sessionContext;
    this.settingsClient = settingsClient;
  }

  public boolean currentUserIsSuperAdmin() {
    HhUser account = sessionContext.getAccount();

    Set<Integer> adminUserIds = Arrays.stream(settingsClient.getString(ADMIN_USER_IDS_SETTING_NAME).orElseThrow().split(","))
        .map(Integer::valueOf)
        .collect(Collectors.toSet());

    return adminUserIds.contains(account.userId);
  }

  public void checkUserIsCurrentOrAdmin(int userId) {
    HhUser account = sessionContext.getAccount();
    if (account == null) {
      throw new SecurityException(String.format("User %s not found", userId));
    }

    if (account.userId == userId) {
      return;
    }

    if(!currentUserIsSuperAdmin()) {
      throw new SecurityException(String.format("User %s is not current or admin", userId));
    }
  }

  public void checkCurrentUserIsBackOffice() {
    Session session = sessionContext.getSession();
    if (session == null
        || session.getHhid() == null
        || session.hhSession.account == null) {
      throw new NotAuthorizedException("Session is null");
    }

    if (session.hhSession.account.type != UserType.back_office_user || isAdminUnderExistedEmployer(session)) {
      throw new ForbiddenException();
    }
  }

  private boolean isAdminUnderExistedEmployer(Session session) {
    return session.hhSession.actualAccount != null
        && session.hhSession.actualAccount.type == UserType.back_office_user;
  }
}
