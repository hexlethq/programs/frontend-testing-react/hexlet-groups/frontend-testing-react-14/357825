package ru.hh.content.engine.vacancy.of.the.day.resource;

import java.time.LocalDate;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.vacancy.of.the.day.VacancyOfTheDayPaths.VACANCY_OF_THE_DAY_VIEW_ROOT;
import ru.hh.content.engine.dto.ImpressionRequestDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayFromPlace;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayViewDtoList;
import ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayViewService;

@Path(VACANCY_OF_THE_DAY_VIEW_ROOT)
public class VacancyOfTheDayViewResource {
  private final VacancyOfTheDayViewService vacancyOfTheDayViewService;

  public VacancyOfTheDayViewResource(VacancyOfTheDayViewService vacancyOfTheDayViewService) {
    this.vacancyOfTheDayViewService = vacancyOfTheDayViewService;
  }

  @GET
  @Produces(MediaType.APPLICATION_XML)
  public Response getToView(@BeanParam ImpressionRequestDto impressionRequestDto,
                            @QueryParam("fromPlace") VacancyOfTheDayFromPlace vacancyOfTheDayFromPlace) {
    return Response.ok(
        new VacancyOfTheDayViewDtoList(
            vacancyOfTheDayViewService.getVacanciesToView(LocalDate.now(), impressionRequestDto, vacancyOfTheDayFromPlace)
        )
    ).build();
  }
}
