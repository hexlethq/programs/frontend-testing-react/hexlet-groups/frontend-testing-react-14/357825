package ru.hh.content.engine.vacancy.of.the.day.dto.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class VacancyOfTheDayCompensationDto {
  private Integer from;
  private Integer to;
  private CompensationCurrency currencyCode;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Boolean noCompensation;

  public VacancyOfTheDayCompensationDto() {
  }

  public VacancyOfTheDayCompensationDto(Integer from, Integer to, CompensationCurrency currencyCode) {
    this.from = from;
    this.to = to;
    this.currencyCode = currencyCode;
  }

  public VacancyOfTheDayCompensationDto(Boolean noCompensation) {
    this.noCompensation = noCompensation;
  }
}
