package ru.hh.content.engine.analytics.dto;

import java.util.List;

public class MailingAnalyticsListDto {
  private final List<MailingIntervalDto> types;

  public MailingAnalyticsListDto(List<MailingIntervalDto> types) {
    this.types = types;
  }

  public List<MailingIntervalDto> getTypes() {
    return types;
  }
}
