package ru.hh.content.engine.work.in.company.resource;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_EMPLOYER_ROOT;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.EmployerService;

@Path(WORK_IN_COMPANY_EMPLOYER_ROOT)
public class WorkInCompanyEmployerResource {
  private final EmployerService employerService;
  private final SecurityContext securityContext;

  public WorkInCompanyEmployerResource(EmployerService employerService, SecurityContext securityContext) {
    this.employerService = employerService;
    this.securityContext = securityContext;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response get(@QueryParam("employerId") Integer employerId) {
    securityContext.checkCurrentUserIsBackOffice();
    if (employerId == null) {
      return Response.status(Response.Status.BAD_REQUEST).build();
    }
    return Response.ok(employerService.getEmployerInfo(employerId)).build();
  }
}
