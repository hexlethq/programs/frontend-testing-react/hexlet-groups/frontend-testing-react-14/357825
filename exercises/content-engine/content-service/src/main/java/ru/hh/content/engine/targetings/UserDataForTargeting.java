package ru.hh.content.engine.targetings;

import java.util.Set;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.TargetingGender;
import ru.hh.content.engine.model.enums.TargetingLanguage;
import ru.hh.content.engine.model.enums.TargetingUserType;

public class UserDataForTargeting {
  private Integer age;
  private TargetingGender gender;
  private TargetingDeviceType deviceType;
  private Long salary;
  private Long predictedSalary;
  private Set<Integer> specializationIds;
  private Set<Integer> regionIds;
  private Set<Integer> profAreaIds;
  private Set<TargetingLanguage> languages;
  private TargetingUserType userType;

  public Integer getAge() {
    return age;
  }

  public void setAge(Integer age) {
    this.age = age;
  }

  public TargetingDeviceType getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(TargetingDeviceType deviceType) {
    this.deviceType = deviceType;
  }

  public TargetingGender getGender() {
    return gender;
  }

  public void setGender(TargetingGender gender) {
    this.gender = gender;
  }

  public Set<TargetingLanguage> getLanguages() {
    return languages;
  }

  public void setLanguages(Set<TargetingLanguage> languages) {
    this.languages = languages;
  }

  public Set<Integer> getProfAreaIds() {
    return profAreaIds;
  }

  public void setProfAreaIds(Set<Integer> profAreaIds) {
    this.profAreaIds = profAreaIds;
  }

  public Set<Integer> getRegionIds() {
    return regionIds;
  }

  public void setRegionIds(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  public Long getSalary() {
    return salary;
  }

  public void setSalary(Long salary) {
    this.salary = salary;
  }

  public Long getPredictedSalary() {
    return predictedSalary;
  }

  public void setPredictedSalary(Long predictedSalary) {
    this.predictedSalary = predictedSalary;
  }

  public Set<Integer> getSpecializationIds() {
    return specializationIds;
  }

  public void setSpecializationIds(Set<Integer> specializationIds) {
    this.specializationIds = specializationIds;
  }

  public TargetingUserType getUserType() {
    return userType;
  }

  public void setUserType(TargetingUserType userType) {
    this.userType = userType;
  }
}
