package ru.hh.content.engine.packets;

import java.util.List;
import javax.inject.Inject;
import org.hibernate.SessionFactory;
import ru.hh.content.engine.model.enums.PacketType;

public class PacketDao {
  private final SessionFactory sessionFactory;

  @Inject
  public PacketDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public List<Integer> getRegionsByPacketType(PacketType packetType) {
    return sessionFactory.getCurrentSession()
        .createQuery("SELECT regionId FROM PacketToRegion where packetType = :packetType", Integer.class)
        .setParameter("packetType", packetType)
        .list();
  }
}
