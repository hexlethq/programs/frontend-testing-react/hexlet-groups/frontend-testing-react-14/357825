package ru.hh.content.engine.vacancy.of.the.day.dto.view;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

@XmlAccessorType(XmlAccessType.FIELD)
public class VacancyOfTheDayEmployerViewDto {
  private Integer id;
  private String visibleName;
  @JsonInclude(JsonInclude.Include.NON_NULL)
  private VacancyOfTheDayEmployerDepartmentDto department;

  public VacancyOfTheDayEmployerViewDto() {
  }

  public VacancyOfTheDayEmployerViewDto(Integer id, String visibleName, VacancyOfTheDayEmployerDepartmentDto department) {
    this.id = id;
    this.visibleName = visibleName;
    this.department = department;
  }
}
