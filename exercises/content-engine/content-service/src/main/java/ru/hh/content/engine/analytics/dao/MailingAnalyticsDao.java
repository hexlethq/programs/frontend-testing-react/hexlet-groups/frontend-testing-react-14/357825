package ru.hh.content.engine.analytics.dao;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.hh.content.engine.analytics.dto.MailingAnalyticsDto;

public class MailingAnalyticsDao {
  private final NamedParameterJdbcTemplate clickHouseNamedJdbcTemplate;

  @Inject
  public MailingAnalyticsDao(NamedParameterJdbcTemplate clickHouseNamedJdbcTemplate) {
    this.clickHouseNamedJdbcTemplate = clickHouseNamedJdbcTemplate;
  }

  public List<MailingAnalyticsDto> getEmployerMailingAnalytics(int vacancyId,
                                                               List<String> utmList,
                                                               LocalDate periodStart,
                                                               LocalDate periodEnd,
                                                               int viewsThreshold) {
    return clickHouseNamedJdbcTemplate.query(
        "select count(event_type) as views, event_date, utm_campaign, utm_medium " +
            "from applicant_action_distr " +
            "where vacancy_id = :vacancyId " +
            "and event_type = 'VACANCY_VIEW' " +
            "and utm_source = 'HeadHunter' " +
            "and utm_medium in (:utmList) " +
            "and event_date between :periodStart and :periodEnd " +
            "group by event_date, utm_campaign, utm_medium",
        Map.of("vacancyId", vacancyId,
            "utmList", utmList,
            "periodStart", periodStart,
            "periodEnd", periodEnd,
            "viewsThreshold", viewsThreshold),
        new BeanPropertyRowMapper<>(MailingAnalyticsDto.class));
  }
}
