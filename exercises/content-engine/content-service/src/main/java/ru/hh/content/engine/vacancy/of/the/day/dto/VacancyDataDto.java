package ru.hh.content.engine.vacancy.of.the.day.dto;

import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;

public class VacancyDataDto {
  private int id;
  private String name;
  
  public VacancyDataDto() {
  }
  
  public VacancyDataDto(int id, String name) {
    this.id = id;
    this.name = name;
  }
  
  public VacancyDataDto(VacancyDto vacancyDto) {
    this.id = vacancyDto.getVacancyId();
    this.name = vacancyDto.getName();
  }
  
  public int getId() {
    return id;
  }
  
  public void setId(int id) {
    this.id = id;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
}
