package ru.hh.content.engine.statistics;

public class StatisticRow {
  private Clicks clicks;
  private Views views;
  private Ctr ctr;

  public StatisticRow(Clicks clicks, Views views, Ctr ctr) {
    this.clicks = clicks;
    this.views = views;
    this.ctr = ctr;
  }

  public Clicks getClicks() {
    return clicks;
  }

  public void setClicks(Clicks clicks) {
    this.clicks = clicks;
  }

  public Views getViews() {
    return views;
  }

  public void setViews(Views views) {
    this.views = views;
  }

  public Ctr getCtr() {
    return ctr;
  }

  public void setCtr(Ctr ctr) {
    this.ctr = ctr;
  }
}
