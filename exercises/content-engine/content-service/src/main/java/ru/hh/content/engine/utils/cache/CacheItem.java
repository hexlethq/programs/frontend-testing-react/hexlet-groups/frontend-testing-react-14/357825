package ru.hh.content.engine.utils.cache;

import java.time.LocalDateTime;

public class CacheItem<T> {
  private LocalDateTime expirationTime;
  private T cachedObject;

  public CacheItem(LocalDateTime expirationTime, T cachedObject) {
    this.expirationTime = expirationTime;
    this.cachedObject = cachedObject;
  }

  public LocalDateTime getExpirationTime() {
    return expirationTime;
  }

  public void setExpirationTime(LocalDateTime expirationTime) {
    this.expirationTime = expirationTime;
  }

  public T getCachedObject() {
    return cachedObject;
  }

  public void setCachedObject(T cachedObject) {
    this.cachedObject = cachedObject;
  }
}
