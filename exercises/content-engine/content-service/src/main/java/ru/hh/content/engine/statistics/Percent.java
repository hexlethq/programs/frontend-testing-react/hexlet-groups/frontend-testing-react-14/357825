package ru.hh.content.engine.statistics;

public class Percent {
  private Integer mobile;
  private Integer desktop;

  public Percent() {
  }

  public Percent(Integer mobile, Integer desktop) {
    this.mobile = mobile;
    this.desktop = desktop;
  }

  public Integer getMobile() {
    return mobile;
  }

  public void setMobile(Integer mobile) {
    this.mobile = mobile;
  }

  public Integer getDesktop() {
    return desktop;
  }

  public void setDesktop(Integer desktop) {
    this.desktop = desktop;
  }
}
