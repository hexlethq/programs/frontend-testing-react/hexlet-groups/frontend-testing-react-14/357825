package ru.hh.content.engine;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import ru.hh.content.api.resources.Paths;
import ru.hh.content.engine.config.ProdConfig;
import ru.hh.content.engine.resources.filter.HHSessionFilter;
import ru.hh.content.engine.utils.ObjectMapperResolver;
import ru.hh.content.engine.utils.exceptions.PreconditionExceptionMapper;
import ru.hh.nab.starter.NabApplication;
import ru.hh.nab.starter.exceptions.AnyExceptionMapper;
import ru.hh.nab.starter.exceptions.IllegalArgumentExceptionMapper;
import ru.hh.nab.starter.exceptions.IllegalStateExceptionMapper;
import ru.hh.nab.starter.exceptions.NotFoundExceptionMapper;
import ru.hh.nab.starter.exceptions.SecurityExceptionMapper;
import ru.hh.nab.starter.exceptions.WebApplicationExceptionMapper;

public class App {
  public static void main(String[] args) {
    buildApplication().run(ProdConfig.class);
  }

  static NabApplication buildApplication() {
    return NabApplication.builder()
        .addFilterBean(ctx -> ctx.getBean(HHSessionFilter.class)).bindTo(Paths.FRONT_PATH + "/*")
        .configureJersey()
        .executeOnConfig(App::registerMultiPartFeature)
        .executeOnConfig(App::registerExceptionMappers)
        .executeOnConfig(App::registerObjectMapper)
        .bindToRoot()
        .build();
  }

  private static void registerObjectMapper(ResourceConfig config) {
    config.register(ObjectMapperResolver.class);
  }

  private static void registerMultiPartFeature(ResourceConfig config) {
    config.register(MultiPartFeature.class);
  }

  private static void registerExceptionMappers(ResourceConfig config) {
    config.register(PreconditionExceptionMapper.class);

    config.register(AnyExceptionMapper.class);
    config.register(IllegalArgumentExceptionMapper.class);
    config.register(IllegalStateExceptionMapper.class);
    config.register(NotFoundExceptionMapper.class);
    config.register(SecurityExceptionMapper.class);
    config.register(WebApplicationExceptionMapper.class);
  }
}
