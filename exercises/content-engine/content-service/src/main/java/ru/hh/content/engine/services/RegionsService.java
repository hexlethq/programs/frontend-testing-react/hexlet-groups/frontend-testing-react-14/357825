package ru.hh.content.engine.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toMap;
import java.util.stream.Stream;
import javax.inject.Inject;
import ru.hh.content.engine.clients.xmlback.HHAreasClient;
import ru.hh.content.engine.clients.xmlback.dto.AreasDto;
import ru.hh.content.engine.dto.Countries;
import static ru.hh.content.engine.dto.Countries.RUSSIA;
import static ru.hh.content.engine.utils.ExceptionUtils.getWithExceptionsToRuntimeWrapper;
import ru.hh.jclient.common.ResultWithStatus;

public class RegionsService {
  public static final int OTHER_COUNTRIES_ID = 1001;
  private final HHAreasClient areasClient;
  private final Translations translations;
  private final HHSessionContext sessionContext;

  @Inject
  public RegionsService(HHAreasClient areasClient,
                        Translations translations,
                        HHSessionContext sessionContext) {
    this.areasClient = areasClient;
    this.translations = translations;
    this.sessionContext = sessionContext;
  }

  public List<AreasDto> getAreasTree() {
    ResultWithStatus<AreasDto> resultWithStatus = getWithExceptionsToRuntimeWrapper(
        () -> areasClient.getAreasList().get()
    );

    if (!resultWithStatus.isSuccess()) {
      throw new RuntimeException(String.format("Fail get areas tree with status code %s", resultWithStatus.getStatusCode()));
    }

    Optional<AreasDto> areasDto = resultWithStatus.get();

    if (areasDto.isEmpty()) {
      throw new RuntimeException("Fail get areas tree cause OPTIONAL");
    }

    return areasDto.get().getChildren();
  }

  public Map<Integer, List<Integer>> getCountryRegionsMap() {
    return getAreasTree().stream()
        .filter(areasDto -> areasDto.getId() != OTHER_COUNTRIES_ID) // Удаляем прочие страны, так как они не нужны нам
        .collect(toMap(AreasDto::getId, areasDto ->
                areasDto.getChildren().stream()
                    .map(AreasDto::getId)
                    .collect(Collectors.toList())
            )
        );
  }

  public List<Integer> getRussiaRegions() {
    return getCountryRegionsMap().get(RUSSIA.getCountryId());
  }

  public Map<Integer, String> getTranslatedRussianRegion() {
    return getTranslatedRegions(getRussiaRegions());
  }

  public Map<Integer, String> getTranslatedAllRegions() {
    List<Integer> allIds = getCountryRegionsMap().entrySet().stream()
        .flatMap(entry -> {
          if (entry.getKey() != Countries.RUSSIA.getCountryId()) {
            return Stream.of(entry.getKey());
          }
          ArrayList<Integer> list = new ArrayList<>(entry.getValue());
          return list.stream();
        }).collect(Collectors.toList());
    return getTranslatedRegions(allIds);
  }

  public Map<Integer, String> getTranslatedRegions(List<Integer> regions) {
    return regions.stream()
        .collect(Collectors.toMap(
            Function.identity(),
            area -> translations.getTranslation("area." + area, sessionContext.getLocalization()))
        );
  }
}
