package ru.hh.content.engine.work.in.company.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.content.api.dto.ContentEvent;
import ru.hh.content.api.dto.ContentType;
import ru.hh.content.api.dto.EventType;
import ru.hh.content.engine.area.region.mapping.AreaRegionMappingDao;
import ru.hh.content.engine.clients.xmlback.WorkInCompanyClient;
import ru.hh.content.engine.clients.xmlback.WorkInCompanyDisplayInfos;
import ru.hh.content.engine.dto.ImpressionRequestDto;
import ru.hh.content.engine.kafka.banner.KafkaBannerPublisher;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;
import ru.hh.content.engine.services.ContentWebDavService;
import ru.hh.content.engine.services.HHSessionContext;
import static ru.hh.content.engine.utils.CommonUtils.getPlatformBySession;
import static ru.hh.content.engine.utils.ExceptionUtils.getWithExceptionsToRuntimeWrapper;
import ru.hh.content.engine.utils.cache.CacheService;
import static ru.hh.content.engine.work.in.company.WorkInCompanyConstants.MAX_COMPANIES_ON_PAGE_SETTING_NAME;
import ru.hh.content.engine.work.in.company.dao.WorkInCompanyDao;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyViewDto;
import ru.hh.jclient.common.ResultWithStatus;
import ru.hh.microcore.DisplayType;
import ru.hh.nab.datasource.DataSourceType;
import ru.hh.nab.hibernate.transaction.ExecuteOnDataSource;
import ru.hh.settings.SettingsClient;

public class WorkInCompanyViewService {
  public static final Logger LOGGER = LoggerFactory.getLogger(WorkInCompanyViewService.class);

  private final WorkInCompanyDao workInCompanyDao;
  private final WorkInCompanyClient workInCompanyClient;
  private final CacheService<List<WorkInCompanyViewDto>> cacheService;
  private final SettingsClient settingsClient;
  private final KafkaBannerPublisher kafkaBannerPublisher;
  private final HHSessionContext sessionContext;
  private final ContentWebDavService contentWebDavService;
  private final AreaRegionMappingDao areaRegionMappingDao;

  public WorkInCompanyViewService(WorkInCompanyDao workInCompanyDao,
                                  WorkInCompanyClient workInCompanyClient,
                                  CacheService<List<WorkInCompanyViewDto>> workInCompanyViewServiceCacheService,
                                  SettingsClient settingsClient,
                                  KafkaBannerPublisher kafkaBannerPublisher,
                                  HHSessionContext sessionContext,
                                  ContentWebDavService contentWebDavService,
                                  AreaRegionMappingDao areaRegionMappingDao) {
    this.workInCompanyDao = workInCompanyDao;
    this.workInCompanyClient = workInCompanyClient;
    this.cacheService = workInCompanyViewServiceCacheService;
    this.settingsClient = settingsClient;
    this.kafkaBannerPublisher = kafkaBannerPublisher;
    this.sessionContext = sessionContext;
    this.contentWebDavService = contentWebDavService;
    this.areaRegionMappingDao = areaRegionMappingDao;
  }

  public List<WorkInCompanyViewDto> getWorkInCompanyToView(LocalDate dateTime, ImpressionRequestDto impressionRequestDto) {

    TargetingDeviceType deviceType;
    if (sessionContext.getSession().displayType == DisplayType.DESKTOP) {
      deviceType = TargetingDeviceType.DESKTOP;
    } else {
      deviceType = TargetingDeviceType.MOBILE;
    }
    Optional<Integer> regionByArea = areaRegionMappingDao.getRegionByArea(sessionContext.getSession().locale.domainAreaId);
    String areaPath = sessionContext.getSession().locale.areaPath;
    if (regionByArea.isPresent()) {
      areaPath = String.format("%s%s.", areaPath, regionByArea.get());
    }
    String cacheKey = getCacheKey(dateTime, deviceType, areaPath);
    Optional<List<WorkInCompanyViewDto>> cache = cacheService.getFromCache(cacheKey);
    List<WorkInCompanyViewDto> workInCompaniesDto;

    if (cache.isEmpty()) {
      workInCompaniesDto = getWorkInCompaniesDtos(dateTime, areaPath, deviceType);
      Collections.shuffle(workInCompaniesDto);

      int freeSlotsCount = settingsClient.getInteger(MAX_COMPANIES_ON_PAGE_SETTING_NAME).orElseThrow() - workInCompaniesDto.size();
      if (freeSlotsCount > 0) {
        addStubs(workInCompaniesDto, freeSlotsCount, areaPath);
      }

      cacheService.putInCache(cacheKey, workInCompaniesDto);
    } else {
      workInCompaniesDto = cache.get();
    }

    setPlaceIds(workInCompaniesDto);

    for (WorkInCompanyViewDto workInCompanyViewDto : workInCompaniesDto) {
      kafkaBannerPublisher.sendKafkaMessage(
          "content_events",
          fillContentImpressionEvent(impressionRequestDto, workInCompanyViewDto.getWorkInCompanyId(), workInCompanyViewDto.getPlaceId())
      );
    }

    return workInCompaniesDto;
  }

  private void setPlaceIds(List<WorkInCompanyViewDto> workInCompaniesDto) {
    for (int i = 0; i < workInCompaniesDto.size(); i++) {
      workInCompaniesDto.get(i).setPlaceId(i);
    }
  }

  private static String getCacheKey(LocalDate localDate, TargetingDeviceType targetingDeviceType, String areaPath) {
    return String.format("%s_%s_%s", localDate.toString(), targetingDeviceType, areaPath);
  }

  private List<WorkInCompanyViewDto> getWorkInCompaniesDtos(LocalDate dateTime, String areaPath, TargetingDeviceType deviceType) {
    List<Integer> regionIds = Arrays.stream(areaPath.substring(1).split("\\.")).map(Integer::valueOf).collect(Collectors.toList());
    List<WorkInCompany> allTodayEmployers = workInCompanyDao.getTodayEmployersWithLogos(
        dateTime,
        regionIds,
        deviceType
    );

    List<Long> workInCompaniesExcludedForRegion = workInCompanyDao.getWorkInCompaniesExcludedForRegion(dateTime, regionIds);

    allTodayEmployers = allTodayEmployers.stream()
        .filter(it -> !workInCompaniesExcludedForRegion.contains(it.getWorkInCompanyId()))
        .collect(Collectors.toList());

    WorkInCompanyDisplayInfos workInCompanyDisplayInfos = getWorkInCompanyDisplayInfos(
        allTodayEmployers.stream().map(WorkInCompany::getEmployerId).collect(Collectors.toList())
    );

    return allTodayEmployers
        .stream()
        .map(it -> {
          String displayName = it.getDisplayName();
          if (displayName == null || displayName.isEmpty()) {
            displayName = workInCompanyDisplayInfos.getWorkInCompanyDisplayInfoMap().get(it.getEmployerId()).getCompanyName();
          }

          String logoUrl;
          if (it.getWorkInCompanyLogo() == null) {
            logoUrl = getAbsoluteHHLogoUrl(workInCompanyDisplayInfos.getWorkInCompanyDisplayInfoMap().get(it.getEmployerId()).getLogoUrl());
          } else {
            logoUrl = contentWebDavService.getFullFileUrl(it.getWorkInCompanyLogo().getUrl());
          }

          return new WorkInCompanyViewDto(
              displayName,
              logoUrl,
              workInCompanyDisplayInfos.getWorkInCompanyDisplayInfoMap().get(it.getEmployerId()).getVacancyCount(),
              it.getEmployerId(),
              it.getWorkInCompanyId()
          );
        })
        .collect(Collectors.toList());
  }

  private static String getAbsoluteHHLogoUrl(String relativeLogoUrl) {
    if (relativeLogoUrl == null) {
      return null;
    }
    return "https://hh.ru" + relativeLogoUrl;
  }

  private void addStubs(List<WorkInCompanyViewDto> workInCompaniesDto, int freeSlotsCount, String areaPath) {
    List<Integer> workInCompanyStubs = getWorkInCompanyStubs(areaPath, freeSlotsCount);
    WorkInCompanyDisplayInfos workInCompanyStubsDisplayInfos = getWorkInCompanyDisplayInfos(workInCompanyStubs);

    List<WorkInCompanyViewDto> workInCompaniesStubsDto = workInCompanyStubs
        .stream()
        .map(employerId -> new WorkInCompanyViewDto(
            workInCompanyStubsDisplayInfos.getWorkInCompanyDisplayInfoMap().get(employerId).getCompanyName(),
            getAbsoluteHHLogoUrl(workInCompanyStubsDisplayInfos.getWorkInCompanyDisplayInfoMap().get(employerId).getLogoUrl()),
            workInCompanyStubsDisplayInfos.getWorkInCompanyDisplayInfoMap().get(employerId).getVacancyCount(),
            employerId,
            0L
        ))
        .collect(Collectors.toList());

    Collections.shuffle(workInCompaniesStubsDto);

    workInCompaniesDto.addAll(workInCompaniesStubsDto);
  }

  private WorkInCompanyDisplayInfos getWorkInCompanyDisplayInfos(List<Integer> employerIds) {
    ResultWithStatus<WorkInCompanyDisplayInfos> resultWithStatus = getWithExceptionsToRuntimeWrapper(
        () -> workInCompanyClient.getWorkInCompanyDisplayInfos(employerIds).get()
    );

    if (!resultWithStatus.isSuccess()) {
      throw new RuntimeException(String.format("Fail get display info with status code %s", resultWithStatus.getStatusCode()));
    }

    Optional<WorkInCompanyDisplayInfos> companyDisplayInfosOptional = resultWithStatus.get();

    if (companyDisplayInfosOptional.isEmpty()) {
      throw new RuntimeException("Fail get display info cause OPTIONAL");
    }

    return companyDisplayInfosOptional.get();
  }

  private List<Integer> getWorkInCompanyStubs(String areaPath, int limit) {
    ResultWithStatus<Integer[]> resultWithStatus = getWithExceptionsToRuntimeWrapper(
        () -> workInCompanyClient.getWorkInCompanyStubs(
            areaPath, limit).get()
    );

    if (!resultWithStatus.isSuccess()) {
      throw new RuntimeException(String.format("Fail get work in company stub with status code %s", resultWithStatus.getStatusCode()));
    }

    Optional<Integer[]> companyDisplayInfosOptional = resultWithStatus.get();

    if (companyDisplayInfosOptional.isEmpty()) {
      throw new RuntimeException("Fail get work in company stub cause OPTIONAL");
    }

    return Arrays.asList(companyDisplayInfosOptional.get());
  }

  @ExecuteOnDataSource(dataSourceType = DataSourceType.READONLY)
  public int getEmployerByWorkInCompanyById(long workInCompany) {
    return workInCompanyDao.getById(workInCompany).getEmployerId();
  }

  private ContentEvent fillContentImpressionEvent(ImpressionRequestDto requestDto, long contentId, int placeId) {
    Optional<Integer> regionByArea = areaRegionMappingDao.getRegionByArea(sessionContext.getSession().locale.domainAreaId);
    if (regionByArea.isEmpty()) {
      LOGGER.warn("User with hhuid {} has null region", sessionContext.getSession().uid);
    }

    return ContentEvent.newBuilder()
        .contentType(ContentType.CAMPAIGN_OF_THE_DAY)
        .eventType(EventType.IMPRESSION)
        .hhuid(sessionContext.getSession().uid)
        .hhid(sessionContext.getSession().getHhid())
        .contentId(contentId)
        .contentPlaceId(placeId)
        .domainAreaId(regionByArea.orElse(0))
        .platform(getPlatformBySession(sessionContext))
        .remoteHostIp(requestDto.getRemoteHostIp())
        .requestPath(requestDto.getUriInfo().getRequestUri().toString())
        .userAgent(requestDto.getUserAgent())
        .build();
  }
}
