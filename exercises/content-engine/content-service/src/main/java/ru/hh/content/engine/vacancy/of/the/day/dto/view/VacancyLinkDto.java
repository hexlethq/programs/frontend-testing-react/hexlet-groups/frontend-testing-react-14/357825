package ru.hh.content.engine.vacancy.of.the.day.dto.view;

public class VacancyLinkDto {
  private String desktop;

  public VacancyLinkDto() {
  }

  public VacancyLinkDto(String desktop) {
    this.desktop = desktop;
  }

  public String getDesktop() {
    return desktop;
  }

  public void setDesktop(String desktop) {
    this.desktop = desktop;
  }
}
