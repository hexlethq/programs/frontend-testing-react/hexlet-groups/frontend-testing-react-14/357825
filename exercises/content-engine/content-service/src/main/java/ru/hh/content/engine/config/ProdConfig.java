package ru.hh.content.engine.config;

import com.datastax.oss.driver.api.core.CqlSession;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;
import ru.headhunter.storage.cfg.FileStorageConfiguration;
import ru.headhunter.storage.core.FileStorageFactory;
import ru.headhunter.storage.exception.FileStorageConfigurationException;
import ru.hh.cassandra.clientv4.CassandraSessionFactory;
import ru.hh.content.engine.services.ContentWebDavService;
import ru.hh.memcached.HHMemcachedClient;
import ru.hh.memcached.HHMemcachedClientFactory;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.hibernate.NabHibernateProdConfig;
import ru.hh.nab.metrics.StatsDSender;
import ru.hh.nab.starter.NabProdConfig;
import ru.hh.settings.SettingsClient;
import ru.hh.settings.SettingsClientBuilder;
import ru.hh.webdav.WebDavService;
import ru.hh.webdav.impl.FileStorageManager;
import ru.hh.webdav.impl.WebDavServiceImpl;

@Configuration
@Import({
    NabProdConfig.class,
    NabHibernateProdConfig.class,

    CommonConfig.class,

    JClientsConfig.class,
    ContentWebDavService.class,
    FileStorageManager.class,
    NotificationConfig.class,
})
@EnableScheduling
public class ProdConfig {
  @Bean
  public SettingsClient settingsClient(CqlSession cassandraSession, String serviceName) {
    return SettingsClientBuilder.createBuilder()
        .withSession(cassandraSession)
        .withService(serviceName)
        .withUpdateInterval(10)
        .startOnInternalExecuterService()
        .buildAndStart();
  }

  @Bean
  CqlSession cassandraSession(FileSettings fileSettings, String serviceName, String datacenter) {
    var properties = fileSettings.getSubProperties("cassandra.main");
    return CassandraSessionFactory.createCassandraSession(properties,
        datacenter, false, null, null, serviceName, "main");
  }

  @Bean
  HHMemcachedClient hhMemcachedClient(StatsDSender statsDSender, String serviceName, FileSettings fileSettings) throws Exception {
    return HHMemcachedClientFactory.create(
        fileSettings.getSubProperties("memcached"),
        serviceName,
        statsDSender
    );
  }
  
  @Bean
  public FileStorageFactory fileStorageFactory(FileSettings fileSettings) throws FileStorageConfigurationException {
    return new FileStorageFactory(FileStorageConfiguration.getInstance(fileSettings.getSubProperties("webdav")));
  }

  @Bean
  public WebDavService webDavService(FileSettings fileSettings) {
    return new WebDavServiceImpl(fileSettings.getSubProperties("webdav"));
  }
}
