package ru.hh.content.engine.model.limits;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "impressions_limit")
public class ImpressionsLimit {
  @Id
  @Column(name = "content_limit_id", nullable = false)
  private Long contentLimitId;

  @Column(name = "current_total_impressions", nullable = false)
  private Long currentTotalImpressions;

  @Column(name = "current_daily_impressions", nullable = false)
  private Long currentDailyImpressions;

  @Column(name = "limit_total_impressions")
  private Long limitTotalImpressions;

  @Column(name = "limit_daily_impressions")
  private Long limitDailyImpressions;

  public ImpressionsLimit() {
    this.currentTotalImpressions = 0L;
    this.currentDailyImpressions = 0L;
  }

  public Long getContentLimitId() {
    return contentLimitId;
  }

  public void setContentLimitId(Long contentLimitId) {
    this.contentLimitId = contentLimitId;
  }

  public Long getCurrentTotalImpressions() {
    return currentTotalImpressions;
  }

  public void setCurrentTotalImpressions(Long currentTotalImpressions) {
    this.currentTotalImpressions = currentTotalImpressions;
  }

  public Long getCurrentDailyImpressions() {
    return currentDailyImpressions;
  }

  public void setCurrentDailyImpressions(Long currentDailyImpressions) {
    this.currentDailyImpressions = currentDailyImpressions;
  }

  public Long getLimitTotalImpressions() {
    return limitTotalImpressions;
  }

  public void setLimitTotalImpressions(Long limitTotalImpressions) {
    this.limitTotalImpressions = limitTotalImpressions;
  }

  public Long getLimitDailyImpressions() {
    return limitDailyImpressions;
  }

  public void setLimitDailyImpressions(Long limitDailyImpressions) {
    this.limitDailyImpressions = limitDailyImpressions;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ImpressionsLimit that = (ImpressionsLimit) o;
    return Objects.equals(contentLimitId, that.contentLimitId) &&
        Objects.equals(currentTotalImpressions, that.currentTotalImpressions) &&
        Objects.equals(currentDailyImpressions, that.currentDailyImpressions) &&
        Objects.equals(limitTotalImpressions, that.limitTotalImpressions) &&
        Objects.equals(limitDailyImpressions, that.limitDailyImpressions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contentLimitId, currentTotalImpressions, currentDailyImpressions, limitTotalImpressions, limitDailyImpressions);
  }

  @Override
  public String toString() {
    return "ImpressionsLimit{" +
        "contentLimitId=" + contentLimitId +
        ", currentTotalImpressions=" + currentTotalImpressions +
        ", currentDailyImpressions=" + currentDailyImpressions +
        ", limitTotalImpressions=" + limitTotalImpressions +
        ", limitDailyImpressions=" + limitDailyImpressions +
        '}';
  }
}
