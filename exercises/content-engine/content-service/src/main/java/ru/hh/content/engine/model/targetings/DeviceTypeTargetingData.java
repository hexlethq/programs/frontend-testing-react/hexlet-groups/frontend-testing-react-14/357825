package ru.hh.content.engine.model.targetings;

import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class DeviceTypeTargetingData extends TargetingData {
  private Set<TargetingDeviceType> deviceTypes;

  public DeviceTypeTargetingData() {
  }

  public DeviceTypeTargetingData(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }

  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    TargetingDeviceType deviceType = userDataForTargeting.getDeviceType();
    if (deviceType == null) {
      return false;
    }
    return deviceTypes.contains(deviceType);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DeviceTypeTargetingData that = (DeviceTypeTargetingData) o;
    return Objects.equals(deviceTypes, that.deviceTypes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(deviceTypes);
  }

  @Override
  public String toString() {
    return "DeviceTypeTargetingData{" +
        "deviceTypes=" + deviceTypes +
        '}';
  }
}
