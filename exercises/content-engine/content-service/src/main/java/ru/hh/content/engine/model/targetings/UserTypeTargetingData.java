package ru.hh.content.engine.model.targetings;

import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.model.enums.TargetingUserType;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class UserTypeTargetingData extends TargetingData {
  private Set<TargetingUserType> userTypes;

  public UserTypeTargetingData() {
  }

  public UserTypeTargetingData(Set<TargetingUserType> userTypes) {
    this.userTypes = userTypes;
  }

  public Set<TargetingUserType> getUserTypes() {
    return userTypes;
  }

  public void setUserTypes(Set<TargetingUserType> userTypes) {
    this.userTypes = userTypes;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    return userTypes.contains(userDataForTargeting.getUserType());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    UserTypeTargetingData that = (UserTypeTargetingData) o;
    return Objects.equals(userTypes, that.userTypes);
  }

  @Override
  public int hashCode() {
    return Objects.hash(userTypes);
  }

  @Override
  public String toString() {
    return "UserTypeTargetingData{" +
        "userTypes=" + userTypes +
        '}';
  }
}
