package ru.hh.content.engine.targetings;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.Targeting;

public class TargetingService {
  private final TargetingDao targetingDao;
  private Map<Long, List<Targeting>> targetingIdToTargetings;

  @Inject
  public TargetingService(TargetingDao targetingDao) {
    this.targetingDao = targetingDao;
    this.targetingIdToTargetings = new HashMap<>();
  }

  @Transactional(readOnly = true)
  public List<Targeting> getTargetings(long targetingId) {
    return targetingIdToTargetings.computeIfAbsent(targetingId, targetingDao::getTargetings);
  }

  @Transactional
  public long getNewTargetingId() {
    return targetingDao.getNewTargetingId();
  }

  public void saveTargetings(List<Targeting> targetings) {
    targetingDao.saveTargetings(targetings);
  }
}
