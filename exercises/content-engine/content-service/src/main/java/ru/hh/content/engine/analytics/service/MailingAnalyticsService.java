package ru.hh.content.engine.analytics.service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import ru.hh.content.engine.analytics.MailingServiceType;
import ru.hh.content.engine.analytics.dao.MailingAnalyticsDao;
import ru.hh.content.engine.analytics.dto.MailingAnalyticsDto;
import ru.hh.content.engine.analytics.dto.MailingAnalyticsListDto;
import ru.hh.content.engine.analytics.dto.MailingIntervalDto;
import ru.hh.settings.SettingsClient;

public class MailingAnalyticsService {
  private static final String VIEWS_THRESHOLD_SETTING_NAME = "mailing_analytics.views_threshold";
  private static final String MAX_DAYS_SETTING_NAME = "mailing_analytics.max_days";
  private static final String MAX_ZERO_DAYS_SETTING_NAME = "mailing_analytics.max_zero_days";

  private final MailingAnalyticsDao mailingAnalyticsDao;
  private final SettingsClient settingsClient;

  @Inject
  public MailingAnalyticsService(MailingAnalyticsDao mailingAnalyticsDao, SettingsClient settingsClient) {
    this.mailingAnalyticsDao = mailingAnalyticsDao;
    this.settingsClient = settingsClient;
  }

  public MailingAnalyticsListDto getVacancyMailingAnalytics(int employerId, LocalDate periodStart, LocalDate periodEnd) {
    int viewsThreshold = settingsClient.getInteger(VIEWS_THRESHOLD_SETTING_NAME, 5);
    int maxDays = settingsClient.getInteger(MAX_DAYS_SETTING_NAME, 5);
    int maxZeroViewsDays = settingsClient.getInteger(MAX_ZERO_DAYS_SETTING_NAME, 2);

    Map<String, List<MailingAnalyticsDto>> viewsByUtmMedium = mailingAnalyticsDao.getEmployerMailingAnalytics(
        employerId,
        MailingServiceType.getAllUtm(),
        periodStart,
        periodEnd,
        viewsThreshold
    ).stream()
        .collect(Collectors.groupingBy(MailingAnalyticsDto::getUtmMedium));

    List<MailingIntervalDto> resultList = new ArrayList<>();
    for (List<MailingAnalyticsDto> list : viewsByUtmMedium.values()) {
      Map<String, List<MailingAnalyticsDto>> viewsByUtmCampaign = list.stream()
          .collect(Collectors.groupingBy(MailingAnalyticsDto::getUtmCampaign));
      for (List<MailingAnalyticsDto> campaignViews : viewsByUtmCampaign.values()) {
        campaignViews = campaignViews.stream()
            .sorted(Comparator.comparing(MailingAnalyticsDto::getEventDate))
            .dropWhile(dto -> dto.getViews() < viewsThreshold)
            .collect(Collectors.toList());
        if (campaignViews.size() == 0) {
          continue;
        }

        LocalDate lastIntervalDate = campaignViews.get(campaignViews.size() - 1).getEventDate();
        for (int i = 0; i < campaignViews.size() - 1; i++) {
          LocalDate currentEventDate = campaignViews.get(i).getEventDate();
          LocalDate nextEventDate = campaignViews.get(i + 1).getEventDate();
          if (nextEventDate.isAfter(currentEventDate.plusDays(maxZeroViewsDays))) {
            lastIntervalDate = currentEventDate;
            break;
          }
        }
        LocalDate firstIntervalDate = campaignViews.get(0).getEventDate();
        lastIntervalDate = lastIntervalDate.isAfter(firstIntervalDate.plusDays(maxDays - 1)) ?
            firstIntervalDate.plusDays(maxDays - 1) :
            lastIntervalDate;

        MailingIntervalDto dto = new MailingIntervalDto(
            MailingServiceType.findByUtm(campaignViews.get(0).getUtmMedium()),
            firstIntervalDate,
            lastIntervalDate
        );
        resultList.add(dto);
      }
    }
    return new MailingAnalyticsListDto(resultList);
  }
}
