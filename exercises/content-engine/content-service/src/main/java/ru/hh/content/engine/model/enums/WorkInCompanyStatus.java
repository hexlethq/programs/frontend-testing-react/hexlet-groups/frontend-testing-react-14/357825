package ru.hh.content.engine.model.enums;

public enum WorkInCompanyStatus {
  ACTIVE,
  PAUSED,
  DELETED,
  ;
}
