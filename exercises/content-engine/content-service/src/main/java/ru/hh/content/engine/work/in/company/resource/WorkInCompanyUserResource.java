package ru.hh.content.engine.work.in.company.resource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyUserDataPath.USER_DATA;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyUserData;
import ru.hh.hh.session.client.HhUser;

@Path(USER_DATA)
public class WorkInCompanyUserResource {
  private final HHSessionContext sessionContext;
  private final SecurityContext securityContext;

  @Inject
  public WorkInCompanyUserResource(HHSessionContext sessionContext, SecurityContext securityContext) {
    this.sessionContext = sessionContext;
    this.securityContext = securityContext;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getUserData() {
    securityContext.checkCurrentUserIsBackOffice();
    HhUser hhUser = sessionContext.getAccount();
    String name = String.format("%s %s", hhUser.name.getFirst(), hhUser.name.getLast());
    WorkInCompanyUserData userData = new WorkInCompanyUserData(hhUser.userId, name, securityContext.currentUserIsSuperAdmin());
    return Response.ok(userData).build();
  }
}
