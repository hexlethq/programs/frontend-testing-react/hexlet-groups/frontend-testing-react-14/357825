package ru.hh.content.engine.vacancy.of.the.day.dto;

import java.util.List;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayViewDto;

public class ScoredVacancyResult {
  private List<VacancyOfTheDayViewDto> scoredVacancies;
  private boolean isVacancyOfTheDayRelevant;
  
  public ScoredVacancyResult() {
  }
  
  public ScoredVacancyResult(boolean isVacancyOfTheDayRelevant) {
    this.isVacancyOfTheDayRelevant = isVacancyOfTheDayRelevant;
  }
  
  public ScoredVacancyResult(List<VacancyOfTheDayViewDto> scoredVacancies, boolean isVacancyOfTheDayRelevant) {
    this.scoredVacancies = scoredVacancies;
    this.isVacancyOfTheDayRelevant = isVacancyOfTheDayRelevant;
  }
  
  public List<VacancyOfTheDayViewDto> getScoredVacancies() {
    return scoredVacancies;
  }
  
  public void setScoredVacancies(List<VacancyOfTheDayViewDto> scoredVacancies) {
    this.scoredVacancies = scoredVacancies;
  }
  
  public boolean isVacancyOfTheDayRelevant() {
    return isVacancyOfTheDayRelevant;
  }
  
  public void setVacancyOfTheDayRelevant(boolean vacancyOfTheDayRelevant) {
    isVacancyOfTheDayRelevant = vacancyOfTheDayRelevant;
  }
}
