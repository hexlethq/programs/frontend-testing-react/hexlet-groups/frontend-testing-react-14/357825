package ru.hh.content.engine.utils;

@FunctionalInterface
public interface SupplierWithException<T> {
  T get() throws Exception;
}
