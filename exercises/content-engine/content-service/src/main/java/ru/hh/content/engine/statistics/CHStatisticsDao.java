package ru.hh.content.engine.statistics;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.hh.content.api.dto.ContentType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.services.RegionsService;

public class CHStatisticsDao {
  private final NamedParameterJdbcTemplate clickHouseNamedJdbcTemplate;
  private final RegionsService regionsService;

  @Inject
  public CHStatisticsDao(NamedParameterJdbcTemplate clickHouseNamedJdbcTemplate, RegionsService regionsService) {
    this.clickHouseNamedJdbcTemplate = clickHouseNamedJdbcTemplate;
    this.regionsService = regionsService;
  }

  public AggregatedStatistic getCommonAggregatedStatistic(LocalDate dateFrom,
                                                          LocalDate dateTo,
                                                          ContentType contentType,
                                                          Set<Integer> regionIds,
                                                          Set<TargetingDeviceType> deviceTypes) {
    return clickHouseNamedJdbcTemplate.query(
        "select countIf(1, event_type = 'IMPRESSION') as views, " +
            "countIf(1, event_type = 'CLICK') as clicks " +
            "from clickme.content_log_distr" +
            " where event_date BETWEEN :dateFrom and :dateTo " +
            "and content_type = :eventType " +
            "and domain_area_id in (:regionIds) " +
            "and hhuid != '-' " +
            "and platform in (:deviceTypes)",
        Map.of(
            "eventType", contentType,
            "dateFrom", dateFrom.format(DateTimeFormatter.ISO_DATE),
            "dateTo", dateTo.format(DateTimeFormatter.ISO_DATE),
            "regionIds", regionIds,
            "deviceTypes", deviceTypes
        ), rs -> {
          if (rs.next()) {
            return new AggregatedStatistic(rs.getLong("views"), rs.getLong("clicks"));
          } else {
            return new AggregatedStatistic(0L, 0L);
          }
        });
  }

  public Map<Long, StatisticByContentId> getCommonProductStatistic(LocalDate dateFrom,
                                                                   LocalDate dateTo,
                                                                   ContentType contentType,
                                                                   List<Long> contentIds) {
    return clickHouseNamedJdbcTemplate.query(
        "select content_id," +
            "countIf(1, event_type = 'IMPRESSION') as views, " +
            "countIf(1, event_type = 'CLICK') as clicks " +
            "from clickme.content_log_distr" +
            " where event_date BETWEEN :dateFrom and :dateTo and content_type = :eventType and content_id in (:contentIds) and hhuid != '-'" +
            " group by content_id",
        Map.of(
            "eventType", contentType,
            "dateFrom", dateFrom.format(DateTimeFormatter.ISO_DATE),
            "dateTo", dateTo.format(DateTimeFormatter.ISO_DATE),
            "contentIds", contentIds
        ), rs -> {
          Map<Long, StatisticByContentId> statisticByContentIds = new HashMap<>();

          while (rs.next()) {
            statisticByContentIds.put(rs.getLong("content_id"),
                new StatisticByContentId(
                    rs.getLong("content_id"),
                    rs.getLong("views"),
                    rs.getLong("clicks"),
                    getPercentCtr(rs.getLong("clicks"), rs.getLong("views"))
                )
            );
          }

          return statisticByContentIds;
        });
  }

  public SumStatisticByDate getStatisticsByDate(LocalDate dateFrom, LocalDate dateTo, int contentId, ContentType contentType) {
    List<StatisticsByDate> statisticsByDateList = clickHouseNamedJdbcTemplate.query(
        "select event_date," +
            "countIf(1, event_type = 'IMPRESSION' and platform = 'DESKTOP') as desktopViews, " +
            "countIf(1, event_type = 'CLICK' and platform = 'DESKTOP') as desktopClicks, " +
            "countIf(1, event_type = 'IMPRESSION' and platform = 'MOBILE') as mobileViews, " +
            "countIf(1, event_type = 'CLICK' and platform = 'MOBILE') as mobileClicks " +
            "from clickme.content_log_distr" +
            " where event_date BETWEEN :dateFrom and :dateTo and content_type = :eventType and content_id = :contentId and hhuid != '-'" +
            " group by event_date",
        Map.of(
            "eventType", contentType,
            "dateFrom", dateFrom.format(DateTimeFormatter.ISO_DATE),
            "dateTo", dateTo.format(DateTimeFormatter.ISO_DATE),
            "contentId", contentId
        ), rs -> {
          List<StatisticsByDate> statisticsByDates = new ArrayList<>();

          while (rs.next()) {
            StatisticRow statisticRow = getStatisticRow(rs);
            statisticsByDates.add(new StatisticsByDate(
                rs.getDate("event_date").toLocalDate(),
                statisticRow)
            );
          }

          return statisticsByDates;
        });

    if (statisticsByDateList == null) {
      return new SumStatisticByDate(
          List.of(),
          new StatisticRow(
              new Clicks(0L, 0L, 0L, new Percent()),
              new Views(0L, 0L, 0L, new Percent()),
              new Ctr(0.0f, 0.0f, 0.0f))
      );
    }

    return new SumStatisticByDate(
        statisticsByDateList,
        getTotalStatistic(statisticsByDateList.stream().map(StatisticsByDate::getStatisticRow).collect(Collectors.toList()))
    );
  }

  private StatisticRow getTotalStatistic(List<StatisticRow> statisticRows) {
    long desktopViews = 0;
    long mobileViews = 0;
    long totalViews = 0;

    long desktopClicks = 0;
    long mobileClicks = 0;
    long totalClicks = 0;

    for (StatisticRow statisticRow : statisticRows) {
      desktopViews += statisticRow.getViews().getDesktop();
      mobileViews += statisticRow.getViews().getMobile();
      totalViews += statisticRow.getViews().getTotal();

      desktopClicks += statisticRow.getClicks().getDesktop();
      mobileClicks += statisticRow.getClicks().getMobile();
      totalClicks += statisticRow.getClicks().getTotal();
    }

    return new StatisticRow(
        new Clicks(
            mobileClicks,
            desktopClicks,
            totalClicks,
            new Percent((int) getPercent(mobileClicks, totalClicks), (int) getPercent(desktopClicks, totalClicks))
        ),
        new Views(
            mobileViews,
            desktopViews,
            totalViews,
            new Percent((int) getPercent(mobileViews, totalViews), (int) getPercent(desktopViews, totalViews))

        ),
        new Ctr(
            getPercentCtr(mobileClicks, mobileViews),
            getPercentCtr(desktopClicks, desktopViews),
            getPercentCtr(totalClicks, totalViews)
        )
    );
  }

  public SumStatisticByRegion getStatisticsByRegion(LocalDate dateFrom, LocalDate dateTo, int contentId, ContentType contentType) {
    List<StatisticByRegion> statisticByRegionList = clickHouseNamedJdbcTemplate.query(
        "select domain_area_id," +
            "countIf(1, event_type = 'IMPRESSION' and platform = 'DESKTOP') as desktopViews, " +
            "countIf(1, event_type = 'CLICK' and platform = 'DESKTOP') as desktopClicks, " +
            "countIf(1, event_type = 'IMPRESSION' and platform = 'MOBILE') as mobileViews, " +
            "countIf(1, event_type = 'CLICK' and platform = 'MOBILE') as mobileClicks " +
            "from clickme.content_log_distr" +
            " where event_date BETWEEN :dateFrom and :dateTo and content_type = :eventType and content_id = :contentId and hhuid != '-'" +
            " group by domain_area_id",
        Map.of(
            "eventType", contentType,
            "dateFrom", dateFrom.format(DateTimeFormatter.ISO_DATE),
            "dateTo", dateTo.format(DateTimeFormatter.ISO_DATE),
            "contentId", contentId
        ), rs -> {
          Map<Integer, String> translatedAllRegions = regionsService.getTranslatedAllRegions();
          List<StatisticByRegion> statisticByRegions = new ArrayList<>();

          while (rs.next()) {
            StatisticRow statisticRow = getStatisticRow(rs);
            statisticByRegions.add(new StatisticByRegion(
                translatedAllRegions.get(rs.getInt("domain_area_id")),
                statisticRow)
            );
          }

          return statisticByRegions;
        });

    return new SumStatisticByRegion(
        statisticByRegionList,
        getTotalStatistic(statisticByRegionList.stream().map(StatisticByRegion::getStatisticRow).collect(Collectors.toList()))
    );
  }

  private StatisticRow getStatisticRow(ResultSet rs) throws SQLException {
    long desktopViews = rs.getLong("desktopViews");
    long mobileViews = rs.getLong("mobileViews");
    long totalViews = desktopViews + mobileViews;

    long desktopClicks = rs.getLong("desktopClicks");
    long mobileClicks = rs.getLong("mobileClicks");
    long totalClicks = desktopClicks + mobileClicks;

    return new StatisticRow(
        new Clicks(
            mobileClicks,
            desktopClicks,
            totalClicks,
            new Percent((int) getPercent(mobileClicks, totalClicks), (int) getPercent(desktopClicks, totalClicks))
        ),
        new Views(
            mobileViews,
            desktopViews,
            totalViews,
            new Percent((int) getPercent(mobileViews, totalViews), (int) getPercent(desktopViews, totalViews))

        ),
        new Ctr(
            getPercentCtr(mobileClicks, mobileViews),
            getPercentCtr(desktopClicks, desktopViews),
            getPercentCtr(totalClicks, totalViews)
        )
    );
  }

  public static float getPercentCtr(long clicks, long shows) {
    if (shows == 0) {
      return 0;
    }

    return ((float) clicks) / shows * 100;
  }

  public static long getPercent(long part, long total) {
    if (total == 0) {
      return 0;
    }

    return (long) (((float) part) / total * 100);
  }
}
