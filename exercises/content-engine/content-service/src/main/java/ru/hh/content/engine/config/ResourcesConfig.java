package ru.hh.content.engine.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.content.engine.resources.ClickResource;
import ru.hh.content.engine.resources.RegionResource;
import ru.hh.content.engine.resources.UserResource;
import ru.hh.content.engine.resources.filter.HHSessionFilter;

@Configuration
@Import({
    HHSessionFilter.class,
    ClickResource.class,
    RegionResource.class,
    UserResource.class,
})
public class ResourcesConfig {
}
