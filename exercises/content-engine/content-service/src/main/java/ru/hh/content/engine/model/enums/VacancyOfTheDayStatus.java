package ru.hh.content.engine.model.enums;

public enum VacancyOfTheDayStatus {
  NEW,
  ON_MODERATION,
  REJECTED,
  APPROVED,
  PAUSED,
  DELETE,
}
