package ru.hh.content.engine.vacancy.of.the.day.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.Set;
import static ru.hh.content.api.CommonApiConstants.COMMON_DATE_FORMAT;
import ru.hh.content.engine.dto.CreatorDto;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.work.in.company.dto.EmployerInfo;

public class VacancyFrontDto {
  private long vacancyOfTheDayId;
  private VacancyOfTheDayStatus campaignStatus;
  private Set<TargetingDeviceType> deviceTypes;
  private PacketType packet;
  private Set<Integer> regionList;
  private VacancyOfTheDayPayedStatus paymentStatus;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateFrom;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateTo;
  private EmployerInfo employer;
  private VacancyDataDto vacancy;
  private CreatorDto creator;
  private String crmUrl;
  private String comment;
  private VacancyOfTheDayCampaignType campaignType;
  
  public VacancyFrontDto() {
  }

  public VacancyFrontDto(VacancyOfTheDay vacancyOfTheDay) {
    this.vacancyOfTheDayId = vacancyOfTheDay.getVacancyOfTheDayId();
    this.campaignStatus = vacancyOfTheDay.getStatus();
    this.paymentStatus = vacancyOfTheDay.getPayedStatus();
    this.deviceTypes = vacancyOfTheDay.getDeviceTypesFromReservations();
    this.packet = vacancyOfTheDay.getPacketType();
    this.regionList = vacancyOfTheDay.getRegionIdsFromReservations();
    this.dateFrom = vacancyOfTheDay.getDateStartFromReservations();
    this.dateTo = vacancyOfTheDay.getDateEndFromReservations();
    this.creator = new CreatorDto(vacancyOfTheDay.getManagerUserId(), vacancyOfTheDay.getManagerName(), vacancyOfTheDay.getManagerEmail());
    this.crmUrl = vacancyOfTheDay.getCrmUrl();
    this.comment = vacancyOfTheDay.getComment();
    if (vacancyOfTheDay.getEmployerId() != null) {
      this.employer = new EmployerInfo(vacancyOfTheDay.getEmployerId(), vacancyOfTheDay.getEmployerName());
    } else {
      this.employer = new EmployerInfo(0, "Не указан");
    }
    if (vacancyOfTheDay.getVacancyId() != null) {
      this.vacancy = new VacancyDataDto();
      this.vacancy.setId(vacancyOfTheDay.getVacancyId());
    }
    this.campaignType = vacancyOfTheDay.getCampaignType();
  }
  
  public long getVacancyOfTheDayId() {
    return vacancyOfTheDayId;
  }
  
  public void setVacancyOfTheDayId(int vacancyOfTheDayId) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }
  
  public VacancyOfTheDayStatus getCampaignStatus() {
    return campaignStatus;
  }
  
  public void setCampaignStatus(VacancyOfTheDayStatus campaignStatus) {
    this.campaignStatus = campaignStatus;
  }
  
  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }
  
  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }
  
  public PacketType getPacket() {
    return packet;
  }
  
  public void setPacket(PacketType packet) {
    this.packet = packet;
  }
  
  public Set<Integer> getRegionList() {
    return regionList;
  }
  
  public void setRegionList(Set<Integer> regionList) {
    this.regionList = regionList;
  }
  
  public VacancyOfTheDayPayedStatus getPaymentStatus() {
    return paymentStatus;
  }
  
  public void setPaymentStatus(VacancyOfTheDayPayedStatus paymentStatus) {
    this.paymentStatus = paymentStatus;
  }
  
  public LocalDate getDateFrom() {
    return dateFrom;
  }
  
  public void setDateFrom(LocalDate dateFrom) {
    this.dateFrom = dateFrom;
  }
  
  public LocalDate getDateTo() {
    return dateTo;
  }
  
  public void setDateTo(LocalDate dateTo) {
    this.dateTo = dateTo;
  }
  
  public EmployerInfo getEmployer() {
    return employer;
  }
  
  public void setEmployer(EmployerInfo employer) {
    this.employer = employer;
  }
  
  public VacancyDataDto getVacancy() {
    return vacancy;
  }
  
  public void setVacancy(VacancyDataDto vacancy) {
    this.vacancy = vacancy;
  }
  
  public CreatorDto getCreator() {
    return creator;
  }
  
  public void setCreator(CreatorDto creator) {
    this.creator = creator;
  }
  
  public String getCrmUrl() {
    return crmUrl;
  }
  
  public void setCrmUrl(String crmUrl) {
    this.crmUrl = crmUrl;
  }
  
  public String getComment() {
    return comment;
  }
  
  public void setComment(String comment) {
    this.comment = comment;
  }

  public void setVacancyOfTheDayId(long vacancyOfTheDayId) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }

  public VacancyOfTheDayCampaignType getCampaignType() {
    return campaignType;
  }

  public void setCampaignType(VacancyOfTheDayCampaignType campaignType) {
    this.campaignType = campaignType;
  }
}
