package ru.hh.content.engine.vacancy.of.the.day.dao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import static java.util.Collections.emptyMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDayReservation;

public class VacancyOfTheDayReservationDao {
  private final SessionFactory sessionFactory;

  public VacancyOfTheDayReservationDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public Map<Integer, Set<Integer>> getRegionToReservationNumbers(LocalDate dateFrom, TargetingDeviceType deviceType, List<Integer> regionIds) {
    if (regionIds.isEmpty()) {
      return Map.of();
    }

    List<VacancyOfTheDayReservation> vacancyOfTheDayReservationList = sessionFactory.getCurrentSession()
        .createQuery("from VacancyOfTheDayReservation " +
         "where dateStart = :dateStart " +
         "and deviceType = :deviceType " +
         "and regionId in :regionIds ", VacancyOfTheDayReservation.class)
        .setParameter("dateStart", dateFrom)
        .setParameter("deviceType", deviceType)
        .setParameterList("regionIds", regionIds)
        .list();

    Map<Integer, Set<Integer>> regionToReservationNumber = new HashMap<>();

    for (VacancyOfTheDayReservation vacancyOfTheDayReservation : vacancyOfTheDayReservationList) {
      regionToReservationNumber
          .computeIfAbsent(
              vacancyOfTheDayReservation.getRegionId(),
              (key) -> new HashSet<>()).add(vacancyOfTheDayReservation.getReservationNumber()
      );
    }

    return regionToReservationNumber;
  }

  public void saveReservations(List<VacancyOfTheDayReservation> vacancyOfTheDayReservationList) {
    for (VacancyOfTheDayReservation vacancyOfTheDayReservation : vacancyOfTheDayReservationList) {
      sessionFactory.getCurrentSession().save(vacancyOfTheDayReservation);
    }
  }

  @Transactional
  public void deleteReservations(long vacancyOfTheDayId) {
    sessionFactory.getCurrentSession()
        .createQuery("DELETE FROM VacancyOfTheDayReservation WHERE vacancyOfTheDayId = :vacancyOfTheDayId")
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .executeUpdate();
  }

  @Transactional(readOnly = true)
  public List<Integer> getBusyRegionsForPeriod(Set<TargetingDeviceType> deviceTypes,
                                               Collection<Integer> regionIds,
                                               LocalDate dateStart,
                                               LocalDate dateEnd,
                                               int maxCompaniesOnPage) {
    if (regionIds.isEmpty() || deviceTypes.isEmpty()) {
      return List.of();
    }
    return sessionFactory.getCurrentSession().createNativeQuery("select distinct region_id from vacancy_of_the_day_reservation " +
        "where date_start < :dateEnd and date_start >= :dateStart and region_id in :regionIds and device_type in :deviceTypes " +
        "group by date_start, device_type, region_id having count(*) > :maxCompaniesOnPage")
        .setParameter("dateEnd", dateEnd)
        .setParameter("dateStart", dateStart)
        .setParameterList("regionIds", regionIds)
        .setParameterList("deviceTypes", deviceTypes.stream().map(Enum::name).collect(Collectors.toList()))
        .setParameter("maxCompaniesOnPage", maxCompaniesOnPage - 1)
        .list();
  }

  @SuppressWarnings("unchecked")
  @Transactional(readOnly = true)
  public Map<Integer, LocalDate> getNextFreeDatesByRegions(Set<TargetingDeviceType> deviceTypes,
                                                           int duration,
                                                           Collection<Integer> regionIds,
                                                           LocalDate dateStart,
                                                           int maxCompaniesOnPage
  ) {
    if (regionIds.isEmpty() || deviceTypes.isEmpty()) {
      return emptyMap();
    }

    List<Object[]> list = sessionFactory.getCurrentSession().createNativeQuery("select region_id, min(date_start) from (" +
        "select date_start, region_id from vacancy_of_the_day_reservation as votd " +
        "where votd.region_id in :regionIds and votd.device_type in :deviceTypes and votd.date_start >= :dateStart " +
        "group by date_start, device_type, region_id " +
        "having count(*) > :maxCompaniesOnPage and not exists(" +
        "select 1 from vacancy_of_the_day_reservation inner_votd where inner_votd.date_start > votd.date_start " +
        "and inner_votd.date_start - interval '1' day * :duration <= votd.date_start and inner_votd.region_id = votd.region_id " +
        "and inner_votd.device_type = votd.device_type group by date_start, region_id having count(*) > :maxCompaniesOnPage)) " +
        "as full_reservs group by region_id")
        .setParameterList("deviceTypes", deviceTypes.stream().map(Enum::name).collect(Collectors.toList()))
        .setParameterList("regionIds", regionIds)
        .setParameter("dateStart", dateStart)
        .setParameter("duration", duration)
        .setParameter("maxCompaniesOnPage", maxCompaniesOnPage - 1)
        .list();

    Map<Integer, LocalDate> regionToNearestFreeDate = new HashMap<>();

    for (Object[] row : list) {
      regionToNearestFreeDate.put((Integer) row[0], ((Date) row[1]).toLocalDate());
    }

    return regionToNearestFreeDate;
  }
}
