package ru.hh.content.engine.limits;

import ru.hh.content.engine.model.enums.ContentLimitType;

public interface ContentLimitCheckerService {
  public boolean isReached(long contentLimitId);
  public ContentLimitType getContentLimitType();
}
