package ru.hh.content.engine.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import ru.hh.content.engine.clients.xmlback.HHVacancyClient;
import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyListDto;
import static ru.hh.content.engine.utils.ExceptionUtils.getWithExceptionsToRuntimeWrapper;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyDataDto;
import ru.hh.jclient.common.ResultWithStatus;

public class VacancyService {
  private final HHVacancyClient hhVacancyClient;

  @Inject
  public VacancyService(HHVacancyClient hhVacancyClient) {
    this.hhVacancyClient = hhVacancyClient;
  }

  public List<VacancyDto> getVacancies(Set<Integer> vacancies) {
    if (vacancies.isEmpty()) {
      return Collections.emptyList();
    }
    ResultWithStatus<VacancyListDto> resultWithStatus = getWithExceptionsToRuntimeWrapper(
        () -> hhVacancyClient.getVacancies(new ArrayList<>(vacancies)).get()
    );

    if (!resultWithStatus.isSuccess()) {
      throw new RuntimeException(String.format("Fail get vacancies with id %s with status code %s", vacancies, resultWithStatus.getStatusCode()));
    }

    Optional<VacancyListDto> optionalVacancyListDto = resultWithStatus.get();

    if (optionalVacancyListDto.isEmpty()
        || optionalVacancyListDto.get().getVacancies() == null
        || optionalVacancyListDto.get().getVacancies().size() < 1) {
      return Collections.emptyList();
    }

    VacancyListDto vacancyListDto = optionalVacancyListDto.get();
    
    return vacancyListDto.getVacancies();
  }

  public VacancyDto getVacancy(Integer vacancyId) {
    return getVacancies(Set.of(vacancyId)).get(0);
  }

  public List<VacancyDataDto> getFrontVacancies(Set<Integer> vacancies) {
    return getVacancies(vacancies).stream().map(VacancyDataDto::new).collect(Collectors.toList());
  }

  public VacancyDataDto getFrontVacancy(int vacancyId) {
     List<VacancyDataDto> vacancyDtoList = getFrontVacancies(Set.of(vacancyId));
     if (vacancyDtoList.isEmpty()) {
       throw new NotFoundException();
     }
     
     return vacancyDtoList.get(0);
  }
}
