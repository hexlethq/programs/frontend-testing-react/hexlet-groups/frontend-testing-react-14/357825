package ru.hh.content.engine.services;

import java.util.concurrent.Callable;
import org.springframework.util.Assert;
import ru.hh.hh.session.client.HhLocale;
import ru.hh.hh.session.client.HhUser;
import ru.hh.hh.session.client.Session;
import ru.hh.hh.session.client.UserType;

public class HHSessionContext {
  private static final ThreadLocal<Session> SESSION_SUPPLIER = new InheritableThreadLocal<>();

  public boolean isSessionPresent() {
    return SESSION_SUPPLIER.get() != null;
  }

  public Session getSession() {
    return SESSION_SUPPLIER.get();
  }

  public HhLocale getLocalization() {
    return getSession().locale;
  }

  public boolean isUnknownUser() {
    return isUnknownUser(getAccount());
  }

  public boolean isUnknownUser(HhUser user) {
    return !isSessionPresent() || user == null;
  }

  public boolean isEmployer() {
    return !isUnknownUser() && getAccount().type == UserType.employer;
  }

  public boolean isBackOffice() {
    return isBackOffice(getAccount());
  }

  public boolean isBackOffice(HhUser user) {
    return !isUnknownUser(user) && user.type == UserType.back_office_user;
  }

  public HhUser getAccount() {
    return getSession().hhSession.account;
  }

  public String getUserFullName() {
    return String.format("%s %s", getAccount().name.getFirst(), getAccount().name.getLast());
  }

  public HhUser getActualAccount() {
    return getSession().hhSession.actualAccount;
  }

  public <T> T runInContext(Session session, Callable<T> callable) throws Exception {
    Assert.notNull(session, "session supplier cannot be null");
    Assert.notNull(callable, "Callable cannot be null");

    Session oldSessionSupplier = SESSION_SUPPLIER.get();
    SESSION_SUPPLIER.set(session);
    try {
      return callable.call();
    } finally {
      SESSION_SUPPLIER.set(oldSessionSupplier);
    }
  }

  public void runInContext(Session session, Runnable runnable) throws Exception {
    runInContext(session, () -> {
          runnable.run();
          return null;
        }
    );
  }
}
