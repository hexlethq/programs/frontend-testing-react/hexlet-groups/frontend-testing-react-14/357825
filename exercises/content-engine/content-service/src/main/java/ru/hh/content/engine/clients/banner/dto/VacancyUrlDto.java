package ru.hh.content.engine.clients.banner.dto;

import java.util.List;
import java.util.Map;

public class VacancyUrlDto {
  private String url;
  private Integer vacancyId;
  private Map<String, List<String>> params;
  
  public VacancyUrlDto() {
  }

  public VacancyUrlDto(String url, Integer vacancyId) {
    this.url = url;
    this.vacancyId = vacancyId;
  }
  
  public String getUrl() {
    return url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public Integer getVacancyId() {
    return vacancyId;
  }
  
  public void setVacancyId(Integer vacancyId) {
    this.vacancyId = vacancyId;
  }
  
  public Map<String, List<String>> getParams() {
    return params;
  }
  
  public void setParams(Map<String, List<String>> params) {
    this.params = params;
  }
}
