package ru.hh.content.engine.limits.clicks;

import ru.hh.content.engine.limits.ContentLimitCheckerService;
import ru.hh.content.engine.model.limits.ClicksLimit;
import ru.hh.content.engine.model.enums.ContentLimitType;

public class ClicksLimitCheckerService implements ContentLimitCheckerService {
  private final ClicksLimitDao clicksLimitDao;

  public ClicksLimitCheckerService(ClicksLimitDao clicksLimitDao) {
    this.clicksLimitDao = clicksLimitDao;
  }

  @Override
  public boolean isReached(long contentLimitId) {
    ClicksLimit clicksLimit = clicksLimitDao.getClicksLimit(contentLimitId);
    return isDailyReached(clicksLimit) || isTotalReached(clicksLimit);
  }

  @Override
  public ContentLimitType getContentLimitType() {
    return ContentLimitType.CLICKS;
  }

  private boolean isDailyReached(ClicksLimit clicksLimit) {
    if (clicksLimit.getLimitDailyClicks() == null) {
      return false;
    }

    return clicksLimit.getCurrentDailyClicks() >= clicksLimit.getLimitDailyClicks();
  }

  private boolean isTotalReached(ClicksLimit clicksLimit) {
    if (clicksLimit.getLimitTotalClicks() == null) {
      return false;
    }

    return clicksLimit.getCurrentTotalClicks() >= clicksLimit.getLimitTotalClicks();
  }
}
