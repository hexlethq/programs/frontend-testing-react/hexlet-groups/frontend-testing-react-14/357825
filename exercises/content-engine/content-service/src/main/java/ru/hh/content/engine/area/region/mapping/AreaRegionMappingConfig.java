package ru.hh.content.engine.area.region.mapping;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
    AreaRegionMappingResource.class,
    AreaRegionMappingUpdaterCronService.class,
    AreaRegionMappingUpdaterDao.class,
    AreaRegionMappingDao.class,
})
public class AreaRegionMappingConfig {
}
