package ru.hh.content.engine.utils;

import javax.validation.constraints.NotNull;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;
import ru.hh.nab.common.properties.FileSettings;

public class TestEnvironmentCondition implements Condition {

  @Override
  public boolean matches(ConditionContext context, @NotNull AnnotatedTypeMetadata metadata) {
    FileSettings fileSettings = context.getBeanFactory().getBean("fileSettings", FileSettings.class);
    return !fileSettings.getBoolean("is.test.environment");
  }
}
