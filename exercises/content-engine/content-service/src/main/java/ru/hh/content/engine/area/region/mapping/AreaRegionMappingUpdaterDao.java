package ru.hh.content.engine.area.region.mapping;

import javax.inject.Inject;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.work.in.company.AreaRegionMapping;

public class AreaRegionMappingUpdaterDao {
  private final SessionFactory sessionFactory;

  @Inject
  public AreaRegionMappingUpdaterDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Transactional
  public void updateAreaRegionMapping(int areaId, int regionId) {
    AreaRegionMapping areaRegionMapping = new AreaRegionMapping(areaId, regionId);
    sessionFactory.getCurrentSession().saveOrUpdate(areaRegionMapping);
  }
}
