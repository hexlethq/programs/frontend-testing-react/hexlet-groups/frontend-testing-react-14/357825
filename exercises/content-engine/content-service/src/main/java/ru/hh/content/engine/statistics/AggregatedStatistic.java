package ru.hh.content.engine.statistics;

public class AggregatedStatistic {
  private Long impressions;
  private Long clicks;

  public AggregatedStatistic() {
  }

  public AggregatedStatistic(Long impressions, Long clicks) {
    this.impressions = impressions;
    this.clicks = clicks;
  }

  public Long getImpressions() {
    return impressions;
  }

  public void setImpressions(Long impressions) {
    this.impressions = impressions;
  }

  public Long getClicks() {
    return clicks;
  }

  public void setClicks(Long clicks) {
    this.clicks = clicks;
  }
}
