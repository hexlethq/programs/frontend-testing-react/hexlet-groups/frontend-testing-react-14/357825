package ru.hh.content.engine.clients.xmlback;

public class EmployerInfoResponse {
  private Integer id;
  private String category;
  private boolean talentLinkClient;
  private String name;
  private String structureName;
  private Integer areaId;
  private Integer organizationFormId;
  private Integer mainEmployerId;

  public EmployerInfoResponse() {
  }

  public EmployerInfoResponse(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public boolean isTalentLinkClient() {
    return talentLinkClient;
  }

  public void setTalentLinkClient(boolean talentLinkClient) {
    this.talentLinkClient = talentLinkClient;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getStructureName() {
    return structureName;
  }

  public void setStructureName(String structureName) {
    this.structureName = structureName;
  }

  public Integer getAreaId() {
    return areaId;
  }

  public void setAreaId(Integer areaId) {
    this.areaId = areaId;
  }

  public Integer getOrganizationFormId() {
    return organizationFormId;
  }

  public void setOrganizationFormId(Integer organizationFormId) {
    this.organizationFormId = organizationFormId;
  }

  public Integer getMainEmployerId() {
    return mainEmployerId;
  }

  public void setMainEmployerId(Integer mainEmployerId) {
    this.mainEmployerId = mainEmployerId;
  }
}
