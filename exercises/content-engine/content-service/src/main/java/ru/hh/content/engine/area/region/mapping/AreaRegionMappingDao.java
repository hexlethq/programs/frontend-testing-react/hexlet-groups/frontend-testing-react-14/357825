package ru.hh.content.engine.area.region.mapping;

import java.util.Optional;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.work.in.company.AreaRegionMapping;

public class AreaRegionMappingDao {
  private final SessionFactory sessionFactory;

  public AreaRegionMappingDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Transactional(readOnly = true)
  public Optional<Integer> getRegionByArea(int areaId) {
    AreaRegionMapping areaRegionMapping = sessionFactory.getCurrentSession().get(AreaRegionMapping.class, areaId);

    if (areaRegionMapping == null) {
      return Optional.empty();
    }
    return Optional.of(areaRegionMapping.getRegionId());
  }
}
