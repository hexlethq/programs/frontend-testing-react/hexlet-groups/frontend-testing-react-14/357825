package ru.hh.content.engine.limits;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.content.engine.limits.clicks.ClicksLimitCheckerService;
import ru.hh.content.engine.limits.clicks.ClicksLimitDao;
import ru.hh.content.engine.limits.impressions.ImpressionsLimitCheckerService;
import ru.hh.content.engine.limits.impressions.ImpressionsLimitDao;

@Configuration
@Import({
    ClicksLimitCheckerService.class,
    ImpressionsLimitCheckerService.class,

    ClicksLimitDao.class,
    ImpressionsLimitDao.class,

    ContentLimitDao.class,
    ContentLimitService.class,
})
public class ContentLimitConfig {
}
