package ru.hh.content.engine.vacancy.of.the.day.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import java.util.Set;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import static ru.hh.content.api.CommonApiConstants.COMMON_DATE_FORMAT;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;

public class VacancyOfTheDayFormDto {
  private Set<Integer> regionIds;
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateStart;
  @NotNull
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateEnd;
  private String comment;
  private String crmUrl;
  private Integer vacancyId;
  @NotNull
  @NotEmpty
  private Set<TargetingDeviceType> deviceTypes;
  private VacancyOfTheDayStatus vacancyOfTheDayStatus;
  private VacancyOfTheDayPayedStatus vacancyOfTheDayPayedStatus;
  private VacancyOfTheDayCampaignType campaignType;
  private PacketType packetType;

  public VacancyOfTheDayFormDto() {
  }

  public VacancyOfTheDayFormDto(Set<Integer> regionIds,
                                @NotNull LocalDate dateStart,
                                @NotNull LocalDate dateEnd,
                                String comment,
                                String crmUrl,
                                @NotNull Integer vacancyId,
                                @NotNull @NotEmpty Set<TargetingDeviceType> deviceTypes,
                                VacancyOfTheDayStatus vacancyOfTheDayStatus,
                                VacancyOfTheDayPayedStatus vacancyOfTheDayPayedStatus,
                                VacancyOfTheDayCampaignType campaignType,
                                PacketType packetType) {
    this.regionIds = regionIds;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.comment = comment;
    this.crmUrl = crmUrl;
    this.vacancyId = vacancyId;
    this.deviceTypes = deviceTypes;
    this.vacancyOfTheDayStatus = vacancyOfTheDayStatus;
    this.vacancyOfTheDayPayedStatus = vacancyOfTheDayPayedStatus;
    this.campaignType = campaignType;
    this.packetType = packetType;
  }

  public Set<Integer> getRegionIds() {
    return regionIds;
  }

  public void setRegionIds(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  public LocalDate getDateStart() {
    return dateStart;
  }

  public void setDateStart(LocalDate dateStart) {
    this.dateStart = dateStart;
  }

  public LocalDate getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(LocalDate dateEnd) {
    this.dateEnd = dateEnd;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Integer getVacancyId() {
    return vacancyId;
  }

  public void setVacancyId(Integer vacancyId) {
    this.vacancyId = vacancyId;
  }

  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }

  public VacancyOfTheDayStatus getVacancyOfTheDayStatus() {
    return vacancyOfTheDayStatus;
  }

  public void setVacancyOfTheDayStatus(VacancyOfTheDayStatus vacancyOfTheDayStatus) {
    this.vacancyOfTheDayStatus = vacancyOfTheDayStatus;
  }

  public VacancyOfTheDayPayedStatus getVacancyOfTheDayPayedStatus() {
    return vacancyOfTheDayPayedStatus;
  }

  public void setVacancyOfTheDayPayedStatus(VacancyOfTheDayPayedStatus vacancyOfTheDayPayedStatus) {
    this.vacancyOfTheDayPayedStatus = vacancyOfTheDayPayedStatus;
  }

  public String getCrmUrl() {
    return crmUrl;
  }

  public void setCrmUrl(String crmUrl) {
    this.crmUrl = crmUrl;
  }

  public PacketType getPacketType() {
    return packetType;
  }

  public void setPacketType(PacketType packetType) {
    this.packetType = packetType;
  }

  public VacancyOfTheDayCampaignType getCampaignType() {
    return campaignType;
  }

  public void setCampaignType(VacancyOfTheDayCampaignType campaignType) {
    this.campaignType = campaignType;
  }
}
