package ru.hh.content.engine.services;

import java.util.Map;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.content.engine.model.enums.MailTemplateCode;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.mailer.utils.DefaultMailCommand;
import ru.hh.mailer.utils.Localization;
import ru.hh.mailer.utils.MailService;

public class NotificationService {
  private static final Logger LOGGER = LoggerFactory.getLogger(NotificationService.class);
  private static final String MAIL_LOCALE = "RU";
  private static final int SITE_ID = 1;
  private static final String SITE_BASE_URL = "hh.ru";
  
  private final MailService mailService;
  
  @Inject
  public NotificationService(MailService mailService) {
    this.mailService = mailService;
  }
  
  public void sendEmail(VacancyOfTheDay vacancyOfTheDay, String reason) {
    Map<String, Object> params = new java.util.HashMap<>(Map.of(
        "username", vacancyOfTheDay.getManagerName(),
        "vacancyOfTheDayId", vacancyOfTheDay.getVacancyOfTheDayId(),
        "comment", reason
    ));
    
    final Localization mailLocalization = new Localization(Localization.Lang.valueOf(MAIL_LOCALE), SITE_ID, SITE_BASE_URL);
    final DefaultMailCommand command = new DefaultMailCommand(
        vacancyOfTheDay.getManagerEmail(), null, null, MailTemplateCode.MODERATION.getName(), params, mailLocalization
    );
    LOGGER.info("sending email with template code '{}' to '{}'", MailTemplateCode.MODERATION.getName(), vacancyOfTheDay.getManagerEmail());
    mailService.sendMailCommand(command);
  }
}
