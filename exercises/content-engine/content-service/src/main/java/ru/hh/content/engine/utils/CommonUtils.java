package ru.hh.content.engine.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import java.io.IOException;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import ru.hh.content.api.dto.Platform;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.microcore.DisplayType;

public class CommonUtils {
  public static Platform getPlatformBySession(HHSessionContext sessionContext) {
    Platform platform;
    if (sessionContext.getSession().displayType == DisplayType.DESKTOP) {
      platform = Platform.DESKTOP;
    } else {
      platform = Platform.MOBILE;
    }
    return platform;
  }

  public static ObjectMapper createObjectMapper() {
    ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    objectMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
    objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    objectMapper.configure(MapperFeature.USE_ANNOTATIONS, true);

    objectMapper.registerModule(new JavaTimeModule());
    objectMapper.registerModule(new Jdk8Module());
    return objectMapper;
  }

  public static class FloatTwoDigitSerializer extends JsonSerializer<Float> {

    @Override
    public void serialize(Float aFloat, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
      if (aFloat == null || aFloat == 0f) {
        jsonGenerator.writeNumber("0");
      } else {
        jsonGenerator.writeNumber(String.format(Locale.ENGLISH, "%.2f", aFloat));
      }
    }
  }
  
  public static <T> Predicate<T> distinctByKey(
      Function<? super T, ?> keyExtractor) {
    
    Map<Object, Boolean> seen = new ConcurrentHashMap<>();
    return t -> seen.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
  }
}
