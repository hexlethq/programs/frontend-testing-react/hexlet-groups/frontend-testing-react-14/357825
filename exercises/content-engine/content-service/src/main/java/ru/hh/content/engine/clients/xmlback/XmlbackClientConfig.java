package ru.hh.content.engine.clients.xmlback;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import static ru.hh.content.engine.config.JClientsConfig.fillHttpClientFactoryBuilder;
import ru.hh.jclient.common.HttpClientContext;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.RequestStrategy;
import ru.hh.jclient.common.balancing.RequestBalancerBuilder;
import ru.hh.jclient.common.util.storage.Storage;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.metrics.StatsDSender;

@Configuration
@Import({
    WorkInCompanyClient.class,
    HHAreasClient.class,
    HHVacancyClient.class,
    EmployerClient.class,
})
public class XmlbackClientConfig {
  @Bean
  HttpClientFactory xmlBackHttpClientFactory(FileSettings fileSettings,
                                             String serviceName,
                                             RequestStrategy<RequestBalancerBuilder> requestStrategy,
                                             StatsDSender statsDSender,
                                             Storage<HttpClientContext> simpleHttpClientContextStorage) {
    return fillHttpClientFactoryBuilder(
        fileSettings, serviceName, "hh-xmlback", requestStrategy, statsDSender, simpleHttpClientContextStorage
    ).withHostsWithSession(fileSettings.getStringList("jclient.hh-xmlback.internalEndpointUrl")).build();
  }
}
