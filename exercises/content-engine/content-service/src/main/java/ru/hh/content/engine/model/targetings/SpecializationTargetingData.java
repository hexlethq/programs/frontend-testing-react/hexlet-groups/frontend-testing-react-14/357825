package ru.hh.content.engine.model.targetings;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class SpecializationTargetingData extends TargetingData {
  private Set<Integer> specializationIds;

  public SpecializationTargetingData() {
  }

  public SpecializationTargetingData(Set<Integer> specializationIds) {
    this.specializationIds = specializationIds;
  }

  public Set<Integer> getSpecializationIds() {
    return specializationIds;
  }

  public void setSpecializationIds(Set<Integer> specializationIds) {
    this.specializationIds = specializationIds;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    if (specializationIds.isEmpty()) {
      return true;
    }

    if (userDataForTargeting.getSpecializationIds() == null) {
      return false;
    }

    return !Collections.disjoint(specializationIds, userDataForTargeting.getSpecializationIds());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SpecializationTargetingData that = (SpecializationTargetingData) o;
    return Objects.equals(specializationIds, that.specializationIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(specializationIds);
  }

  @Override
  public String toString() {
    return "SpecializationTargetingData{" +
        "specializationIds=" + specializationIds +
        '}';
  }
}
