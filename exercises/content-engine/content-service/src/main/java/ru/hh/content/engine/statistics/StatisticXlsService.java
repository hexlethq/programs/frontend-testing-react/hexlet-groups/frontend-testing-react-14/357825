package ru.hh.content.engine.statistics;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import ru.hh.content.api.dto.ContentType;

public class StatisticXlsService {
  private final CHStatisticsDao chStatisticsDao;

  public StatisticXlsService(CHStatisticsDao chStatisticsDao) {
    this.chStatisticsDao = chStatisticsDao;
  }

  public Workbook getStatisticByDateXls(LocalDate parsedDateFrom, LocalDate parsedDateTo, Integer contentId, ContentType contentType) {
    SumStatisticByDate statisticsByDate = chStatisticsDao.getStatisticsByDate(parsedDateFrom, parsedDateTo, contentId, contentType);
    Workbook workbook = new XSSFWorkbook();
    Sheet sheet = workbook.createSheet("Statistics");

    Font headerFont = workbook.createFont();
    headerFont.setBold(true);
    headerFont.setFontHeightInPoints((short) 12);

    CellStyle headerCellStyle = workbook.createCellStyle();
    headerCellStyle.setFont(headerFont);

    CellStyle cellStyle = workbook.createCellStyle();
    CreationHelper createHelper = workbook.getCreationHelper();
    cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd.MM.yyyy"));
    cellStyle.setAlignment(HorizontalAlignment.RIGHT);

    CellStyle percentDoubleStyle = workbook.createCellStyle();
    percentDoubleStyle.setDataFormat((short) 10);

    List<String> headers = new ArrayList<>();
    headers.add("Дата");
    headers.add("Просмотры, DESKTOP");
    headers.add("Просмотры, MOBILE");
    headers.add("Клики, DESKTOP");
    headers.add("Клики, MOBILE");
    headers.add("CTR DESKTOP %");
    headers.add("CTR MOBILE %");
    headers.add("Просмотры, TOTAL");
    headers.add("Клики, TOTAL");
    headers.add("CTR TOTAL %");

    fillHeaderRow(sheet, headerCellStyle, headers);

    fillCellDataForDates(sheet, cellStyle, percentDoubleStyle, statisticsByDate.getStatisticsByDates());

    for (int i = 0; i < headers.size(); i++) {
      sheet.autoSizeColumn(i);
    }

    fillSummaryRow(sheet, cellStyle, percentDoubleStyle, statisticsByDate.getTotalStatistic());

    return workbook;
  }

  public Workbook getStatisticByRegionXls(LocalDate parsedDateFrom, LocalDate parsedDateTo, Integer contentId, ContentType contentType) {
    SumStatisticByRegion statisticsByDate = chStatisticsDao.getStatisticsByRegion(parsedDateFrom, parsedDateTo, contentId, contentType);
    Workbook workbook = new XSSFWorkbook();
    Sheet sheet = workbook.createSheet("Statistics");

    Font headerFont = workbook.createFont();
    headerFont.setBold(true);
    headerFont.setFontHeightInPoints((short) 12);

    CellStyle headerCellStyle = workbook.createCellStyle();
    headerCellStyle.setFont(headerFont);

    CellStyle cellStyle = workbook.createCellStyle();
    CreationHelper createHelper = workbook.getCreationHelper();
    cellStyle.setDataFormat(createHelper.createDataFormat().getFormat("dd.MM.yyyy"));
    cellStyle.setAlignment(HorizontalAlignment.RIGHT);

    CellStyle percentDoubleStyle = workbook.createCellStyle();
    percentDoubleStyle.setDataFormat((short) 10);

    List<String> headers = new ArrayList<>();
    headers.add("Регион");
    headers.add("Просмотры, DESKTOP");
    headers.add("Просмотры, MOBILE");
    headers.add("Клики, DESKTOP");
    headers.add("Клики, MOBILE");
    headers.add("CTR DESKTOP %");
    headers.add("CTR MOBILE %");
    headers.add("Просмотры, TOTAL");
    headers.add("Клики, TOTAL");
    headers.add("CTR TOTAL %");

    fillHeaderRow(sheet, headerCellStyle, headers);

    fillCellDataForRegions(sheet, cellStyle, percentDoubleStyle, statisticsByDate.getStatisticByRegions());

    for (int i = 0; i < headers.size(); i++) {
      sheet.autoSizeColumn(i);
    }

    fillSummaryRow(sheet, cellStyle, percentDoubleStyle, statisticsByDate.getTotalStatistic());

    return workbook;
  }

  private void fillHeaderRow(Sheet sheet, CellStyle headerCellStyle, List<String> headers) {
    Row headerRow = sheet.createRow(0);
    for (int i = 0; i < headers.size(); i++) {
      Cell cell = headerRow.createCell(i);
      cell.setCellValue(headers.get(i));
      cell.setCellStyle(headerCellStyle);
    }
  }

  private void fillCellDataForDates(Sheet sheet,
                                    CellStyle cellStyle,
                                    CellStyle percentDoubleStyle,
                                    List<StatisticsByDate> statisticsByDates) {
    int rowNum = 1;
    for (StatisticsByDate statisticsByDate : statisticsByDates) {
      Row row = sheet.createRow(rowNum++);
      int column = 0;
      StatisticRow statisticRow = statisticsByDate.getStatisticRow();
      Cell dateCell = row.createCell(column++);
      dateCell.setCellValue(statisticsByDate.getDate());
      dateCell.setCellStyle(cellStyle);

      fillRowAfterPrimaryId(percentDoubleStyle, statisticRow, row, column);
    }
  }

  private void fillCellDataForRegions(Sheet sheet,
                                      CellStyle cellStyle,
                                      CellStyle percentDoubleStyle,
                                      List<StatisticByRegion> statisticByRegions) {
    int rowNum = 1;
    for (StatisticByRegion statisticByRegion : statisticByRegions) {
      Row row = sheet.createRow(rowNum++);
      int column = 0;
      StatisticRow statisticRow = statisticByRegion.getStatisticRow();
      Cell dateCell = row.createCell(column++);
      dateCell.setCellValue(statisticByRegion.getRegionName());
      dateCell.setCellStyle(cellStyle);

      fillRowAfterPrimaryId(percentDoubleStyle, statisticRow, row, column);
    }
  }

  private void fillSummaryRow(Sheet sheet,
                              CellStyle cellStyle,
                              CellStyle percentDoubleStyle,
                              StatisticRow statisticRow) {
    int rowNum = sheet.getLastRowNum() + 1;
    Row row = sheet.createRow(rowNum);

    int column = 0;
    Cell dateCell = row.createCell(column++);
    dateCell.setCellValue("Итого: ");
    dateCell.setCellStyle(cellStyle);

    fillRowAfterPrimaryId(percentDoubleStyle, statisticRow, row, column);
  }

  private void fillRowAfterPrimaryId(CellStyle percentDoubleStyle,
                                     StatisticRow statisticRow,
                                     Row row,
                                     int column) {
    row.createCell(column++).setCellValue(statisticRow.getViews().getDesktop());
    row.createCell(column++).setCellValue(statisticRow.getViews().getMobile());
    row.createCell(column++).setCellValue(statisticRow.getClicks().getDesktop());
    row.createCell(column++).setCellValue(statisticRow.getClicks().getMobile());

    Cell ctrCell = row.createCell(column++);
    ctrCell.setCellValue(statisticRow.getCtr().getDesktop() / 100.0);
    ctrCell.setCellStyle(percentDoubleStyle);
    ctrCell = row.createCell(column++);
    ctrCell.setCellValue(statisticRow.getCtr().getMobile() / 100.0);
    ctrCell.setCellStyle(percentDoubleStyle);
    row.createCell(column++).setCellValue(statisticRow.getViews().getTotal());
    row.createCell(column++).setCellValue(statisticRow.getClicks().getTotal());
    ctrCell = row.createCell(column);
    ctrCell.setCellValue(statisticRow.getCtr().getTotal() / 100.0);
    ctrCell.setCellStyle(percentDoubleStyle);
  }

  public static void saveWorkBookToFile(Workbook workbook, String fileName) throws IOException {
    FileOutputStream outputStream = new FileOutputStream(fileName);
    workbook.write(outputStream);
    outputStream.close();
    workbook.close();
  }
}
