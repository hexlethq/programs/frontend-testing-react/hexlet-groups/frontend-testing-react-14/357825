package ru.hh.content.engine.utils;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.stream.ImageInputStream;
import static org.apache.commons.io.FileUtils.ONE_KB;

public class ContentImage {
  public static final int MAX_PICTURE_FILE_SIZE = Math.toIntExact(1000 * ONE_KB) + 1;

  private BufferedImage bufferedImage;

  private final byte[] bytes;
  private ImageReader imageReader;

  private ContentImage(byte[] bytes) {
    this.bytes = bytes;
  }

  public static ContentImage fromBytes(byte[] bytes) {
    return new ContentImage(bytes);
  }

  public static ContentImage fromBufferedImage(BufferedImage bufferedImage, String imageFormat) {
    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    try {
      ImageIO.write(bufferedImage, imageFormat, outputStream);
      outputStream.flush();
    } catch (IOException e) {
      throw new RuntimeException("Failed to get bytes from buffered image", e);
    }
    ContentImage contentImage = new ContentImage(outputStream.toByteArray());

    contentImage.setBufferedImage(bufferedImage);

    return contentImage;
  }

  public BufferedImage getBufferedImage() {
    if (bufferedImage == null) {
      try {
        bufferedImage = ImageIO.read(new ByteArrayInputStream(bytes));
      } catch (IOException e) {
        throw new RuntimeException("Fail to get buffered image from bytes", e);
      }
    }
    return bufferedImage;
  }

  public byte[] getBytes() {
    return bytes;
  }

  public String getImageFormat() {
    try {
      return getImageReader().getFormatName();
    } catch (IOException e) {
      throw new RuntimeException("Failed to get format name", e);
    }
  }

  public int getSizeInBytes() {
    return getBytes().length;
  }

  public int getWidth() {
    return getBufferedImage().getWidth();
  }

  public int getHeight() {
    return getBufferedImage().getHeight();
  }

  private ImageReader getImageReader() {
    if (imageReader == null) {
      InputStream is = new ByteArrayInputStream(getBytes());
      imageReader = getImageReader(is);
    }
    return imageReader;
  }

  private void setBufferedImage(BufferedImage bufferedImage) {
    this.bufferedImage = bufferedImage;
  }

  private void setImageReader(ImageReader imageReader) {
    this.imageReader = imageReader;
  }

  private static ImageReader getImageReader(ImageInputStream iis) {
    Iterator<ImageReader> imageReaders = ImageIO.getImageReaders(iis);
    ImageReader imageReader = imageReaders.next();
    imageReader.setInput(iis);
    return imageReader;
  }

  private static ImageReader getImageReader(InputStream is) {
    Iterator<ImageReader> imageReaders = null;
    try {
      ImageInputStream imageInputStream = ImageIO.createImageInputStream(is);
      imageReaders = ImageIO.getImageReaders(imageInputStream);

      ImageReader imageReader = imageReaders.next();
      imageReader.setInput(imageInputStream);
      return imageReader;
    } catch (IOException e) {
      throw new RuntimeException("Failed to get image input stream frm input stream", e);
    }
  }
}
