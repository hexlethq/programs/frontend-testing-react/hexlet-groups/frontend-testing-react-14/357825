package ru.hh.content.engine.model.work.in.company;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import ru.hh.content.engine.model.enums.ImageType;

@Entity
@Table(name = "work_in_company_logo")
public class WorkInCompanyLogo {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "work_in_company_logo_id", nullable = false)
  private int workInCompanyLogoId;

  @Column(name = "logo_type", nullable = false)
  @Enumerated(EnumType.STRING)
  private ImageType imageType;
  
  @Column(name = "url", nullable = false)
  private String url;

  public WorkInCompanyLogo() {

  }

  public WorkInCompanyLogo(ImageType imageType, String url) {
    this.imageType = imageType;
    this.url = url;
  }
  
  public int getWorkInCompanyLogoId() {
    return workInCompanyLogoId;
  }
  
  public void setWorkInCompanyLogoId(int workInCompanyLogoId) {
    this.workInCompanyLogoId = workInCompanyLogoId;
  }
  
  public ImageType getImageType() {
    return imageType;
  }
  
  public void setImageType(ImageType imageType) {
    this.imageType = imageType;
  }
  
  public String getUrl() {
    return url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WorkInCompanyLogo that = (WorkInCompanyLogo) o;
    return Objects.equals(workInCompanyLogoId, that.workInCompanyLogoId) &&
        Objects.equals(url, that.url) &&
        Objects.equals(imageType, that.imageType);
  }
  
  @Override
  public int hashCode() {
    return Objects.hash(workInCompanyLogoId, imageType, url);
  }
  
  @Override
  public String toString() {
    return "WorkInCompanyReservation{" +
        "workInCompanyLogoId=" + workInCompanyLogoId +
        ", logoType=" + imageType +
        ", url=" + url +
        '}';
  }
}
