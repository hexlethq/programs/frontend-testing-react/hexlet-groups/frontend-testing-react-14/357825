package ru.hh.content.engine.work.in.company.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDate;
import java.util.Set;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.WorkInCompanyStatus;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;

public class WorkInCompaniesListItem {
  private Long workInCompanyId;
  private WorkInCompaniesListEmployerInfo employer;
  private LocalDate dateFrom;
  private LocalDate dateTo;
  private String creatorName;
  private Integer creatorUserId;
  private Set<Integer> regionList;
  private Set<Integer> excludedRegions;
  private boolean isRussia;
  private Set<TargetingDeviceType> deviceTypes;
  private WorkInCompanyStatus status;
  private String comment;

  public WorkInCompaniesListItem() {
  }

  public WorkInCompaniesListItem(WorkInCompany workInCompany) {
    workInCompanyId = workInCompany.getWorkInCompanyId();
    employer = new WorkInCompaniesListEmployerInfo(workInCompany.getEmployerId(), workInCompany.getDisplayName());
    dateFrom = workInCompany.getDateStartFromReservations();
    dateTo = workInCompany.getDateEndFromReservations();
    creatorName = workInCompany.getManagerName();
    creatorUserId = workInCompany.getManagerUserId();
    regionList = workInCompany.getNotExcludedRegionIdsFromReservations();
    deviceTypes = workInCompany.getDeviceTypesFromReservations();
    status = workInCompany.getStatus();
    comment = workInCompany.getComment();
    excludedRegions = workInCompany.getExcludedRegionIdsFromReservations();
    isRussia = workInCompany.isRussianFromReservations();
  }

  public Long getWorkInCompanyId() {
    return workInCompanyId;
  }

  public void setWorkInCompanyId(Long workInCompanyId) {
    this.workInCompanyId = workInCompanyId;
  }

  public WorkInCompaniesListEmployerInfo getEmployer() {
    return employer;
  }

  public void setEmployer(WorkInCompaniesListEmployerInfo employer) {
    this.employer = employer;
  }

  public LocalDate getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(LocalDate dateFrom) {
    this.dateFrom = dateFrom;
  }

  public LocalDate getDateTo() {
    return dateTo;
  }

  public void setDateTo(LocalDate dateTo) {
    this.dateTo = dateTo;
  }

  public String getCreatorName() {
    return creatorName;
  }

  public void setCreatorName(String creatorName) {
    this.creatorName = creatorName;
  }

  public Integer getCreatorUserId() {
    return creatorUserId;
  }

  public void setCreatorUserId(Integer creatorUserId) {
    this.creatorUserId = creatorUserId;
  }

  public Set<Integer> getRegionList() {
    return regionList;
  }

  public void setRegionList(Set<Integer> regionList) {
    this.regionList = regionList;
  }

  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }

  @JsonProperty("deviceType")
  public String getDeviceTypesAsString() {
    if (deviceTypes.size() > 1) {
      return "ALL";
    } else {
      return deviceTypes.iterator().next().name();
    }
  }

  public WorkInCompanyStatus getStatus() {
    return status;
  }

  public void setStatus(WorkInCompanyStatus status) {
    this.status = status;
  }

  public String getComment() {
    return comment;
  }

  public void setComment(String comment) {
    this.comment = comment;
  }

  public Set<Integer> getExcludedRegions() {
    return excludedRegions;
  }

  public void setExcludedRegions(Set<Integer> excludedRegions) {
    this.excludedRegions = excludedRegions;
  }

  public boolean getIsRussia() {
    return isRussia;
  }

  public void setIsRussia(boolean russia) {
    isRussia = russia;
  }
}
