package ru.hh.content.engine.config;

import com.datastax.oss.driver.api.core.CqlSession;
import java.util.Collections;
import static java.util.Collections.emptyList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.ScheduledExecutorService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.analytics.jclient.ExperimentsCheckClient;
import ru.hh.content.engine.clients.banner.BannerClient;
import ru.hh.content.engine.clients.logic.LogicClient;
import ru.hh.content.engine.clients.xmlback.XmlbackClientConfig;
import ru.hh.hh.session.client.HhSessionClientFactory;
import ru.hh.hh.session.jclient.HhSessionJClientFactory;
import ru.hh.hhid.client.impl.HhidJClientFactory;
import ru.hh.hhinvoker.client.InvokerClient;
import ru.hh.jclient.common.HttpClientContext;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.HttpClientFactoryBuilder;
import ru.hh.jclient.common.RequestStrategy;
import ru.hh.jclient.common.balancing.RequestBalancerBuilder;
import ru.hh.jclient.common.balancing.UpstreamManager;
import ru.hh.jclient.common.metrics.MetricsConsumer;
import ru.hh.jclient.common.metrics.MetricsConsumerFactory;
import ru.hh.jclient.common.metrics.MonitoringUpstreamManagerFactory;
import ru.hh.jclient.common.util.storage.MDCStorage;
import ru.hh.jclient.common.util.storage.SingletonStorage;
import ru.hh.jclient.common.util.storage.Storage;
import ru.hh.jclient.common.util.storage.StorageUtils;
import ru.hh.jdebug.jclient.DisabledRequestDebug;
import ru.hh.nab.common.executor.MonitoredThreadPoolExecutor;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.jclient.NabJClientConfig;
import ru.hh.nab.metrics.StatsDSender;
import ru.hh.settings.SettingsClient;
import ru.hh.settings.SettingsClientBuilder;

@Configuration
@Import({
    NabJClientConfig.class,
    XmlbackClientConfig.class,
})
public class JClientsConfig {

  @Bean
  InvokerClient invokerClient(FileSettings fileSettings, HttpClientFactory invokerHttpClientFactory) {
    return new InvokerClient(fileSettings.getString("invoker.jclient.host"), invokerHttpClientFactory);
  }

  @Bean
  HttpClientFactory invokerHttpClientFactory(FileSettings fileSettings,
                                             String serviceName,
                                             RequestStrategy<RequestBalancerBuilder> requestStrategy,
                                             StatsDSender statsDSender,
                                             Storage<HttpClientContext> simpleHttpClientContextStorage) {
    return fillHttpClientFactoryBuilder(
        fileSettings, serviceName, "hh-invoker", requestStrategy, statsDSender, simpleHttpClientContextStorage
    ).withHostsWithSession(fileSettings.getStringList("invoker.jclient.host")).build();
  }

  @Bean
  public HttpClientFactory commonHttpClientFactory(FileSettings settings,
                                                   String serviceName,
                                                   HttpClientFactoryBuilder httpClientFactoryBuilder,
                                                   Storage<HttpClientContext> httpClientContextStorage,
                                                   MetricsConsumer metricsConsumer,
                                                   RequestStrategy<RequestBalancerBuilder> requestStrategy,
                                                   StatsDSender statsDSender) {
    FileSettings jClientSettings = settings.getSubSettings("jclient");

    var callbackExecutor = MonitoredThreadPoolExecutor.create(
        jClientSettings.getSubSettings("threadPool"), "jclient", statsDSender, serviceName
    );

    return httpClientFactoryBuilder
        .withProperties(jClientSettings.getProperties())
        .withRequestStrategy(requestStrategy)
        .withCallbackExecutor(callbackExecutor)
        .withStorage(httpClientContextStorage)
        .withHostsWithSession(jClientSettings.getStringList("hostsWithSession"))
        .withMetricsConsumer(metricsConsumer)
        .build();
  }

  @Bean
  public MetricsConsumer metricsConsumer(String serviceName,
                                         FileSettings settings,
                                         StatsDSender statsDSender) {
    return MetricsConsumerFactory.buildMetricsConsumer(
        settings.getSubProperties("jclient.metrics.asynchttpclient"), serviceName, statsDSender
    );
  }

  @Bean
  public RequestStrategy<RequestBalancerBuilder> upstreamManager(String serviceName,
                                                                 FileSettings settings,
                                                                 String datacenter,
                                                                 CqlSession cassandraSession,
                                                                 StatsDSender statsDSender,
                                                                 ScheduledExecutorService scheduledExecutor) {
    return MonitoringUpstreamManagerFactory
        .create(
            serviceName,
            datacenter,
            settings.getBoolean("allowCrossDCRequests"),
            statsDSender,
            null,
            scheduledExecutor,
            upstreamManager -> setupUpstreamUpdater(upstreamManager, cassandraSession)
        );
  }

  @Bean
  Storage<HttpClientContext> simpleHttpClientContextStorage() {
    return new SingletonStorage<>(() ->
        new HttpClientContext(Map.of(), Map.of(), List.of(DisabledRequestDebug::new), StorageUtils.build(new MDCStorage()))
    );
  }

  @Bean
  HhSessionClientFactory sessionClientFactory(FileSettings fileSettings,
                                              HttpClientFactory commonHttpClientFactory) {
    return new HhSessionJClientFactory(fileSettings.getSubProperties("jclient.hh-session"), commonHttpClientFactory);
  }

  @Bean
  HhidJClientFactory hhidClientFactory(FileSettings fileSettings, HttpClientFactory commonHttpClientFactory) {
    return new HhidJClientFactory(fileSettings.getSubProperties("jclient.hhid"), commonHttpClientFactory);
  }
  
  @Bean
  public ExperimentsCheckClient experimentsCheckClient(FileSettings settings, HttpClientFactory commonHttpClientFactory) {
    String host = settings.getString("analytics.jclient.host");
    return new ExperimentsCheckClient(host, commonHttpClientFactory, "content-engine");
  }

  @Bean
  public BannerClient bannerClient(FileSettings fileSettings, HttpClientFactory commonHttpClientFactory) {
    return new BannerClient(
        fileSettings.getString("banner-engine.jclient.host"),
        commonHttpClientFactory,
        fileSettings.getInteger("banner-engine.jclient.requestTimeout")
    );
  }

  @Bean
  public LogicClient logicClient(FileSettings fileSettings, HttpClientFactory commonHttpClientFactory) {
    return new LogicClient(
        fileSettings.getString("logic.jclient.host"),
        commonHttpClientFactory,
        fileSettings.getInteger("logic.jclient.requestTimeout")
    );
  }

  public static HttpClientFactoryBuilder fillHttpClientFactoryBuilder(FileSettings fileSettings,
                                                                      String serviceName,
                                                                      String clientName,
                                                                      RequestStrategy<RequestBalancerBuilder> requestStrategy,
                                                                      StatsDSender statsDSender,
                                                                      Storage<HttpClientContext> storage
                                                                      ) {
    FileSettings jClientSettings = fileSettings.getSubSettings("jclient." + clientName);
    Executor callbackExecutor = MonitoredThreadPoolExecutor.create(
        jClientSettings.getSubSettings("threadPool"), jClientSettings.getString("threadpool.name"), statsDSender, serviceName
    );
    return initHttpClientFactoryBuilder(
        jClientSettings,
        storage,
        requestStrategy,
        callbackExecutor
    );
  }

  private static HttpClientFactoryBuilder initHttpClientFactoryBuilder(FileSettings jClientSettings,
                                                                       Storage<HttpClientContext> contextStorage,
                                                                       RequestStrategy<RequestBalancerBuilder> requestStrategy,
                                                                       Executor threadpool) {
    return new HttpClientFactoryBuilder(contextStorage, emptyList()).withProperties(jClientSettings.getProperties())
        .withHostsWithSession(Collections.emptyList())
        .withRequestStrategy(requestStrategy)
        .withCallbackExecutor(threadpool);
  }

  private static void setupUpstreamUpdater(UpstreamManager upstreamManager, CqlSession cassandraSession) {
    SettingsClient settingsClient = SettingsClientBuilder.createBuilder()
        .withSession(cassandraSession)
        .withService(SettingsClient.SYSTEM_SERVICE_KEY)
        .withUpdateInterval(SettingsClient.DEFAULT_UPDATE_INTERVAL_SECONDS)
        .startOnInternalExecuterService()
        .buildAndStart();
    settingsClient.addListener("*", upstreamManager::updateUpstream, true);
  }
}
