package ru.hh.content.engine.statistics;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import static ru.hh.content.api.CommonApiConstants.COMMON_DATE_FORMAT;

public class StatisticsByDate {
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate date;
  private StatisticRow statisticRow;

  public StatisticsByDate() {
  }

  public StatisticsByDate(LocalDate date, StatisticRow statisticRow) {
    this.date = date;
    this.statisticRow = statisticRow;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public StatisticRow getStatisticRow() {
    return statisticRow;
  }

  public void setStatisticRow(StatisticRow statisticRow) {
    this.statisticRow = statisticRow;
  }
}
