package ru.hh.content.engine.clients.xmlback;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import javax.inject.Inject;
import static javax.ws.rs.core.HttpHeaders.ACCEPT;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import ru.hh.content.engine.clients.converter.Json2Converter;
import static ru.hh.content.engine.clients.converter.Json2Converter.APPLICATION_JSON2;
import ru.hh.content.engine.clients.xmlback.dto.VacancyId;
import ru.hh.content.engine.clients.xmlback.dto.VacancyIdsList;
import ru.hh.content.engine.clients.xmlback.dto.VacancyListDto;
import static ru.hh.content.engine.utils.CommonUtils.createObjectMapper;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.JClientBase;
import ru.hh.jclient.common.Param;
import ru.hh.jclient.common.RequestBuilder;
import ru.hh.jclient.common.ResultWithStatus;
import ru.hh.nab.common.properties.FileSettings;

public class HHVacancyClient extends JClientBase {
  private static final String VACANCY_PATH = "/rs/vacancy/shortFromMaster";
  private static final String VACANCY_OF_THE_DAY_STUBS = "/rs/ofTheDay/vacancy";

  private final FileSettings fileSettings;
  private final Json2Converter<VacancyListDto> vacancyListDtoJson2Converter;
  private final JAXBContext jaxb;

  @Inject
  public HHVacancyClient(FileSettings fileSettings,
                         HttpClientFactory xmlBackHttpClientFactory) throws JAXBException {
    super(fileSettings.getString("jclient.hh-xmlback.internalEndpointUrl"), xmlBackHttpClientFactory);
    this.fileSettings = fileSettings;
    ObjectMapper objectMapper = createObjectMapper();
    this.vacancyListDtoJson2Converter = new Json2Converter<>(objectMapper, VacancyListDto.class);

    this.jaxb = JAXBContext.newInstance(
        VacancyIdsList.class,
        VacancyId.class
    );
  }

  public CompletableFuture<ResultWithStatus<VacancyIdsList>> getVacanciesOfTheDay(int areaId,
                                                                                  int maxCount,
                                                                                  List<Integer> excludedVacancyIds) {
    RequestBuilder request = get(url(VACANCY_OF_THE_DAY_STUBS),
        "areaId", Integer.toString(areaId),
        "maxCount", Integer.toString(maxCount)
    );

    Optional.ofNullable(excludedVacancyIds)
        .ifPresent(l -> l.forEach(id -> request.addQueryParam("excludedVacancyIds", id.toString())));

    return http.with(request.build()).readOnly().expectXml(jaxb, VacancyIdsList.class).resultWithStatus();
  }

  public CompletableFuture<ResultWithStatus<VacancyListDto>> getVacancies(List<Integer> vacancies) {
    return http
        .with(get(jerseyUrl(VACANCY_PATH))
            .addQueryParams(vacancies.stream().map(vacancy -> new Param("id", vacancy.toString())).collect(Collectors.toList()))
            .setHeader(ACCEPT, APPLICATION_JSON2)
            .setRequestTimeout(fileSettings.getInteger("jclient.hh-xmlback.vacancy.timeout"))
            .build()
        )
        .expect(vacancyListDtoJson2Converter)
        .resultWithStatus();
  }

  public CompletableFuture<ResultWithStatus<VacancyListDto>> getVacancy(int vacancyId) {
    return getVacancies(List.of(vacancyId));
  }
}
