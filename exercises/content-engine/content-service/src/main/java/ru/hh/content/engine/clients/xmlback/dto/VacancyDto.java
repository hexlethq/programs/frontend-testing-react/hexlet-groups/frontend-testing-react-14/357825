package ru.hh.content.engine.clients.xmlback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VacancyDto {
  private int vacancyId;
  private String name;
  @JsonProperty("company")
  private VacancyEmployerDto vacancyEmployerDto;
  private CompensationDto compensation;
  private AreaDto area;

  public VacancyDto() {
  }
  
  public VacancyDto(int vacancyId, String name) {
    this.vacancyId = vacancyId;
    this.name = name;
  }

  public VacancyDto(int vacancyId, String name, VacancyEmployerDto vacancyEmployerDto, CompensationDto compensation, AreaDto area) {
    this.vacancyId = vacancyId;
    this.name = name;
    this.vacancyEmployerDto = vacancyEmployerDto;
    this.compensation = compensation;
    this.area = area;
  }

  public int getVacancyId() {
    return vacancyId;
  }
  
  public void setVacancyId(int vacancyId) {
    this.vacancyId = vacancyId;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }

  public VacancyEmployerDto getVacancyEmployerDto() {
    return vacancyEmployerDto;
  }

  public void setVacancyEmployerDto(VacancyEmployerDto vacancyEmployerDto) {
    this.vacancyEmployerDto = vacancyEmployerDto;
  }

  public CompensationDto getCompensation() {
    return compensation;
  }

  public void setCompensation(CompensationDto compensation) {
    this.compensation = compensation;
  }

  public AreaDto getArea() {
    return area;
  }

  public void setArea(AreaDto area) {
    this.area = area;
  }
}
