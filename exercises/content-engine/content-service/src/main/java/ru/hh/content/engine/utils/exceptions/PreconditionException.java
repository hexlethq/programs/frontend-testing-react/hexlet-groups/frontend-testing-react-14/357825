package ru.hh.content.engine.utils.exceptions;

public class PreconditionException extends RuntimeException {
  public PreconditionException() {
    super();
  }

  public PreconditionException(String message) {
    super(message);
  }

  public PreconditionException(String message, Throwable cause) {
    super(message, cause);
  }
}
