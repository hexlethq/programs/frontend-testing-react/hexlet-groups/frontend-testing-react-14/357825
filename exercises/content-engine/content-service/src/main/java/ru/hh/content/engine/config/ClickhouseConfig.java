package ru.hh.content.engine.config;

import com.zaxxer.hikari.HikariConfig;
import java.util.Properties;
import javax.sql.DataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import ru.hh.content.engine.analytics.dao.MailingAnalyticsDao;
import ru.hh.content.engine.analytics.resource.ServicesAnalyticsResource;
import ru.hh.content.engine.analytics.service.MailingAnalyticsService;
import ru.hh.content.engine.services.EmployerService;
import ru.hh.content.engine.services.RegionsService;
import ru.hh.content.engine.statistics.CHStatisticsDao;
import ru.hh.content.engine.statistics.StatisticResource;
import ru.hh.content.engine.statistics.StatisticXlsService;
import ru.hh.content.engine.utils.TestEnvironmentCondition;
import ru.hh.content.engine.work.in.company.dao.WorkInCompanyDao;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.datasource.DataSourceFactory;
import static ru.hh.nab.datasource.DataSourceSettings.DEFAULT_VALIDATION_TIMEOUT_INCREMENT_MS;
import ru.hh.settings.SettingsClient;
import ru.yandex.clickhouse.BalancedClickhouseDataSource;

@Configuration
public class ClickhouseConfig {

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public DataSource clickHouseDataSource(DataSourceFactory dataSourceFactory, FileSettings fileSettings) {
    Properties hikariSettings = fileSettings.getSubProperties("clickhouse.banner.pool");
    Properties clickHouseProps = fileSettings.getSubProperties("clickhouse.banner.client");
    HikariConfig config = new HikariConfig(hikariSettings);
    config.setPoolName("clickhouse_banner");
    config.setValidationTimeout(config.getConnectionTimeout() + DEFAULT_VALIDATION_TIMEOUT_INCREMENT_MS);
    config.setDataSource(new BalancedClickhouseDataSource(clickHouseProps.getProperty("jdbcUrl"), clickHouseProps));
    return dataSourceFactory.create(config, new FileSettings(hikariSettings), true);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public NamedParameterJdbcTemplate clickHouseNamedJdbcTemplate(DataSource clickHouseDataSource) {
    return new NamedParameterJdbcTemplate(clickHouseDataSource);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public CHStatisticsDao chStatisticsDao(NamedParameterJdbcTemplate clickHouseNamedJdbcTemplate, RegionsService regionsService) {
    return new CHStatisticsDao(clickHouseNamedJdbcTemplate, regionsService);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public StatisticResource statisticResource(CHStatisticsDao chStatisticsDao,
                                             StatisticXlsService statisticXlsService,
                                             EmployerService employerService,
                                             WorkInCompanyDao workInCompanyDao) {
    return new StatisticResource(chStatisticsDao, statisticXlsService, employerService, workInCompanyDao);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public StatisticXlsService statisticXlsService(CHStatisticsDao chStatisticsDao) {
    return new StatisticXlsService(chStatisticsDao);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public MailingAnalyticsDao mailingAnalyticsDao(NamedParameterJdbcTemplate clickHouseNamedJdbcTemplate) {
    return new MailingAnalyticsDao(clickHouseNamedJdbcTemplate);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public MailingAnalyticsService mailingAnalyticsService(MailingAnalyticsDao mailingAnalyticsDao, SettingsClient settingsClient) {
    return new MailingAnalyticsService(mailingAnalyticsDao, settingsClient);
  }

  @Bean
  @Conditional(TestEnvironmentCondition.class)
  public ServicesAnalyticsResource servicesAnalyticsResource(MailingAnalyticsService mailingAnalyticsService) {
    return new ServicesAnalyticsResource(mailingAnalyticsService);
  }
}
