package ru.hh.content.engine.vacancy.of.the.day.service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.content.engine.clients.banner.dto.VacancyUrlDto;
import ru.hh.content.engine.services.BannerService;
import ru.hh.content.engine.services.LogicService;
import ru.hh.content.engine.vacancy.of.the.day.dto.ScoredVacancyResult;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayViewDto;
import ru.hh.settings.SettingsClient;

public class ScoredVacancyService {
  public static final Logger LOGGER = LoggerFactory.getLogger(ScoredVacancyService.class);
  private static final int DEFAULT_LIMIT = 20;
  public static final String SEARCH_SCORE_LIMIT_SETTING_NAME = "vacancy_of_the_day.score.limit";
  
  private final BannerService bannerService;
  private final LogicService logicService;
  private final SettingsClient settingsClient;
  
  public ScoredVacancyService(BannerService bannerService,
                              LogicService logicService,
                              SettingsClient settingsClient) {
    this.bannerService = bannerService;
    this.logicService = logicService;
    this.settingsClient = settingsClient;
  }
  
  public ScoredVacancyResult score(Set<Integer> vacanciesOfTheDay) {
    LOGGER.info("Scoring start");
    Map<Integer, VacancyUrlDto> integerVacancyUrlDtoMap;
    try {
      integerVacancyUrlDtoMap = bannerService.getCachedAdVacanciesMap();
    } catch (RuntimeException e) {
      LOGGER.error("Scoring fail because of banners: ", e);
      return new ScoredVacancyResult(true);
    }
    
    Set<Integer> vacancies = integerVacancyUrlDtoMap.keySet();
    
    Set<Integer> mixedVacancies = new HashSet<>();
    mixedVacancies.addAll(vacanciesOfTheDay);
    mixedVacancies.addAll(vacancies);
  
    List<Integer> scoredVacancies;
    try {
      scoredVacancies = logicService.score(mixedVacancies, settingsClient.getInteger(SEARCH_SCORE_LIMIT_SETTING_NAME, DEFAULT_LIMIT));
    } catch (TimeoutException e) {
      LOGGER.error("Scoring fail because of logic: ", e);
      return new ScoredVacancyResult(true);
    }
    Set<Integer> scoredVacanciesSet = new HashSet<>(scoredVacancies);
    scoredVacanciesSet.retainAll(vacanciesOfTheDay);
  
    if (!scoredVacanciesSet.isEmpty() || scoredVacancies.size() < vacanciesOfTheDay.size()) {
      LOGGER.info("Scoring success but votd more relevant");
      return new ScoredVacancyResult(true);
    } else {
      LOGGER.info("Scoring success and clickme vacancies more relevant");
      return new ScoredVacancyResult(integerVacancyUrlDtoMap
          .entrySet()
          .stream()
          .filter(dtoEntry -> scoredVacancies.subList(0, vacanciesOfTheDay.size()).contains(dtoEntry.getKey()))
          .map(dtoEntry -> {
            VacancyOfTheDayViewDto vacancyOfTheDayViewDto = new VacancyOfTheDayViewDto(dtoEntry.getKey(), dtoEntry.getValue().getUrl());
            vacancyOfTheDayViewDto.setParams(dtoEntry.getValue().getParams());
            return vacancyOfTheDayViewDto;
          })
          .collect(Collectors.toList()), false);
    }
  }
}
