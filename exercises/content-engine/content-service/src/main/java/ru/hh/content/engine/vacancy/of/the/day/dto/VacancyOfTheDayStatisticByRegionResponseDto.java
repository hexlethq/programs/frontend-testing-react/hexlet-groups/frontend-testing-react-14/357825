package ru.hh.content.engine.vacancy.of.the.day.dto;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import ru.hh.content.engine.statistics.SumStatisticByRegion;

public class VacancyOfTheDayStatisticByRegionResponseDto {
  @JsonUnwrapped
  private SumStatisticByRegion statisticByRegion;
  private VacancyDataDto vacancyInfo;

  public VacancyOfTheDayStatisticByRegionResponseDto() {
  }

  public VacancyOfTheDayStatisticByRegionResponseDto(SumStatisticByRegion statisticByRegion, VacancyDataDto vacancyInfo) {
    this.statisticByRegion = statisticByRegion;
    this.vacancyInfo = vacancyInfo;
  }

  public SumStatisticByRegion getStatisticByRegion() {
    return statisticByRegion;
  }

  public void setStatisticByRegion(SumStatisticByRegion statisticByRegion) {
    this.statisticByRegion = statisticByRegion;
  }

  public VacancyDataDto getVacancyInfo() {
    return vacancyInfo;
  }

  public void setVacancyInfo(VacancyDataDto vacancyInfo) {
    this.vacancyInfo = vacancyInfo;
  }
}
