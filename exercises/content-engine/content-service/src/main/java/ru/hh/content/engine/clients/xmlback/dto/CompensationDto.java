package ru.hh.content.engine.clients.xmlback.dto;

import ru.hh.content.engine.vacancy.of.the.day.dto.view.CompensationCurrency;

public class CompensationDto {
  private Integer from;
  private Integer to;
  private CompensationCurrency currencyCode;

  public CompensationDto() {
  }

  public CompensationDto(Integer from, Integer to, CompensationCurrency currencyCode) {
    this.from = from;
    this.to = to;
    this.currencyCode = currencyCode;
  }

  public Integer getFrom() {
    return from;
  }

  public void setFrom(Integer from) {
    this.from = from;
  }

  public Integer getTo() {
    return to;
  }

  public void setTo(Integer to) {
    this.to = to;
  }

  public CompensationCurrency getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(CompensationCurrency currencyCode) {
    this.currencyCode = currencyCode;
  }
}
