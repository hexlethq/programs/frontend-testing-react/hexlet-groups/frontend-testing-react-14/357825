package ru.hh.content.engine.limits.clicks;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.limits.ClicksLimit;

public class ClicksLimitDao {
  private final SessionFactory sessionFactory;

  public ClicksLimitDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Transactional(readOnly = true)
  public ClicksLimit getClicksLimit(long contentLimitId) {
    return sessionFactory.getCurrentSession()
        .createQuery("from ClicksLimit where contentLimitId = :contentLimitId", ClicksLimit.class)
        .setParameter("contentLimitId", contentLimitId)
        .uniqueResult();
  }
}
