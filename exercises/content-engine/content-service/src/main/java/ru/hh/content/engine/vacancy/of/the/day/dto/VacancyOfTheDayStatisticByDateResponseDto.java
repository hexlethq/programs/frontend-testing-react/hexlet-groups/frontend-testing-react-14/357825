package ru.hh.content.engine.vacancy.of.the.day.dto;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import ru.hh.content.engine.statistics.SumStatisticByDate;

public class VacancyOfTheDayStatisticByDateResponseDto {
  @JsonUnwrapped
  private SumStatisticByDate statisticsByDate;
  private VacancyDataDto vacancyInfo;

  public VacancyOfTheDayStatisticByDateResponseDto() {
  }

  public VacancyOfTheDayStatisticByDateResponseDto(SumStatisticByDate statisticsByDate, VacancyDataDto vacancyInfo) {
    this.statisticsByDate = statisticsByDate;
    this.vacancyInfo = vacancyInfo;
  }

  public SumStatisticByDate getStatisticsByDate() {
    return statisticsByDate;
  }

  public void setStatisticsByDate(SumStatisticByDate statisticsByDate) {
    this.statisticsByDate = statisticsByDate;
  }

  public VacancyDataDto getVacancyInfo() {
    return vacancyInfo;
  }

  public void setVacancyInfo(VacancyDataDto vacancyInfo) {
    this.vacancyInfo = vacancyInfo;
  }
}
