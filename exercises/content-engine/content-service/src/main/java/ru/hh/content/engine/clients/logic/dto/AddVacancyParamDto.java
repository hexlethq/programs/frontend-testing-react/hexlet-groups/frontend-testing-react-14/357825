package ru.hh.content.engine.clients.logic.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

public class AddVacancyParamDto {
  @JsonProperty("add_vacancy_ids")
  Set<Integer> addVacancyIds;
  
  public AddVacancyParamDto() {
  }
  
  public AddVacancyParamDto(Set<Integer> addVacancyIds) {
    this.addVacancyIds = addVacancyIds;
  }
  
  public Set<Integer> getAddVacancyIds() {
    return addVacancyIds;
  }
  
  public void setAddVacancyIds(Set<Integer> addVacancyIds) {
    this.addVacancyIds = addVacancyIds;
  }
}
