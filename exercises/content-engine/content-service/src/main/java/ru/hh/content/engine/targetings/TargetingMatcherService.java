package ru.hh.content.engine.targetings;

public class TargetingMatcherService {
  private final TargetingService targetingService;

  public TargetingMatcherService(TargetingService targetingService) {
    this.targetingService = targetingService;
  }

  public boolean matchTargetings(UserDataForTargeting userDataForTargetings, long targetingId) {
    return targetingService.getTargetings(targetingId).stream()
        .allMatch(it -> {
          if (it.getTargetingData() == null) {
            return true;
          }
          return it.getTargetingData().match(userDataForTargetings);
        });
  }
}
