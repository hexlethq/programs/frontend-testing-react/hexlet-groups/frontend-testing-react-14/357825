package ru.hh.content.engine.vacancy.of.the.day.dao;

import java.time.LocalDate;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayPayedStatus;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;

public class VacancyOfTheDayDao {
  private final SessionFactory sessionFactory;

  public VacancyOfTheDayDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public void save(VacancyOfTheDay vacancyOfTheDay) {
    sessionFactory.getCurrentSession().save(vacancyOfTheDay);
  }

  @Transactional
  public VacancyOfTheDay get(long vacancyOfTheDayId) {
    return sessionFactory.getCurrentSession()
        .createQuery("FROM VacancyOfTheDay WHERE vacancyOfTheDayId = :vacancyOfTheDayId", VacancyOfTheDay.class)
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .uniqueResult();
  }

  @Transactional
  public List<VacancyOfTheDay> get(List<Long> vacancyOfTheDayIdList) {
    return sessionFactory.getCurrentSession()
        .createQuery("FROM VacancyOfTheDay WHERE vacancyOfTheDayId in :vacancyOfTheDayIdList", VacancyOfTheDay.class)
        .setParameterList("vacancyOfTheDayIdList", vacancyOfTheDayIdList)
        .list();
  }

  @Transactional
  public VacancyOfTheDay update(VacancyOfTheDay vacancyOfTheDay) {
    sessionFactory.getCurrentSession().update(vacancyOfTheDay);
    return vacancyOfTheDay;
  }

  @Transactional
  public void delete(long vacancyOfTheDayId) {
    sessionFactory.getCurrentSession()
        .createQuery("DELETE FROM VacancyOfTheDay WHERE vacancyOfTheDayId = :vacancyOfTheDayId")
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .executeUpdate();
  }

  @Transactional
  public void setPaymentStatus(long vacancyOfTheDayId, VacancyOfTheDayPayedStatus payedStatus) {
    sessionFactory.getCurrentSession()
        .createQuery("UPDATE VacancyOfTheDay SET payedStatus=:payedStatus WHERE vacancyOfTheDayId = :vacancyOfTheDayId")
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .setParameter("payedStatus", payedStatus)
        .executeUpdate();
  }

  @Transactional
  public void setStatus(List<Long> vacancyOfTheDayIdList, VacancyOfTheDayStatus status) {
    if (vacancyOfTheDayIdList.isEmpty()) {
      return;
    }

    sessionFactory.getCurrentSession()
        .createQuery("UPDATE VacancyOfTheDay SET status=:status " +
            "WHERE vacancyOfTheDayId IN :vacancyOfTheDayIdList")
        .setParameterList("vacancyOfTheDayIdList", vacancyOfTheDayIdList)
        .setParameter("status", status)
        .executeUpdate();
  }

  @Transactional(readOnly = true)
  public List<Long> checkOwner(List<Long> vacancyOfTheDayIdList, int managerUserId) {
    return sessionFactory.getCurrentSession()
        .createQuery("SELECT vacancyOfTheDayId " +
            "FROM VacancyOfTheDay " +
            "WHERE vacancyOfTheDayId IN :vacancyOfTheDayId AND managerUserId = :managerUserId", Long.class)
        .setParameterList("vacancyOfTheDayId", vacancyOfTheDayIdList)
        .setParameter("managerUserId", managerUserId)
        .list();
  }

  @Transactional(readOnly = true)
  public List<VacancyOfTheDay> getTodayVacanciesByRegions(LocalDate now,
                                                          List<Integer> regionIds,
                                                          TargetingDeviceType deviceType) {
    return sessionFactory.getCurrentSession()
        .createQuery("Select distinct votd from VacancyOfTheDay votd " +
                "join VacancyOfTheDayReservation reserv on votd.vacancyOfTheDayId = reserv.vacancyOfTheDayId " +
                "where :now between reserv.dateStart and reserv.dateEnd and reserv.deviceType = :deviceType " +
                "and votd.status = :activeStatus and reserv.regionId in :regionIds",
            VacancyOfTheDay.class)
        .setParameter("now", now)
        .setParameter("regionIds", regionIds)
        .setParameter("deviceType", deviceType)
        .setParameter("activeStatus", VacancyOfTheDayStatus.APPROVED)
        .list();
  }

  @Transactional(readOnly = true)
  public List<VacancyOfTheDay> getTodayVacanciesByPacket(LocalDate now,
                                                         TargetingDeviceType deviceType,
                                                         Collection<PacketType> packetTypes) {
    return sessionFactory.getCurrentSession()
        .createQuery("Select distinct votd from VacancyOfTheDay votd " +
                "join VacancyOfTheDayReservation reserv on votd.vacancyOfTheDayId = reserv.vacancyOfTheDayId " +
                "where :now between reserv.dateStart and reserv.dateEnd and reserv.deviceType = :deviceType " +
                "and votd.status = :activeStatus and votd.packetType in :packetTypes",
            VacancyOfTheDay.class)
        .setParameter("now", now)
        .setParameter("deviceType", deviceType)
        .setParameter("activeStatus", VacancyOfTheDayStatus.APPROVED)
        .setParameterList("packetTypes", packetTypes)
        .list();
  }

  public List<VacancyOfTheDay> getVacanciesOfTheDayWithDatesByPeriodAndEmployer(Long vacancyOfTheDayId,
                                                                                Integer vacancyId,
                                                                                Integer employerId,
                                                                                String employerName,
                                                                                Integer managerUserId,
                                                                                String managerName,
                                                                                String crmUrl,
                                                                                LocalDate dateFrom,
                                                                                LocalDate dateTo,
                                                                                Set<TargetingDeviceType> deviceTypes,
                                                                                Set<VacancyOfTheDayStatus> campaignStatuses,
                                                                                Set<VacancyOfTheDayPayedStatus> paymentStatuses,
                                                                                Set<VacancyOfTheDayCampaignType> campaignTypes,
                                                                                Set<PacketType> packetTypes
  ) {
    Query<VacancyOfTheDay> query = sessionFactory.getCurrentSession()
        .createQuery("select votd " +
            "from VacancyOfTheDayReservation votdr join VacancyOfTheDay votd on votd.vacancyOfTheDayId = votdr.vacancyOfTheDayId " +
            buildFilterSql(vacancyOfTheDayId,
                vacancyId,
                employerId,
                employerName,
                managerUserId,
                managerName,
                crmUrl,
                deviceTypes,
                campaignStatuses,
                paymentStatuses,
                campaignTypes,
                packetTypes) +
            " group by votd " +
            buildHavingSql(dateFrom, dateTo), VacancyOfTheDay.class);
    
    query = addFilterParams(vacancyOfTheDayId,
        vacancyId,
        employerId,
        managerUserId,
        dateFrom,
        dateTo,
        deviceTypes,
        campaignStatuses,
        paymentStatuses,
        campaignTypes,
        packetTypes,
        query);

    return query.list();
  }

  private Query<VacancyOfTheDay> addFilterParams(Long vacancyOfTheDayId,
                                                 Integer vacancyId,
                                                 Integer employerId,
                                                 Integer managerUserId,
                                                 LocalDate dateFrom,
                                                 LocalDate dateTo,
                                                 Set<TargetingDeviceType> deviceTypes,
                                                 Set<VacancyOfTheDayStatus> campaignStatuses,
                                                 Set<VacancyOfTheDayPayedStatus> paymentStatuses,
                                                 Set<VacancyOfTheDayCampaignType> campaignTypes,
                                                 Set<PacketType> packetTypes,
                                                 Query<VacancyOfTheDay> query) {
    if (vacancyOfTheDayId != null) {
      query = query.setParameter("vacancyOfTheDayId", vacancyOfTheDayId);
    }
    if (vacancyId != null) {
      query = query.setParameter("vacancyId", vacancyId);
    }
    if (employerId != null) {
      query = query.setParameter("employerId", employerId);
    }
    if (managerUserId != null) {
      query = query.setParameter("managerUserId", managerUserId);
    }
    if (dateFrom != null) {
      query = query.setParameter("dateFrom", dateFrom);
    }
    if (dateTo != null) {
      query = query.setParameter("dateTo", dateTo);
    }
    if (deviceTypes != null && !deviceTypes.isEmpty()) {
      query = query.setParameterList("deviceTypes", deviceTypes);
    }
    if (campaignStatuses != null && !campaignStatuses.isEmpty()) {
      query = query.setParameterList("campaignStatuses", campaignStatuses);
    }
    if (paymentStatuses != null && !paymentStatuses.isEmpty()) {
      query = query.setParameterList("paymentStatuses", paymentStatuses);
    }

    if (campaignTypes != null && !campaignTypes.isEmpty()) {
      query = query.setParameterList("campaignTypes", campaignTypes);
    }

    if (packetTypes != null && !packetTypes.isEmpty()) {
      query = query.setParameterList("packetTypes", packetTypes);
    }
    return query;
  }

  private String buildFilterSql(Long vacancyOfTheDayId,
                                Integer vacancyId,
                                Integer employerId,
                                String employerName,
                                Integer managerUserId,
                                String managerName,
                                String crmUrl,
                                Set<TargetingDeviceType> deviceTypes,
                                Set<VacancyOfTheDayStatus> campaignStatuses,
                                Set<VacancyOfTheDayPayedStatus> paymentStatuses,
                                Set<VacancyOfTheDayCampaignType> campaignTypes,
                                Set<PacketType> packetTypes) {
    StringBuilder filter = new StringBuilder();

    boolean notFirstConditionFlag = false;

    Map<String, String> exactlyParams = new HashMap<>();
    exactlyParams.put("vacancyOfTheDayId", toString(vacancyOfTheDayId));
    exactlyParams.put("vacancyId", toString(vacancyId));
    exactlyParams.put("employerId", toString(employerId));
    exactlyParams.put("managerUserId", toString(managerUserId));

    Map<String, String> likeParams = new HashMap<>();
    likeParams.put("managerName", toLike(managerName));
    likeParams.put("crmUrl", toLike(crmUrl));
    likeParams.put("employerName", toLike(employerName));

    notFirstConditionFlag = addExacltyParams(filter, notFirstConditionFlag, exactlyParams);

    notFirstConditionFlag = addLikeParams(filter, notFirstConditionFlag, likeParams);

    notFirstConditionFlag = addCollectionFilter(packetTypes, filter, notFirstConditionFlag, " votd.packetType in :packetTypes");
    notFirstConditionFlag = addCollectionFilter(paymentStatuses, filter, notFirstConditionFlag, " votd.payedStatus in :paymentStatuses");
    notFirstConditionFlag = addCollectionFilter(campaignTypes, filter, notFirstConditionFlag, " votd.campaignType in :campaignTypes");
    notFirstConditionFlag = addCollectionFilter(deviceTypes, filter, notFirstConditionFlag, " votdr.deviceType in :deviceTypes");
    addCollectionFilter(campaignStatuses, filter, notFirstConditionFlag, " votd.status in :campaignStatuses");

    return filter.toString();
  }

  private boolean addLikeParams(StringBuilder filter, boolean notFirstConditionFlag, Map<String, String> likeParams) {
    for (Map.Entry<String, String> entry : likeParams.entrySet()) {
      if (entry.getValue() != null) {
        if (notFirstConditionFlag) {
          filter.append(" and");
        } else {
          filter.append(" where");
          notFirstConditionFlag = true;
        }
        filter.append(String.format(" lower(votd.%s) like '%s'", entry.getKey(), entry.getValue()));
      }
    }
    return notFirstConditionFlag;
  }

  private boolean addExacltyParams(StringBuilder filter, boolean notFirstConditionFlag, Map<String, String> exactlyParams) {
    for (Map.Entry<String, String> entry : exactlyParams.entrySet()) {
      if (entry.getValue() != null) {
        if (notFirstConditionFlag) {
          filter.append(" and");
        } else {
          filter.append(" where");
          notFirstConditionFlag = true;
        }
        filter.append(String.format(" votd.%s = :%s", entry.getKey(), entry.getKey()));
      }
    }
    return notFirstConditionFlag;
  }

  private <T> boolean addCollectionFilter(Collection<T> collection, StringBuilder filter, boolean notFirstConditionFlag, String filterSql) {
    if (collection != null && !collection.isEmpty()) {
      if (notFirstConditionFlag) {
        filter.append(" and");
      } else {
        filter.append(" where");
        notFirstConditionFlag = true;
      }

      filter.append(filterSql);
    }
    return notFirstConditionFlag;
  }

  private String buildHavingSql(LocalDate dateFrom, LocalDate dateTo) {
    StringBuilder having = new StringBuilder();
    if (dateFrom != null && dateTo == null) {
      having.append("having max(votdr.dateEnd) >= :dateFrom ");
    } else if (dateFrom == null && dateTo != null) {
      having.append("having min(votdr.dateStart) <= :dateTo) ");
    } else if (dateFrom != null && dateTo != null) {
      having.append("having ((min(votdr.dateStart) >= :dateFrom and min(votdr.dateStart) <= :dateTo) " +
          "or (max(votdr.dateEnd) >= :dateFrom and max(votdr.dateEnd) <= :dateTo)  " +
          "or (min(votdr.dateStart) <= :dateFrom and max(votdr.dateEnd) >= :dateTo)) ");
    }
    return having.toString();
  }

  private String toString(Number value) {
    if (value != null) {
      return value.toString();
    } else {
      return null;
    }
  }

  private String toLike(String name) {
    if (name != null) {
      return "%" + name.toLowerCase() + "%";
    } else {
      return null;
    }
  }
}
