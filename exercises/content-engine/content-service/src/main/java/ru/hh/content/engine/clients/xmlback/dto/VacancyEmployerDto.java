package ru.hh.content.engine.clients.xmlback.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VacancyEmployerDto {
  @JsonProperty("id")
  private Integer employerId;
  @JsonProperty("name")
  private String employerName;
  private EmployerDepartmentDto department;

  public VacancyEmployerDto() {
  }

  public VacancyEmployerDto(Integer employerId, String employerName) {
    this.employerId = employerId;
    this.employerName = employerName;
  }

  public Integer getEmployerId() {
    return employerId;
  }

  public void setEmployerId(Integer employerId) {
    this.employerId = employerId;
  }

  public String getEmployerName() {
    return employerName;
  }

  public void setEmployerName(String employerName) {
    this.employerName = employerName;
  }

  public EmployerDepartmentDto getDepartment() {
    return department;
  }

  public void setDepartment(EmployerDepartmentDto department) {
    this.department = department;
  }
}
