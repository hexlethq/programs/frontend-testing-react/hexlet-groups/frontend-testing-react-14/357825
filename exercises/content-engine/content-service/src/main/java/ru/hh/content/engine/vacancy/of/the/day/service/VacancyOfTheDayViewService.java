package ru.hh.content.engine.vacancy.of.the.day.service;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.util.UriComponentsBuilder;
import ru.hh.content.api.dto.ContentEvent;
import ru.hh.content.api.dto.ContentType;
import ru.hh.content.api.dto.EventType;
import ru.hh.content.engine.area.region.mapping.AreaRegionMappingDao;
import ru.hh.content.engine.clients.xmlback.HHVacancyClient;
import ru.hh.content.engine.clients.xmlback.dto.CompensationDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyEmployerDto;
import ru.hh.content.engine.clients.xmlback.dto.VacancyIdsList;
import ru.hh.content.engine.dto.ImpressionRequestDto;
import ru.hh.content.engine.kafka.banner.KafkaBannerPublisher;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDay;
import ru.hh.content.engine.services.ExperimentService;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.content.engine.services.VacancyService;
import static ru.hh.content.engine.utils.CommonUtils.getPlatformBySession;
import static ru.hh.content.engine.utils.ExceptionUtils.getWithExceptionsToRuntimeWrapper;
import ru.hh.content.engine.utils.cache.CacheService;
import ru.hh.content.engine.vacancy.of.the.day.dao.VacancyOfTheDayDao;
import ru.hh.content.engine.vacancy.of.the.day.dto.ScoredVacancyResult;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyAreaDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyLinkDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayCompensationDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayEmployerDepartmentDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayEmployerViewDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayFromPlace;
import ru.hh.content.engine.vacancy.of.the.day.dto.view.VacancyOfTheDayViewDto;
import static ru.hh.content.engine.vacancy.of.the.day.service.VacancyOfTheDayService.MAX_VACANCIES_ON_PAGE;
import ru.hh.jclient.common.ResultWithStatus;
import ru.hh.microcore.DisplayType;
import ru.hh.settings.SettingsClient;

public class VacancyOfTheDayViewService {
  private static final Logger LOGGER = LoggerFactory.getLogger(VacancyOfTheDayViewService.class);
  public static final int MAX_VACANCIES_ON_ARTICLES = 8;
  public static final String AB_TEST_NAME_SETTING = "vacancy_of_the_day.score.abtest.name";

  private final HHSessionContext sessionContext;
  private final AreaRegionMappingDao areaRegionMappingDao;
  private final CacheService<List<VacancyOfTheDayViewDto>> cacheService;
  private final KafkaBannerPublisher kafkaBannerPublisher;
  private final VacancyOfTheDayDao vacancyOfTheDayDao;
  private final HHVacancyClient vacancyClient;
  private final VacancyService vacancyService;
  private final ExperimentService experimentService;
  private final ScoredVacancyService scoredVacancyService;
  private final SettingsClient settingsClient;

  public VacancyOfTheDayViewService(HHSessionContext sessionContext,
                                    AreaRegionMappingDao areaRegionMappingDao,
                                    CacheService<List<VacancyOfTheDayViewDto>> vacancyOfTheDayViewServiceCacheService,
                                    KafkaBannerPublisher kafkaBannerPublisher,
                                    VacancyOfTheDayDao vacancyOfTheDayDao,
                                    HHVacancyClient vacancyClient,
                                    VacancyService vacancyService,
                                    ExperimentService experimentService,
                                    ScoredVacancyService scoredVacancyService,
                                    SettingsClient settingsClient) {
    this.sessionContext = sessionContext;
    this.areaRegionMappingDao = areaRegionMappingDao;
    this.cacheService = vacancyOfTheDayViewServiceCacheService;
    this.kafkaBannerPublisher = kafkaBannerPublisher;
    this.vacancyOfTheDayDao = vacancyOfTheDayDao;
    this.vacancyClient = vacancyClient;
    this.vacancyService = vacancyService;
    this.experimentService = experimentService;
    this.scoredVacancyService = scoredVacancyService;
    this.settingsClient = settingsClient;
  }

  public List<VacancyOfTheDayViewDto> getVacanciesToView(LocalDate today,
                                                         ImpressionRequestDto impressionRequestDto,
                                                         VacancyOfTheDayFromPlace vacancyOfTheDayFromPlace) {

    TargetingDeviceType deviceType;
    if (sessionContext.getSession().displayType == DisplayType.DESKTOP) {
      deviceType = TargetingDeviceType.DESKTOP;
    } else {
      deviceType = TargetingDeviceType.MOBILE;
    }
    Optional<Integer> regionByArea = areaRegionMappingDao.getRegionByArea(sessionContext.getSession().locale.domainAreaId);
    String areaPath = sessionContext.getSession().locale.areaPath;
    if (regionByArea.isPresent()) {
      areaPath = String.format("%s%s.", areaPath, regionByArea.get());
    }
    String cacheKey = getCacheKey(today, deviceType, areaPath);
    Optional<List<VacancyOfTheDayViewDto>> cache = cacheService.getFromCache(cacheKey);
    List<VacancyOfTheDayViewDto> vacancyOfTheDayViewDtos;

    if (cache.isEmpty()) {
      vacancyOfTheDayViewDtos = getVacancyOfTheDayDtos(today, areaPath, deviceType);
      Collections.shuffle(vacancyOfTheDayViewDtos);

      int freeSlotsCount = MAX_VACANCIES_ON_PAGE - vacancyOfTheDayViewDtos.size();
      if (freeSlotsCount > 0) {
        addStubs(vacancyOfTheDayViewDtos, freeSlotsCount, sessionContext.getSession().locale.domainAreaId);
      }

      setPlaceIds(vacancyOfTheDayViewDtos);

      fillInfo(vacancyOfTheDayViewDtos, vacancyOfTheDayFromPlace);

      cacheService.putInCache(cacheKey, vacancyOfTheDayViewDtos);
    } else {
      vacancyOfTheDayViewDtos = cache.get();
    }
  
    if (experimentService.check(getAbTestName())) {
      ScoredVacancyResult scoredVacancyResult = scoredVacancyService
          .score(vacancyOfTheDayViewDtos
              .stream()
              .map(VacancyOfTheDayViewDto::getVacancyId)
              .collect(Collectors.toSet()));
      if (!scoredVacancyResult.isVacancyOfTheDayRelevant()) {
        if (experimentService.checkABTest(getAbTestName())) {
          vacancyOfTheDayViewDtos = scoredVacancyResult.getScoredVacancies();
          setPlaceIds(vacancyOfTheDayViewDtos);
          fillInfo(vacancyOfTheDayViewDtos, vacancyOfTheDayFromPlace);
        }
        experimentService.send();
      }
    }
    

    if (vacancyOfTheDayFromPlace == VacancyOfTheDayFromPlace.ARTICLE) {
      vacancyOfTheDayViewDtos = vacancyOfTheDayViewDtos.subList(0, MAX_VACANCIES_ON_ARTICLES);
    }


    for (VacancyOfTheDayViewDto vacancyOfTheDayViewDto : vacancyOfTheDayViewDtos) {
      kafkaBannerPublisher.sendKafkaMessage(
          "content_events",
          fillContentImpressionEvent(
              impressionRequestDto,
              Optional.ofNullable(vacancyOfTheDayViewDto.getVacancyOfTheDayId()).orElse(0L),
              vacancyOfTheDayViewDto.getPlaceId()
          )
      );
    }

    return vacancyOfTheDayViewDtos;
  }

  private void fillInfo(List<VacancyOfTheDayViewDto> vacancyOfTheDayViewDtos, VacancyOfTheDayFromPlace vacancyOfTheDayFromPlace) {
    Map<Integer, VacancyDto> vacancyIdToVacancy = vacancyService
        .getVacancies(vacancyOfTheDayViewDtos.stream()
            .map(VacancyOfTheDayViewDto::getVacancyId).collect(Collectors.toSet())
        ).stream().collect(Collectors.toMap(
            VacancyDto::getVacancyId, Function.identity()
        ));

    for (VacancyOfTheDayViewDto vacancyOfTheDayViewDto : vacancyOfTheDayViewDtos) {
      processVacancyOfTheDayInfo(vacancyIdToVacancy, vacancyOfTheDayViewDto, vacancyOfTheDayFromPlace);
    }
  }

  private void processVacancyOfTheDayInfo(Map<Integer, VacancyDto> vacancyIdToVacancy,
                                          VacancyOfTheDayViewDto vacancyOfTheDayViewDto,
                                          VacancyOfTheDayFromPlace vacancyOfTheDayFromPlace) {
    VacancyDto vacancyDto = vacancyIdToVacancy.get(vacancyOfTheDayViewDto.getVacancyId());

    addClickUrl(vacancyOfTheDayViewDto, vacancyOfTheDayFromPlace);

    vacancyOfTheDayViewDto.setArea(new VacancyAreaDto(vacancyDto.getArea().getId()));
    vacancyOfTheDayViewDto.setName(vacancyDto.getName());

    addEmployer(vacancyOfTheDayViewDto, vacancyDto);

    addCompensation(vacancyOfTheDayViewDto, vacancyDto);
  }

  private void addEmployer(VacancyOfTheDayViewDto vacancyOfTheDayViewDto, VacancyDto vacancyDto) {
    VacancyEmployerDto vacancyEmployerDto = vacancyDto.getVacancyEmployerDto();
    vacancyOfTheDayViewDto.setCompany(
        new VacancyOfTheDayEmployerViewDto(
            vacancyEmployerDto.getEmployerId(),
            vacancyEmployerDto.getEmployerName(),
            Optional.ofNullable(vacancyEmployerDto.getDepartment())
                .map(it -> new VacancyOfTheDayEmployerDepartmentDto(it.getName(), it.getCode()))
                .orElse(null)
        ));
  }

  private void addCompensation(VacancyOfTheDayViewDto vacancyOfTheDayViewDto, VacancyDto vacancyDto) {
    CompensationDto compensation = vacancyDto.getCompensation();
    VacancyOfTheDayCompensationDto compensationDto;
    if (compensation.getFrom() == null && compensation.getTo() == null) {
      compensationDto = new VacancyOfTheDayCompensationDto(true);
    } else {
      compensationDto = new VacancyOfTheDayCompensationDto(
          compensation.getFrom(),
          compensation.getTo(),
          compensation.getCurrencyCode()
      );
    }
    vacancyOfTheDayViewDto.setCompensation(compensationDto);
  }

  private void addClickUrl(VacancyOfTheDayViewDto vacancyOfTheDayViewDto, VacancyOfTheDayFromPlace vacancyOfTheDayFromPlace) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.newInstance()
        .scheme("https")
        .host("content.hh.ru")
        .path("/api/v1/vacancy_of_the_day/click")
        .queryParam("vacancyId", vacancyOfTheDayViewDto.getVacancyId())
        .queryParam("contentId", Optional.ofNullable(vacancyOfTheDayViewDto.getVacancyOfTheDayId()).orElse(0L))
        .queryParam("placeId", vacancyOfTheDayViewDto.getPlaceId())
        .queryParam("domainAreaId", sessionContext.getSession().locale.domainAreaId)
        .queryParam("host", sessionContext.getSession().locale.host)
        .queryParam("hhid", sessionContext.getSession().getHhid())
        .queryParam("hhuid", sessionContext.getSession().uid);
    if (vacancyOfTheDayFromPlace == VacancyOfTheDayFromPlace.ARTICLE) {
      uriComponentsBuilder = uriComponentsBuilder.queryParam("from", "articles_vacancy_day");
    }
  
    if (vacancyOfTheDayViewDto.getParams() != null && !vacancyOfTheDayViewDto.getParams().isEmpty()) {
      for (Map.Entry<String, List<String>> param : vacancyOfTheDayViewDto.getParams().entrySet()) {
        for (String paramValue : param.getValue()) {
          uriComponentsBuilder.queryParam(param.getKey(), paramValue);
        }
      }
    }

    String clickUrl = uriComponentsBuilder
        .build(false).toUriString();
    vacancyOfTheDayViewDto.setClick(clickUrl);
    vacancyOfTheDayViewDto.setLinks(new VacancyLinkDto(clickUrl));
  }

  private void addStubs(List<VacancyOfTheDayViewDto> vacancyOfTheDayViewDtos, int freeSlotsCount, Integer domainAreaId) {
    ResultWithStatus<VacancyIdsList> vacancyIdsListResultWithStatus = getWithExceptionsToRuntimeWrapper(() -> vacancyClient.getVacanciesOfTheDay(
        domainAreaId,
        freeSlotsCount,
        vacancyOfTheDayViewDtos.stream().map(VacancyOfTheDayViewDto::getVacancyId).collect(Collectors.toList())
    ).get());

    if (!vacancyIdsListResultWithStatus.isSuccess()) {
      LOGGER.error("Vacancy of the day stubs failed to get with status code {}", vacancyIdsListResultWithStatus.getStatusCode());
      return;
    }

    Optional<VacancyIdsList> vacancyIdsListOptional = vacancyIdsListResultWithStatus.get();

    if (vacancyIdsListOptional.isEmpty() || vacancyIdsListOptional.get().getVacancyIds() == null) {
      LOGGER.error("Vacancy of the day stubs failed to get with empty response");
      return;
    }

    vacancyOfTheDayViewDtos.addAll(
        vacancyIdsListOptional.get().getVacancyIds().stream()
            .map(
                vacancyId -> new VacancyOfTheDayViewDto(vacancyId.getId())
            ).collect(Collectors.toList()));
  }

  private List<VacancyOfTheDayViewDto> getVacancyOfTheDayDtos(LocalDate dateTime, String areaPath, TargetingDeviceType deviceType) {
    List<Integer> regionIds = Arrays.stream(areaPath.substring(1).split("\\.")).map(Integer::valueOf).collect(Collectors.toList());

    List<VacancyOfTheDay> allTodayVacancyOfTheDay;

    if (regionIds.equals(List.of(113))) {
      Set<PacketType> packetTypes = new HashSet<>();

      packetTypes.add(PacketType.RUSSIA);
      packetTypes.add(PacketType.RUSSIA_WITHOUT_MOSCOW);

      allTodayVacancyOfTheDay = vacancyOfTheDayDao.getTodayVacanciesByPacket(
          dateTime,
          deviceType,
          packetTypes
      );
    } else {
      allTodayVacancyOfTheDay = vacancyOfTheDayDao.getTodayVacanciesByRegions(
          dateTime,
          regionIds,
          deviceType
      );
    }

    return allTodayVacancyOfTheDay
        .stream()
        .map(it -> new VacancyOfTheDayViewDto(it.getVacancyId(), it.getVacancyOfTheDayId()))
        .collect(Collectors.toList());
  }

  private void setPlaceIds(List<VacancyOfTheDayViewDto> vacancyOfTheDayViewDtoList) {
    for (int i = 0; i < vacancyOfTheDayViewDtoList.size(); i++) {
      vacancyOfTheDayViewDtoList.get(i).setPlaceId(i);
    }
  }

  private static String getCacheKey(LocalDate localDate, TargetingDeviceType targetingDeviceType, String areaPath) {
    return String.format("%s_%s_%s", localDate.toString(), targetingDeviceType, areaPath);
  }

  private ContentEvent fillContentImpressionEvent(ImpressionRequestDto requestDto, long contentId, int placeId) {
    Optional<Integer> regionByArea = areaRegionMappingDao.getRegionByArea(sessionContext.getSession().locale.domainAreaId);
    if (regionByArea.isEmpty()) {
      LOGGER.warn("User with hhuid {} has null region", sessionContext.getSession().uid);
    }

    return ContentEvent.newBuilder()
        .contentType(ContentType.VACANCY_OF_THE_DAY)
        .eventType(EventType.IMPRESSION)
        .hhuid(sessionContext.getSession().uid)
        .hhid(sessionContext.getSession().getHhid())
        .contentId(contentId)
        .contentPlaceId(placeId)
        .domainAreaId(regionByArea.orElse(0))
        .platform(getPlatformBySession(sessionContext))
        .remoteHostIp(requestDto.getRemoteHostIp())
        .requestPath(requestDto.getUriInfo().getRequestUri().toString())
        .userAgent(requestDto.getUserAgent())
        .build();
  }
  
  public String getAbTestName() {
    return settingsClient.getString(AB_TEST_NAME_SETTING).orElse(null);
  }
}
