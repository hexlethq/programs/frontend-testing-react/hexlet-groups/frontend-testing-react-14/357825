package ru.hh.content.engine.limits.impressions;

import ru.hh.content.engine.limits.ContentLimitCheckerService;
import ru.hh.content.engine.model.limits.ImpressionsLimit;
import ru.hh.content.engine.model.enums.ContentLimitType;

public class ImpressionsLimitCheckerService implements ContentLimitCheckerService {
  private final ImpressionsLimitDao impressionsLimitDao;

  public ImpressionsLimitCheckerService(ImpressionsLimitDao impressionsLimitDao) {
    this.impressionsLimitDao = impressionsLimitDao;
  }

  @Override
  public boolean isReached(long contentLimitId) {
    ImpressionsLimit impressionsLimit = impressionsLimitDao.getImpressionsLimit(contentLimitId);
    return isDailyReached(impressionsLimit) || isTotalReached(impressionsLimit);
  }

  @Override
  public ContentLimitType getContentLimitType() {
    return ContentLimitType.IMPRESSIONS;
  }

  private boolean isDailyReached(ImpressionsLimit impressionsLimit) {
    if (impressionsLimit.getLimitDailyImpressions() == null) {
      return false;
    }

    return impressionsLimit.getCurrentDailyImpressions() >= impressionsLimit.getLimitDailyImpressions();
  }

  private boolean isTotalReached(ImpressionsLimit impressionsLimit) {
    if (impressionsLimit.getLimitTotalImpressions() == null) {
      return false;
    }

    return impressionsLimit.getCurrentTotalImpressions() >= impressionsLimit.getLimitTotalImpressions();
  }
}
