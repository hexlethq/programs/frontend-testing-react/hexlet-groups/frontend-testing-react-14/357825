package ru.hh.content.engine.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.RegionsPath.REGIONS;
import static ru.hh.content.api.resources.RegionsPath.TRANSLATION_DICT;
import ru.hh.content.engine.services.FrontRegionService;

@Path(REGIONS)
public class RegionResource {
  private final FrontRegionService frontRegionService;

  @Inject
  public RegionResource(FrontRegionService frontRegionService) {
    this.frontRegionService = frontRegionService;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getRegions() {
    return Response.ok(
        frontRegionService.getRegions()
    ).build();
  }

  @GET
  @Path(TRANSLATION_DICT)
  @Produces(MediaType.APPLICATION_JSON)
  public Response getRegionsTranslationDict() {
    return Response.ok(
        frontRegionService.getRegionsTranslationDict()
    ).build();
  }
}
