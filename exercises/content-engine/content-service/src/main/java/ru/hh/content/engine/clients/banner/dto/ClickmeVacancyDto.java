package ru.hh.content.engine.clients.banner.dto;

public class ClickmeVacancyDto {
  private String url;
  
  public ClickmeVacancyDto() {
  }
  
  public ClickmeVacancyDto(String url) {
    this.url = url;
  }
  
  public String getUrl() {
    return url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
}
