package ru.hh.content.engine.vacancy.of.the.day.dto;

public class VacancyCreatorInfoDto {
  private int id;
  private String name;

  public VacancyCreatorInfoDto() {
  }

  public VacancyCreatorInfoDto(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
