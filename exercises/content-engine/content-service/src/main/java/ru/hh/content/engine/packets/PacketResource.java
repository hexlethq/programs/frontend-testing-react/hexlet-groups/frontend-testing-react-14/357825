package ru.hh.content.engine.packets;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.PacketsPath.PACKETS;

@Path(PACKETS)
public class PacketResource {
  private final PacketService packetService;

  @Inject
  public PacketResource(PacketService packetService) {
    this.packetService = packetService;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getRegions() {
    return Response.ok(
        packetService.getPackets()
    ).build();
  }
}
