package ru.hh.content.engine.model.enums;

public enum VacancyOfTheDayPayedStatus {
  NOT_PAYED,
  PAYED,
  POST_PAYED,
}
