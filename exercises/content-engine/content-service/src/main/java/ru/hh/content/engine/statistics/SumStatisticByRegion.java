package ru.hh.content.engine.statistics;

import java.util.List;
import ru.hh.content.engine.work.in.company.dto.EmployerInfo;

public class SumStatisticByRegion {
  private List<StatisticByRegion> statisticByRegions;
  private StatisticRow totalStatistic;
  private EmployerInfo employerInfo;

  public SumStatisticByRegion() {
  }

  public SumStatisticByRegion(List<StatisticByRegion> statisticByRegions, StatisticRow totalStatistic) {
    this.statisticByRegions = statisticByRegions;
    this.totalStatistic = totalStatistic;
  }

  public List<StatisticByRegion> getStatisticByRegions() {
    return statisticByRegions;
  }

  public void setStatisticByRegions(List<StatisticByRegion> statisticByRegions) {
    this.statisticByRegions = statisticByRegions;
  }

  public StatisticRow getTotalStatistic() {
    return totalStatistic;
  }

  public void setTotalStatistic(StatisticRow totalStatistic) {
    this.totalStatistic = totalStatistic;
  }

  public EmployerInfo getEmployerInfo() {
    return employerInfo;
  }

  public void setEmployerInfo(EmployerInfo employerInfo) {
    this.employerInfo = employerInfo;
  }
}
