package ru.hh.content.engine.clients.xmlback.dto;

import java.util.List;

public class VacancyListDto {
  private List<VacancyDto> vacancies;
  
  public VacancyListDto() {
  }
  
  public VacancyListDto(List<VacancyDto> vacancyList) {
    this.vacancies = vacancyList;
  }
  
  public List<VacancyDto> getVacancies() {
    return vacancies;
  }
  
  public void setVacancies(List<VacancyDto> vacancyList) {
    this.vacancies = vacancyList;
  }
}
