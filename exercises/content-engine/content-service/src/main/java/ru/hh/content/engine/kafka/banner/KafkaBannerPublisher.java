package ru.hh.content.engine.kafka.banner;

import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.nab.kafka.producer.KafkaProducer;
import ru.hh.nab.kafka.producer.KafkaSendResult;

public class KafkaBannerPublisher {
  private static final Logger LOGGER = LoggerFactory.getLogger(KafkaBannerPublisher.class);
  private static final String LOG_STRING = "Trying to publish kafka while no kafka producer is present. Need to configure kafka producer in app.";

  private Optional<KafkaProducer> kafkaBannerProducer;

  @Inject
  public KafkaBannerPublisher(Optional<KafkaProducer> kafkaBannerProducer) {
    this.kafkaBannerProducer = kafkaBannerProducer;
  }

  public <T> CompletableFuture<KafkaSendResult<T>> sendKafkaMessage(String topic, T kafkaMessage) {
    return sendKafkaMessage(topic, null, kafkaMessage);
  }

  public <T> CompletableFuture<KafkaSendResult<T>> sendKafkaMessage(String topic, String key, T kafkaMessage) {
    return kafkaBannerProducer.map(template ->
        template.sendMessage(topic, key, kafkaMessage))
        .orElseGet(() -> {
          LOGGER.error(LOG_STRING);
          return CompletableFuture.failedFuture(new IllegalStateException(LOG_STRING));
        });
  }
}
