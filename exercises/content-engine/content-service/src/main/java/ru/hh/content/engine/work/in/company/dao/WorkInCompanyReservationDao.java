package ru.hh.content.engine.work.in.company.dao;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collection;
import static java.util.Collections.emptyMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.work.in.company.WorkInCompanyReservation;

public class WorkInCompanyReservationDao {
  private final SessionFactory sessionFactory;

  public WorkInCompanyReservationDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @SuppressWarnings("unchecked")
  public Map<Integer, Integer> getRegionToReservationNumber(LocalDate dateFrom, TargetingDeviceType deviceType, List<Integer> regionIds) {
    if (regionIds.isEmpty()) {
      return Map.of();
    }

    String withClauseBuilder = getWithClause(regionIds).toString();

    List<Object[]> list = sessionFactory.getCurrentSession().createNativeQuery(
        String.format("%sSELECT gen_reservations.region_id, min(gen_reservation_number) " +
        "FROM gen_reservations " +
        "left join work_in_company_reservation reserv " +
        "on reserv.region_id = gen_reservations.region_id " +
            "and reserv.reservation_number = gen_reservations.gen_reservation_number " +
            "and reserv.date_start = :dateStart " +
            "and reserv.device_type = :deviceType " +
        "where reserv.region_id is null " +
        "GROUP BY gen_reservations.region_id", withClauseBuilder))
        .setParameter("dateStart", dateFrom)
        .setParameter("deviceType", deviceType.name())
        .list();

    Map<Integer, Integer> regionToReservationNumber = new HashMap<>();

    for (Object[] row : list) {
      regionToReservationNumber.put((Integer) row[0], (Integer) row[1]);
    }

    return regionToReservationNumber;
  }

  private StringBuilder getWithClause(List<Integer> regionIds) {
    StringBuilder withClauseBuilder = new StringBuilder("with gen_reservations as (select region_id, gen_reservation_number from (values ");

    for (int i = 0; i < 12; i++) {
      for (int j = 0; j < regionIds.size(); j++) {
        withClauseBuilder.append(String.format("(%s, %s)", regionIds.get(j), i));
        if (i != 11 || j != regionIds.size() - 1) {
          withClauseBuilder.append(", ");
        }
      }
    }

    withClauseBuilder.append(") as region_ids(region_id, gen_reservation_number))");
    return withClauseBuilder;
  }

  public void saveWorkInEmployerReservations(List<WorkInCompanyReservation> workInCompanyReservations) {
    for (WorkInCompanyReservation workInCompanyReservation : workInCompanyReservations) {
      sessionFactory.getCurrentSession().save(workInCompanyReservation);
    }
  }

  public void deleteWorkInEmployerReservations(long workInCompanyId) {
    sessionFactory.getCurrentSession()
        .createQuery("delete from WorkInCompanyReservation where workInCompanyId = :workInCompanyId")
        .setParameter("workInCompanyId", workInCompanyId)
        .executeUpdate();
  }

  @SuppressWarnings("unchecked")
  @Transactional(readOnly = true)
  public List<Integer> getBusyRegionsForPeriod(Collection<TargetingDeviceType> deviceTypes,
                                               Collection<Integer> regionIds,
                                               LocalDate dateStart,
                                               LocalDate dateEnd,
                                               int maxCompaniesOnPage) {
    if (deviceTypes.isEmpty() || regionIds.isEmpty()) {
      return List.of();
    }
    return sessionFactory.getCurrentSession().createNativeQuery("select distinct region_id from work_in_company_reservation " +
        "where date_start < :dateEnd and date_start >= :dateStart and region_id in :regionIds and device_type in :deviceTypes " +
        "group by date_start, device_type, region_id having count(*) > :maxCompaniesOnPage")
        .setParameter("dateEnd", dateEnd)
        .setParameter("dateStart", dateStart)
        .setParameterList("regionIds", regionIds)
        .setParameterList("deviceTypes", deviceTypes.stream().map(Enum::name).collect(Collectors.toList()))
        .setParameter("maxCompaniesOnPage", maxCompaniesOnPage - 1)
        .list();
  }

  @SuppressWarnings("unchecked")
  @Transactional(readOnly = true)
  public Map<Integer, LocalDate> getNextFreeDatesByRegions(Collection<TargetingDeviceType> deviceTypes,
                                                           int duration,
                                                           Collection<Integer> regionIds,
                                                           LocalDate dateStart,
                                                           int maxCompaniesOnPage
                                                           ) {
    if (regionIds.isEmpty() || deviceTypes.isEmpty()) {
      return emptyMap();
    }

    List<Object[]> list = sessionFactory.getCurrentSession().createNativeQuery("select region_id, min(date_start) from (" +
        "select date_start, region_id from work_in_company_reservation as wicr " +
        "where wicr.region_id in :regionIds and wicr.device_type in :deviceTypes and wicr.date_start >= :dateStart " +
        "group by date_start, device_type, region_id " +
        "having count(*) > :maxCompaniesOnPage and not exists(" +
        "select 1 from work_in_company_reservation inner_wicr where inner_wicr.date_start > wicr.date_start " +
        "and inner_wicr.date_start - interval '1' day * :duration <= wicr.date_start and inner_wicr.region_id = wicr.region_id " +
        "and inner_wicr.device_type = wicr.device_type group by date_start, device_type, region_id having count(*) > :maxCompaniesOnPage)) " +
        "as full_reservs group by region_id")
        .setParameterList("deviceTypes", deviceTypes.stream().map(Enum::name).collect(Collectors.toList()))
        .setParameterList("regionIds", regionIds)
        .setParameter("dateStart", dateStart)
        .setParameter("duration", duration)
        .setParameter("maxCompaniesOnPage", maxCompaniesOnPage - 1)
        .list();

    Map<Integer, LocalDate> regionToNearestFreeDate = new HashMap<>();

    for (Object[] row : list) {
      regionToNearestFreeDate.put((Integer) row[0], ((Date) row[1]).toLocalDate());
    }

    return regionToNearestFreeDate;
  }
}
