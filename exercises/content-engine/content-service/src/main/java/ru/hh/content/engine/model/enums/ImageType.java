package ru.hh.content.engine.model.enums;

public enum ImageType {
  XS(40, 130),
  SCREEN(10000, 10000),
  ;
  
  private final int height;
  private final int width;
  
  ImageType(int height, int width) {
    this.height = height;
    this.width = width;
  }
  
  public int getHeight() {
    return height;
  }
  
  public int getWidth() {
    return width;
  }
}
