package ru.hh.content.engine.model.limits;

import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "clicks_limit")
public class ClicksLimit {
  @Id
  @Column(name = "content_limit_id", nullable = false)
  private Long contentLimitId;

  @Column(name = "current_total_clicks", nullable = false)
  private Long currentTotalClicks;

  @Column(name = "current_daily_clicks", nullable = false)
  private Long currentDailyClicks;

  @Column(name = "limit_total_clicks")
  private Long limitTotalClicks;

  @Column(name = "limit_daily_clicks")
  private Long limitDailyClicks;

  public ClicksLimit() {
    this.currentTotalClicks = 0L;
    this.currentDailyClicks = 0L;
  }

  public Long getContentLimitId() {
    return contentLimitId;
  }

  public void setContentLimitId(Long contentLimitId) {
    this.contentLimitId = contentLimitId;
  }

  public Long getCurrentTotalClicks() {
    return currentTotalClicks;
  }

  public void setCurrentTotalClicks(Long currentTotalClicks) {
    this.currentTotalClicks = currentTotalClicks;
  }

  public Long getCurrentDailyClicks() {
    return currentDailyClicks;
  }

  public void setCurrentDailyClicks(Long currentDailyClicks) {
    this.currentDailyClicks = currentDailyClicks;
  }

  public Long getLimitTotalClicks() {
    return limitTotalClicks;
  }

  public void setLimitTotalClicks(Long limitTotalClicks) {
    this.limitTotalClicks = limitTotalClicks;
  }

  public Long getLimitDailyClicks() {
    return limitDailyClicks;
  }

  public void setLimitDailyClicks(Long limitDailyClicks) {
    this.limitDailyClicks = limitDailyClicks;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ClicksLimit that = (ClicksLimit) o;
    return Objects.equals(contentLimitId, that.contentLimitId) &&
        Objects.equals(currentTotalClicks, that.currentTotalClicks) &&
        Objects.equals(currentDailyClicks, that.currentDailyClicks) &&
        Objects.equals(limitTotalClicks, that.limitTotalClicks) &&
        Objects.equals(limitDailyClicks, that.limitDailyClicks);
  }

  @Override
  public int hashCode() {
    return Objects.hash(contentLimitId, currentTotalClicks, currentDailyClicks, limitTotalClicks, limitDailyClicks);
  }

  @Override
  public String toString() {
    return "ClicksLimit{" +
        "contentLimitId=" + contentLimitId +
        ", currentTotalClicks=" + currentTotalClicks +
        ", currentDailyClicks=" + currentDailyClicks +
        ", limitTotalClicks=" + limitTotalClicks +
        ", limitDailyClicks=" + limitDailyClicks +
        '}';
  }
}
