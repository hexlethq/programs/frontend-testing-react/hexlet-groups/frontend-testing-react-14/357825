package ru.hh.content.engine.model.targetings;

import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class SalaryTargetingData extends TargetingData {
  private Set<SalaryRange> salaryRanges;
  private boolean includePredictedSalary;

  public SalaryTargetingData() {
  }

  public SalaryTargetingData(Long min, Long max, boolean includePredictedSalary) {
    salaryRanges = Set.of(new SalaryRange(min, max));
    this.includePredictedSalary = includePredictedSalary;
  }

  public SalaryTargetingData(Set<SalaryRange> salaryRanges, boolean includePredictedSalary) {
    this.salaryRanges = salaryRanges;
    this.includePredictedSalary = includePredictedSalary;
  }

  public Set<SalaryRange> getSalaryRanges() {
    return salaryRanges;
  }

  public void setSalaryRanges(Set<SalaryRange> salaryRanges) {
    this.salaryRanges = salaryRanges;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    if (salaryRanges.isEmpty()) {
      return true;
    }
    Long userSalary = userDataForTargeting.getSalary();

    if (includePredictedSalary && userSalary == null) {
      userSalary = userDataForTargeting.getPredictedSalary();
    }

    if (userSalary == null) {
      return false;
    }

    Long finalUserSalary = userSalary;
    return salaryRanges.stream().anyMatch(it -> it.isBetweenRange(finalUserSalary));
  }

  public static class SalaryRange {
    private Long min;
    private Long max;

    public SalaryRange() {
    }

    public SalaryRange(Long min, Long max) {
      this.min = min;
      this.max = max;
    }

    public long getMin() {
      return min;
    }

    public void setMin(Long min) {
      this.min = min;
    }

    public long getMax() {
      return max;
    }

    public void setMax(Long max) {
      this.max = max;
    }

    private boolean isBetweenRange(long salary) {
      return (min == null || salary >= min)
          && (max == null || salary <= max);
    }
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SalaryTargetingData that = (SalaryTargetingData) o;
    return includePredictedSalary == that.includePredictedSalary &&
        Objects.equals(salaryRanges, that.salaryRanges);
  }

  @Override
  public int hashCode() {
    return Objects.hash(salaryRanges, includePredictedSalary);
  }

  @Override
  public String toString() {
    return "SalaryTargetingData{" +
        "salaryRanges=" + salaryRanges +
        ", includePredictedSalary=" + includePredictedSalary +
        '}';
  }
}
