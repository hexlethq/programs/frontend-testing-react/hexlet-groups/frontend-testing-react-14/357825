package ru.hh.content.engine.clients.xmlback;

public class WorkInCompanyDisplayInfo {
  private String logoUrl;
  private Long vacancyCount;
  private String companyName;

  public WorkInCompanyDisplayInfo() {
  }

  public WorkInCompanyDisplayInfo(String companyName) {
    this.companyName = companyName;
  }

  public WorkInCompanyDisplayInfo(String logoUrl, Long vacancyCount) {
    this.logoUrl = logoUrl;
    this.vacancyCount = vacancyCount;
  }

  public String getLogoUrl() {
    return logoUrl;
  }

  public void setLogoUrl(String logoUrl) {
    this.logoUrl = logoUrl;
  }

  public Long getVacancyCount() {
    return vacancyCount;
  }

  public void setVacancyCount(Long vacancyCount) {
    this.vacancyCount = vacancyCount;
  }

  public String getCompanyName() {
    return companyName;
  }

  public void setCompanyName(String companyName) {
    this.companyName = companyName;
  }
}
