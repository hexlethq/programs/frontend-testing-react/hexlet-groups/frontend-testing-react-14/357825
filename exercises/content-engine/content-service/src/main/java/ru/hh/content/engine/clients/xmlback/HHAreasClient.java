package ru.hh.content.engine.clients.xmlback;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.concurrent.CompletableFuture;
import javax.inject.Inject;
import ru.hh.content.engine.clients.xmlback.dto.AreasDto;
import static ru.hh.content.engine.utils.CommonUtils.createObjectMapper;
import ru.hh.jclient.common.HttpClientFactory;
import ru.hh.jclient.common.JClientBase;
import ru.hh.jclient.common.ResultWithStatus;
import ru.hh.nab.common.properties.FileSettings;

public class HHAreasClient extends JClientBase {
  private static final String AREAS_PATH = "/rs/area/tree";

  private final ObjectMapper objectMapper;
  private final FileSettings fileSettings;

  @Inject
  public HHAreasClient(FileSettings fileSettings,
                       HttpClientFactory xmlBackHttpClientFactory) {
    super(fileSettings.getString("jclient.hh-xmlback.internalEndpointUrl"), xmlBackHttpClientFactory);
    this.objectMapper = createObjectMapper();
    this.fileSettings = fileSettings;
  }

  public CompletableFuture<ResultWithStatus<AreasDto>> getAreasList() {
    return http
        .with(get(jerseyUrl(AREAS_PATH))
            .setRequestTimeout(fileSettings.getInteger("jclient.hh-xmlback.areas.timeout"))
            .build()
        )
        .expectJson(objectMapper, AreasDto.class)
        .resultWithStatus();
  }
}
