package ru.hh.content.engine.model.enums;

public enum PacketType {
  CUSTOM("Пакет \"Кастомный\"", true),
  RUSSIA_WITHOUT_MOSCOW("Пакет \"Вся Россия без Москвы\"", false),
  RUSSIA("Пакет \"Вся Россия\"", false),
  ;

  private final String translation;
  private final Boolean canChooseRegions;

  PacketType(String translation, Boolean canChooseRegions) {
    this.translation = translation;
    this.canChooseRegions = canChooseRegions;
  }

  public String getTranslation() {
    return translation;
  }

  public Boolean getCanChooseRegions() {
    return canChooseRegions;
  }
}
