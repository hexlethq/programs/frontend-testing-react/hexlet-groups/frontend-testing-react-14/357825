package ru.hh.content.engine.model.packets;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import ru.hh.content.engine.model.enums.PacketType;

@Entity
@Table(name = "packet_to_region")
public class PacketToRegion implements Serializable {
  @Id
  @Column(name = "packet_type", nullable = false)
  @Enumerated(EnumType.STRING)
  private PacketType packetType;

  @Id
  @Column(name = "region_id", nullable = false)
  private int regionId;

  public PacketType getPacketType() {
    return packetType;
  }

  public void setPacketType(PacketType packetType) {
    this.packetType = packetType;
  }

  public int getRegionId() {
    return regionId;
  }

  public void setRegionId(int regionId) {
    this.regionId = regionId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PacketToRegion that = (PacketToRegion) o;
    return regionId == that.regionId &&
        packetType == that.packetType;
  }

  @Override
  public int hashCode() {
    return Objects.hash(packetType, regionId);
  }

  @Override
  public String toString() {
    return "PacketToRegion{" +
        "packetType=" + packetType +
        ", regionId=" + regionId +
        '}';
  }
}
