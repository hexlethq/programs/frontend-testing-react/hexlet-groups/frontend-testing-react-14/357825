package ru.hh.content.engine.vacancy.of.the.day.dto.view;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.FIELD)
public class VacancyOfTheDayEmployerDepartmentDto {
  @XmlAttribute
  private String name;
  @XmlAttribute
  private String code;

  public VacancyOfTheDayEmployerDepartmentDto() {
  }

  public VacancyOfTheDayEmployerDepartmentDto(String name, String code) {
    this.name = name;
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }
}
