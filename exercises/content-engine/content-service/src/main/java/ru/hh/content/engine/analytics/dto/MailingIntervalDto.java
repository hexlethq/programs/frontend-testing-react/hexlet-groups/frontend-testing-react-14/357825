package ru.hh.content.engine.analytics.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.time.LocalDate;
import static ru.hh.content.api.CommonApiConstants.COMMON_DATE_FORMAT;
import ru.hh.content.engine.analytics.MailingServiceType;

public class MailingIntervalDto {
  private MailingServiceType type;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate startDate;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate endDate;

  public MailingIntervalDto(MailingServiceType type, LocalDate startDate, LocalDate endDate) {
    this.type = type;
    this.startDate = startDate;
    this.endDate = endDate;
  }

  public MailingServiceType getType() {
    return type;
  }

  public void setType(MailingServiceType type) {
    this.type = type;
  }

  public LocalDate getStartDate() {
    return startDate;
  }

  public void setStartDate(LocalDate startDate) {
    this.startDate = startDate;
  }

  public LocalDate getEndDate() {
    return endDate;
  }

  public void setEndDate(LocalDate endDate) {
    this.endDate = endDate;
  }
}
