package ru.hh.content.engine.utils;

import java.util.function.Consumer;

public class ExceptionUtils {
  public static <T> T getWithExceptionsToRuntimeWrapper(SupplierWithException<T> supplier) {
    return getWithExceptionsToRuntimeWrapper(supplier, null);
  }

  public static <T> T getWithExceptionsToRuntimeWrapper(SupplierWithException<T> supplier, Consumer<Exception> customRuntimeExceptionConsumer) {
    try {
      return supplier.get();
    } catch (Exception e) {
      if (e instanceof InterruptedException) {
        Thread.currentThread().interrupt();
      }
      if (customRuntimeExceptionConsumer != null) {
        customRuntimeExceptionConsumer.accept(e);
      }
      throw new RuntimeException(e);
    }
  }
}
