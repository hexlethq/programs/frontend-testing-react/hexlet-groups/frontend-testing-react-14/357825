package ru.hh.content.engine.services;

import ru.hh.content.engine.model.enums.ImageType;
import static ru.hh.content.engine.services.ContentWebDavService.getRandomBigIntegerFromUUID;
import ru.hh.content.engine.utils.ContentImage;
import ru.hh.errors.common.Errors;

public class ImageLoaderService {
  private final ContentWebDavService contentWebDavService;

  public ImageLoaderService(ContentWebDavService contentWebDavService) {
    this.contentWebDavService = contentWebDavService;
  }

  public String uploadImageAndGetFileName(ContentImage contentImage, ImageType imageType) {
    validateImage(contentImage, imageType);
    try {
      String fileName = getFileName(contentImage.getImageFormat());
      contentWebDavService.saveFile(fileName, contentImage.getBytes());
      return fileName;
    } catch (Exception e) {
      throw new Errors(502, "webdav_error", "Webdav error").toWebApplicationException();
    }
  }

  private String getFileName(String formatName) {
    return String.format("%040d.%s", getRandomBigIntegerFromUUID(), formatName);
  }

  public String getFullFileUrl(String filename) {
    return contentWebDavService.getFullFileUrl(filename);
  }

  private void validateImage(ContentImage contentImage, ImageType imageType) {
    if (contentImage.getHeight() > imageType.getHeight() || contentImage.getWidth() > imageType.getWidth()) {
      String errorDescription = String.format(
          "{\"width\": %s, \"height\": %s}",
          contentImage.getWidth(),
          contentImage.getHeight()
      );
      throw new Errors(400, "validation_error", errorDescription).toWebApplicationException();
    }
  }
}
