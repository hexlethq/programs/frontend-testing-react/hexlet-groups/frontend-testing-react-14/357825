package ru.hh.content.engine.statistics;

public class StatisticByContentId {
  private Long contentId;
  private Long views;
  private Long clicks;
  private Float ctr;

  public StatisticByContentId() {
  }

  public StatisticByContentId(Long contentId, Long views, Long clicks, Float ctr) {
    this.contentId = contentId;
    this.views = views;
    this.clicks = clicks;
    this.ctr = ctr;
  }

  public Long getContentId() {
    return contentId;
  }

  public void setContentId(Long contentId) {
    this.contentId = contentId;
  }

  public Long getViews() {
    return views;
  }

  public void setViews(Long views) {
    this.views = views;
  }

  public Long getClicks() {
    return clicks;
  }

  public void setClicks(Long clicks) {
    this.clicks = clicks;
  }

  public Float getCtr() {
    return ctr;
  }

  public void setCtr(Float ctr) {
    this.ctr = ctr;
  }
}
