package ru.hh.content.engine.model.enums;

import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import static javax.persistence.EnumType.STRING;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Parameter;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import ru.hh.content.engine.model.JsonBType;
import ru.hh.content.engine.model.targetings.TargetingData;

@Entity
@Table(name = "targeting")
@TypeDef(name = "JsonBType", typeClass = JsonBType.class)
public class Targeting implements Serializable {
  @Id
  @Column(name = "targeting_id", nullable = false)
  private Long targetingId;

  @Id
  @Column(name = "targeting_type", nullable = false)
  @Enumerated(value = STRING)
  private TargetingType targetingType;

  @Column(name = "targeting_data", nullable = false)
  @Type(
      type = "ru.hh.content.engine.model.JsonBType",
      parameters = {@Parameter(name = "jsonClass", value = "ru.hh.content.engine.model.targetings.TargetingData")}
  )
  private TargetingData targetingData;

  public Targeting() {
  }

  public Targeting(Long targetingId, TargetingType targetingType, TargetingData targetingData) {
    this.targetingId = targetingId;
    this.targetingType = targetingType;
    this.targetingData = targetingData;
  }

  public Long getTargetingId() {
    return targetingId;
  }

  public void setTargetingId(Long targetingId) {
    this.targetingId = targetingId;
  }

  public TargetingData getTargetingData() {
    return targetingData;
  }

  public void setTargetingData(TargetingData targetingData) {
    this.targetingData = targetingData;
  }

  public TargetingType getTargetingType() {
    return targetingType;
  }

  public void setTargetingType(TargetingType targetingType) {
    this.targetingType = targetingType;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Targeting targeting = (Targeting) o;
    return Objects.equals(targetingId, targeting.targetingId) &&
        targetingType == targeting.targetingType &&
        Objects.equals(targetingData, targeting.targetingData);
  }

  @Override
  public int hashCode() {
    return Objects.hash(targetingId, targetingType, targetingData);
  }

  @Override
  public String toString() {
    return "Targeting{" +
        "targetingId=" + targetingId +
        ", targetingType=" + targetingType +
        ", targetingData=" + targetingData +
        '}';
  }
}
