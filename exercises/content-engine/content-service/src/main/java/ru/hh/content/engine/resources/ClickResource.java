package ru.hh.content.engine.resources;

import java.net.URI;
import java.util.Optional;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.hh.content.api.dto.ContentEvent;
import ru.hh.content.api.dto.ContentType;
import ru.hh.content.api.dto.EventType;
import static ru.hh.content.api.resources.EventResourcePath.CLICK_RESOURCE;
import ru.hh.content.engine.area.region.mapping.AreaRegionMappingDao;
import ru.hh.content.engine.dto.ClickRequestDto;
import ru.hh.content.engine.kafka.banner.KafkaBannerPublisher;
import ru.hh.content.engine.services.HHSessionContext;
import static ru.hh.content.engine.utils.CommonUtils.getPlatformBySession;

@Path(CLICK_RESOURCE)
public class ClickResource {
  public static final Logger LOGGER = LoggerFactory.getLogger(ClickResource.class);

  private final KafkaBannerPublisher kafkaBannerPublisher;
  private final HHSessionContext sessionContext;
  private final AreaRegionMappingDao areaRegionMappingDao;

  @Inject
  public ClickResource(KafkaBannerPublisher kafkaBannerPublisher,
                       HHSessionContext sessionContext,
                       AreaRegionMappingDao areaRegionMappingDao) {
    this.kafkaBannerPublisher = kafkaBannerPublisher;
    this.sessionContext = sessionContext;
    this.areaRegionMappingDao = areaRegionMappingDao;
  }

  @GET
  public Response click(@BeanParam ClickRequestDto clickRequestDto, @Context HttpServletRequest request) {
    kafkaBannerPublisher.sendKafkaMessage("content_events", fillContentClickEvent(clickRequestDto));
    return Response.temporaryRedirect(
        URI.create(String.format("%s/employer/%s", clickRequestDto.getHost(), clickRequestDto.getEmployerId()))
    ).build();
  }

  private ContentEvent fillContentClickEvent(ClickRequestDto requestDto) {
    Optional<Integer> regionByArea = areaRegionMappingDao.getRegionByArea(requestDto.getDomainAreaId());
    if (regionByArea.isEmpty()) {
      LOGGER.error("User with hhuid {} has null region", requestDto.getHhuid());
    }

    return ContentEvent.newBuilder()
        .contentType(ContentType.CAMPAIGN_OF_THE_DAY)
        .eventType(EventType.CLICK)
        .contentId(requestDto.getContentId())
        .contentPlaceId(requestDto.getPlaceId())
        .hhuid(sessionContext.getSession().uid)
        .hhid(Optional.ofNullable(requestDto.getHhid()).orElse(0L))
        .domainAreaId(regionByArea.orElse(0))
        .remoteHostIp(requestDto.getRemoteHostIp())
        .platform(getPlatformBySession(sessionContext))
        .requestPath(requestDto.getUriInfo().getRequestUri().toString())
        .userAgent(requestDto.getUserAgent())
        .build();
  }
}
