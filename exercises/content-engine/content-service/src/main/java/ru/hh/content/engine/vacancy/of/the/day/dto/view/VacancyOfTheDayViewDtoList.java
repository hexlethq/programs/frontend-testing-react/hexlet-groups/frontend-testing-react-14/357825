package ru.hh.content.engine.vacancy.of.the.day.dto.view;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "vacancies-of-the-day")
@XmlAccessorType(XmlAccessType.FIELD)
public class VacancyOfTheDayViewDtoList {
  @XmlElement(name = "vacancy")
  private List<VacancyOfTheDayViewDto> vacancyOfTheDayViewDtoList;

  public VacancyOfTheDayViewDtoList() {
  }

  public VacancyOfTheDayViewDtoList(List<VacancyOfTheDayViewDto> vacancyOfTheDayViewDtoList) {
    this.vacancyOfTheDayViewDtoList = vacancyOfTheDayViewDtoList;
  }

  public List<VacancyOfTheDayViewDto> getVacancyOfTheDayViewDtoList() {
    return vacancyOfTheDayViewDtoList;
  }

  public void setVacancyOfTheDayViewDtoList(List<VacancyOfTheDayViewDto> vacancyOfTheDayViewDtoList) {
    this.vacancyOfTheDayViewDtoList = vacancyOfTheDayViewDtoList;
  }
}
