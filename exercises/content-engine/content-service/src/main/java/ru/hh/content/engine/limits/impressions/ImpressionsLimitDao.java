package ru.hh.content.engine.limits.impressions;

import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.limits.ImpressionsLimit;

public class ImpressionsLimitDao {
  private final SessionFactory sessionFactory;

  public ImpressionsLimitDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Transactional(readOnly = true)
  public ImpressionsLimit getImpressionsLimit(long contentLimitId) {
    return sessionFactory.getCurrentSession()
        .createQuery("from ImpressionsLimit where contentLimitId = :contentLimitId", ImpressionsLimit.class)
        .setParameter("contentLimitId", contentLimitId)
        .uniqueResult();
  }
}
