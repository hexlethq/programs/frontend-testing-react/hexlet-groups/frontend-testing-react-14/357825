package ru.hh.content.engine.work.in.company.resource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.RegionsPath.REGIONS;
import static ru.hh.content.api.resources.RegionsPath.WITH_TRANSLATIONS;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.work.in.company.service.WorkInCompanyRegionsService;

@Path(REGIONS)
public class WorkInCompanyRegionsResource {
  private final WorkInCompanyRegionsService regionsService;
  private final SecurityContext securityContext;

  @Inject
  public WorkInCompanyRegionsResource(WorkInCompanyRegionsService regionsService, SecurityContext securityContext) {
    this.regionsService = regionsService;
    this.securityContext = securityContext;
  }

  @GET
  @Path(WITH_TRANSLATIONS)
  public Response getRegionsWithTranslations() {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(regionsService.getRegionsForWorkInCompany()).build();
  }
}
