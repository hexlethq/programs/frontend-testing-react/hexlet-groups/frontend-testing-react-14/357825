package ru.hh.content.engine.clients.logic.dto;

import java.util.List;

public class ScoredVacanciesDto {
  List<Integer> vacancies;
  
  public ScoredVacanciesDto() {
  
  }
  
  public ScoredVacanciesDto(List<Integer> vacancies) {
    this.vacancies = vacancies;
  }
  
  public List<Integer> getVacancies() {
    return vacancies;
  }
  
  public void setVacancies(List<Integer> vacancies) {
    this.vacancies = vacancies;
  }
}
