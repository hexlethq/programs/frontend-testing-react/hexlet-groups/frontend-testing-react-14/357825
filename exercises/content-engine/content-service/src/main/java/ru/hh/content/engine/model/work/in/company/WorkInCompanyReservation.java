package ru.hh.content.engine.model.work.in.company;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import ru.hh.content.engine.model.enums.TargetingDeviceType;

@Entity
@Table(name = "work_in_company_reservation")
public class WorkInCompanyReservation {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "work_in_company_reservation_id", nullable = false)
  private int workInCompanyReservationId;

  @Column(name = "date_start", nullable = false)
  private LocalDate dateStart;

  @Column(name = "date_end", nullable = false)
  private LocalDate dateEnd;

  @Column(name = "work_in_company_id", nullable = false)
  private Long workInCompanyId;

  @Column(name = "device_type", nullable = false)
  @Enumerated(EnumType.STRING)
  private TargetingDeviceType deviceType;

  @Column(name = "region_id", nullable = false)
  private Integer regionId;

  @Column(name = "is_exclusion", nullable = false)
  private Boolean isExclusion;

  @Column(name = "reservation_number")
  private Integer reservationNumber;

  public WorkInCompanyReservation() {
    this.isExclusion = false;
  }

  public WorkInCompanyReservation(LocalDate dateStart,
                                  LocalDate dateEnd,
                                  Long workInCompanyId,
                                  TargetingDeviceType deviceType,
                                  Integer regionId,
                                  Integer reservationNumber) {
    this();

    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.workInCompanyId = workInCompanyId;
    this.deviceType = deviceType;
    this.regionId = regionId;
    this.reservationNumber = reservationNumber;
  }

  public WorkInCompanyReservation(LocalDate dateStart,
                                  LocalDate dateEnd,
                                  Long workInCompanyId,
                                  TargetingDeviceType deviceType,
                                  Integer regionId,
                                  Boolean isExclusion) {
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.workInCompanyId = workInCompanyId;
    this.deviceType = deviceType;
    this.regionId = regionId;
    this.isExclusion = isExclusion;
  }

  public int getWorkInCompanyReservationId() {
    return workInCompanyReservationId;
  }

  public void setWorkInCompanyReservationId(int workInEmployerReservationId) {
    this.workInCompanyReservationId = workInEmployerReservationId;
  }

  public LocalDate getDateStart() {
    return dateStart;
  }

  public void setDateStart(LocalDate displayDate) {
    this.dateStart = displayDate;
  }

  public Long getWorkInCompanyId() {
    return workInCompanyId;
  }

  public void setWorkInCompanyId(Long workInEmployerId) {
    this.workInCompanyId = workInEmployerId;
  }

  public TargetingDeviceType getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(TargetingDeviceType deviceType) {
    this.deviceType = deviceType;
  }

  public Integer getRegionId() {
    return regionId;
  }

  public void setRegionId(Integer region) {
    this.regionId = region;
  }

  public LocalDate getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(LocalDate dateTo) {
    this.dateEnd = dateTo;
  }

  public Integer getReservationNumber() {
    return reservationNumber;
  }

  public void setReservationNumber(Integer reservationNumber) {
    this.reservationNumber = reservationNumber;
  }

  public Boolean getExclusion() {
    return isExclusion;
  }

  public void setExclusion(Boolean exclusion) {
    isExclusion = exclusion;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    WorkInCompanyReservation that = (WorkInCompanyReservation) o;
    return workInCompanyReservationId == that.workInCompanyReservationId &&
        Objects.equals(dateStart, that.dateStart) &&
        Objects.equals(dateEnd, that.dateEnd) &&
        Objects.equals(workInCompanyId, that.workInCompanyId) &&
        deviceType == that.deviceType &&
        Objects.equals(regionId, that.regionId) &&
        Objects.equals(isExclusion, that.isExclusion) &&
        Objects.equals(reservationNumber, that.reservationNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(workInCompanyReservationId, dateStart, dateEnd, workInCompanyId, deviceType, regionId, isExclusion, reservationNumber);
  }

  @Override
  public String toString() {
    return "WorkInCompanyReservation{" +
        "workInCompanyReservationId=" + workInCompanyReservationId +
        ", dateStart=" + dateStart +
        ", dateEnd=" + dateEnd +
        ", workInCompanyId=" + workInCompanyId +
        ", deviceType=" + deviceType +
        ", regionId=" + regionId +
        ", isExclusion=" + isExclusion +
        ", reservationNumber=" + reservationNumber +
        '}';
  }
}
