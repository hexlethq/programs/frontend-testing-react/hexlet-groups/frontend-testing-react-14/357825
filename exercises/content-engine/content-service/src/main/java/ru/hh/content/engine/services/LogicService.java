package ru.hh.content.engine.services;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import javax.inject.Inject;
import javax.ws.rs.core.Response;
import ru.hh.content.engine.clients.logic.LogicClient;
import ru.hh.content.engine.clients.logic.dto.ScoredVacanciesDto;
import static ru.hh.content.engine.utils.ExceptionUtils.getWithExceptionsToRuntimeWrapper;
import ru.hh.jclient.common.ResultWithStatus;

public class LogicService {
  private final LogicClient logicClient;
  
  @Inject
  public LogicService(LogicClient logicClient) {
    this.logicClient = logicClient;
  }
  
  public List<Integer> score(Set<Integer> vacancies, int limit) throws TimeoutException {
    ResultWithStatus<ScoredVacanciesDto> resultWithStatus = getWithExceptionsToRuntimeWrapper(
        () -> logicClient.scoreVacancies(vacancies, limit).get()
    );
    
    if(resultWithStatus.getStatusCode() == Response.Status.REQUEST_TIMEOUT.getStatusCode()) {
      throw new TimeoutException();
    }
    
    if (!resultWithStatus.isSuccess()) {
      throw new RuntimeException(String.format("Fail get vacancies with status code %s", resultWithStatus.getStatusCode()));
    }
    
    Optional<ScoredVacanciesDto> optionalVacancyListDto = resultWithStatus.get();
    
    if (optionalVacancyListDto.isEmpty()
        || optionalVacancyListDto.get().getVacancies() == null
        || optionalVacancyListDto.get().getVacancies().size() < 1) {
      return Collections.emptyList();
    }
    
    return optionalVacancyListDto.get().getVacancies();
  }
}
