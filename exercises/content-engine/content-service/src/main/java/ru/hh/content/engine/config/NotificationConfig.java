package ru.hh.content.engine.config;

import java.util.Properties;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.mailer.utils.MailClientFactory;
import ru.hh.mailer.utils.MailClientFactorySettings;
import ru.hh.mailer.utils.MailService;
import ru.hh.mailer.utils.MailServiceImpl;
import ru.hh.nab.common.properties.FileSettings;
import ru.hh.nab.metrics.StatsDSender;
import ru.hh.nab.starter.NabProdConfig;

@Configuration
@Import({
    NabProdConfig.class,
})
public class NotificationConfig {
  @Bean
  protected MailService mailService(final FileSettings settings, String serviceName, StatsDSender statsDSender) {
    final Properties mailClientFactoryProperties = settings.getSubProperties("mail.client");
    final MailClientFactorySettings mailClientFactorySettings = new MailClientFactorySettings(mailClientFactoryProperties);
    final MailClientFactory mailClientFactory = new MailClientFactory(
        mailClientFactorySettings,
        BooleanUtils.toBoolean(settings.getBoolean("sendStats")),
        serviceName,
        statsDSender
    );
    return new MailServiceImpl(mailClientFactory);
  }
}
