package ru.hh.content.engine.model.vacancy.of.the.day;

import java.time.LocalDate;
import java.util.Objects;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;
import ru.hh.content.engine.model.enums.TargetingDeviceType;

@Entity
@Table(name = "vacancy_of_the_day_reservation")
public class VacancyOfTheDayReservation {
  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "vacancy_of_the_day_reservation_id", nullable = false)
  private int vacancyOfTheDayReservationId;

  @Column(name = "date_start", nullable = false)
  private LocalDate dateStart;

  @Column(name = "date_end", nullable = false)
  private LocalDate dateEnd;

  @Column(name = "vacancy_of_the_day_id", nullable = false)
  private Long vacancyOfTheDayId;

  @Column(name = "device_type", nullable = false)
  @Enumerated(EnumType.STRING)
  private TargetingDeviceType deviceType;

  @Column(name = "region_id", nullable = false)
  private Integer regionId;

  @Column(name = "reservation_number")
  private Integer reservationNumber;

  public VacancyOfTheDayReservation() {
  }

  public VacancyOfTheDayReservation(LocalDate dateStart,
                                    LocalDate dateEnd,
                                    Long vacancyOfTheDayId,
                                    TargetingDeviceType deviceType,
                                    Integer regionId,
                                    Integer reservationNumber) {
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
    this.vacancyOfTheDayId = vacancyOfTheDayId;
    this.deviceType = deviceType;
    this.regionId = regionId;
    this.reservationNumber = reservationNumber;
  }

  public int getVacancyOfTheDayReservationId() {
    return vacancyOfTheDayReservationId;
  }

  public void setVacancyOfTheDayReservationId(int vacancyOfTheDayReservationId) {
    this.vacancyOfTheDayReservationId = vacancyOfTheDayReservationId;
  }

  public LocalDate getDateStart() {
    return dateStart;
  }

  public void setDateStart(LocalDate dateStart) {
    this.dateStart = dateStart;
  }

  public LocalDate getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(LocalDate dateEnd) {
    this.dateEnd = dateEnd;
  }

  public Long getVacancyOfTheDayId() {
    return vacancyOfTheDayId;
  }

  public void setVacancyOfTheDayId(Long vacancyOfTheDayId) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }

  public TargetingDeviceType getDeviceType() {
    return deviceType;
  }

  public void setDeviceType(TargetingDeviceType deviceType) {
    this.deviceType = deviceType;
  }

  public Integer getRegionId() {
    return regionId;
  }

  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }

  public Integer getReservationNumber() {
    return reservationNumber;
  }

  public void setReservationNumber(Integer reservationNumber) {
    this.reservationNumber = reservationNumber;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    VacancyOfTheDayReservation that = (VacancyOfTheDayReservation) o;
    return vacancyOfTheDayReservationId == that.vacancyOfTheDayReservationId &&
        Objects.equals(dateStart, that.dateStart) &&
        Objects.equals(dateEnd, that.dateEnd) &&
        Objects.equals(vacancyOfTheDayId, that.vacancyOfTheDayId) &&
        deviceType == that.deviceType &&
        Objects.equals(regionId, that.regionId) &&
        Objects.equals(reservationNumber, that.reservationNumber);
  }

  @Override
  public int hashCode() {
    return Objects.hash(vacancyOfTheDayReservationId, dateStart, dateEnd, vacancyOfTheDayId, deviceType, regionId, reservationNumber);
  }

  @Override
  public String toString() {
    return "VacancyOfTheDayReservation{" +
        "vacancyOfTheDayReservationId=" + vacancyOfTheDayReservationId +
        ", dateStart=" + dateStart +
        ", dateEnd=" + dateEnd +
        ", vacancyOfTheDayId=" + vacancyOfTheDayId +
        ", deviceType=" + deviceType +
        ", regionId=" + regionId +
        ", reservationNumber=" + reservationNumber +
        '}';
  }
}
