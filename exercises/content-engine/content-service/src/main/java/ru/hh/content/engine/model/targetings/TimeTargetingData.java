package ru.hh.content.engine.model.targetings;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class TimeTargetingData extends TargetingData {
  private Set<DayOfWeek> dayOfWeeks;
  private Set<Integer> hours;

  public TimeTargetingData() {
  }

  public TimeTargetingData(Set<DayOfWeek> dayOfWeeks, Set<Integer> hours) {
    this.dayOfWeeks = dayOfWeeks;
    this.hours = hours;
  }

  public Set<DayOfWeek> getDayOfWeeks() {
    return dayOfWeeks;
  }

  public void setDayOfWeeks(Set<DayOfWeek> dayOfWeeks) {
    this.dayOfWeeks = dayOfWeeks;
  }

  public Set<Integer> getHours() {
    return hours;
  }

  public void setHours(Set<Integer> hours) {
    this.hours = hours;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    return innerMatch(LocalDateTime.now());
  }

  boolean innerMatch(LocalDateTime now) {
    return matchDayOfAWeek(now) && matchHour(now);
  }

  private boolean matchDayOfAWeek(LocalDateTime now) {
    if (dayOfWeeks == null || dayOfWeeks.isEmpty()) {
      return true;
    }

    return dayOfWeeks.contains(now.getDayOfWeek());
  }

  private boolean matchHour(LocalDateTime now) {
    if (hours == null || hours.isEmpty()) {
      return true;
    }

    return hours.contains(now.getHour());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    TimeTargetingData that = (TimeTargetingData) o;
    return Objects.equals(dayOfWeeks, that.dayOfWeeks) &&
        Objects.equals(hours, that.hours);
  }

  @Override
  public int hashCode() {
    return Objects.hash(dayOfWeeks, hours);
  }

  @Override
  public String toString() {
    return "TimeTargetingData{" +
        "dayOfWeeks=" + dayOfWeeks +
        ", hours=" + hours +
        '}';
  }
}
