package ru.hh.content.engine.statistics;

import java.time.LocalDate;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import static javax.ws.rs.core.HttpHeaders.CONTENT_DISPOSITION;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import org.apache.poi.ss.usermodel.Workbook;
import static ru.hh.content.api.dto.ContentType.CAMPAIGN_OF_THE_DAY;
import static ru.hh.content.api.resources.StatisticResourcePath.STATISTIC_BY_DATE;
import static ru.hh.content.api.resources.StatisticResourcePath.STATISTIC_BY_DATE_XLS;
import static ru.hh.content.api.resources.StatisticResourcePath.STATISTIC_BY_REGION;
import static ru.hh.content.api.resources.StatisticResourcePath.STATISTIC_BY_REGION_XLS;
import static ru.hh.content.api.resources.StatisticResourcePath.STATISTIC_ROOT;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;
import ru.hh.content.engine.services.EmployerService;
import ru.hh.content.engine.utils.ResourceUtils;
import static ru.hh.content.engine.utils.ResourceUtils.assertDateFromIsBeforeDateTo;
import ru.hh.content.engine.work.in.company.dao.WorkInCompanyDao;
import ru.hh.content.engine.work.in.company.dto.EmployerInfo;

@Path(STATISTIC_ROOT)
public class StatisticResource {
  public static final String XLS_MEDIA_TYPE = "application/vnd.ms-excel";
  public static final String XLSX_EXT = "xlsx";
  private final CHStatisticsDao statisticsDao;
  private final StatisticXlsService statisticXlsService;
  private final EmployerService employerService;
  private final WorkInCompanyDao workInCompanyDao;

  @Inject
  public StatisticResource(CHStatisticsDao statisticsDao,
                           StatisticXlsService statisticXlsService,
                           EmployerService employerService,
                           WorkInCompanyDao workInCompanyDao) {
    this.statisticsDao = statisticsDao;
    this.statisticXlsService = statisticXlsService;
    this.employerService = employerService;
    this.workInCompanyDao = workInCompanyDao;
  }

  @GET
  @Path(STATISTIC_BY_DATE)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByDate(@QueryParam("fromDate") String dateFrom,
                                  @QueryParam("toDate") String dateTo,
                                  @NotNull @QueryParam("workInCompanyId") Integer workInCompanyId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);
    SumStatisticByDate statisticsByDate = statisticsDao.getStatisticsByDate(
        parsedDateFrom, parsedDateTo, workInCompanyId, CAMPAIGN_OF_THE_DAY
    );
    statisticsByDate.setEmployerInfo(getEmployerInfoByWorkInCompanyId(workInCompanyId));
    return Response.ok(statisticsByDate).build();
  }

  @GET
  @Path(STATISTIC_BY_DATE_XLS)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByDateXls(@QueryParam("fromDate") String dateFrom,
                                     @QueryParam("toDate") String dateTo,
                                     @NotNull @QueryParam("workInCompanyId") Integer workInCompanyId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);
    EmployerInfo employerInfo = getEmployerInfoByWorkInCompanyId(workInCompanyId);

    Workbook workbook = statisticXlsService.getStatisticByDateXls(parsedDateFrom, parsedDateTo, workInCompanyId, CAMPAIGN_OF_THE_DAY);
    StreamingOutput stream = workbook::write;
    String fileName = String.format("wic_id%s_%s_%s_%s_by_date", workInCompanyId, employerInfo.getName(), dateFrom, dateTo);
    return Response.ok(stream, XLS_MEDIA_TYPE)
        .header(CONTENT_DISPOSITION, String.format("inline; filename=\"%s.%s\"", fileName, XLSX_EXT))
        .build();
  }

  @GET
  @Path(STATISTIC_BY_REGION_XLS)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByRegionXls(@QueryParam("fromDate") String dateFrom,
                                       @QueryParam("toDate") String dateTo,
                                       @NotNull @QueryParam("workInCompanyId") Integer workInCompanyId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);
    EmployerInfo employerInfo = getEmployerInfoByWorkInCompanyId(workInCompanyId);

    Workbook workbook = statisticXlsService.getStatisticByRegionXls(parsedDateFrom, parsedDateTo, workInCompanyId, CAMPAIGN_OF_THE_DAY);
    StreamingOutput stream = workbook::write;
    String fileName = String.format("wic_id%s_%s_%s_%s_by_region", workInCompanyId, employerInfo.getName(), dateFrom, dateTo);
    return Response.ok(stream, XLS_MEDIA_TYPE)
        .header(CONTENT_DISPOSITION, String.format("inline; filename=\"%s.%s\"", fileName, XLSX_EXT))
        .build();
  }

  @GET
  @Path(STATISTIC_BY_REGION)
  @Produces(MediaType.APPLICATION_JSON)
  public Response statisticByRegion(@QueryParam("fromDate") String dateFrom,
                                    @QueryParam("toDate") String dateTo,
                                    @NotNull @QueryParam("workInCompanyId") Integer workInCompanyId
  ) {
    LocalDate parsedDateFrom = ResourceUtils.parseLocalDate(dateFrom);
    LocalDate parsedDateTo = ResourceUtils.parseLocalDate(dateTo);
    assertDateFromIsBeforeDateTo(parsedDateFrom, parsedDateTo);
    SumStatisticByRegion statisticsByRegion = statisticsDao.getStatisticsByRegion(parsedDateFrom, parsedDateTo, workInCompanyId, CAMPAIGN_OF_THE_DAY);
    statisticsByRegion.setEmployerInfo(getEmployerInfoByWorkInCompanyId(workInCompanyId));
    return Response.ok(statisticsByRegion).build();
  }

  private EmployerInfo getEmployerInfoByWorkInCompanyId(int workInCompanyId) {
    WorkInCompany workInCompany = workInCompanyDao.getWorkInCompany(workInCompanyId);
    return employerService.getEmployerInfo(workInCompany.getEmployerId());
  }
}
