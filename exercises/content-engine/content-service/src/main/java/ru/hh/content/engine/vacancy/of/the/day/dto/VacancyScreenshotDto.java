package ru.hh.content.engine.vacancy.of.the.day.dto;

import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDayScreenshot;

public class VacancyScreenshotDto {
  private String url;
  private int screenId;
  
  public VacancyScreenshotDto() {
  }
  
  public VacancyScreenshotDto(String src, int screenId) {
    this.url = src;
    this.screenId = screenId;
  }
  
  public VacancyScreenshotDto(VacancyOfTheDayScreenshot vacancyOfTheDayScreenshot) {
    this.url = vacancyOfTheDayScreenshot.getUrl();
    this.screenId = vacancyOfTheDayScreenshot.getVacancyOfTheDayScreenshotId();
  }
  
  public String getSrc() {
    return url;
  }
  
  public void setSrc(String src) {
    this.url = src;
  }
  
  public int getScreenId() {
    return screenId;
  }
  
  public void setScreenId(int screenId) {
    this.screenId = screenId;
  }
}
