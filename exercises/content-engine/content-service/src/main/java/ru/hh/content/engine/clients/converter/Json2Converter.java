package ru.hh.content.engine.clients.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.net.MediaType;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import ru.hh.jclient.common.responseconverter.JsonConverter;

public class Json2Converter<T> extends JsonConverter<T> {
  public static final String APPLICATION_JSON2 = "application/json2";

  static final Set<MediaType> MEDIA_TYPES = Collections.singleton(MediaType.parse(APPLICATION_JSON2));

  public Json2Converter(ObjectMapper objectMapper, Class<T> jsonClass) {
    super(objectMapper, jsonClass);
  }

  @Override
  protected Collection<MediaType> getMediaTypes() {
    return MEDIA_TYPES;
  }

}
