package ru.hh.content.engine.vacancy.of.the.day.dto.view;

public enum CompensationCurrency {
  USD,
  EUR,
  RUR,
  UAH,
  KZT,
  BYR,
  AZN,
  UZS,
  GEL,
  KGS,
  ;
}
