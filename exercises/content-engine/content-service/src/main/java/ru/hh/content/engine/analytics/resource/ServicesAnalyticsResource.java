package ru.hh.content.engine.analytics.resource;

import java.time.LocalDate;
import javax.inject.Inject;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import ru.hh.content.api.resources.MailingAnalyticsPaths;
import static ru.hh.content.api.resources.MailingAnalyticsPaths.VACANCY_MAILING_ANALYTICS_PATH;
import ru.hh.content.engine.analytics.service.MailingAnalyticsService;
import ru.hh.content.engine.utils.ResourceUtils;
import static ru.hh.content.engine.utils.ResourceUtils.assertDateFromIsBeforeDateTo;

@Path(MailingAnalyticsPaths.MAILING_ANALYTICS_PATH)
public class ServicesAnalyticsResource {
  private final MailingAnalyticsService mailingAnalyticsService;

  @Inject
  public ServicesAnalyticsResource(MailingAnalyticsService mailingAnalyticsService) {
    this.mailingAnalyticsService = mailingAnalyticsService;
  }

  @GET
  @Path(VACANCY_MAILING_ANALYTICS_PATH)
  @Produces(MediaType.APPLICATION_JSON)
  public Response getVacancyMailingAnalytics(@NotNull @PathParam("vacancyId") int vacancyId,
                                             @NotNull @QueryParam("startDate") String periodStartParam,
                                             @NotNull @QueryParam("endDate") String periodEndParam) {
    LocalDate periodStart = ResourceUtils.parseLocalDate(periodStartParam);
    LocalDate periodEnd = ResourceUtils.parseLocalDate(periodEndParam);
    assertDateFromIsBeforeDateTo(periodStart, periodEnd);
    var vacancyMailingAnalytics = mailingAnalyticsService.getVacancyMailingAnalytics(vacancyId, periodStart, periodEnd);
    return Response.ok(vacancyMailingAnalytics).build();
  }
}
