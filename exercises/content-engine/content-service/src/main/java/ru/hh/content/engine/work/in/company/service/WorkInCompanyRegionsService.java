package ru.hh.content.engine.work.in.company.service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.inject.Inject;
import ru.hh.content.engine.services.RegionsService;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyRegionDto;

public class WorkInCompanyRegionsService {
  public static final int OTHER_COUNTRIES = 1001;
  private final RegionsService regionsService;

  @Inject
  public WorkInCompanyRegionsService(RegionsService regionsService) {
    this.regionsService = regionsService;
  }

  public List<WorkInCompanyRegionDto> getRegionsForWorkInCompany() {
    List<Integer> russiaRegions = regionsService.getRussiaRegions();

    Map<Integer, String> translatedRegions = regionsService.getTranslatedAllRegions();
    return translatedRegions.entrySet().stream()
        .filter(it -> it.getKey() != OTHER_COUNTRIES)
        .map(it -> new WorkInCompanyRegionDto(it.getKey().toString(), it.getValue(), russiaRegions.contains(it.getKey())))
        .collect(Collectors.toList());
  }
}
