package ru.hh.content.engine.vacancy.of.the.day.dto.view;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.List;
import java.util.Map;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;

@XmlAccessorType(XmlAccessType.FIELD)
public class VacancyOfTheDayViewDto {
  @XmlAttribute
  private String click;
  private Integer vacancyId;
  private String name;
  private VacancyOfTheDayEmployerViewDto company;
  private VacancyOfTheDayCompensationDto compensation;
  private VacancyLinkDto links;
  private VacancyAreaDto area;

  @JsonIgnore
  private Integer placeId;
  @JsonIgnore
  private Long vacancyOfTheDayId;
  @JsonIgnore
  @XmlTransient
  private Map<String, List<String>> params;

  public VacancyOfTheDayViewDto() {
  }

  public VacancyOfTheDayViewDto(Integer vacancyId) {
    this.vacancyId = vacancyId;
  }

  public VacancyOfTheDayViewDto(Integer vacancyId, String link) {
    this.vacancyId = vacancyId;
    this.links = new VacancyLinkDto(link);
  }

  public VacancyOfTheDayViewDto(Integer vacancyId, Long vacancyOfTheDayId) {
    this.vacancyId = vacancyId;
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }

  public VacancyOfTheDayViewDto(String click,
                                Integer vacancyId,
                                String name,
                                VacancyOfTheDayEmployerViewDto company,
                                VacancyOfTheDayCompensationDto compensation,
                                VacancyLinkDto vacancyLinkDto,
                                VacancyAreaDto area) {
    this.click = click;
    this.vacancyId = vacancyId;
    this.name = name;
    this.company = company;
    this.compensation = compensation;
    this.area = area;
    this.links = vacancyLinkDto;
  }

  public String getClick() {
    return click;
  }

  public void setClick(String click) {
    this.click = click;
  }

  public Integer getVacancyId() {
    return vacancyId;
  }

  public void setVacancyId(Integer vacancyId) {
    this.vacancyId = vacancyId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public VacancyOfTheDayEmployerViewDto getCompany() {
    return company;
  }

  public void setCompany(VacancyOfTheDayEmployerViewDto company) {
    this.company = company;
  }

  public VacancyOfTheDayCompensationDto getCompensation() {
    return compensation;
  }

  public void setCompensation(VacancyOfTheDayCompensationDto compensation) {
    this.compensation = compensation;
  }

  public VacancyAreaDto getArea() {
    return area;
  }

  public void setArea(VacancyAreaDto area) {
    this.area = area;
  }

  public Integer getPlaceId() {
    return placeId;
  }

  public void setPlaceId(Integer placeId) {
    this.placeId = placeId;
  }

  public Long getVacancyOfTheDayId() {
    return vacancyOfTheDayId;
  }

  public void setVacancyOfTheDayId(Long vacancyOfTheDayId) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }

  public VacancyLinkDto getLinks() {
    return links;
  }

  public void setLinks(VacancyLinkDto links) {
    this.links = links;
  }
  
  public Map<String, List<String>> getParams() {
    return params;
  }
  
  public void setParams(Map<String, List<String>> params) {
    this.params = params;
  }
}
