package ru.hh.content.engine.model.work.in.company;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "area_region_mapping")
public class AreaRegionMapping {
  @Id
  @Column(name = "area_id", nullable = false)
  private Integer areaId;

  @Column(name = "region_id", nullable = false)
  private Integer regionId;

  public AreaRegionMapping() {
  }

  public AreaRegionMapping(Integer areaId, Integer regionId) {
    this.areaId = areaId;
    this.regionId = regionId;
  }

  public Integer getAreaId() {
    return areaId;
  }

  public void setAreaId(Integer areaId) {
    this.areaId = areaId;
  }

  public Integer getRegionId() {
    return regionId;
  }

  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }
}
