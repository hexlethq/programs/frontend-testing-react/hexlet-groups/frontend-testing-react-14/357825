package ru.hh.content.engine.work.in.company.dto;

public class WorkInCompanyRegionDto {
  private String id;
  private String text;
  private Boolean isRussian;

  public WorkInCompanyRegionDto() {
  }

  public WorkInCompanyRegionDto(String id, String text) {
    this.id = id;
    this.text = text;
  }

  public WorkInCompanyRegionDto(String id, String text, Boolean isRussian) {
    this.id = id;
    this.text = text;
    this.isRussian = isRussian;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Boolean getRussian() {
    return isRussian;
  }

  public void setRussian(Boolean russian) {
    isRussian = russian;
  }
}
