package ru.hh.content.engine.model.targetings;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class ProfAreaTargetingData extends TargetingData {
  private Set<Integer> profAreaIds;

  public ProfAreaTargetingData() {
  }

  public ProfAreaTargetingData(Set<Integer> profAreaIds) {
    this.profAreaIds = profAreaIds;
  }

  public Set<Integer> getProfAreaIds() {
    return profAreaIds;
  }

  public void setProfAreaIds(Set<Integer> profAreaIds) {
    this.profAreaIds = profAreaIds;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    if (profAreaIds.isEmpty() || userDataForTargeting.getProfAreaIds() == null) {
      return true;
    }

    return !Collections.disjoint(profAreaIds, userDataForTargeting.getProfAreaIds());
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ProfAreaTargetingData that = (ProfAreaTargetingData) o;
    return Objects.equals(profAreaIds, that.profAreaIds);
  }

  @Override
  public int hashCode() {
    return Objects.hash(profAreaIds);
  }

  @Override
  public String toString() {
    return "ProfAreaTargetingData{" +
        "profAreaIds=" + profAreaIds +
        '}';
  }
}
