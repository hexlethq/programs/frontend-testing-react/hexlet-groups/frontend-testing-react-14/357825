package ru.hh.content.engine.vacancy.of.the.day.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.LocalDate;
import java.util.Set;
import static ru.hh.content.api.CommonApiConstants.COMMON_DATE_FORMAT;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.utils.CommonUtils;

public class VacancyOfTheDayCommonStatisticDto {
  private Long vacancyOfTheDayId;
  private VacancyEmployerInfoDto employer;
  private VacancyCreatorInfoDto creator;
  private VacancyDataDto vacancy;
  private Set<Integer> regionIds;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateFrom;
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = COMMON_DATE_FORMAT)
  private LocalDate dateTo;
  private Long views;
  private Long clicks;
  @JsonSerialize(using = CommonUtils.FloatTwoDigitSerializer.class)
  private Float ctr;
  private VacancyOfTheDayCampaignType campaignType;
  private PacketType packet;
  private Set<TargetingDeviceType> deviceTypes;

  public VacancyOfTheDayCommonStatisticDto() {
  }

  public VacancyOfTheDayCommonStatisticDto(Long vacancyOfTheDayId,
                                           VacancyEmployerInfoDto employer,
                                           VacancyCreatorInfoDto creator,
                                           VacancyDataDto vacancy,
                                           Set<Integer> regionIds,
                                           LocalDate dateFrom,
                                           LocalDate dateTo,
                                           Long views,
                                           Long clicks,
                                           Float ctr,
                                           VacancyOfTheDayCampaignType campaignType,
                                           PacketType packetType,
                                           Set<TargetingDeviceType> deviceTypes) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
    this.employer = employer;
    this.creator = creator;
    this.vacancy = vacancy;
    this.regionIds = regionIds;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
    this.views = views;
    this.clicks = clicks;
    this.ctr = ctr;
    this.campaignType = campaignType;
    this.packet = packetType;
    this.deviceTypes = deviceTypes;
  }

  public Long getVacancyOfTheDayId() {
    return vacancyOfTheDayId;
  }

  public void setVacancyOfTheDayId(Long vacancyOfTheDayId) {
    this.vacancyOfTheDayId = vacancyOfTheDayId;
  }

  public VacancyEmployerInfoDto getEmployer() {
    return employer;
  }

  public void setEmployer(VacancyEmployerInfoDto employer) {
    this.employer = employer;
  }

  public VacancyCreatorInfoDto getCreator() {
    return creator;
  }

  public void setCreator(VacancyCreatorInfoDto creator) {
    this.creator = creator;
  }

  public VacancyDataDto getVacancy() {
    return vacancy;
  }

  public void setVacancy(VacancyDataDto vacancy) {
    this.vacancy = vacancy;
  }

  public Set<Integer> getRegionIds() {
    return regionIds;
  }

  public void setRegionIds(Set<Integer> regionIds) {
    this.regionIds = regionIds;
  }

  public LocalDate getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(LocalDate dateFrom) {
    this.dateFrom = dateFrom;
  }

  public LocalDate getDateTo() {
    return dateTo;
  }

  public void setDateTo(LocalDate dateTo) {
    this.dateTo = dateTo;
  }

  public Long getViews() {
    return views;
  }

  public void setViews(Long views) {
    this.views = views;
  }

  public Long getClicks() {
    return clicks;
  }

  public void setClicks(Long clicks) {
    this.clicks = clicks;
  }

  public Float getCtr() {
    return ctr;
  }

  public void setCtr(Float ctr) {
    this.ctr = ctr;
  }

  public VacancyOfTheDayCampaignType getCampaignType() {
    return campaignType;
  }

  public void setCampaignType(VacancyOfTheDayCampaignType campaignType) {
    this.campaignType = campaignType;
  }

  public PacketType getPacket() {
    return packet;
  }

  public void setPacket(PacketType packet) {
    this.packet = packet;
  }

  public Set<TargetingDeviceType> getDeviceTypes() {
    return deviceTypes;
  }

  public void setDeviceTypes(Set<TargetingDeviceType> deviceTypes) {
    this.deviceTypes = deviceTypes;
  }
}
