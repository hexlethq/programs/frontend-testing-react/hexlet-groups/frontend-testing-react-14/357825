package ru.hh.content.engine.utils;

import static java.lang.String.format;
import java.util.List;
import java.util.Properties;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.nab.common.properties.FileSettings;

public class GenericDao {
  private final SessionFactory sessionFactory;
  private final FileSettings hibernateProperties;

  public GenericDao(SessionFactory sessionFactory, Properties hibernateProperties) {
    this.sessionFactory = sessionFactory;
    this.hibernateProperties = new FileSettings(hibernateProperties);
  }

  @Transactional
  public void save(Object object) {
    sessionFactory.getCurrentSession().save(object);
  }

  @Transactional(readOnly = true)
  public long nextSequenceValue(String seqName) {
    String SQL = format("select nextval('{h-schema}%s')", seqName);
    return ((Number) sessionFactory.getCurrentSession().createNativeQuery(SQL).uniqueResult()).longValue();
  }

  public void bulkInsert(List<?> objects) {
    int batchSize = hibernateProperties.getInteger("hibernate.jdbc.batch_size");
    EntityManager session = sessionFactory.createEntityManager();
    EntityTransaction entityManager = session.getTransaction();
    entityManager.begin();

    try {
      for (int i = 0; i < objects.size(); i++) {
        if (i % batchSize == 0 && i > 0) {
          session.flush();
          session.clear();
        }
        session.persist(objects.get(i));
      }
      entityManager.commit();
    } catch (Exception e) {
      entityManager.rollback();
    }
    session.close();
  }
}
