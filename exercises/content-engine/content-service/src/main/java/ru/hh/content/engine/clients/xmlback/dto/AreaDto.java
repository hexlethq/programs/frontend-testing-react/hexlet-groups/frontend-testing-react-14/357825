package ru.hh.content.engine.clients.xmlback.dto;

import javax.xml.bind.annotation.XmlAttribute;

public class AreaDto {
  @XmlAttribute(name = "id")
  private Integer id;

  public AreaDto() {
  }

  public AreaDto(Integer id) {
    this.id = id;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }
}
