package ru.hh.content.engine.model.targetings;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import static com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME;
import java.io.Serializable;
import ru.hh.content.engine.targetings.UserDataForTargeting;

@JsonTypeInfo(
    use = NAME,
    property = "type"
)
@JsonSubTypes({
    @Type(value = AgeTargetingData.class, name = "age"),
    @Type(value = DeviceTypeTargetingData.class, name = "deviceType"),
    @Type(value = GenderTargetingData.class, name = "gender"),
    @Type(value = LanguageTargetingData.class, name = "language"),
    @Type(value = ProfAreaTargetingData.class, name = "profArea"),
    @Type(value = RegionTargetingData.class, name = "region"),
    @Type(value = SalaryTargetingData.class, name = "salary"),
    @Type(value = SpecializationTargetingData.class, name = "specialization"),
    @Type(value = TimeTargetingData.class, name = "time"),
    @Type(value = UserTypeTargetingData.class, name = "userType"),
})
public abstract class TargetingData implements Serializable {
  @JsonIgnore
  public abstract boolean match(UserDataForTargeting userDataForTargeting);
}
