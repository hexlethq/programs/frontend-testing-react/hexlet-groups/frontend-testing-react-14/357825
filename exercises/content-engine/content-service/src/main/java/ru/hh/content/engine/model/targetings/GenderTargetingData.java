package ru.hh.content.engine.model.targetings;

import java.util.Objects;
import ru.hh.content.engine.model.enums.TargetingGender;
import ru.hh.content.engine.targetings.UserDataForTargeting;

public class GenderTargetingData extends TargetingData {
  private TargetingGender gender;

  public GenderTargetingData() {
  }

  public GenderTargetingData(TargetingGender gender) {
    this.gender = gender;
  }

  public TargetingGender getGender() {
    return gender;
  }

  public void setGender(TargetingGender gender) {
    this.gender = gender;
  }

  @Override
  public boolean match(UserDataForTargeting userDataForTargeting) {
    TargetingGender userGender = userDataForTargeting.getGender();
    if (userGender == null) {
      return false;
    }
    return userGender == gender;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    GenderTargetingData that = (GenderTargetingData) o;
    return gender == that.gender;
  }

  @Override
  public int hashCode() {
    return Objects.hash(gender);
  }

  @Override
  public String toString() {
    return "GenderTargetingData{" +
        "gender=" + gender +
        '}';
  }
}
