package ru.hh.content.engine.vacancy.of.the.day.service;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import static ru.hh.content.api.dto.ContentType.VACANCY_OF_THE_DAY;
import ru.hh.content.engine.model.enums.PacketType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayCampaignType;
import ru.hh.content.engine.model.enums.VacancyOfTheDayStatus;
import ru.hh.content.engine.packets.PacketService;
import ru.hh.content.engine.statistics.AggregatedStatistic;
import ru.hh.content.engine.statistics.CHStatisticsDao;
import ru.hh.content.engine.statistics.StatisticByContentId;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyCreatorInfoDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyDataDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyEmployerInfoDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyFrontDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayAggregatedStatisticDto;
import ru.hh.content.engine.vacancy.of.the.day.dto.VacancyOfTheDayCommonStatisticDto;

public class VacancyOfTheDayStatisticService {
  private final CHStatisticsDao statisticsDao;
  private final VacancyOfTheDayService vacancyOfTheDayService;
  private final PacketService packetService;

  public VacancyOfTheDayStatisticService(CHStatisticsDao statisticsDao, VacancyOfTheDayService vacancyOfTheDayService, PacketService packetService) {
    this.statisticsDao = statisticsDao;
    this.vacancyOfTheDayService = vacancyOfTheDayService;
    this.packetService = packetService;
  }

  public VacancyOfTheDayAggregatedStatisticDto getCommonAggregatedStatistic(LocalDate dateFrom,
                                                                            LocalDate dateTo,
                                                                            PacketType packetType,
                                                                            Set<Integer> regionIds,
                                                                            Set<TargetingDeviceType> deviceTypes) {
    if (packetType.getCanChooseRegions()) {
      if (regionIds == null || regionIds.isEmpty()) {
        throw new IllegalArgumentException();
      }

      regionIds = new HashSet<>(regionIds);
    } else {
      regionIds = packetService.getRegionsForPacket(packetType);
    }

    AggregatedStatistic commonAggregatedStatistic = statisticsDao.getCommonAggregatedStatistic(
        dateFrom, dateTo, VACANCY_OF_THE_DAY, regionIds, deviceTypes
    );

    return new VacancyOfTheDayAggregatedStatisticDto(commonAggregatedStatistic.getImpressions(), commonAggregatedStatistic.getClicks());
  }

  public List<VacancyOfTheDayCommonStatisticDto> getCommonStatistic(Integer vacancyId,
                                                                    Long vacancyOfTheDayId,
                                                                    String vacancyName,
                                                                    Integer employerId,
                                                                    String employerName,
                                                                    Integer creatorId,
                                                                    String creatorName,
                                                                    LocalDate dateFrom,
                                                                    LocalDate dateTo,
                                                                    Set<Integer> regionIds,
                                                                    boolean exactlyRegionFilter,
                                                                    Set<TargetingDeviceType> deviceTypes,
                                                                    Set<VacancyOfTheDayStatus> campaignStatuses,
                                                                    Set<VacancyOfTheDayCampaignType> campaignTypes,
                                                                    Set<PacketType> packetTypes) {

    List<VacancyFrontDto> vacancyOfTheDayList = vacancyOfTheDayService.getVacancyOfTheDayList(
        vacancyOfTheDayId,
        vacancyId,
        vacancyName,
        employerId,
        employerName,
        creatorId,
        creatorName,
        null,
        dateFrom,
        dateTo,
        regionIds,
        exactlyRegionFilter,
        deviceTypes,
        campaignStatuses,
        null,
        campaignTypes,
        packetTypes
    );

    if (vacancyOfTheDayList.isEmpty()) {
      return List.of();
    }

    Map<Long, StatisticByContentId> contentIdToStatistic = statisticsDao.getCommonProductStatistic(
        dateFrom, dateTo, VACANCY_OF_THE_DAY, vacancyOfTheDayList.stream().map(VacancyFrontDto::getVacancyOfTheDayId).collect(Collectors.toList())
    );

    return vacancyOfTheDayList.stream()
        .map(vacancyOfTheDay -> {
          StatisticByContentId statisticByContentId = contentIdToStatistic.get(vacancyOfTheDay.getVacancyOfTheDayId());

          return new VacancyOfTheDayCommonStatisticDto(
              vacancyOfTheDay.getVacancyOfTheDayId(),
              new VacancyEmployerInfoDto(vacancyOfTheDay.getEmployer().getId(), vacancyOfTheDay.getEmployer().getName()),
              new VacancyCreatorInfoDto(vacancyOfTheDay.getCreator().getId(), vacancyOfTheDay.getCreator().getName()),
              new VacancyDataDto(vacancyOfTheDay.getVacancy().getId(), vacancyOfTheDay.getVacancy().getName()),
              vacancyOfTheDay.getRegionList(),
              vacancyOfTheDay.getDateFrom(),
              vacancyOfTheDay.getDateTo(),
              Optional.ofNullable(statisticByContentId).map(StatisticByContentId::getViews).orElse(0L),
              Optional.ofNullable(statisticByContentId).map(StatisticByContentId::getClicks).orElse(0L),
              Optional.ofNullable(statisticByContentId).map(StatisticByContentId::getCtr).orElse(0f),
              vacancyOfTheDay.getCampaignType(),
              vacancyOfTheDay.getPacket(),
              vacancyOfTheDay.getDeviceTypes()
          );
        })
        .collect(Collectors.toList());
  }
}
