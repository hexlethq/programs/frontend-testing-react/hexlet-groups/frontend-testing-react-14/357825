package ru.hh.content.engine.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import ru.hh.content.engine.kafka.banner.KafkaBannerConfig;

@Configuration
@Import({
    KafkaBannerConfig.class,
})
public class KafkaConfig {
}
