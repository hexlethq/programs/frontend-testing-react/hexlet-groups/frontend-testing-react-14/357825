package ru.hh.content.engine.work.in.company.dto;

import java.time.LocalDate;

public class ReserveNextFreePeriod {
  private Integer regionId;
  private ReserveStatus status;
  private LocalDate dateFrom;
  private LocalDate dateTo;

  public ReserveNextFreePeriod() {
  }

  public ReserveNextFreePeriod(Integer regionId, ReserveStatus status) {
    this.regionId = regionId;
    this.status = status;
  }

  public ReserveNextFreePeriod(Integer regionId, ReserveStatus status, LocalDate dateFrom, LocalDate dateTo) {
    this.regionId = regionId;
    this.status = status;
    this.dateFrom = dateFrom;
    this.dateTo = dateTo;
  }

  public Integer getRegionId() {
    return regionId;
  }

  public void setRegionId(Integer regionId) {
    this.regionId = regionId;
  }

  public ReserveStatus getStatus() {
    return status;
  }

  public void setStatus(ReserveStatus status) {
    this.status = status;
  }

  public LocalDate getDateFrom() {
    return dateFrom;
  }

  public void setDateFrom(LocalDate dateFrom) {
    this.dateFrom = dateFrom;
  }

  public LocalDate getDateTo() {
    return dateTo;
  }

  public void setDateTo(LocalDate dateTo) {
    this.dateTo = dateTo;
  }
}
