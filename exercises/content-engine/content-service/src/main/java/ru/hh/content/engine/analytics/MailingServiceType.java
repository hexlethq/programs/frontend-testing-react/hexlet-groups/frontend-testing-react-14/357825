package ru.hh.content.engine.analytics;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public enum MailingServiceType {
  VACANCY_EMAIL("ras_vac"),
  VACANCY_SMS("ras_sms"),
  APPLICANT_EMAIL("ras_mas_app"),
  INTERNSHIP_EMAIL("ras_staj"),
  ;

  private final String utmMedium;

  MailingServiceType(String utmMedium) {
    this.utmMedium = utmMedium;
  }

  public String getUtmMedium() {
    return utmMedium;
  }

  public static List<String> getAllUtm() {
    return Arrays.stream(values())
        .map(MailingServiceType::getUtmMedium)
        .collect(Collectors.toUnmodifiableList());
  }

  public static MailingServiceType findByUtm(String utmMedium) {
    return Arrays.stream(values())
        .filter(type -> type.getUtmMedium().equals(utmMedium))
        .findFirst()
        .orElseThrow(() -> new IllegalArgumentException("Can't find service type with utm_medium = " + utmMedium));
  }
}
