package ru.hh.content.engine.work.in.company.dao;

import java.time.LocalDate;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;

public class WorkInCompanyWithPeriod {
  private WorkInCompany workInCompany;
  private LocalDate dateStart;
  private LocalDate dateEnd;

  public WorkInCompanyWithPeriod(WorkInCompany workInCompany, LocalDate dateStart, LocalDate dateEnd) {
    this.workInCompany = workInCompany;
    this.dateStart = dateStart;
    this.dateEnd = dateEnd;
  }

  public WorkInCompany getWorkInCompany() {
    return workInCompany;
  }

  public void setWorkInCompany(WorkInCompany workInCompany) {
    this.workInCompany = workInCompany;
  }

  public LocalDate getDateStart() {
    return dateStart;
  }

  public void setDateStart(LocalDate dateStart) {
    this.dateStart = dateStart;
  }

  public LocalDate getDateEnd() {
    return dateEnd;
  }

  public void setDateEnd(LocalDate dateEnd) {
    this.dateEnd = dateEnd;
  }
}
