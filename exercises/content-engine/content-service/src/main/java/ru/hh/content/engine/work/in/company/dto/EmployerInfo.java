package ru.hh.content.engine.work.in.company.dto;

public class EmployerInfo {
  private Integer id;
  private String name;

  public EmployerInfo() {
  }

  public EmployerInfo(Integer id, String name) {
    this.id = id;
    this.name = name;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}
