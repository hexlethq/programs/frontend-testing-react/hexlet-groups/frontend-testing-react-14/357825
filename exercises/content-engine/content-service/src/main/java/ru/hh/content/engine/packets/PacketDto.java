package ru.hh.content.engine.packets;

import java.util.Objects;
import ru.hh.content.engine.model.enums.PacketType;

public class PacketDto {
  private PacketType code;
  private String name;
  private boolean useRegions;

  public PacketDto() {
  }

  public PacketDto(PacketType code, String name, boolean useRegions) {
    this.code = code;
    this.name = name;
    this.useRegions = useRegions;
  }

  public PacketType getCode() {
    return code;
  }

  public void setCode(PacketType code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean isUseRegions() {
    return useRegions;
  }

  public void setUseRegions(boolean useRegions) {
    this.useRegions = useRegions;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PacketDto packetDto = (PacketDto) o;
    return useRegions == packetDto.useRegions &&
        code == packetDto.code &&
        Objects.equals(name, packetDto.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(code, name, useRegions);
  }

  @Override
  public String toString() {
    return "PacketDto{" +
        "code=" + code +
        ", name='" + name + '\'' +
        ", useRegions=" + useRegions +
        '}';
  }
}
