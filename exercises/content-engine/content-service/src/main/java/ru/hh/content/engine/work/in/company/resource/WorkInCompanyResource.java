package ru.hh.content.engine.work.in.company.resource;

import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.Set;
import javax.validation.Valid;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.CHECK_BUSY;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_DELETE;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_EDIT;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_EDIT_STATUS;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_GET_TO_VIEW;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_LIST;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_LOGO;
import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_ROOT;
import ru.hh.content.engine.dto.ImpressionRequestDto;
import ru.hh.content.engine.model.enums.ImageType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.WorkInCompanyStatus;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.utils.ContentImage;
import static ru.hh.content.engine.utils.ContentImage.MAX_PICTURE_FILE_SIZE;
import static ru.hh.content.engine.utils.ResourceUtils.parseLocalDate;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyCheckRequestDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyFormDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyLogoDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyViewDtoList;
import ru.hh.content.engine.work.in.company.service.WorkInCompanyService;
import ru.hh.content.engine.work.in.company.service.WorkInCompanyViewService;
import ru.hh.errors.common.Errors;

@Path(WORK_IN_COMPANY_ROOT)
public class WorkInCompanyResource {
  private static final Logger LOGGER = LoggerFactory.getLogger(WorkInCompanyResource.class);

  private final WorkInCompanyService workInCompanyService;
  private final SecurityContext securityContext;
  private final WorkInCompanyViewService workInCompanyViewService;

  public WorkInCompanyResource(WorkInCompanyService workInCompanyService,
                               SecurityContext securityContext,
                               WorkInCompanyViewService workInCompanyViewService) {
    this.workInCompanyService = workInCompanyService;
    this.securityContext = securityContext;
    this.workInCompanyViewService = workInCompanyViewService;
  }

  @POST
  public Response create(@RequestBody @Valid WorkInCompanyFormDto workInCompanyFormDto) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(workInCompanyService.createWorkInCompany(workInCompanyFormDto)).build();
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response get(@QueryParam("workInCompanyId") Long workInCompanyId) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(workInCompanyService.getWorkInCompany(workInCompanyId)).build();
  }

  @GET
  @Path(WORK_IN_COMPANY_GET_TO_VIEW)
  @Produces(MediaType.APPLICATION_XML)
  public Response getToView(@BeanParam ImpressionRequestDto impressionRequestDto) {
    return Response.ok(
        new WorkInCompanyViewDtoList(workInCompanyViewService.getWorkInCompanyToView(LocalDate.now(), impressionRequestDto))
    ).build();
  }

  @POST
  @Path(WORK_IN_COMPANY_EDIT)
  public Response edit(
      @RequestBody @Valid WorkInCompanyFormDto workInCompanyFormDto,
      @PathParam(value = "workInCompanyId") Integer workInCompanyId) {
    securityContext.checkCurrentUserIsBackOffice();
    workInCompanyService.editWorkInCompany(workInCompanyFormDto, workInCompanyId);
    return Response.ok().build();
  }

  @POST
  @Path(WORK_IN_COMPANY_EDIT_STATUS)
  public Response editStatus(
      @QueryParam("status") WorkInCompanyStatus status,
      @PathParam(value = "workInCompanyId") Integer workInCompanyId) {
    securityContext.checkCurrentUserIsBackOffice();
    workInCompanyService.editWorkInCompanyStatus(status, workInCompanyId);
    return Response.ok().build();
  }

  @DELETE
  @Path(WORK_IN_COMPANY_DELETE)
  public Response delete(@PathParam(value = "workInCompanyId") Integer workInCompanyId) {
    securityContext.checkCurrentUserIsBackOffice();
    workInCompanyService.deleteWorkInCompanyStatus(workInCompanyId);
    return Response.ok().build();
  }

  @PUT
  @Path(WORK_IN_COMPANY_LOGO)
  @Consumes(MediaType.MULTIPART_FORM_DATA)
  @Produces(MediaType.APPLICATION_JSON)
  public Response uploadLogo(@FormDataParam("file") InputStream fileInputStream,
                             @FormDataParam("file") FormDataContentDisposition fileMetaData) {
    securityContext.checkCurrentUserIsBackOffice();
    try {
      byte[] bytes = IOUtils.toByteArray(new BoundedInputStream(fileInputStream, MAX_PICTURE_FILE_SIZE));
      WorkInCompanyLogoDto workInCompanyLogoDto = workInCompanyService.createLogoWorkInCompany(
          ImageType.XS, ContentImage.fromBytes(bytes)
      );
      return Response.ok(workInCompanyLogoDto).build();
    } catch (IOException e) {
      LOGGER.warn("Wrong image", e);
      throw new Errors(500, "wrong_image", "Something wrong with image").toWebApplicationException();
    }
  }

  @GET
  @Path(WORK_IN_COMPANY_LIST)
  @Produces(MediaType.APPLICATION_JSON)
  public Response getWorkInCompanyList(@QueryParam("workInCompanyId") Long workInCompanyId,
                                       @QueryParam("employerId") Integer employerId,
                                       @QueryParam("dateFrom") String dateFrom,
                                       @QueryParam("dateTo") String dateTo,
                                       @QueryParam("regionId") Set<Integer> regionIds,
                                       @QueryParam("exactlyRegionFilter") boolean exactlyRegionFilter,
                                       @QueryParam("deviceType") Set<TargetingDeviceType> deviceTypes,
                                       @QueryParam("status") Set<WorkInCompanyStatus> statuses
  ) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(
        workInCompanyService.getWorkInCompaniesByFilter(
            workInCompanyId,
            employerId,
            parseLocalDate(dateFrom),
            parseLocalDate(dateTo),
            regionIds,
            exactlyRegionFilter,
            deviceTypes,
            statuses
        )
    ).build();
  }

  @PUT
  @Path(CHECK_BUSY)
  @Produces(MediaType.APPLICATION_JSON)
  public Response checkBusy(@RequestBody @Valid WorkInCompanyCheckRequestDto workInCompanyCheckRequestDto) {
    securityContext.checkCurrentUserIsBackOffice();
    return Response.ok(workInCompanyService.checkBusy(workInCompanyCheckRequestDto)).build();
  }
}
