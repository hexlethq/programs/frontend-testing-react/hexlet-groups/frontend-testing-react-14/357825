package ru.hh.content.engine.targetings;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
    TargetingService.class,
    TargetingDao.class,
    TargetingMatcherService.class,
})
public class TargetingConfig {
}
