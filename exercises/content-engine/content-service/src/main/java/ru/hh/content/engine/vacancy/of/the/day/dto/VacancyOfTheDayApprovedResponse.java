package ru.hh.content.engine.vacancy.of.the.day.dto;

import java.util.Map;
import java.util.Set;

public class VacancyOfTheDayApprovedResponse {
  private Map<VacancyOfTheDayApprovedErrorType, Set<Long>> errorToVacancyOfTheDayIds;

  public VacancyOfTheDayApprovedResponse() {
  }

  public VacancyOfTheDayApprovedResponse(Map<VacancyOfTheDayApprovedErrorType, Set<Long>> errorToVacancyOfTheDayIds) {
    this.errorToVacancyOfTheDayIds = errorToVacancyOfTheDayIds;
  }

  public Map<VacancyOfTheDayApprovedErrorType, Set<Long>> getErrorToVacancyOfTheDayIds() {
    return errorToVacancyOfTheDayIds;
  }

  public void setErrorToVacancyOfTheDayIds(Map<VacancyOfTheDayApprovedErrorType, Set<Long>> errorToVacancyOfTheDayIds) {
    this.errorToVacancyOfTheDayIds = errorToVacancyOfTheDayIds;
  }
}
