package ru.hh.content.engine.vacancy.of.the.day.dto;

public class VacancyOfTheDayAggregatedStatisticDto {
  private Long impressions;
  private Long clicks;

  public VacancyOfTheDayAggregatedStatisticDto() {
  }

  public VacancyOfTheDayAggregatedStatisticDto(Long impressions, Long clicks) {
    this.impressions = impressions;
    this.clicks = clicks;
  }

  public Long getImpressions() {
    return impressions;
  }

  public void setImpressions(Long impressions) {
    this.impressions = impressions;
  }

  public Long getClicks() {
    return clicks;
  }

  public void setClicks(Long clicks) {
    this.clicks = clicks;
  }
}
