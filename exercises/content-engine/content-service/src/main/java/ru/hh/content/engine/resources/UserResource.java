package ru.hh.content.engine.resources;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import static ru.hh.content.api.resources.UserDataPath.USER_DATA;
import ru.hh.content.engine.dto.UserDataDto;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.hh.session.client.HhUser;

@Path(USER_DATA)
public class UserResource {
  private final HHSessionContext sessionContext;
  private final SecurityContext securityContext;

  @Inject
  public UserResource(HHSessionContext sessionContext, SecurityContext securityContext) {
    this.sessionContext = sessionContext;
    this.securityContext = securityContext;
  }

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getUserData() {
    securityContext.checkCurrentUserIsBackOffice();
    HhUser hhUser = sessionContext.getAccount();
    String name = String.format("%s %s", hhUser.name.getFirst(), hhUser.name.getLast());
    UserDataDto userData = new UserDataDto(hhUser.userId, name, securityContext.currentUserIsSuperAdmin());
    return Response.ok(userData).build();
  }
}
