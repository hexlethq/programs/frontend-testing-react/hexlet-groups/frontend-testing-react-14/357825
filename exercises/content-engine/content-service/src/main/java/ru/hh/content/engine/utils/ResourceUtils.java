package ru.hh.content.engine.utils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import ru.hh.errors.common.Errors;

public class ResourceUtils {

  public static void assertDateFromIsBeforeDateTo(LocalDate dateFrom, LocalDate dateTo) {
    if (dateTo.isBefore(dateFrom)) {
      throw new Errors(BAD_REQUEST, "wrong_dates", "date_to must be after or equals date_from").toWebApplicationException();
    }
  }

  public static LocalDate parseLocalDate(String date) {
    try {
      return LocalDate.parse(date);
    } catch (DateTimeParseException e) {
      throw new Errors(BAD_REQUEST, "wrong_date_format", "wrong date format, use YYYY-MM-DD").toWebApplicationException();
    }
  }
}
