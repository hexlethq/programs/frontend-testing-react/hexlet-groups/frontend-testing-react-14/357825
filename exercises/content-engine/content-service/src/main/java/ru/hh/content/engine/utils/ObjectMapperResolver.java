package ru.hh.content.engine.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import javax.ws.rs.ext.ContextResolver;
import ru.hh.nab.starter.jersey.JsonCharacterEscapes;

public class ObjectMapperResolver implements ContextResolver<ObjectMapper> {
  public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
  static {
    OBJECT_MAPPER.getFactory().setCharacterEscapes(new JsonCharacterEscapes());
    OBJECT_MAPPER.registerModule(new JavaTimeModule());
    OBJECT_MAPPER.registerModule(new Jdk8Module());
  }

  @Override
  public ObjectMapper getContext(Class<?> type) {
    return OBJECT_MAPPER;
  }
}
