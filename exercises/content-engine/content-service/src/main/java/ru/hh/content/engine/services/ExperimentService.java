package ru.hh.content.engine.services;

import java.util.Set;
import javax.inject.Inject;
import ru.hh.analytics.jclient.ExperimentsCheckClient;
import ru.hh.hh.session.client.Session;

public class ExperimentService {
  private final ExperimentsCheckClient experimentsCheckClient;
  private final HHSessionContext sessionContext;
  
  @Inject
  public ExperimentService(ExperimentsCheckClient experimentsCheckClient, HHSessionContext sessionContext) {
    this.experimentsCheckClient = experimentsCheckClient;
    this.sessionContext = sessionContext;
  }
  
  public boolean check(String abName) {
    return checkABTest(abName) || checkControl(abName);
  }
  
  public boolean checkABTest(String abName) {
    return abName != null && sessionContext.getSession().activeAbExperimentNames.contains(abName);
  }
  
  public boolean checkControl(String abName) {
    return abName != null && sessionContext.getSession().controlAbExperimentNames.contains(abName);
  }
  
  
  public void send() {
    Session session = sessionContext.getSession();
    experimentsCheckClient.sendExperimentsCheckEvent(
        session.activeAbExperimentNames,
        session.controlAbExperimentNames,
        Set.of(),
        null,
        null,
        session.uid
    );
  }
}
