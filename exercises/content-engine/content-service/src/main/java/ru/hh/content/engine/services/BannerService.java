package ru.hh.content.engine.services;

import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toList;
import javax.inject.Inject;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import ru.hh.content.engine.clients.banner.BannerClient;
import ru.hh.content.engine.clients.banner.dto.ClickmeVacancyDto;
import ru.hh.content.engine.clients.banner.dto.VacancyUrlDto;
import static ru.hh.content.engine.utils.CommonUtils.distinctByKey;
import static ru.hh.content.engine.utils.ExceptionUtils.getWithExceptionsToRuntimeWrapper;
import ru.hh.content.engine.utils.cache.CacheService;
import ru.hh.jclient.common.ResultWithStatus;

public class BannerService {
  private final static Pattern VACANCY_URL_PATTERN = Pattern.compile("hh.ru/vacancy/(?<vacancyId>\\d+)");
  private final static String UTM_PARAM_NAME = "utm_source";
  private final static String UTM_PARAM_OLD = "clickmehhru";
  private final static String UTM_PARAM_NEW = "votdhhru";
  private final BannerClient bannerClient;
  private final CacheService<Map<Integer, VacancyUrlDto>> cacheService;
  
  @Inject
  public BannerService(BannerClient bannerClient, CacheService<Map<Integer, VacancyUrlDto>> clickmeVacanciesCacheService) {
    this.bannerClient = bannerClient;
    this.cacheService = clickmeVacanciesCacheService;
  }
  
  private List<ClickmeVacancyDto> getVacancyUrls() {
    ResultWithStatus<ClickmeVacancyDto[]> resultWithStatus = getWithExceptionsToRuntimeWrapper(
        () -> bannerClient.getClickmeVacancies().get()
    );
    
    if (!resultWithStatus.isSuccess()) {
      throw new RuntimeException(String.format("Fail get vacancies with status code %s", resultWithStatus.getStatusCode()));
    }
    
    Optional<ClickmeVacancyDto[]> optionalVacancyListDto = resultWithStatus.get();
    
    if (optionalVacancyListDto.isEmpty() || optionalVacancyListDto.get().length < 1) {
      return Collections.emptyList();
    }
    
    return Arrays.asList(optionalVacancyListDto.get());
  }
  
  public List<VacancyUrlDto> getVacancies() {
    return getVacancyUrls()
        .stream()
        .map(url -> {
          Matcher vacancyUrlMatch = VACANCY_URL_PATTERN.matcher(url.getUrl());
          if (vacancyUrlMatch.find()) {
            int vacancyId = Integer.parseInt(vacancyUrlMatch.group("vacancyId"));
            return new VacancyUrlDto(url.getUrl(), vacancyId);
          } else {
            return null;
          }
        })
        .filter(Objects::nonNull)
        .filter(distinctByKey(VacancyUrlDto::getVacancyId))
        .collect(Collectors.toList());
  }
  
  public Map<Integer, VacancyUrlDto> getAdVacanciesMap() {
    return getVacancies()
        .stream()
        .peek(vacancyUrlDto -> vacancyUrlDto.setParams(
            URLEncodedUtils.parse(vacancyUrlDto.getUrl(), StandardCharsets.UTF_8)
                .stream()
                .collect(groupingBy(NameValuePair::getName, mapping(
                    pair -> {
                      if (Objects.equals(pair.getName(), UTM_PARAM_NAME)) {
                        return pair.getValue().replace(UTM_PARAM_OLD, UTM_PARAM_NEW);
                      }
                      return pair.getValue();
                    }, toList())))))
        .collect(Collectors.toMap(VacancyUrlDto::getVacancyId, Function.identity()));
  }
  
  public Map<Integer, VacancyUrlDto> getCachedAdVacanciesMap() {
    Optional<Map<Integer, VacancyUrlDto>> cache = cacheService.getFromCache(getCacheKey());
    if (cache.isEmpty()) {
      Map<Integer, VacancyUrlDto> vacancies = getAdVacanciesMap();
      cacheService.putInCache(getCacheKey(), vacancies);
      return vacancies;
    }
    
    return cache.get();
  }
  
  private static String getCacheKey() {
    return LocalDate.now().toString();
  }
}
