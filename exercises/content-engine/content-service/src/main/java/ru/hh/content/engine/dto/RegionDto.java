package ru.hh.content.engine.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Set;

public class RegionDto {
  private String id;
  private String text;
  @JsonProperty("items")
  @JsonInclude(NON_NULL)
  private Set<RegionDto> children;

  public RegionDto() {
  }

  public RegionDto(String id, String text) {
    this.id = id;
    this.text = text;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
  }

  public Set<RegionDto> getChildren() {
    return children;
  }

  public void setChildren(Set<RegionDto> children) {
    this.children = children;
  }
}
