package ru.hh.content.engine.vacancy.of.the.day.dao;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.vacancy.of.the.day.VacancyOfTheDayScreenshot;

public class VacancyOfTheDayScreenshotDao {
  private final SessionFactory sessionFactory;

  public VacancyOfTheDayScreenshotDao(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  public void save(VacancyOfTheDayScreenshot vacancyOfTheDayScreenshot) {
    sessionFactory.getCurrentSession().save(vacancyOfTheDayScreenshot);
  }
  
  @Transactional(readOnly = true)
  public List<VacancyOfTheDayScreenshot> getScreenshots(long vacancyOfTheDayId) {
    return sessionFactory.getCurrentSession()
        .createQuery("FROM VacancyOfTheDayScreenshot " +
            "WHERE vacancyOfTheDayId = :vacancyOfTheDayId", VacancyOfTheDayScreenshot.class)
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .list();
  }
  
  @Transactional
  public void deleteScreenshot(int screenId) {
    sessionFactory.getCurrentSession()
        .createQuery("DELETE FROM VacancyOfTheDayScreenshot " +
            "WHERE vacancyOfTheDayScreenshotId = :screenId")
        .setParameter("screenId", screenId)
        .executeUpdate();
  }
  
  @Transactional
  public void deleteAllScreenshots(long vacancyOfTheDayId) {
    sessionFactory.getCurrentSession()
        .createQuery("DELETE FROM VacancyOfTheDayScreenshot " +
            "WHERE vacancyOfTheDayId = :vacancyOfTheDayId")
        .setParameter("vacancyOfTheDayId", vacancyOfTheDayId)
        .executeUpdate();
  }
}
