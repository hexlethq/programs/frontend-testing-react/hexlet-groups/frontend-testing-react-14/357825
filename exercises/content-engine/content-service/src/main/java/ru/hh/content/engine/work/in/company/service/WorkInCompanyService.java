package ru.hh.content.engine.work.in.company.service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Period;
import static java.time.temporal.ChronoUnit.DAYS;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import org.springframework.transaction.annotation.Transactional;
import ru.hh.content.engine.model.enums.ImageType;
import ru.hh.content.engine.model.enums.TargetingDeviceType;
import ru.hh.content.engine.model.enums.WorkInCompanyStatus;
import static ru.hh.content.engine.model.enums.WorkInCompanyStatus.ACTIVE;
import static ru.hh.content.engine.model.enums.WorkInCompanyStatus.DELETED;
import static ru.hh.content.engine.model.enums.WorkInCompanyStatus.PAUSED;
import ru.hh.content.engine.model.work.in.company.WorkInCompany;
import ru.hh.content.engine.model.work.in.company.WorkInCompanyLogo;
import ru.hh.content.engine.model.work.in.company.WorkInCompanyReservation;
import ru.hh.content.engine.security.SecurityContext;
import ru.hh.content.engine.services.EmployerService;
import ru.hh.content.engine.services.HHSessionContext;
import ru.hh.content.engine.services.ImageLoaderService;
import ru.hh.content.engine.utils.ContentImage;
import ru.hh.content.engine.utils.GenericDao;
import ru.hh.content.engine.utils.exceptions.WorkInCompanyReservationException;
import static ru.hh.content.engine.work.in.company.WorkInCompanyConstants.MAX_COMPANIES_ON_PAGE_SETTING_NAME;
import ru.hh.content.engine.work.in.company.dao.WorkInCompanyDao;
import ru.hh.content.engine.work.in.company.dao.WorkInCompanyReservationDao;
import ru.hh.content.engine.work.in.company.dto.EmployerInfo;
import ru.hh.content.engine.work.in.company.dto.ReserveNextFreePeriod;
import ru.hh.content.engine.work.in.company.dto.ReserveStatus;
import ru.hh.content.engine.work.in.company.dto.WorkInCompaniesListItem;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyCheckRequestDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyFormDto;
import ru.hh.content.engine.work.in.company.dto.WorkInCompanyLogoDto;
import ru.hh.errors.common.Errors;
import ru.hh.nab.datasource.DataSourceType;
import ru.hh.nab.hibernate.transaction.ExecuteOnDataSource;
import ru.hh.settings.SettingsClient;

public class WorkInCompanyService {
  private final WorkInCompanyDao workInCompanyDao;
  private final WorkInCompanyReservationDao workInCompanyReservationDao;
  private final SettingsClient settingsClient;
  private final ImageLoaderService imageLoaderService;
  private final GenericDao genericDao;
  private final HHSessionContext hhSessionContext;
  private final SecurityContext securityContext;
  private final EmployerService employerService;

  @Inject
  public WorkInCompanyService(WorkInCompanyDao workInCompanyDao,
                              WorkInCompanyReservationDao workInCompanyReservationDao,
                              SettingsClient settingsClient,
                              ImageLoaderService imageLoaderService,
                              GenericDao genericDao,
                              HHSessionContext hhSessionContext,
                              SecurityContext securityContext,
                              EmployerService employerService) {
    this.workInCompanyDao = workInCompanyDao;
    this.workInCompanyReservationDao = workInCompanyReservationDao;
    this.settingsClient = settingsClient;
    this.imageLoaderService = imageLoaderService;
    this.genericDao = genericDao;
    this.hhSessionContext = hhSessionContext;
    this.securityContext = securityContext;
    this.employerService = employerService;
  }

  @ExecuteOnDataSource(dataSourceType = DataSourceType.READONLY)
  public List<WorkInCompaniesListItem> getWorkInCompaniesByFilter(Long workInCompanyId,
                                                                  Integer employerId,
                                                                  LocalDate dateFrom,
                                                                  LocalDate dateTo,
                                                                  Set<Integer> regionIds,
                                                                  boolean exactlyRegionFilter,
                                                                  Set<TargetingDeviceType> deviceTypes,
                                                                  Set<WorkInCompanyStatus> statuses) {
    if (workInCompanyId != null) {
      return getSingleCompanyId(workInCompanyId);
    }
    List<WorkInCompany> workInCompanyWithPeriodList =
        workInCompanyDao.getWorkInCompaniesWithDatesByPeriodAndEmployer(employerId, dateFrom, dateTo, deviceTypes, statuses);

    Set<Integer> employerIds = workInCompanyWithPeriodList.stream().map(WorkInCompany::getEmployerId).collect(Collectors.toSet());
    Map<Integer, String> employerIdNameMap = employerService.getEmployerInfoList(employerIds).stream()
        .collect(Collectors.toMap(EmployerInfo::getId, EmployerInfo::getName));

    return workInCompanyWithPeriodList.stream()
        .map(WorkInCompaniesListItem::new)
        .peek(item -> item.getEmployer().setEmployerName(employerIdNameMap.get(item.getEmployer().getEmployerId())))
        .filter(item -> regionFilter(regionIds, exactlyRegionFilter, item))
        .collect(Collectors.toList());
  }

  @Transactional
  public void editWorkInCompanyStatus(WorkInCompanyStatus status, long workInCompanyId) {
    if (!Set.of(ACTIVE, PAUSED).contains(status)) {
      throw new Errors(Response.Status.BAD_REQUEST, "wrong_status", null).toWebApplicationException();
    }

    WorkInCompany workInCompany = workInCompanyDao.getWorkInCompany(workInCompanyId);

    if (workInCompany == null) {
      throw new NotFoundException();
    }

    securityContext.checkUserIsCurrentOrAdmin(workInCompany.getManagerUserId());

    workInCompany.setStatus(status);

    workInCompanyDao.updateWorkInCompany(workInCompany);
  }

  @Transactional
  public void deleteWorkInCompanyStatus(long workInCompanyId) {
    WorkInCompany workInCompany = workInCompanyDao.getWorkInCompany(workInCompanyId);

    if (workInCompany == null) {
      throw new NotFoundException();
    }

    securityContext.checkUserIsCurrentOrAdmin(workInCompany.getManagerUserId());

    workInCompany.setStatus(DELETED);

    workInCompanyDao.updateWorkInCompany(workInCompany);

    workInCompanyReservationDao.deleteWorkInEmployerReservations(workInCompanyId);
  }

  @Transactional
  public void editWorkInCompany(WorkInCompanyFormDto workInCompanyFormDto, long workInCompanyId) {
    validateWorkInCompanyPeriod(workInCompanyFormDto.getDateStart(), workInCompanyFormDto.getDateEnd());

    WorkInCompany workInCompany = workInCompanyDao.getWorkInCompany(workInCompanyId);

    securityContext.checkUserIsCurrentOrAdmin(workInCompany.getManagerUserId());

    workInCompany.setEmployerId(workInCompanyFormDto.getEmployerId());
    workInCompany.setDisplayName(workInCompanyFormDto.getDisplayName());
    workInCompany.setWorkInCompanyLogoId(workInCompanyFormDto.getWorkInCompanyLogoId());
    workInCompany.setComment(workInCompanyFormDto.getComment());

    workInCompany = workInCompanyDao.updateWorkInCompany(workInCompany);

    workInCompanyReservationDao.deleteWorkInEmployerReservations(workInCompanyId);
    saveReservations(workInCompanyFormDto, workInCompany);
  }

  @ExecuteOnDataSource(dataSourceType = DataSourceType.READONLY)
  public WorkInCompanyFormDto getWorkInCompany(Long workInCompanyId) {
    WorkInCompany workInCompany = workInCompanyDao.getWorkInCompany(workInCompanyId);

    if (workInCompany == null || workInCompany.getStatus() == DELETED) {
      throw new NotFoundException();
    }

    WorkInCompanyFormDto workInCompanyFormDto = new WorkInCompanyFormDto(
        workInCompany.getNotExcludedRegionIdsFromReservations(),
        workInCompany.getExcludedRegionIdsFromReservations(),
        workInCompany.isRussianFromReservations(),
        workInCompany.getDateStartFromReservations(),
        workInCompany.getDateEndFromReservations(),
        workInCompany.getDisplayName(),
        workInCompany.getEmployerId(),
        workInCompany.getWorkInCompanyLogoId(),
        workInCompany.getDeviceTypesFromReservations(),
        workInCompany.getComment(),
        workInCompany.getStatus()
    );
    if (workInCompany.getWorkInCompanyLogo() != null) {
      workInCompanyFormDto.setWorkInCompanyLogoUrl(imageLoaderService.getFullFileUrl(workInCompany.getWorkInCompanyLogo().getUrl()));
    }
    return workInCompanyFormDto;
  }

  @Transactional
  public long createWorkInCompany(WorkInCompanyFormDto workInCompanyFormDto) {
    validateWorkInCompanyPeriod(workInCompanyFormDto.getDateStart(), workInCompanyFormDto.getDateEnd());

    WorkInCompany workInCompany = workInCompanyDao.saveWorkInCompany(
        new WorkInCompany(
            workInCompanyFormDto.getDisplayName(),
            workInCompanyFormDto.getEmployerId(),
            workInCompanyFormDto.getWorkInCompanyLogoId(),
            hhSessionContext.getAccount().userId,
            hhSessionContext.getUserFullName(),
            workInCompanyFormDto.getComment()
        )
    );

    saveReservations(workInCompanyFormDto, workInCompany);

    return workInCompany.getWorkInCompanyId();
  }

  private void saveReservations(WorkInCompanyFormDto workInCompanyFormDto, WorkInCompany workInCompany) {
    List<WorkInCompanyReservation> workInCompanyReservationList = new ArrayList<>();

    workInCompanyFormDto.getDateStart().datesUntil(workInCompanyFormDto.getDateEnd(), Period.ofWeeks(1)).forEach(dateStart ->
        processWeek(workInCompanyFormDto, workInCompany, workInCompanyReservationList, dateStart)
    );

    workInCompanyReservationDao.saveWorkInEmployerReservations(workInCompanyReservationList);
  }

  private void validateWorkInCompanyPeriod(LocalDate dateStart, LocalDate dateEnd) {
    if (!dateStart.isBefore(dateEnd)) {
      throw new IllegalArgumentException("Date end before date start");
    }

    if (dateStart.getDayOfWeek() != DayOfWeek.MONDAY) {
      throw new IllegalArgumentException("Start day must be monday");
    }

    if (dateEnd.getDayOfWeek() != DayOfWeek.SUNDAY) {
      throw new IllegalArgumentException("End day must be sunday");
    }
  }

  @Transactional
  public WorkInCompanyLogoDto createLogoWorkInCompany(ImageType imageType, ContentImage contentImage) {
    String url = imageLoaderService.uploadImageAndGetFileName(contentImage, imageType);
    WorkInCompanyLogo workInCompanyLogo = new WorkInCompanyLogo(imageType, url);
    genericDao.save(workInCompanyLogo);
    return new WorkInCompanyLogoDto(imageLoaderService.getFullFileUrl(url), workInCompanyLogo.getWorkInCompanyLogoId());
  }

  public List<ReserveNextFreePeriod> checkBusy(WorkInCompanyCheckRequestDto workInCompanyCheckRequestDto) {
    validateWorkInCompanyPeriod(workInCompanyCheckRequestDto.getDateFrom(), workInCompanyCheckRequestDto.getDateTo());

    int duration = (int) workInCompanyCheckRequestDto.getDateFrom().until(workInCompanyCheckRequestDto.getDateTo(), DAYS) + 1;

    int maxCompaniesOnPage = settingsClient.getInteger(MAX_COMPANIES_ON_PAGE_SETTING_NAME).orElseThrow();

    List<Integer> busyRegions = workInCompanyReservationDao.getBusyRegionsForPeriod(workInCompanyCheckRequestDto.getDeviceTypes(),
        workInCompanyCheckRequestDto.getRegionIds(),
        workInCompanyCheckRequestDto.getDateFrom(),
        workInCompanyCheckRequestDto.getDateTo(),
        maxCompaniesOnPage
    );

    Map<Integer, LocalDate> nextFreeDatesByRegions = workInCompanyReservationDao.getNextFreeDatesByRegions(
        workInCompanyCheckRequestDto.getDeviceTypes(),
        duration,
        busyRegions,
        workInCompanyCheckRequestDto.getDateFrom(),
        maxCompaniesOnPage
    );

    List<ReserveNextFreePeriod> reserveNextFreePeriods = new ArrayList<>();

    for (Integer regionId : workInCompanyCheckRequestDto.getRegionIds()) {
      if (nextFreeDatesByRegions.containsKey(regionId)) {
        LocalDate nextFree = nextFreeDatesByRegions.get(regionId);
        reserveNextFreePeriods.add(new ReserveNextFreePeriod(
            regionId, ReserveStatus.BUSY, nextFree.plusWeeks(1), nextFree.plusWeeks(1).plusDays(duration - 1))
        );
      } else {
        reserveNextFreePeriods.add(new ReserveNextFreePeriod(regionId, ReserveStatus.FREE));
      }
    }

    return reserveNextFreePeriods;
  }

  private List<WorkInCompaniesListItem> getSingleCompanyId(Long workInCompanyId) {
    WorkInCompany workInCompany = workInCompanyDao.getWorkInCompany(workInCompanyId);
    EmployerInfo employerInfo = employerService.getEmployerInfo(workInCompany.getEmployerId());
    WorkInCompaniesListItem item = new WorkInCompaniesListItem(workInCompany);
    item.getEmployer().setEmployerName(employerInfo.getName());
    return List.of(item);
  }

  private boolean regionFilter(Set<Integer> regionIds, boolean exactlyRegionFilter, WorkInCompaniesListItem it) {
    if (exactlyRegionFilter) {
      return it.getRegionList().equals(regionIds);
    } else {
      return it.getRegionList().containsAll(regionIds);
    }
  }

  private void processWeek(WorkInCompanyFormDto workInCompanyFormDto,
                           WorkInCompany workInCompany,
                           List<WorkInCompanyReservation> workInCompanyReservationList,
                           LocalDate dateStart) {
    for (TargetingDeviceType deviceType : workInCompanyFormDto.getDeviceTypes()) {
      Set<Integer> regionIds = new HashSet<>(workInCompanyFormDto.getRegionIds());
      if (workInCompanyFormDto.getIsRussia()) {
        regionIds.add(113);
      }
      Map<Integer, Integer> regionToReservationNumber = workInCompanyReservationDao.getRegionToReservationNumber(
          dateStart,
          deviceType,
          List.copyOf(regionIds)
      );

      if (regionToReservationNumber.size() != regionIds.size()) {
        throw new WorkInCompanyReservationException();
      }

      for (Integer regionId : regionIds) {
        workInCompanyReservationList.add(new WorkInCompanyReservation(
            dateStart,
            dateStart.plusDays(6),
            workInCompany.getWorkInCompanyId(),
            deviceType,
            regionId,
            regionToReservationNumber.get(regionId))
        );
      }

      if (workInCompanyFormDto.getExcludedRegions() != null && !workInCompanyFormDto.getExcludedRegions().isEmpty()) {
        for (Integer excludedRegion : workInCompanyFormDto.getExcludedRegions()) {
          workInCompanyReservationList.add(new WorkInCompanyReservation(
              dateStart,
              dateStart.plusDays(6),
              workInCompany.getWorkInCompanyId(),
              deviceType,
              excludedRegion,
              true)
          );
        }
      }
    }
  }
}
