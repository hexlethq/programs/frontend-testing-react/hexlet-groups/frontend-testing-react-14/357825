package ru.hh.content.engine.work.in.company.dto;

public class WorkInCompanyLogoDto {
  
  private String logo;
  private int workInCompanyLogoId;
  
  public WorkInCompanyLogoDto() {
  }
  
  public WorkInCompanyLogoDto(String logo, int workInCompanyLogoId) {
    this.logo = logo;
    this.workInCompanyLogoId = workInCompanyLogoId;
  }
  
  public String getLogo() {
    return logo;
  }
  
  public void setLogo(String logo) {
    this.logo = logo;
  }
  
  public int getWorkInCompanyLogoId() {
    return workInCompanyLogoId;
  }
  
  public void setWorkInCompanyLogoId(int workInCompanyLogoId) {
    this.workInCompanyLogoId = workInCompanyLogoId;
  }
}
