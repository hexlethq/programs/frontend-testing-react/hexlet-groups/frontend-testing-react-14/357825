package ru.hh.content.engine.utils.exceptions;

public class WorkInCompanyReservationException extends PreconditionException {
  public WorkInCompanyReservationException() {
    super();
  }

  public WorkInCompanyReservationException(String message) {
    super("Can not reserve \n" + message);
  }
}
