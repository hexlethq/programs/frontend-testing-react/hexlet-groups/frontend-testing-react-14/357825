package ru.hh.content.engine.services;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.inject.Inject;
import ru.hh.content.engine.dto.RegionDto;

public class FrontRegionService {
  // Россия, Украина, Беларусь
  public static final Set<Integer> countriesWithRegions = Set.of(113, 5, 16);
  private final RegionsService regionsService;

  @Inject
  public FrontRegionService(RegionsService regionsService) {
    this.regionsService = regionsService;
  }

  public List<RegionDto> getRegions() {
    List<RegionDto> regions = new ArrayList<>();

    Map<Integer, List<Integer>> countryRegionsMap = regionsService.getCountryRegionsMap();

    Map<Integer, String> translatedRegions = getRegionsTranslationDict();

    for (Map.Entry<Integer, List<Integer>> countryToRegions : countryRegionsMap.entrySet()) {
      RegionDto regionDto = new RegionDto(countryToRegions.getKey().toString(), translatedRegions.get(countryToRegions.getKey()));

      // Возможность выбора области есть только в указанных здесь странах - countriesWithRegions
      if (countriesWithRegions.contains(countryToRegions.getKey())) {
        Set<RegionDto> children = countryToRegions.getValue()
            .stream()
            .map(region -> new RegionDto(region.toString(), translatedRegions.get(region)))
            .collect(Collectors.toSet());
        regionDto.setChildren(children);
      }

      regions.add(regionDto);
    }

    return regions;
  }

  public Map<Integer, String> getRegionsTranslationDict() {
    Map<Integer, List<Integer>> countryRegionsMap = regionsService.getCountryRegionsMap();

    List<Integer> toLoadTranslation = countryRegionsMap.values().stream().flatMap(List::stream).collect(Collectors.toList());
    toLoadTranslation.addAll(countryRegionsMap.keySet());

    return regionsService.getTranslatedRegions(
        toLoadTranslation
    );
  }
}
