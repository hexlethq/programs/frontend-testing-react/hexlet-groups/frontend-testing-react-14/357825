INSERT INTO mail_template(code, name, comment, from_mail, processor_class)
VALUES('ContentVacancyOfTheDayModerationMailTemplate', 'ContentVacancyOfTheDayModerationMailTemplate.','Письмо с информацией об отказе модерации вакансии дня','clickme@hh.ru','com.headhunter.core.mail.templates.FreemarkerTemplateProcessor');

SELECT inserttranslation('ContentVacancyOfTheDayModerationMailTemplate.from', 'HeadHunter <clickme@hh.ru>');
SELECT inserttranslation('ContentVacancyOfTheDayModerationMailTemplate.subject', 'Вакансия дня не прошла модерацию');
SELECT inserttranslation('ContentVacancyOfTheDayModerationMailTemplate.bodyHtml',
                         '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

  <title>Письмо</title>

  <style type="text/css">
    #outlook a {padding:0;}
    body {width:100% !important;margin:0;padding:0;background-color:#ffffff;color:#333333;font-family:Arial, sans-serif;-webkit-font-smoothing:antialiased;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;}

    img {outline:none;text-decoration:none;-ms-interpolation-mode:bicubic;}
    a img {border:none;}

    p {margin:1em 0;}

    table td {border-collapse:collapse;}
    table {border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt;}

    a {color:#009cd5;}
  </style>
</head>
<body style="color:#333333;font-family:Arial, sans-serif;-webkit-text-size-adjust:100%;" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix">
  <!--WRAPPER-->
  <table class="deviceWidth" width="750" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
      <td style="padding:27px 20px 0 30px;vertical-align:top;" valign="top">
      <!--BODY-->
      <table style="width:100%;" width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
          <td style="padding:30px 0 0;vertical-align:top;" valign="top">
            <table class="deviceWidth" width="700" border="0" cellpadding="0" cellspacing="0" align="left">
              <tr>
                <td style="padding:0;vertical-align:top;text-align:left;color:#333333;font-family:Arial, sans-serif;font-size:15px;font-weight:normal;line-height:21px;" valign="top">
                  <!--CONTENT-->

                  <table style="width:100%;" width="100%" border="0" cellpadding="0" cellspacing="0" align="left">
                    <tr>
                      <td style="padding:0 0 16px;vertical-align:top;color:#000;font-family:Arial,sans-serif;font-size:20px;font-weight:normal;line-height:23px;" valign="top">
                        <p style="margin:0;mso-table-lspace:0;mso-table-rspace:0;">
                          Здравствуйте, ${username}.
                        </p>
                      </td>
                    </tr>
                    <tr>
                        <td style="padding:0 0 25px;vertical-align:top;font-family:Arial,sans-serif;font-size:14px;font-weight:normal;line-height:23px;" valign="top">
                              <p style="margin:0;mso-table-lspace:0;mso-table-rspace:0;">
                              Вакансия дня не прошла модерацию
                            <ul>
                                <li>Вакансия дня: <a href=https://content.hh.ru/vacancy-of-the-day/campaign/${vacancyOfTheDayId}>${vacancyOfTheDayId}</a></li>
                                <li>Причина: ${comment}</li>
                            </ul>
                            </p>
                        </td>
                    </tr>
                  </table>
                  <!--/content-->
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <!--/body-->
      </td>
    </tr>
  </table>
  <!--/wrapper-->

</body>
</html>');
