INSERT INTO hh_invoker.task(
  name,
  description,
  enabled,
  schedule_type,
  repeat_interval_sec,
  target_url,
  use_disabled_period_during_day)
VALUES (
         'ContentEngineAreaRegionMappingUpdater',
         'Обновление маппинга арий на регионы в content-engine',
         FALSE,
         'COUNTER_STARTS_AFTER_TASK_START',
         15*60,
         'http://content-engine/cron/area_mapping_updater',
         FALSE
);
