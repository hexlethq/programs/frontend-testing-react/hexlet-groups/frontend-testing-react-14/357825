package ru.hh.content.api.resources;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class UserDataPath {
  public static final String USER_DATA = FRONT_PATH + "/user";
}
