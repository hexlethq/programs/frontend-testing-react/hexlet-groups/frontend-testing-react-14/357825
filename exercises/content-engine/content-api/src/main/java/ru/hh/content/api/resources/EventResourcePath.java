package ru.hh.content.api.resources;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class EventResourcePath {
  public static final String CLICK_RESOURCE = FRONT_PATH + "/click";
}
