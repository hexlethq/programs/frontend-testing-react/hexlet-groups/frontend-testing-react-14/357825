package ru.hh.content.api.dto;

public enum EventType {
  IMPRESSION(1),
  CLICK(3);

  private final int value;

  EventType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
