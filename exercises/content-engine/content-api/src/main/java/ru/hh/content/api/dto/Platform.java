package ru.hh.content.api.dto;

public enum Platform {
  DESKTOP(0),
  MOBILE(1),
  ;

  private final int value;

  Platform(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
