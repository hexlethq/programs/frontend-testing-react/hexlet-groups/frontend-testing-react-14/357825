package ru.hh.content.api.resources;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class MailingAnalyticsPaths {
  public static final String MAILING_ANALYTICS_PATH = FRONT_PATH + "/mailing-analytics";
  public static final String VACANCY_MAILING_ANALYTICS_PATH =  "/vacancy/{vacancyId:[\\d]+}";
}
