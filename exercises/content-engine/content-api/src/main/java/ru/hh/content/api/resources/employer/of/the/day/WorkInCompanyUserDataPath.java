package ru.hh.content.api.resources.employer.of.the.day;

import static ru.hh.content.api.resources.employer.of.the.day.WorkInCompanyPaths.WORK_IN_COMPANY_ROOT;

public class WorkInCompanyUserDataPath {
  public static final String USER_DATA = WORK_IN_COMPANY_ROOT + "/user";
}
