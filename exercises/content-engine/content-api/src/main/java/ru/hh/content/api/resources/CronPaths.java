package ru.hh.content.api.resources;

public class CronPaths {
  public static final String CRON_ROOT = "/cron";

  public static final String AREA_MAPPING_UPDATER_CRON = CRON_ROOT + "/area_mapping_updater";
}
