package ru.hh.content.api.resources;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class StatisticResourcePath {
  public static final String STATISTIC_ROOT = FRONT_PATH + "/statistic";
  public static final String STATISTIC_BY_DATE = "/by_date";
  public static final String STATISTIC_BY_DATE_XLS = "/by_date/xls";
  public static final String STATISTIC_BY_REGION = "/by_region";
  public static final String STATISTIC_BY_REGION_XLS = "/by_region/xls";
}
