package ru.hh.content.api.resources.employer.of.the.day;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class WorkInCompanyPaths {
  public static final String WORK_IN_COMPANY_ROOT = FRONT_PATH + "/work_in_company";

  public static final String WORK_IN_COMPANY_EMPLOYER_ROOT = WORK_IN_COMPANY_ROOT + "/employer";

  public static final String CHECK_BUSY = "/check_busy";
  public static final String WORK_IN_COMPANY_LOGO = "/logo";
  public static final String WORK_IN_COMPANY_EDIT = "/{workInCompanyId:[\\d]+}/edit";
  public static final String WORK_IN_COMPANY_GET_TO_VIEW = "/get_to_view";
  public static final String WORK_IN_COMPANY_EDIT_STATUS = "/{workInCompanyId:[\\d]+}/edit_status";
  public static final String WORK_IN_COMPANY_DELETE = "/{workInCompanyId:[\\d]+}/delete";
  public static final String WORK_IN_COMPANY_LIST = "/work_in_company_list";
}
