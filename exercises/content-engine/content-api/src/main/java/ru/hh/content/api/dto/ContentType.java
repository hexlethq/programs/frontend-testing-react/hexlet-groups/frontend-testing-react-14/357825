package ru.hh.content.api.dto;

public enum ContentType {
  BANNER(1),
  VACANCY_OF_THE_DAY(2),
  CAMPAIGN_OF_THE_DAY(3);

  private final int value;

  ContentType(int value) {
    this.value = value;
  }

  public int getValue() {
    return value;
  }
}
