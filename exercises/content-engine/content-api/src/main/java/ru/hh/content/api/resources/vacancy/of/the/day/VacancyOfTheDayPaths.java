package ru.hh.content.api.resources.vacancy.of.the.day;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class VacancyOfTheDayPaths {
  public static final String VACANCY_OF_THE_DAY_ROOT = FRONT_PATH + "/vacancy_of_the_day";
  public static final String VACANCY_OF_THE_DAY_VIEW_ROOT = VACANCY_OF_THE_DAY_ROOT + "/view";
  public static final String VACANCY = "/vacancy";
  public static final String EDIT = "/{vacancyOfTheDayId:[\\d]+}/edit";
  public static final String PAYMENT_STATUS = "/{vacancyOfTheDayId:[\\d]+}/payment_status";
  public static final String STATUS_APPROVE = "/status/approve";
  public static final String STATUS_TO_MODERATION = "/status/to_moderation";
  public static final String STATUS_REJECT = "/status/reject";
  public static final String STATUS_ACTIVE = "/status/activate";
  public static final String STATUS_PAUSE = "/status/pause";
  public static final String DELETE = "/{vacancyOfTheDayId:[\\d]+}/delete";
  public static final String SCREENSHOT = "/screenshot";
  public static final String CHECK_BUSY = "/check_busy";
  public static final String STATISTIC_ROOT = VACANCY_OF_THE_DAY_ROOT + "/statistic";
  public static final String AGGREGATED_STATISTIC= "/aggregated";
  public static final String STATISTIC_BY_DATE = "/by_date";
  public static final String STATISTIC_BY_DATE_XLS = "/by_date/xls";
  public static final String STATISTIC_BY_REGION = "/by_region";
  public static final String STATISTIC_BY_REGION_XLS = "/by_region/xls";
  public static final String VACANCY_OF_THE_DAY_LIST = "/vacancy_of_the_day_list";
  public static final String CLICK_RESOURCE = VACANCY_OF_THE_DAY_ROOT + "/click";
}
