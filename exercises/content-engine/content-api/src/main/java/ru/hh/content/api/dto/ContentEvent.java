package ru.hh.content.api.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;
import java.time.LocalDateTime;
import java.util.List;

public class ContentEvent {
  @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
  private final LocalDateTime eventDateTime;
  private ContentType contentType;
  private EventType eventType;
  private Platform platform;
  private long contentId;
  private long contentPlaceId;
  private String hhuid;
  private long hhid;
  private long domainAreaId;
  private String remoteHostIp;
  private String requestPath;
  private String userAgent;
  private List<Object> tags;

  public ContentEvent() {
    eventDateTime = LocalDateTime.now();
  }

  public LocalDateTime getEventDateTime() {
    return eventDateTime;
  }

  public ContentType getContentType() {
    return contentType;
  }

  public void setContentType(ContentType contentType) {
    this.contentType = contentType;
  }

  public EventType getEventType() {
    return eventType;
  }

  public void setEventType(EventType eventType) {
    this.eventType = eventType;
  }

  public long getContentId() {
    return contentId;
  }

  public void setContentId(long contentId) {
    this.contentId = contentId;
  }

  public long getContentPlaceId() {
    return contentPlaceId;
  }

  public void setContentPlaceId(long contentPlaceId) {
    this.contentPlaceId = contentPlaceId;
  }

  public String getHhuid() {
    return hhuid;
  }

  public void setHhuid(String hhuid) {
    this.hhuid = hhuid;
  }

  public long getHhid() {
    return hhid;
  }

  public void setHhid(long hhid) {
    this.hhid = hhid;
  }

  public long getDomainAreaId() {
    return domainAreaId;
  }

  public void setDomainAreaId(long domainAreaId) {
    this.domainAreaId = domainAreaId;
  }

  public String getRemoteHostIp() {
    return remoteHostIp;
  }

  public void setRemoteHostIp(String remoteHostIp) {
    this.remoteHostIp = remoteHostIp;
  }

  public String getRequestPath() {
    return requestPath;
  }

  public void setRequestPath(String requestPath) {
    this.requestPath = requestPath;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public List<Object> getTags() {
    return tags;
  }

  public void setTags(List<Object> tags) {
    this.tags = tags;
  }

  public Platform getPlatform() {
    return platform;
  }

  public void setPlatform(Platform platform) {
    this.platform = platform;
  }

  @JsonIgnore
  public static Builder newBuilder() {
    return new Builder();
  }

  @JsonIgnoreType
  public static class Builder {
    private final ContentEvent contentEvent;

    public Builder() {
      contentEvent = new ContentEvent();
    }

    public Builder contentType(ContentType contentType) {
      contentEvent.setContentType(contentType);
      return this;
    }

    public Builder eventType(EventType eventType) {
      contentEvent.setEventType(eventType);
      return this;
    }

    public Builder contentId(long contentId) {
      contentEvent.setContentId(contentId);
      return this;
    }

    public Builder contentPlaceId(long contentPlaceId) {
      contentEvent.setContentPlaceId(contentPlaceId);
      return this;
    }

    public Builder platform(Platform platform) {
      contentEvent.setPlatform(platform);
      return this;
    }

    public Builder hhuid(String hhuid) {
      contentEvent.setHhuid(hhuid);
      return this;
    }

    public Builder hhid(Long hhid) {
      contentEvent.setHhid(hhid == null ? 0 : hhid);
      return this;
    }

    public Builder domainAreaId(long domainAreaId) {
      contentEvent.setDomainAreaId(domainAreaId);
      return this;
    }

    public Builder remoteHostIp(String remoteHostIp) {
      contentEvent.setRemoteHostIp(remoteHostIp);
      return this;
    }

    public Builder requestPath(String requestPath) {
      contentEvent.setRequestPath(requestPath);
      return this;
    }

    public Builder userAgent(String userAgent) {
      contentEvent.setUserAgent(userAgent);
      return this;
    }

    public Builder tags(List<Object> tags) {
      contentEvent.setTags(tags);
      return this;
    }

    public ContentEvent build() {
      return contentEvent;
    }
  }
}
