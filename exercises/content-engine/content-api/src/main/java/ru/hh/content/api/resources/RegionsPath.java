package ru.hh.content.api.resources;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class RegionsPath {
  public static final String REGIONS = FRONT_PATH + "/regions";

  public static final String WITH_TRANSLATIONS = "/translation";
  public static final String TRANSLATION_DICT = "/translation_dict";
}
