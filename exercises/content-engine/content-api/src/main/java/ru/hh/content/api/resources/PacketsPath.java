package ru.hh.content.api.resources;

import static ru.hh.content.api.resources.Paths.FRONT_PATH;

public class PacketsPath {
  public static final String PACKETS = FRONT_PATH + "/packets";
}
