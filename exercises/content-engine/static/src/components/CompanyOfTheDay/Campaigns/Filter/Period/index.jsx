import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import InputWithIntervalCalendar from 'components/common/InputWithIntervalCalendar';
import { setCampaignsPeriod } from 'store/models/companyOfTheDay/campaigns';
import { FILTER_PERIOD } from 'constants/campaign';

const Period = () => {
    const period = useSelector((state) => state.campaignsCOTD.period);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl(`campaigns.filter.${FILTER_PERIOD}.label`)}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <InputWithIntervalCalendar
                        action={(period) => dispatch(setCampaignsPeriod(period))}
                        from={period.start}
                        to={period.end}
                    />
                </FormRow>
            }
        />
    );
};

export default Period;
