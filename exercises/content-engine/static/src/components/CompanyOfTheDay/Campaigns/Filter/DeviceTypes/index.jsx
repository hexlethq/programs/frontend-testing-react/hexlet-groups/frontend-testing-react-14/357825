import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import CheckboxDeviceTypes from 'components/common/CheckboxDeviceTypes';
import { setCampaignsDeviceTypes } from 'store/models/companyOfTheDay/campaigns';
import { FILTER_DEVICE_TYPES } from 'constants/campaign';

const DeviceTypes = () => {
    const deviceTypes = useSelector((state) => state.campaignsCOTD.deviceTypes);
    const dispatch = useDispatch();

    const setCheckedDeviceTypes = (deviceTypes) => {
        dispatch(setCampaignsDeviceTypes(deviceTypes));
    };

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl(`campaigns.filter.${FILTER_DEVICE_TYPES}.label`)}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <CheckboxDeviceTypes
                        checkedDeviceTypes={deviceTypes}
                        setCheckedDeviceTypes={setCheckedDeviceTypes}
                    />
                </FormRow>
            }
        />
    );
};

export default DeviceTypes;
