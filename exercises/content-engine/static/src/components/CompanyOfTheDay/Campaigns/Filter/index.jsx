import React from 'react';
import { useDispatch } from 'react-redux';
import { FormRow } from 'bloko/blocks/form';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import Period from 'components/CompanyOfTheDay/Campaigns/Filter/Period';
import Campaign from 'components/CompanyOfTheDay/Campaigns/Filter/Campaign';
import Employer from 'components/CompanyOfTheDay/Campaigns/Filter/Employer';
import Regions from 'components/CompanyOfTheDay/Campaigns/Filter/Regions';
import DeviceTypes from 'components/CompanyOfTheDay/Campaigns/Filter/DeviceTypes';
import Status from 'components/CompanyOfTheDay/Campaigns/Filter/Status';
import fetchCampaigns from 'components/CompanyOfTheDay/Campaigns/fetchCampaigns';
import 'components/CompanyOfTheDay/Campaigns/Filter/Filter.less';

const Filter = () => {
    const dispatch = useDispatch();

    return (
        <Gap top>
            <Period />
            <Campaign />
            <Employer />
            <Regions />
            <DeviceTypes />
            <Status />
            <AdvFormColumns
                group={
                    <FormRow>
                        <div className="campaigns-filter-button">
                            <Button kind={Button.kinds.primaryMinor} onClick={() => dispatch(fetchCampaigns())}>
                                {trl('filter.button')}
                            </Button>
                        </div>
                    </FormRow>
                }
            />
        </Gap>
    );
};

export default Filter;
