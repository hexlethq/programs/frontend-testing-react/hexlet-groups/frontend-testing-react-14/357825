import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow, FormItemGroup, FormItem } from 'bloko/blocks/form';
import Checkbox from 'bloko/blocks/checkbox';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import SelectRegions from 'components/CompanyOfTheDay/SelectRegions';
import { setCampaignsExactlyRegion, setCampaignsRegions } from 'store/models/companyOfTheDay/campaigns';
import { FILTER_EXACTLY_REGION, FILTER_REGIONS } from 'constants/campaign';

const Regions = () => {
    const { regions, exactlyRegion } = useSelector((state) => state.campaignsCOTD);
    const dispatch = useDispatch();

    const setSelectedRegions = (regions) => {
        dispatch(setCampaignsRegions(regions));
    };

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl(`campaigns.filter.${FILTER_REGIONS}.label`)}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <FormItemGroup>
                        <FormItem>
                            <div className="campaigns-filter-input">
                                <SelectRegions selectedRegions={regions} setSelectedRegions={setSelectedRegions} />
                            </div>
                        </FormItem>
                        <FormItem baseline={true}>
                            <Checkbox
                                checked={exactlyRegion}
                                onChange={() => dispatch(setCampaignsExactlyRegion(!exactlyRegion))}
                            >
                                {trl(`campaigns.filter.${FILTER_EXACTLY_REGION}.label`)}
                            </Checkbox>
                        </FormItem>
                    </FormItemGroup>
                </FormRow>
            }
        />
    );
};

export default Regions;
