import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import NumberInput from 'components/common/NumberInput';
import { setCampaignsEmployerId } from 'store/models/companyOfTheDay/campaigns';
import { FILTER_EMPLOYER_ID } from 'constants/campaign';

const Employer = () => {
    const employerId = useSelector((state) => state.campaignsCOTD.employerId);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl(`campaigns.filter.${FILTER_EMPLOYER_ID}.label`)}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <div className="campaigns-filter-input">
                        <NumberInput
                            value={employerId}
                            onChange={(e) => dispatch(setCampaignsEmployerId(e.target.value))}
                        />
                    </div>
                </FormRow>
            }
        />
    );
};

export default Employer;
