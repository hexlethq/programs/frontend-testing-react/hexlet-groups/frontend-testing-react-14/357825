import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import NumberInput from 'components/common/NumberInput';
import { setCampaignsCampaignId } from 'store/models/companyOfTheDay/campaigns';
import { FILTER_COTD_ID } from 'constants/campaign';

const Campaign = () => {
    const campaignId = useSelector((state) => state.campaignsCOTD.campaignId);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl(`campaigns.filter.${FILTER_COTD_ID}.label`)}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <div className="campaigns-filter-input">
                        <NumberInput
                            value={campaignId}
                            onChange={(e) => dispatch(setCampaignsCampaignId(e.target.value))}
                        />
                    </div>
                </FormRow>
            }
        />
    );
};

export default Campaign;
