import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import CustomSelect, { CustomSelectOption } from 'bloko/blocks/customSelect';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import { setCampaignsStatus } from 'store/models/companyOfTheDay/campaigns';
import { COTD_CAMPAIGN_STATUSES } from 'constants/campaign';

const Status = () => {
    const selectedStatus = useSelector((state) => state.campaignsCOTD.status);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl('campaigns.filter.status.label')}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <div className="campaigns-filter-input">
                        <CustomSelect value={selectedStatus} onChange={(value) => dispatch(setCampaignsStatus(value))}>
                            {COTD_CAMPAIGN_STATUSES.map((status) => (
                                <CustomSelectOption key={status} value={status}>
                                    {trl(`campaigns.status.${status}.option`)}
                                </CustomSelectOption>
                            ))}
                        </CustomSelect>
                    </div>
                </FormRow>
            }
        />
    );
};

export default Status;
