import urlParser from 'bloko/common/urlParser';
import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import { INIT_SORT_INFO, CAMPAIGN_STATUS_ALL } from 'constants/campaign';
import { setCampaigns, setCampaignsSortInfo } from 'store/models/companyOfTheDay/campaigns';

export default function () {
    return (dispatch, getState) => {
        const {
            period,
            campaignId,
            employerId,
            regions,
            deviceTypes,
            status,
            exactlyRegion,
        } = getState().campaignsCOTD;
        const params = {
            dateFrom: format(period.start, CALENDAR_FORMAT),
            dateTo: format(period.end, CALENDAR_FORMAT),
            regionId: regions,
            exactlyRegionFilter: exactlyRegion,
            deviceType: deviceTypes,
        };
        if (campaignId) {
            params.workInCompanyId = campaignId;
        }
        if (employerId) {
            params.employerId = employerId;
        }
        if (status !== CAMPAIGN_STATUS_ALL) {
            params.status = status;
        }
        const queryStr = urlParser.stringify(params);
        fetcher.get(`/api/v1/work_in_company/work_in_company_list?${queryStr}`).then((data) => {
            dispatch([setCampaigns(data), setCampaignsSortInfo(INIT_SORT_INFO)]);
        });
    };
}
