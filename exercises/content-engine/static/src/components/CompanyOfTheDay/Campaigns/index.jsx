import React, { Fragment, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import Button from 'bloko/blocks/button';
import Header from 'bloko/blocks/header';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import Filter from 'components/CompanyOfTheDay/Campaigns/Filter';
import CampaignsList from 'components/CompanyOfTheDay/Campaigns/CampaignsList';
import fetchCampaigns from 'components/CompanyOfTheDay/Campaigns/fetchCampaigns';
import fetchRegions from 'components/CompanyOfTheDay/fetchRegions';

const Campaigns = () => {
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchCampaigns());
        dispatch(fetchRegions());
    }, [dispatch]);

    return (
        <Fragment>
            <ShiftWrapper>
                <Button
                    kind={Button.kinds.primary}
                    onClick={() => dispatch(push('/company-of-the-day/create/'))}
                    data-qa="add-campaign"
                >
                    {trl('campaigns.create.button')}
                </Button>
            </ShiftWrapper>
            <Header level={2}>{trl('campaigns.header')}</Header>
            <Filter />
            <CampaignsList />
        </Fragment>
    );
};

export default Campaigns;
