import { parseISO } from 'date-fns';
import { sortingByDate, sortingByNumber, sortingByString } from 'modules/sorting';
import {
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_REGIONS,
} from 'constants/campaign';

export default {
    [CAMPAIGNS_LIST_ID]: (campaignFirst, campaignSecond, sortType) =>
        sortingByNumber(campaignFirst.workInCompanyId, campaignSecond.workInCompanyId, sortType),
    [CAMPAIGNS_LIST_EMPLOYER]: (campaignFirst, campaignSecond, sortType) =>
        sortingByNumber(campaignFirst.employer.employerId, campaignSecond.employer.employerId, sortType),
    [CAMPAIGNS_LIST_PERIOD_START]: (campaignFirst, campaignSecond, sortType) =>
        sortingByDate(parseISO(campaignFirst.dateFrom), parseISO(campaignSecond.dateFrom), sortType),
    [CAMPAIGNS_LIST_PERIOD_END]: (campaignFirst, campaignSecond, sortType) =>
        sortingByDate(parseISO(campaignFirst.dateTo), parseISO(campaignSecond.dateTo), sortType),
    [CAMPAIGNS_LIST_REGIONS]: (campaignFirst, campaignSecond, sortType, regionsDict) => {
        if (campaignFirst.regionList.length === 1 && campaignSecond.regionList.length === 1) {
            return sortingByString(
                regionsDict[campaignFirst.regionList[0]],
                regionsDict[campaignSecond.regionList[0]],
                sortType
            );
        }
        return sortingByNumber(campaignFirst.regionList.length, campaignSecond.regionList.length, sortType);
    },
    [CAMPAIGNS_LIST_DEVICE_TYPES]: (campaignFirst, campaignSecond, sortType) =>
        sortingByString(campaignFirst.deviceType, campaignSecond.deviceType, sortType),
    [CAMPAIGNS_LIST_MANAGER]: (campaignFirst, campaignSecond, sortType) =>
        sortingByString(campaignFirst.creatorName, campaignSecond.creatorName, sortType),
};
