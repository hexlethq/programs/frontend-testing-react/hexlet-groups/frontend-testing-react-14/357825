import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import fetchCampaigns from 'components/CompanyOfTheDay/Campaigns/fetchCampaigns';
import { CAMPAIGN_UPDATE_STATUS_SUCCESS } from 'constants/notifications';

export default function (campaignId, status) {
    return (dispatch) => {
        fetcher.post(`/api/v1/work_in_company/${campaignId}/edit_status`, null, { params: { status } }).then(() => {
            dispatch(fetchCampaigns());
            dispatch(addNotification(CAMPAIGN_UPDATE_STATUS_SUCCESS, [campaignId]));
        });
    };
}
