import React, { Fragment, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import Gap from 'bloko/blocks/gap';
import { TextEmphasis, TextTertiary } from 'bloko/blocks/text';
import Link from 'bloko/blocks/link';
import Header from 'bloko/blocks/header';
import Icon from 'bloko/blocks/icon/Icon';
import { IconLink } from 'bloko/blocks/icon';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import { updateSortInfo } from 'modules/sorting';
import { createViewDate } from 'modules/date-format';
import Table from 'components/common/Table';
import TableHead from 'components/common/Table/TableHead';
import TableRow from 'components/common/Table/TableRow';
import TableHeadCell from 'components/common/Table/TableHeadCell';
import TableBody from 'components/common/Table/TableBody';
import TableCell from 'components/common/Table/TableCell';
import SortLinkHeader from 'components/common/SortLinkHeader';
import ElementWithHint from 'components/common/ElementWithHint';
import ModalRegions from 'components/common/ModalRegions';
import ModalFullString from 'components/common/ModalFullString';
import ModalWarning from 'components/common/ModalWarning';
import { setCampaigns, setCampaignsSortInfo } from 'store/models/companyOfTheDay/campaigns';
import changeStatus from 'components/CompanyOfTheDay/Campaigns/CampaignsList/changeStatus';
import fetchCampaigns from 'components/CompanyOfTheDay/Campaigns/fetchCampaigns';
import SORT_CONFIG from 'components/CompanyOfTheDay/Campaigns/CampaignsList/sortConfig';
import {
    COTD_CAMPAIGNS_LIST_TABLE_HEADERS,
    COTD_CAMPAIGNS_LIST_SORT_HEADERS,
    CAMPAIGN_STATUS_ACTIVE,
    CAMPAIGN_STATUS_PAUSED,
    CAMPAIGNS_LIST_COMMENT,
    URL_COMPANY_OF_THE_DAY,
} from 'constants/campaign';

const COMMENT_VIEW_LENGTH = 8;
const EMPLOYER_NAME_VIEW_LENGTH = 20;

const CampaignsList = () => {
    const { superAdmin, userId } = useSelector((state) => state.user);
    const { campaigns, campaignsSortInfo } = useSelector((state) => state.campaignsCOTD);
    const statisticsType = useSelector((state) => state.statisticsCOTD.statisticsType);
    const allRegionsDict = useSelector((state) => state.regions.allRegionsDict);
    const dispatch = useDispatch();

    const [isVisibleWarning, setVisibleWarning, setHiddenWarning] = useOnOffState(false);
    const [campaignId, setCampaignId] = useState(null);

    const sortCampaigns = (head) => {
        const newCampaigns = [...campaigns];
        newCampaigns.sort((campaignFirst, campaignSecond) =>
            SORT_CONFIG[head](campaignFirst, campaignSecond, campaignsSortInfo.type, allRegionsDict)
        );
        dispatch(setCampaigns(newCampaigns));
        updateSortInfo(head, campaignsSortInfo, (info) => dispatch(setCampaignsSortInfo(info)));
    };

    return campaigns.length > 0 ? (
        <Fragment>
            <Table bordered>
                <TableHead>
                    <TableRow>
                        {COTD_CAMPAIGNS_LIST_TABLE_HEADERS.map((head) => (
                            <TableHeadCell position={TableHeadCell.positions.center} key={head}>
                                {COTD_CAMPAIGNS_LIST_SORT_HEADERS.includes(head) ? (
                                    <SortLinkHeader
                                        onClick={() => sortCampaigns(head)}
                                        field={head}
                                        text={trl(`campaigns.${head}.table.head`)}
                                        sortType={campaignsSortInfo.type}
                                        isShowArrow={campaignsSortInfo.head === head}
                                    />
                                ) : (
                                    trl(`campaigns.${head}.table.head`)
                                )}
                            </TableHeadCell>
                        ))}
                        <TableHeadCell colSpan={4} />
                    </TableRow>
                </TableHead>
                <TableBody>
                    {campaigns.map(
                        ({
                            workInCompanyId,
                            employer,
                            dateFrom,
                            dateTo,
                            creatorName,
                            creatorUserId,
                            status,
                            regionList,
                            deviceType,
                            comment,
                        }) => {
                            const revertStatus =
                                status === CAMPAIGN_STATUS_ACTIVE ? CAMPAIGN_STATUS_PAUSED : CAMPAIGN_STATUS_ACTIVE;
                            const admin = superAdmin || creatorUserId === userId;
                            return (
                                <TableRow
                                    key={workInCompanyId}
                                    kind={
                                        status === CAMPAIGN_STATUS_PAUSED
                                            ? TableRow.kinds.impact
                                            : TableRow.kinds.default
                                    }
                                >
                                    <TableCell position={TableCell.positions.center} data-qa="campaign-id">
                                        {workInCompanyId}
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        <Link
                                            href={`https://hh.ru/employer/${employer.employerId}`}
                                            target="_blank"
                                            rel="noopener noreferrer"
                                        >
                                            {employer.employerId}
                                        </Link>
                                        {employer.employerName && (
                                            <TextEmphasis>
                                                <ModalFullString
                                                    title={trl('campaigns.employer.name.text')}
                                                    string={employer.employerName}
                                                    length={EMPLOYER_NAME_VIEW_LENGTH}
                                                />
                                            </TextEmphasis>
                                        )}
                                        {employer.displayName && (
                                            <TextTertiary>
                                                <ModalFullString
                                                    title={trl('campaigns.employer.display.name.text')}
                                                    string={employer.displayName}
                                                    length={EMPLOYER_NAME_VIEW_LENGTH}
                                                />
                                            </TextTertiary>
                                        )}
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        {createViewDate(dateFrom)}
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        {createViewDate(dateTo)}
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        <ModalRegions regions={regionList} />
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>{deviceType}</TableCell>
                                    <TableCell position={TableCell.positions.center}>{creatorName}</TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        {comment && (
                                            <ModalFullString
                                                title={trl(`campaigns.${CAMPAIGNS_LIST_COMMENT}.table.head`)}
                                                string={comment}
                                                length={COMMENT_VIEW_LENGTH}
                                            />
                                        )}
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        <IconLink
                                            onClick={() =>
                                                dispatch(
                                                    push(
                                                        `/company-of-the-day/statistics/${workInCompanyId}/${statisticsType}`
                                                    )
                                                )
                                            }
                                        >
                                            <ElementWithHint
                                                element={
                                                    <Icon
                                                        view={Icon.views.excel}
                                                        size={24}
                                                        initial={Icon.kinds.primary}
                                                    />
                                                }
                                                hint={trl('campaigns.statistics.icon.hint')}
                                            />
                                        </IconLink>
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        <IconLink
                                            onClick={() => {
                                                dispatch(push(`/company-of-the-day/edit/${workInCompanyId}`));
                                            }}
                                            disabled={!admin}
                                        >
                                            <ElementWithHint
                                                element={
                                                    <Icon
                                                        view={Icon.views.edit}
                                                        size={24}
                                                        initial={Icon.kinds.primary}
                                                    />
                                                }
                                                hint={trl('campaign.edit.button')}
                                            />
                                        </IconLink>
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        <IconLink
                                            onClick={() => {
                                                dispatch(changeStatus(workInCompanyId, revertStatus));
                                            }}
                                            disabled={!admin}
                                        >
                                            <ElementWithHint
                                                element={
                                                    <Icon
                                                        view={
                                                            revertStatus === CAMPAIGN_STATUS_ACTIVE
                                                                ? Icon.views.done
                                                                : Icon.views.ban
                                                        }
                                                        size={24}
                                                        initial={
                                                            revertStatus === CAMPAIGN_STATUS_ACTIVE
                                                                ? Icon.kinds.secondary
                                                                : Icon.kinds.tertiary
                                                        }
                                                    />
                                                }
                                                hint={trl(`campaign.status.${revertStatus}.button`)}
                                            />
                                        </IconLink>
                                    </TableCell>
                                    <TableCell position={TableCell.positions.center}>
                                        <IconLink
                                            onClick={() => {
                                                setVisibleWarning();
                                                setCampaignId(workInCompanyId.toString());
                                            }}
                                            disabled={!admin}
                                            data-qa="delete-campaign"
                                        >
                                            <ElementWithHint
                                                element={
                                                    <Icon
                                                        view={Icon.views.remove}
                                                        size={24}
                                                        initial={Icon.kinds.tertiary}
                                                    />
                                                }
                                                hint={trl('campaign.delete.button')}
                                            />
                                        </IconLink>
                                    </TableCell>
                                </TableRow>
                            );
                        }
                    )}
                </TableBody>
            </Table>
            <ModalWarning
                visible={isVisibleWarning}
                setHidden={setHiddenWarning}
                campaignId={campaignId}
                adminUrl={URL_COMPANY_OF_THE_DAY}
                actionAfterDelete={() => dispatch(fetchCampaigns())}
            />
        </Fragment>
    ) : (
        <Gap top>
            <Header level={3}>{trl('campaigns.empty.text')}</Header>
        </Gap>
    );
};

export default CampaignsList;
