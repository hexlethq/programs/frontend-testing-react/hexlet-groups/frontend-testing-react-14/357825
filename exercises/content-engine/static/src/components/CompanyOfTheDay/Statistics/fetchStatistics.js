import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import { getChartData } from 'modules/common';
import {
    setStatistics,
    setStatisticsChart,
    setStatisticsEmployerId,
    setStatisticsEmployerName,
    setStatisticsSortInfo,
    setTotalStatistics,
} from 'store/models/companyOfTheDay/statistics';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import { STATISTICS_BY_DATE, INIT_SORT_INFO } from 'constants/campaign';

export default (workInCompanyId) => {
    return (dispatch, getState) => {
        dispatch(setStatistics({ data: [], loading: true }));
        const { period, statisticsType } = getState().statisticsCOTD;
        const params = {
            fromDate: format(period.start, CALENDAR_FORMAT),
            toDate: format(period.end, CALENDAR_FORMAT),
            workInCompanyId,
        };
        fetcher
            .get(`/api/v1/statistic/by_${statisticsType}`, { params })
            .then((data) => {
                const statistics =
                    statisticsType === STATISTICS_BY_DATE ? data.statisticsByDates : data.statisticByRegions;
                dispatch([
                    setStatisticsEmployerId(data?.employerInfo?.id),
                    setStatisticsEmployerName(data?.employerInfo?.name),
                    setStatistics({ data: statistics, loading: false }),
                    setStatisticsChart(getChartData(statistics, statisticsType)),
                    setTotalStatistics(data?.totalStatistic),
                    setStatisticsSortInfo(INIT_SORT_INFO),
                ]);
            })
            .catch(() => {
                dispatch(setStatistics({ data: [], loading: false }));
            });
    };
};
