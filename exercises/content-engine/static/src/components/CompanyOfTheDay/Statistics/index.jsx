import React, { Fragment, useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { push } from 'connected-react-router';
import Button from 'bloko/blocks/button';
import Header from 'bloko/blocks/header';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import Filter from 'components/common/StatisticsElements/Filter';
import StatisticsTabs from 'components/common/StatisticsElements/StatisticsTabs';
import Chart from 'components/common/StatisticsElements/Chart';
import StatisticsTable from 'components/common/StatisticsElements/StatisticsTable';
import StatisticsDownload from 'components/common/StatisticsElements/StatisticsDownload';
import fetchStatistics from 'components/CompanyOfTheDay/Statistics/fetchStatistics';
import {
    STATISTICS_TABLE_SECOND_HEADERS_FULL,
    STATISTICS_TABLE_SECOND_HEADERS_SHORT,
    COMPANY_OF_THE_DAY,
} from 'constants/campaign';
import {
    setStatistics,
    setStatisticsPeriod,
    setStatisticsSortInfo,
    setStatisticsType,
} from 'store/models/companyOfTheDay/statistics';
import 'components/CompanyOfTheDay/Statistics/Statistics.less';

const Statistics = () => {
    const {
        employerId,
        employerName,
        statisticsType,
        statistics,
        period,
        totalStatistics,
        statisticsSortInfo,
        chartData,
    } = useSelector((state) => state.statisticsCOTD);
    const dispatch = useDispatch();

    const [fullStatistics, setFullStatistics] = useState(false);

    const { campaignId, type } = useParams();

    const STATISTICS_TABLE_SECOND_HEADERS = fullStatistics
        ? STATISTICS_TABLE_SECOND_HEADERS_FULL
        : STATISTICS_TABLE_SECOND_HEADERS_SHORT;

    useEffect(() => {
        if (type !== statisticsType) {
            dispatch([setStatistics({ data: [], loading: true }), setStatisticsType(type)]);
        }
    }, [type, dispatch, statisticsType]);

    useEffect(() => {
        dispatch(fetchStatistics(campaignId));
    }, [campaignId, dispatch, statisticsType]);

    return (
        <Fragment>
            <ShiftWrapper>
                <Button kind={Button.kinds.primary} onClick={() => dispatch(push('/company-of-the-day/campaigns/'))}>
                    {trl('toCampaigns.return.button')}
                </Button>
            </ShiftWrapper>
            <Header level={2}>{trl('statistics.header', [campaignId])}</Header>
            <Header level={3}>{trl('statistics.header.employer', [employerName, employerId])}</Header>
            <Filter
                campaignId={campaignId}
                period={period}
                setPeriod={(period) => dispatch(setStatisticsPeriod(period))}
                fetchStatistics={() => dispatch(fetchStatistics(campaignId))}
            />
            <StatisticsTabs adminType={COMPANY_OF_THE_DAY} />
            {statistics?.data?.length > 0 && (
                <Fragment>
                    <div className="statistics-container">
                        <Chart chartData={chartData} secondHeaders={STATISTICS_TABLE_SECOND_HEADERS} />
                        <StatisticsDownload
                            campaignId={campaignId}
                            statisticsType={statisticsType}
                            period={period}
                            adminType={COMPANY_OF_THE_DAY}
                        />
                    </div>
                    <LinkSwitch onClick={() => setFullStatistics(!fullStatistics)}>
                        {fullStatistics ? trl('statistics.full.close.link') : trl('statistics.full.show.link')}
                    </LinkSwitch>
                </Fragment>
            )}
            <StatisticsTable
                statistics={statistics}
                totalStatistics={totalStatistics}
                statisticsType={statisticsType}
                statisticsSortInfo={statisticsSortInfo}
                secondHeaders={STATISTICS_TABLE_SECOND_HEADERS}
                setStatistics={(statistics) => dispatch(setStatistics(statistics))}
                setStatisticsSortInfo={(info) => dispatch(setStatisticsSortInfo(info))}
            />
        </Fragment>
    );
};

export default Statistics;
