import React, { Fragment, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Gap from 'bloko/blocks/gap';
import Checkbox from 'bloko/blocks/checkbox';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import CompositeSelection from 'bloko/blocks/compositeSelection';
import { fromTree } from 'bloko/common/tree/treeCollectionHelper';
import createStaticDataProvider from 'bloko/blocks/suggest/createStaticDataProvider';
import trl from 'modules/translation';
import { MOSCOW_REGION, ALL_RUSSIA, ALL_RUSSIA_WITHOUT_MOSCOW, SPECIAL_REGIONS } from 'constants/campaign';
import 'components/CompanyOfTheDay/SelectRegions/SelectRegions.less';

const SelectRegions = ({
    selectedRegions,
    setSelectedRegions,
    actions = false,
    setSelectedSpecialRegions,
    initSpecialRegion = '',
}) => {
    const { allRegions, regionIdsRussia, regionIdsRussiaWithoutMoscow } = useSelector((state) => ({
        allRegions: state.regions.allRegionsCOTD,
        regionIdsRussia: state.regions.allRegionsCOTD.filter((region) => region.russian).map((region) => region.id),
        regionIdsRussiaWithoutMoscow: state.regions.allRegionsCOTD
            .filter((region) => region.russian && !MOSCOW_REGION.includes(region.id))
            .map((region) => region.id),
    }));

    const [specialRegion, setSpecialRegion] = useState(initSpecialRegion);

    useEffect(() => {
        setSpecialRegion(initSpecialRegion);
    }, [initSpecialRegion]);

    const collection = fromTree(allRegions);
    const dataProvider = createStaticDataProvider(collection.toList());

    const updateSpecialRegion = (newSpecialRegion) => {
        setSpecialRegion(newSpecialRegion);
        switch (newSpecialRegion) {
            case ALL_RUSSIA:
                setSelectedRegions(regionIdsRussia);
                setSelectedSpecialRegions({ isRussia: true, excludedRegions: [] });
                break;
            case ALL_RUSSIA_WITHOUT_MOSCOW:
                setSelectedRegions(regionIdsRussiaWithoutMoscow);
                setSelectedSpecialRegions({ isRussia: true, excludedRegions: MOSCOW_REGION });
                break;
            default:
                setSelectedSpecialRegions({ isRussia: false, excludedRegions: [] });
        }
    };

    return (
        <Fragment>
            <CompositeSelection
                collection={collection}
                value={selectedRegions}
                onChange={(region) => {
                    if (actions) {
                        updateSpecialRegion('');
                    }
                    setSelectedRegions(region);
                }}
                title={trl('regions.select.title')}
                trl={{
                    submit: trl('regions.select.submit'),
                    cancel: trl('regions.select.cancel'),
                    searchPlaceholder: trl('regions.select.searchPlaceholder'),
                    notFound: trl('regions.select.notFound'),
                }}
            >
                {({ renderInput, renderTagList }) => (
                    <div>
                        {renderTagList()}
                        {renderInput({ suggest: true, dataProvider, 'data-qa': 'regions' })}
                    </div>
                )}
            </CompositeSelection>
            {actions && (
                <div className="regions-actions">
                    <div>
                        {SPECIAL_REGIONS.map((region) => (
                            <Gap right Element="span" key={region}>
                                <Checkbox
                                    value={region}
                                    checked={specialRegion === region}
                                    onChange={(e) => {
                                        if (e.target.value === specialRegion) {
                                            updateSpecialRegion('');
                                            setSelectedRegions([]);
                                        } else {
                                            updateSpecialRegion(e.target.value);
                                        }
                                    }}
                                    data-qa={`checkbox-${region.toLowerCase()}`}
                                >
                                    {trl(`regions.${region}.link`)}
                                </Checkbox>
                            </Gap>
                        ))}
                    </div>
                    {selectedRegions.length > 0 && (
                        <LinkSwitch
                            onClick={() => {
                                setSelectedRegions([]);
                                updateSpecialRegion('');
                            }}
                        >
                            {trl('regions.clear.link')}
                        </LinkSwitch>
                    )}
                </div>
            )}
        </Fragment>
    );
};

SelectRegions.propTypes = {
    selectedRegions: PropTypes.array,
    setSelectedRegions: PropTypes.func,
    actions: PropTypes.bool,
    setSelectedSpecialRegions: PropTypes.func,
    initSpecialRegion: PropTypes.string,
};

export default SelectRegions;
