import fetcher from 'modules/fetcher';
import { sortingByString } from 'modules/sorting';
import { setAllRegionsCOTD } from 'store/models/regions';
import { SORT_ASCENDING } from 'constants/common';

export default function () {
    return (dispatch) => {
        fetcher.get('/api/v1/regions/translation').then((data) => {
            data.sort((regionFirst, regionSecond) =>
                sortingByString(regionFirst.text, regionSecond.text, SORT_ASCENDING)
            );
            const regionsRussia = [];
            const regionsOtherCountries = [];
            data.forEach((region) => {
                if (region.russian) {
                    regionsRussia.push(region);
                } else {
                    regionsOtherCountries.push(region);
                }
            });
            dispatch(setAllRegionsCOTD([...regionsRussia, ...regionsOtherCountries]));
        });
    };
}
