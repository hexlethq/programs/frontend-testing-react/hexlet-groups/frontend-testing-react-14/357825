import { parseISO } from 'date-fns';
import fetcher from 'modules/fetcher';
import {
    setCampaignId,
    setCampaignName,
    setDeviceTypes,
    setEmployerId,
    setEmployerName,
    setLogo,
    setPeriod,
    setRegions,
    setAdditionalRegionsInfo,
} from 'store/models/companyOfTheDay/createCampaign';

export default function (campaignId) {
    return (dispatch) => {
        fetcher
            .get('/api/v1/work_in_company', { params: { workInCompanyId: campaignId } })
            .then(
                ({
                    employerId,
                    displayName,
                    deviceTypes,
                    regionIds,
                    isRussia,
                    excludedRegions,
                    dateStart,
                    dateEnd,
                    workInCompanyLogoId,
                    workInCompanyLogoUrl,
                }) => {
                    fetcher.get('/api/v1/work_in_company/employer', { params: { employerId } }).then(({ name }) => {
                        dispatch([
                            setCampaignId(campaignId),
                            setEmployerId(employerId),
                            setEmployerName(name),
                            setCampaignName(displayName),
                            setDeviceTypes(deviceTypes),
                            setRegions(regionIds.map((id) => id.toString())),
                            setAdditionalRegionsInfo({ isRussia, excludedRegions }),
                            setPeriod({ start: parseISO(dateStart), end: parseISO(dateEnd) }),
                            setLogo({
                                data: {
                                    logoId: workInCompanyLogoId,
                                    logoUrl: workInCompanyLogoUrl,
                                    fileName: workInCompanyLogoId ? `img_${workInCompanyLogoId}` : null,
                                },
                                loading: false,
                            }),
                        ]);
                    });
                }
            );
    };
}
