import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Column from 'bloko/blocks/column';
import Button from 'bloko/blocks/button';
import trl from 'modules/translation';
import AdvFormWrapper from 'components/common/AdvFormWrapper';
import ShiftWrapper from 'components/common/ShiftWrapper';
import Identifier from 'components/common/FormElements/Identifier';
import Name from 'components/CompanyOfTheDay/CreateCampaign/CampaignForm/Name';
import DeviceType from 'components/common/FormElements/DeviceType';
import Logo from 'components/CompanyOfTheDay/CreateCampaign/CampaignForm/Logo';
import Regions from 'components/CompanyOfTheDay/CreateCampaign/CampaignForm/Regions';
import Period from 'components/common/FormElements/Period';
import Comment from 'components/common/FormElements/Comment';
import Busyness from 'components/common/FormElements/Busyness';
import saveCampaign from 'components/CompanyOfTheDay/CreateCampaign/CampaignForm/saveCampaign';
import {
    setPeriod,
    setDeviceTypes,
    setComment,
    setEmployerId,
    setEmployerName,
    setBusyness,
} from 'store/models/companyOfTheDay/createCampaign';
import { ENTITY_EMPLOYER, URL_COMPANY_OF_THE_DAY } from 'constants/campaign';

const CampaignForm = () => {
    const { campaignId, deviceTypes, period, comment, employerName, busyness, regions } = useSelector(
        (state) => state.createCampaignCOTD
    );
    const dispatch = useDispatch();

    return (
        <AdvFormWrapper>
            <Identifier
                entity={ENTITY_EMPLOYER}
                name={employerName}
                saveId={(id) => dispatch(setEmployerId(id))}
                saveName={(name) => dispatch(setEmployerName(name))}
            />
            <Name />
            <DeviceType
                deviceTypes={deviceTypes}
                setDeviceTypes={(deviceTypes) => dispatch(setDeviceTypes(deviceTypes))}
            />
            <Logo />
            <Regions />
            <Period period={period} setPeriod={(period) => dispatch(setPeriod(period))} />
            <Busyness
                busyness={busyness}
                period={period}
                adminUrl={URL_COMPANY_OF_THE_DAY}
                setBusyness={(busyness) => dispatch(setBusyness(busyness))}
                busynessInfo={{ period, regions, deviceTypes }}
            />
            <Comment comment={comment} setComment={(comment) => dispatch(setComment(comment))} />
            <Column xs="4" s="6" m="10" l="12">
                <ShiftWrapper>
                    <Button
                        kind={Button.kinds.primary}
                        onClick={() => dispatch(saveCampaign())}
                        data-qa="save-campaign"
                    >
                        {!campaignId ? trl('createCampaign.create.button') : trl('createCampaign.edit.button')}
                    </Button>
                </ShiftWrapper>
            </Column>
        </AdvFormWrapper>
    );
};

export default CampaignForm;
