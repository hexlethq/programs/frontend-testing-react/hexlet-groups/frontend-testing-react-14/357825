import { push } from 'connected-react-router';
import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import addNotification from 'modules/notification';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import {
    CAMPAIGN_SAVE_SUCCESS,
    CAMPAIGN_SAVE_DATA_ERROR,
    CAMPAIGN_SAVE_BUSY_ERROR,
    CAMPAIGN_NOTIFICATION_ERROR,
    CAMPAIGN_EDIT_SUCCESS,
} from 'constants/notifications';
import { CAMPAIGN_DEVICE_TYPE_MOBILE } from 'constants/campaign';

export default function () {
    return (dispatch, getState) => {
        const {
            campaignId,
            campaignName,
            employerId,
            deviceTypes,
            logo,
            period,
            regions,
            additionalRegionsInfo,
            comment,
        } = getState().createCampaignCOTD;
        const { isRussia, excludedRegions } = additionalRegionsInfo;
        const regionIds = regions.map((el) => parseInt(el, 10));
        const body = {
            displayName: campaignName,
            employerId,
            deviceTypes,
            workInCompanyLogoId: deviceTypes.includes(CAMPAIGN_DEVICE_TYPE_MOBILE) ? logo.data.logoId : null,
            regionIds,
            isRussia,
            excludedRegions,
            dateStart: format(period.start, CALENDAR_FORMAT),
            dateEnd: format(period.end, CALENDAR_FORMAT),
            comment,
        };
        let url = '/api/v1/work_in_company';
        if (campaignId) {
            url += `/${campaignId}/edit`;
        }
        fetcher
            .post(url, body)
            .then((data) => {
                if (!campaignId) {
                    dispatch(addNotification(CAMPAIGN_SAVE_SUCCESS, [data]));
                } else {
                    dispatch(addNotification(CAMPAIGN_EDIT_SUCCESS, [campaignId]));
                }
                dispatch(push('/company-of-the-day/campaigns'));
            })
            .catch((err) => {
                const status = err?.response?.status;
                if (status === 400) {
                    const errorDescription = err.response.data;
                    const errorMessage = err.response.data?.message;
                    if (errorDescription && !errorMessage) {
                        dispatch(addNotification(CAMPAIGN_NOTIFICATION_ERROR[errorDescription]));
                    } else {
                        dispatch(addNotification(CAMPAIGN_SAVE_DATA_ERROR));
                    }
                }
                if (status === 412) {
                    dispatch(addNotification(CAMPAIGN_SAVE_BUSY_ERROR));
                }
            });
    };
}
