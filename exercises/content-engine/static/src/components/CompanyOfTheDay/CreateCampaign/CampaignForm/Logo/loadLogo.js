import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import { setLogo } from 'store/models/companyOfTheDay/createCampaign';
import { LOGO_LOAD_SUCCESS, LOGO_LOAD_ERROR } from 'constants/notifications';

export default function (file) {
    return (dispatch) => {
        dispatch(setLogo({ data: {}, loading: true }));
        const config = { headers: { 'Content-Type': 'multipart/form-data' } };
        const data = new FormData();
        data.append('file', file);
        fetcher.put('/api/v1/work_in_company/logo', data, config).then(
            ({ workInCompanyLogoId, logo }) => {
                dispatch(
                    setLogo({
                        data: { logoId: workInCompanyLogoId, logoUrl: logo, fileName: file.name },
                        loading: false,
                    })
                );
                dispatch(addNotification(LOGO_LOAD_SUCCESS));
            },
            (err) => {
                dispatch(setLogo({ data: {}, loading: false }));
                const status = err?.response?.status;
                if (status === 400) {
                    const size = err?.response?.data?.errors?.[0]?.description;
                    let params = [];
                    if (size) {
                        const { width, height } = JSON.parse(size);
                        params = [width, height];
                    }
                    dispatch(addNotification(LOGO_LOAD_ERROR, params));
                }
            }
        );
    };
}
