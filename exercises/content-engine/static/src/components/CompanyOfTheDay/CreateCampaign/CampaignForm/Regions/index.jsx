import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import SelectRegions from 'components/CompanyOfTheDay/SelectRegions';
import fetchRegions from 'components/CompanyOfTheDay/fetchRegions';
import { setRegions, setAdditionalRegionsInfo } from 'store/models/companyOfTheDay/createCampaign';
import { ALL_RUSSIA, ALL_RUSSIA_WITHOUT_MOSCOW } from 'constants/campaign';

const Regions = () => {
    const { regions, additionalRegionsInfo } = useSelector((state) => state.createCampaignCOTD);
    const dispatch = useDispatch();

    const { isRussia, excludedRegions } = additionalRegionsInfo;

    const [initSpecialRegion, setInitSpecialRegion] = useState('');

    useEffect(() => {
        dispatch(fetchRegions());
    }, [dispatch]);

    useEffect(() => {
        if (isRussia) {
            if (excludedRegions?.length > 0) {
                setInitSpecialRegion(ALL_RUSSIA_WITHOUT_MOSCOW);
            } else {
                setInitSpecialRegion(ALL_RUSSIA);
            }
        } else {
            setInitSpecialRegion('');
        }
    }, [dispatch, excludedRegions, isRussia]);

    const setSelectedRegions = (regions) => {
        dispatch(setRegions(regions));
    };

    const setSelectedSpecialRegions = (additionalRegionsInfo) => {
        dispatch(setAdditionalRegionsInfo(additionalRegionsInfo));
    };

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl('createCampaign.regions.legend')}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <SelectRegions
                        selectedRegions={regions}
                        setSelectedRegions={setSelectedRegions}
                        setSelectedSpecialRegions={setSelectedSpecialRegions}
                        initSpecialRegion={initSpecialRegion}
                        actions
                    />
                </FormRow>
            }
        />
    );
};

export default Regions;
