import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ControlGroup, { ControlGroupItem } from 'bloko/blocks/controlGroup';
import { FormLegend, FormSpacer, FormRow } from 'bloko/blocks/form';
import Input from 'bloko/blocks/input';
import Icon from 'bloko/blocks/icon';
import trl from 'modules/translation';
import ElementWithHint from 'components/common/ElementWithHint';
import AdvFormColumns from 'components/common/AdvFormColumns';
import { setCampaignName } from 'store/models/companyOfTheDay/createCampaign';
import 'components/CompanyOfTheDay/CreateCampaign/CampaignForm/Name/Name.less';

const Name = () => {
    const campaignName = useSelector((state) => state.createCampaignCOTD.campaignName);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label" htmlFor="name">
                        {trl('createCampaign.name.legend')}
                    </FormLegend>
                </FormRow>
            }
            group={
                <ControlGroup>
                    <FormRow>
                        <ControlGroupItem main>
                            <Input
                                id="name"
                                value={campaignName}
                                onChange={(e) => dispatch(setCampaignName(e.target.value))}
                                data-qa="campaign-name"
                            />
                        </ControlGroupItem>
                        <ControlGroupItem>
                            <FormSpacer>
                                <div className="name-hint">
                                    <ElementWithHint
                                        element={<Icon view={Icon.views.hint} initial={Icon.kinds.unique} />}
                                        hint={trl('createCampaign.name.hint')}
                                    />
                                </div>
                            </FormSpacer>
                        </ControlGroupItem>
                    </FormRow>
                </ControlGroup>
            }
        />
    );
};

export default Name;
