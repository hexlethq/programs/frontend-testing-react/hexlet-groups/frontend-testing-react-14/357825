import React from 'react';
import Dropzone from 'react-dropzone';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import Gap from 'bloko/blocks/gap';
import Input from 'bloko/blocks/input';
import Button from 'bloko/blocks/button';
import Loading from 'bloko/blocks/loading';
import ControlGroup, { ControlGroupItem } from 'bloko/blocks/controlGroup';
import { TextTertiary } from 'bloko/blocks/text';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import ElementWithHint from 'components/common/ElementWithHint';
import loadLogo from 'components/CompanyOfTheDay/CreateCampaign/CampaignForm/Logo/loadLogo';
import { CAMPAIGN_DEVICE_TYPE_MOBILE, ACCEPT_IMAGE_TYPES } from 'constants/campaign';
import 'components/CompanyOfTheDay/CreateCampaign/CampaignForm/Logo/Logo.less';

const Logo = () => {
    const { deviceTypes, logo } = useSelector((state) => state.createCampaignCOTD);
    const dispatch = useDispatch();

    const fileName = logo.data.fileName || '';

    const onDrop = (files) => {
        dispatch(loadLogo(files[0]));
    };

    if (!deviceTypes.includes(CAMPAIGN_DEVICE_TYPE_MOBILE)) {
        return null;
    }

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label" htmlFor="logo">
                        {trl('createCampaign.logo.legend')}
                    </FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <Dropzone onDrop={onDrop} accept={ACCEPT_IMAGE_TYPES}>
                        {({ getRootProps, getInputProps }) => (
                            <div {...getRootProps()}>
                                <input {...getInputProps()} />
                                <ControlGroup>
                                    <ControlGroupItem main>
                                        <Input
                                            id="logo"
                                            placeholder={trl('createCampaign.logo.placeholder')}
                                            value={fileName}
                                            onChange={() => {}}
                                            readOnly
                                        />
                                    </ControlGroupItem>
                                    <ControlGroupItem>
                                        <Button loading={logo.loading ? <Loading /> : null}>
                                            {trl('createCampaign.logo.button')}
                                        </Button>
                                    </ControlGroupItem>
                                </ControlGroup>
                            </div>
                        )}
                    </Dropzone>
                    <div className="logo-hint">
                        <TextTertiary>{trl('createCampaign.logo.requirement.text')}</TextTertiary>
                    </div>
                    {logo.data?.logoUrl && (
                        <Gap top>
                            <ElementWithHint
                                element={
                                    <div className="logo-view">
                                        <div className="logo-img">
                                            <img src={logo.data.logoUrl} alt={fileName} />
                                        </div>
                                        <div className="logo-vacancies">{trl('createCampaign.logo.example.text')}</div>
                                    </div>
                                }
                                hint={trl('createCampaign.logo.example.hint')}
                            />
                        </Gap>
                    )}
                </FormRow>
            }
        />
    );
};

export default Logo;
