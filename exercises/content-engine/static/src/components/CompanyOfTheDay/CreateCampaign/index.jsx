import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import { useParams } from 'react-router';
import PropTypes from 'prop-types';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import CampaignForm from 'components/CompanyOfTheDay/CreateCampaign/CampaignForm';
import getCampaignInfo from 'components/CompanyOfTheDay/CreateCampaign/getCampaignInfo';
import { resetCampaignInfo } from 'store/models/companyOfTheDay/createCampaign';

const CreateCampaign = ({ isEdit = false }) => {
    const campaignId = useSelector((state) => state.createCampaignCOTD.campaignId);
    const dispatch = useDispatch();

    const campaignIdFromPath = useParams().campaignId;

    useEffect(() => {
        dispatch(resetCampaignInfo());
        if (isEdit) {
            dispatch(getCampaignInfo(campaignIdFromPath));
        }
    }, [campaignIdFromPath, dispatch, isEdit]);

    if (isEdit && !campaignId) {
        return null;
    }

    return (
        <Fragment>
            <ShiftWrapper>
                <Button kind={Button.kinds.primary} onClick={() => dispatch(push('/company-of-the-day/campaigns/'))}>
                    {trl('toCampaigns.return.button')}
                </Button>
            </ShiftWrapper>
            <Gap top bottom>
                <CampaignForm />
            </Gap>
        </Fragment>
    );
};

CreateCampaign.propTypes = {
    isEdit: PropTypes.bool,
};

export default CreateCampaign;
