import React from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { matchPath } from 'react-router';
import { ColumnsWrapper } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import Icon from 'bloko/blocks/icon/Icon';
import { FormSpacer } from 'bloko/blocks/form';
import trl from 'modules/translation';
import HHLogo from 'components/Header/logo.svg';
import 'components/Header/Header.less';

const Header = () => {
    const url = useSelector((state) => state.router.location.pathname);
    const name = useSelector((state) => state.user.name);

    const adminType = matchPath(url, '/:adminType')?.params.adminType;

    return (
        <header className="header">
            <ColumnsWrapper>
                <div className="header-content">
                    <div className="header-app-name">
                        <Link to="/">
                            <img src={HHLogo} alt={trl('app.name')} />
                        </Link>
                        <Gap left>
                            <p>{trl('app.name', [trl(`main.menu.${adminType}`)])}</p>
                        </Gap>
                    </div>
                    {name && (
                        <div className="header-user-name">
                            <Icon view={Icon.views.shared} initial={Icon.kinds.inverted} size={24} />
                            <FormSpacer />
                            <p>{name}</p>
                        </div>
                    )}
                </div>
            </ColumnsWrapper>
        </header>
    );
};

export default Header;
