import React from 'react';
import PropTypes from 'prop-types';
import NumericInput from 'bloko/blocks/numericInput';
import trl from 'modules/translation';

const NumberInput = ({ value, onChange, placeholder, ...props }) => (
    <NumericInput
        value={value}
        onChange={onChange}
        errors={{
            notNumber: trl('numericInput.id.error'),
        }}
        placeholder={placeholder}
        {...props}
    />
);

NumberInput.defaultProps = {
    placeholder: trl('numericInput.id.placeholder'),
};

NumberInput.propTypes = {
    value: PropTypes.string,
    onChange: PropTypes.func,
    placeholder: PropTypes.string,
};

export default NumberInput;
