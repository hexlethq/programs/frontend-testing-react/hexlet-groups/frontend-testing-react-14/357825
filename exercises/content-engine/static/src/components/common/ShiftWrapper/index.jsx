import React from 'react';
import PropTypes from 'prop-types';
import 'components/common/ShiftWrapper/ShiftWrapper.less';

const ShiftWrapper = ({ children }) => <div className="shift-wrapper">{children}</div>;

ShiftWrapper.propTypes = {
    children: PropTypes.node,
};

export default ShiftWrapper;
