import React from 'react';
import Loading from 'bloko/blocks/loading';
import 'components/common/Loader/Loader.less';

const Loader = () => (
    <div className="loader">
        <Loading />
    </div>
);

export default Loader;
