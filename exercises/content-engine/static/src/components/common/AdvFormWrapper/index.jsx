import React from 'react';
import PropTypes from 'prop-types';
import 'components/common/AdvFormWrapper/AdvFormWrapper.less';

const AdvFormWrapper = ({ children }) => <form className="adv-form-wrapper">{children}</form>;

AdvFormWrapper.propTypes = {
    children: PropTypes.node,
};

export default AdvFormWrapper;
