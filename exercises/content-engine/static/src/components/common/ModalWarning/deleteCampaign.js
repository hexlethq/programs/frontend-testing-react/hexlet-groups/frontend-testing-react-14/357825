import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import { CAMPAIGN_DELETE_SUCCESS } from 'constants/notifications';

export default (campaignId, adminUrl, actionAfterDelete) => {
    return (dispatch) => {
        fetcher.delete(`/api/v1/${adminUrl}/${campaignId}/delete`).then(() => {
            actionAfterDelete();
            dispatch(addNotification(CAMPAIGN_DELETE_SUCCESS, [campaignId]));
        });
    };
};
