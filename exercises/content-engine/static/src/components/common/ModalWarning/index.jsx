import React from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import Modal, { ModalHeader, ModalTitle, ModalContent } from 'bloko/blocks/modal';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import deleteCampaign from 'components/common/ModalWarning/deleteCampaign';

const ModalWarning = ({ visible, setHidden, campaignId, adminUrl, actionAfterDelete }) => {
    const dispatch = useDispatch();

    return (
        <Modal visible={visible} onClose={setHidden}>
            <ModalHeader>
                <ModalTitle>{trl('campaign.delete.modal.title')}</ModalTitle>
            </ModalHeader>
            <ModalContent>
                <p>{trl('campaign.delete.modal.text', [campaignId])}</p>
                <Gap top>
                    <Button
                        onClick={() => {
                            dispatch(deleteCampaign(campaignId, adminUrl, actionAfterDelete));
                            setHidden();
                        }}
                        data-qa="confirm-delete"
                    >
                        {trl('campaign.delete.button')}
                    </Button>
                </Gap>
            </ModalContent>
        </Modal>
    );
};

ModalWarning.propTypes = {
    visible: PropTypes.bool,
    setHidden: PropTypes.func,
    campaignId: PropTypes.string,
    adminUrl: PropTypes.string,
    actionAfterDelete: PropTypes.func,
};

export default ModalWarning;
