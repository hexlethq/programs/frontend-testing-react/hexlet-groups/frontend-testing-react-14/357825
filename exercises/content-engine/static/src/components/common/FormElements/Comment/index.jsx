import React from 'react';
import PropTypes from 'prop-types';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import Input from 'bloko/blocks/input';
import AdvFormColumns from 'components/common/AdvFormColumns';
import trl from 'modules/translation';

const Comment = ({ comment, setComment }) => (
    <AdvFormColumns
        legend={
            <FormRow>
                <FormLegend Element="label" htmlFor="comment">
                    {trl('createCampaign.comment.legend')}
                </FormLegend>
            </FormRow>
        }
        group={
            <FormRow>
                <Input id="comment" value={comment} onChange={(e) => setComment(e.target.value)} data-qa="comment" />
            </FormRow>
        }
    />
);

Comment.propTypes = {
    comment: PropTypes.string.isRequired,
    setComment: PropTypes.func.isRequired,
};

export default Comment;
