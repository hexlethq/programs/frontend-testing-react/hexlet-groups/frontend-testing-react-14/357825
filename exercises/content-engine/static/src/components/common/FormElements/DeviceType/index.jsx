import React from 'react';
import PropTypes from 'prop-types';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import CheckboxDeviceTypes from 'components/common/CheckboxDeviceTypes';

const DeviceType = ({ deviceTypes, setDeviceTypes }) => (
    <AdvFormColumns
        legend={
            <FormRow>
                <FormLegend Element="label">{trl('createCampaign.deviceType.legend')}</FormLegend>
            </FormRow>
        }
        group={
            <FormRow>
                <CheckboxDeviceTypes checkedDeviceTypes={deviceTypes} setCheckedDeviceTypes={setDeviceTypes} />
            </FormRow>
        }
    />
);

DeviceType.propTypes = {
    deviceTypes: PropTypes.array,
    setDeviceTypes: PropTypes.func,
};

export default DeviceType;
