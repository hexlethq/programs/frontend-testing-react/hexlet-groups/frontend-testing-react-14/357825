import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import addNotification from 'modules/notification';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import { CAMPAIGN_BUSYNESS_DATA_ERROR, CAMPAIGN_NOTIFICATION_ERROR } from 'constants/notifications';
import { CAMPAIGN_BUSY } from 'constants/campaign';

export default (setBusyness, adminUrl, busynessInfo) => {
    return (dispatch) => {
        setBusyness({ data: [], checked: false });
        const { period, regions, deviceTypes, packet } = busynessInfo;
        const body = {
            dateFrom: format(period.start, CALENDAR_FORMAT),
            dateTo: format(period.end, CALENDAR_FORMAT),
            regionIds: regions,
            deviceTypes,
        };
        if (packet) {
            body.packet = packet;
        }
        fetcher
            .put(`/api/v1/${adminUrl}/check_busy`, body)
            .then((data) => {
                const busyness = data.filter((busy) => busy.status === CAMPAIGN_BUSY);
                setBusyness({ data: busyness, checked: true });
            })
            .catch((err) => {
                const status = err?.response?.status;
                if (status === 400) {
                    const errorDescription = err.response.data;
                    const errorMessage = err.response.data?.message;
                    if (errorDescription && !errorMessage) {
                        dispatch(addNotification(CAMPAIGN_NOTIFICATION_ERROR[errorDescription]));
                    } else {
                        dispatch(addNotification(CAMPAIGN_BUSYNESS_DATA_ERROR));
                    }
                }
            });
    };
};
