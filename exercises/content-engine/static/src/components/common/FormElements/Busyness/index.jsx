import React, { Fragment } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import { FormRow } from 'bloko/blocks/form';
import Gap from 'bloko/blocks/gap';
import { TextSecondary } from 'bloko/blocks/text';
import trl from 'modules/translation';
import format, { createViewDate } from 'modules/date-format';
import Table from 'components/common/Table';
import TableHead from 'components/common/Table/TableHead';
import TableHeadCell from 'components/common/Table/TableHeadCell';
import TableRow from 'components/common/Table/TableRow';
import TableBody from 'components/common/Table/TableBody';
import TableCell from 'components/common/Table/TableCell';
import AdvFormColumns from 'components/common/AdvFormColumns';
import checkBusy from 'components/common/FormElements/Busyness/checkBusy';
import { VIEW_FORMAT } from 'constants/date-formats';
import 'components/common/FormElements/Busyness/Busyness.less';

const Busyness = ({ busyness, period, setBusyness, adminUrl, busynessInfo }) => {
    const allRegionsDict = useSelector((state) => state.regions.allRegionsDict);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={<FormRow>{trl('createCampaign.busyness.legend')}</FormRow>}
            group={
                <FormRow>
                    <LinkSwitch
                        onClick={() => dispatch(checkBusy(setBusyness, adminUrl, busynessInfo))}
                        data-qa="check-busy"
                    >
                        {trl('createCampaign.busyness.link')}
                    </LinkSwitch>
                    {busyness.checked && (
                        <Fragment>
                            {busyness.data.length > 0 ? (
                                <Gap top bottom>
                                    <Gap bottom>
                                        <TextSecondary>{trl('createCampaign.busyness.busy.text')}</TextSecondary>
                                    </Gap>
                                    <div className="busyness-info busyness-info_busy">
                                        <Table>
                                            <TableHead>
                                                <TableRow>
                                                    <TableHeadCell className="busyness-info_busy">
                                                        {trl('createCampaign.busyness.region.table.head')}
                                                    </TableHeadCell>
                                                    <TableHeadCell className="busyness-info_busy">
                                                        {trl('createCampaign.busyness.choosePeriod.table.head')}
                                                    </TableHeadCell>
                                                    <TableHeadCell className="busyness-info_busy">
                                                        {trl('createCampaign.busyness.freePeriod.table.head')}
                                                    </TableHeadCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {busyness.data.map(({ regionId, dateFrom, dateTo }) => (
                                                    <TableRow key={regionId}>
                                                        <TableCell>{allRegionsDict[regionId]}</TableCell>
                                                        <TableCell>
                                                            {`${format(period.start, VIEW_FORMAT)} - ${format(
                                                                period.end,
                                                                VIEW_FORMAT
                                                            )}`}
                                                        </TableCell>
                                                        <TableCell>{`${createViewDate(dateFrom)} - ${createViewDate(
                                                            dateTo
                                                        )}`}</TableCell>
                                                    </TableRow>
                                                ))}
                                            </TableBody>
                                        </Table>
                                    </div>
                                </Gap>
                            ) : (
                                <Gap top>
                                    <div className="busyness-info busyness-info_free">
                                        {trl('createCampaign.busyness.free.text')}
                                    </div>
                                </Gap>
                            )}
                        </Fragment>
                    )}
                </FormRow>
            }
        />
    );
};

Busyness.propTypes = {
    busyness: PropTypes.object,
    period: PropTypes.shape({
        start: PropTypes.instanceOf(Date),
        end: PropTypes.instanceOf(Date),
    }),
    setBusyness: PropTypes.func,
    adminUrl: PropTypes.string,
    busynessInfo: PropTypes.object,
};

export default Busyness;
