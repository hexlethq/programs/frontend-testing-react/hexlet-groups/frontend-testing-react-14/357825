import fetcher from 'modules/fetcher';
import { ENTITY_EMPLOYER, ENTITY_VACANCY, URL_COMPANY_OF_THE_DAY, URL_VACANCY_OF_THE_DAY } from 'constants/campaign';

const URL = {
    [ENTITY_EMPLOYER]: URL_COMPANY_OF_THE_DAY,
    [ENTITY_VACANCY]: URL_VACANCY_OF_THE_DAY,
};

export default (id, setName, entity) => {
    fetcher
        .get(`/api/v1/${URL[entity]}/${entity}`, { params: { [`${entity}Id`]: id } })
        .then(({ name }) => {
            setName(name);
        })
        .catch(() => setName(''));
};
