import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import Input from 'bloko/blocks/input';
import ControlGroup, { ControlGroupItem } from 'bloko/blocks/controlGroup';
import Button from 'bloko/blocks/button';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import Search from 'components/common/FormElements/Identifier/Search';

const Identifier = ({ entity, name, saveId, saveName }) => {
    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    return (
        <Fragment>
            <AdvFormColumns
                legend={
                    <FormRow>
                        <FormLegend Element="label" htmlFor={entity}>
                            {trl(`createCampaign.${entity}.legend`)}
                        </FormLegend>
                    </FormRow>
                }
                group={
                    <ControlGroup>
                        <FormRow>
                            <ControlGroupItem main>
                                <Input id={entity} value={name} disabled />
                            </ControlGroupItem>
                            <ControlGroupItem>
                                <Button
                                    kind={Button.kinds.primaryMinor}
                                    onClick={setVisible}
                                    data-qa={`search-${entity}`}
                                >
                                    {trl(`createCampaign.search.button`)}
                                </Button>
                            </ControlGroupItem>
                        </FormRow>
                    </ControlGroup>
                }
            />
            <Search visible={isVisible} setHidden={setHidden} entity={entity} saveId={saveId} saveName={saveName} />
        </Fragment>
    );
};

Identifier.propTypes = {
    entity: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    saveId: PropTypes.func.isRequired,
    saveName: PropTypes.func.isRequired,
};

export default Identifier;
