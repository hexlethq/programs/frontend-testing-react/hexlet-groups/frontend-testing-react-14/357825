import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Modal, { ModalHeader, ModalTitle, ModalContent } from 'bloko/blocks/modal';
import Button from 'bloko/blocks/button';
import { TextSecondary } from 'bloko/blocks/text';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import NumberInput from 'components/common/NumberInput';
import fetchName from 'components/common/FormElements/Identifier/Search/fetchName';
import 'components/common/FormElements/Identifier/Search/Search.less';

const Search = ({ visible, setHidden, entity, saveId, saveName }) => {
    const [id, setId] = useState('');
    const [name, setName] = useState('');

    return (
        <Modal visible={visible} onClose={setHidden}>
            <ModalHeader>
                <ModalTitle>{trl(`createCampaign.${entity}.modal.title`)}</ModalTitle>
            </ModalHeader>
            <ModalContent>
                <div className="search-content">
                    <NumberInput
                        value={id}
                        onChange={(e) => {
                            const value = e.target.value;
                            setId(value);
                            fetchName(value, setName, entity);
                        }}
                        data-qa={`${entity}-id`}
                    />
                    {name ? (
                        <div className="search-name">
                            <div className="search-name-header">{name}</div>
                            <Button
                                kind={Button.kinds.primary}
                                onClick={() => {
                                    setHidden();
                                    saveId(id);
                                    saveName(name);
                                }}
                                data-qa={`choose-${entity}-id`}
                            >
                                {trl('choose.button')}
                            </Button>
                        </div>
                    ) : (
                        <Gap top>
                            <TextSecondary>{trl(`createCampaign.${entity}.modal.notFound.text`)}</TextSecondary>
                        </Gap>
                    )}
                </div>
            </ModalContent>
        </Modal>
    );
};

Search.propTypes = {
    visible: PropTypes.bool.isRequired,
    setHidden: PropTypes.func.isRequired,
    entity: PropTypes.string.isRequired,
    saveId: PropTypes.func.isRequired,
    saveName: PropTypes.func.isRequired,
};

export default Search;
