import React from 'react';
import PropTypes from 'prop-types';
import { parseISO, startOfWeek } from 'date-fns';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import { IntervalCalendarPicker } from 'bloko/blocks/calendar';
import { TextTertiary } from 'bloko/blocks/text';
import trl from 'modules/translation';
import format from 'modules/date-format';
import AdvFormColumns from 'components/common/AdvFormColumns';
import { VIEW_FORMAT, CALENDAR_FORMAT } from 'constants/date-formats';
import 'components/common/FormElements/Period/Period.less';

const MONDAY = startOfWeek(new Date(), { weekStartsOn: 1 });

const Period = ({ period, setPeriod }) => {
    const onDateClick = ({ start, end }) => {
        setTimeout(() => {
            setPeriod({ start: parseISO(start), end: parseISO(end) });
        }, 0);
    };

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl(`createCampaign.period.legend`)}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <div className="period">
                        {trl('createCampaign.period.from.to.text', [
                            format(period.start, VIEW_FORMAT),
                            format(period.end, VIEW_FORMAT),
                        ])}
                    </div>
                    <div className="period-hint">
                        <TextTertiary>{trl('createCampaign.period.requirement.text')}</TextTertiary>
                    </div>
                    <div className="calendar">
                        <IntervalCalendarPicker
                            initialDate={format(period.start, CALENDAR_FORMAT)}
                            start={format(period.start, CALENDAR_FORMAT)}
                            end={format(period.end, CALENDAR_FORMAT)}
                            onDateClick={onDateClick}
                            disableDaysBeforeDate={format(MONDAY, CALENDAR_FORMAT)}
                            disablePartMonth={false}
                            data-qa="calendar"
                        />
                    </div>
                </FormRow>
            }
        />
    );
};

Period.propTypes = {
    period: PropTypes.shape({
        start: PropTypes.instanceOf(Date),
        end: PropTypes.instanceOf(Date),
    }),
    setPeriod: PropTypes.func,
};

export default Period;
