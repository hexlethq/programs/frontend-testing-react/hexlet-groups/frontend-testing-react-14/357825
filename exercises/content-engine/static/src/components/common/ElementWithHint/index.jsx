import React from 'react';
import PropTypes from 'prop-types';
import HoverTip from 'bloko/blocks/drop/Tip/HoverTip';
import Tip from 'bloko/blocks/drop/Tip';
import { Down } from 'bloko/blocks/drop';

const ElementWithHint = ({ element, hint }) => (
    <HoverTip
        placement={Tip.placements.top}
        render={() => hint}
        host={document.body}
        DropElement={Tip}
        layer={Down.layers.aboveOverlayContent}
    >
        {element}
    </HoverTip>
);

ElementWithHint.propTypes = {
    element: PropTypes.node,
    hint: PropTypes.string,
};

export default ElementWithHint;
