import React from 'react';
import PropTypes from 'prop-types';
import { parseISO } from 'date-fns';
import IntervalCalendarPicker from 'bloko/blocks/calendar/IntervalCalendarPicker';
import DropDownClick from 'bloko/blocks/drop/Down/Click';
import DropDown from 'bloko/blocks/drop/Down';
import { Down } from 'bloko/blocks/drop';
import InputIcon from 'bloko/blocks/input/inputIcon';
import Icon from 'bloko/blocks/icon';
import trl from 'modules/translation';
import format from 'modules/date-format';
import { CALENDAR_FORMAT, VIEW_FORMAT } from 'constants/date-formats';

const InputWithIntervalCalendar = ({ from, to, action }) => {
    const onDateClick = ({ start, end }) => {
        setTimeout(() => {
            action({ start: parseISO(start), end: parseISO(end) });
        }, 0);
    };

    return (
        <DropDownClick
            render={() => (
                <IntervalCalendarPicker
                    initialDate={format(from, CALENDAR_FORMAT)}
                    start={format(from, CALENDAR_FORMAT)}
                    end={format(to, CALENDAR_FORMAT)}
                    onDateClick={onDateClick}
                    disablePartMonth={false}
                />
            )}
            placement={DropDown.placements.bottomEnd}
            layer={Down.layers.aboveOverlayContent}
        >
            <InputIcon
                placeholder={trl('admin.actsReport.filter.input')}
                icon={<Icon view={Icon.views.calendar} />}
                value={`${format(from, VIEW_FORMAT)} - ${format(to, VIEW_FORMAT)}`}
                onChange={() => {}}
                size="29"
            />
        </DropDownClick>
    );
};

InputWithIntervalCalendar.propTypes = {
    from: PropTypes.instanceOf(Date),
    to: PropTypes.instanceOf(Date),
    action: PropTypes.func,
};

export default InputWithIntervalCalendar;
