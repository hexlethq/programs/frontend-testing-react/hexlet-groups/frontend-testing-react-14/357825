import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const BracketsText = ({ children, withNbsp, withUsualSpace }) => (
    <Fragment>
        {withNbsp && trl('space.nbsp')}
        {withUsualSpace && trl('space.usual')}
        {'('}
        {children}
        {')'}
    </Fragment>
);

BracketsText.defaultProps = {
    withNbsp: false,
    withUsualSpace: false,
};

BracketsText.propTypes = {
    children: PropTypes.node,
    withNbsp: PropTypes.bool,
    withUsualSpace: PropTypes.bool,
};

export default BracketsText;
