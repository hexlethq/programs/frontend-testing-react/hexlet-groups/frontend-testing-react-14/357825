import React from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const PaymentUpdateStatusSuccess = ({ params }) => {
    return <p>{trl('payment.update.status.success.notification', params)}</p>;
};

PaymentUpdateStatusSuccess.propTypes = {
    params: PropTypes.array,
};

export default PaymentUpdateStatusSuccess;
