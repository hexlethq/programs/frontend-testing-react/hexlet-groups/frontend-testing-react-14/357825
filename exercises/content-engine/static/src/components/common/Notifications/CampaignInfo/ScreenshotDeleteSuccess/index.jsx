import trl from 'modules/translation';

const ScreenshotDeleteSuccess = () => {
    return trl('campaignInfo.screenshot.delete.success.notification');
};

export default ScreenshotDeleteSuccess;
