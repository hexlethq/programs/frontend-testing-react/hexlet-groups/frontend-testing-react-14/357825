import trl from 'modules/translation';

const ScreenshotUploadSuccess = () => {
    return trl('campaignInfo.screenshot.upload.success.notification');
};

export default ScreenshotUploadSuccess;
