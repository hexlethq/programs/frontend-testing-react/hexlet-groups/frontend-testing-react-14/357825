import React from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const CampaignModerationVacancyError = ({ params }) => {
    if (params.length === 1) {
        return <p>{trl('campaign.moderation.vacancy.error.notification', params)}</p>;
    }
    return <p>{trl('campaign.moderation.vacancies.error.notification', [params.join(', ')])}</p>;
};

CampaignModerationVacancyError.propTypes = {
    params: PropTypes.array,
};

export default CampaignModerationVacancyError;
