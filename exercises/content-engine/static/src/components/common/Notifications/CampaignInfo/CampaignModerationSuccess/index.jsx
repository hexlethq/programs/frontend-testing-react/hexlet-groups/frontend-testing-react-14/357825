import React from 'react';
import trl from 'modules/translation';

const CampaignModerationSuccess = () => {
    return <p>{trl('campaign.moderation.success.notification')}</p>;
};

export default CampaignModerationSuccess;
