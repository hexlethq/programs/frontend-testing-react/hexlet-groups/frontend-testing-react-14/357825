import React from 'react';
import trl from 'modules/translation';

const IncorrectFieldsError = () => {
    return <p>{trl('incorrect.fields.error.notification')}</p>;
};

export default IncorrectFieldsError;
