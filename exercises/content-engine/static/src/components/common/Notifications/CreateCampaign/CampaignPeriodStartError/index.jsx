import React from 'react';
import trl from 'modules/translation';

const CampaignPeriodStartError = () => {
    return <p>{trl('createCampaign.period.start.error.notification')}</p>;
};

export default CampaignPeriodStartError;
