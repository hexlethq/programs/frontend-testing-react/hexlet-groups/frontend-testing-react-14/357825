import React from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const CampaignEditSuccess = ({ params }) => {
    return <p>{trl('createCampaign.edit.success.notification', params)}</p>;
};

CampaignEditSuccess.propTypes = {
    params: PropTypes.array,
};

export default CampaignEditSuccess;
