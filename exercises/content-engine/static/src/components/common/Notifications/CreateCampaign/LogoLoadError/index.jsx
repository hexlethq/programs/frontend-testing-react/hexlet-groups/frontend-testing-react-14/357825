import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const LogoLoadError = ({ params }) => {
    return (
        <Fragment>
            {params.length > 0 && <p>{trl('createCampaign.logo.sizes.error.notification', params)}</p>}
            <p>{trl('createCampaign.logo.requirement.text')}</p>
        </Fragment>
    );
};

LogoLoadError.propTypes = {
    params: PropTypes.array,
};

export default LogoLoadError;
