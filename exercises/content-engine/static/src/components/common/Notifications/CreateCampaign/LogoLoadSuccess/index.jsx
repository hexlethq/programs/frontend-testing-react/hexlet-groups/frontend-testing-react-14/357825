import trl from 'modules/translation';

const LogoLoadSuccess = () => {
    return trl('createCampaign.logo.success.notification');
};

export default LogoLoadSuccess;
