import React from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const CampaignSaveSuccess = ({ params }) => {
    return <p>{trl('createCampaign.save.success.notification', params)}</p>;
};

CampaignSaveSuccess.propTypes = {
    params: PropTypes.array,
};

export default CampaignSaveSuccess;
