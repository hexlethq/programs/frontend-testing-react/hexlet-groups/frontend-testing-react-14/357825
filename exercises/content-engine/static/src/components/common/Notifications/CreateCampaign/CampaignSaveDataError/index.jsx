import React from 'react';
import trl from 'modules/translation';

const CampaignSaveDataError = () => {
    return <p>{trl('createCampaign.save.data.error.notification')}</p>;
};

export default CampaignSaveDataError;
