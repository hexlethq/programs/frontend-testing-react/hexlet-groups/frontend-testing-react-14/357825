import React from 'react';
import trl from 'modules/translation';

const CampaignSaveBusyError = () => {
    return <p>{trl('createCampaign.save.busy.error.notification')}</p>;
};

export default CampaignSaveBusyError;
