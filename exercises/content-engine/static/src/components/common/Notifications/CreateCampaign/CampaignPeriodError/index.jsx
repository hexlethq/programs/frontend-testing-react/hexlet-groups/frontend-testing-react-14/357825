import React from 'react';
import trl from 'modules/translation';

const CampaignPeriodError = () => {
    return <p>{trl('createCampaign.period.error.notification')}</p>;
};

export default CampaignPeriodError;
