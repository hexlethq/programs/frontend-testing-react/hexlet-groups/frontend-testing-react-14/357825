import React from 'react';
import trl from 'modules/translation';

const CampaignPeriodEndError = () => {
    return <p>{trl('createCampaign.period.end.error.notification')}</p>;
};

export default CampaignPeriodEndError;
