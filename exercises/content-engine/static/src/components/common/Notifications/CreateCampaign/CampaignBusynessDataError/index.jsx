import React from 'react';
import trl from 'modules/translation';

const CampaignBusynessDataError = () => {
    return <p>{trl('createCampaign.busyness.data.error.notification')}</p>;
};

export default CampaignBusynessDataError;
