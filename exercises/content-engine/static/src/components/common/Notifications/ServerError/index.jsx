import trl from 'modules/translation';

const ServerErrorNotification = () => {
    return trl('serverError.text');
};

export default ServerErrorNotification;
