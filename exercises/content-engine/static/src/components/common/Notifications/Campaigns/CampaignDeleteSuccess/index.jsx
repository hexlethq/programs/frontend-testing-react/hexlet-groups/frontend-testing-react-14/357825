import React from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const CampaignDeleteSuccess = ({ params }) => {
    return <p>{trl('campaigns.delete.success.notification', params)}</p>;
};

CampaignDeleteSuccess.propTypes = {
    params: PropTypes.array,
};

export default CampaignDeleteSuccess;
