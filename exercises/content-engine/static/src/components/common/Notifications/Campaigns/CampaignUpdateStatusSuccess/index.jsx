import React from 'react';
import PropTypes from 'prop-types';
import trl from 'modules/translation';

const CampaignUpdateStatusSuccess = ({ params }) => {
    return <p>{trl('campaigns.update.status.success.notification', params)}</p>;
};

CampaignUpdateStatusSuccess.propTypes = {
    params: PropTypes.array,
};

export default CampaignUpdateStatusSuccess;
