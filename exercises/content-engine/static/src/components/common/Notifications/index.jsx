import React from 'react';
import { useSelector } from 'react-redux';
import Notification from 'bloko/blocks/notificationManager/Notification';
import { deleteNotificationAction } from 'store/models/notifications';
import notificationTypes from 'constants/notifications';

const Notifications = () => {
    const notifications = useSelector((state) => state.notifications);

    return notifications.map(({ type, id, props, params }) => {
        if (!notificationTypes[type]) {
            return null;
        }

        const { Element, onClose, ...notificationProps } = notificationTypes[type];
        const removeNotification = deleteNotificationAction.bind(null, id);
        const bindedOnClose = onClose?.bind(null, removeNotification, props);
        const onCloseCallback = (...args) => {
            bindedOnClose?.(...args);
            removeNotification(...args);
        };

        return (
            <Notification {...notificationProps} key={id} onClose={onCloseCallback}>
                <Element {...props} removeNotification={removeNotification} params={params} />
            </Notification>
        );
    });
};

export default Notifications;
