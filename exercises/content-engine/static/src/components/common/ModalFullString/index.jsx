import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import Modal, { ModalHeader, ModalTitle, ModalContent } from 'bloko/blocks/modal';
import useOnOffState from 'hooks/useOnOffState';
import 'components/common/ModalFullString/ModalFullString.less';

const ModalFullString = ({ title, string, length }) => {
    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    return (
        <Fragment>
            {string?.length > length ? (
                <LinkSwitch kind={LinkSwitch.kinds.inherited} onClick={setVisible}>
                    {`${string.slice(0, length)}...`}
                </LinkSwitch>
            ) : (
                string
            )}
            <Modal visible={isVisible} onClose={setHidden}>
                <ModalHeader>
                    <ModalTitle>{title}</ModalTitle>
                </ModalHeader>
                <ModalContent>
                    <div className="modal-full-string">{string}</div>
                </ModalContent>
            </Modal>
        </Fragment>
    );
};

ModalFullString.defaultProps = {
    length: 10,
};

ModalFullString.propTypes = {
    title: PropTypes.string,
    string: PropTypes.string,
    length: PropTypes.number,
};

export default ModalFullString;
