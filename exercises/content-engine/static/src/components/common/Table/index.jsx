import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import 'components/common/Table/Table.less';

const Table = ({ children, fullWidth, withStripes, bordered, ...props }) => (
    <div className="table-container">
        <table
            className={cn(
                'table',
                { 'table_full-width': fullWidth },
                { 'table_with-stripes': withStripes },
                { [`table_bordered`]: bordered }
            )}
            {...props}
        >
            {children}
        </table>
    </div>
);

Table.propTypes = {
    children: PropTypes.node,
    fullWidth: PropTypes.bool,
    withStripes: PropTypes.bool,
    bordered: PropTypes.bool,
};

Table.defaultProps = {
    fullWidth: false,
    withStripes: false,
    bordered: false,
};

export default Table;
