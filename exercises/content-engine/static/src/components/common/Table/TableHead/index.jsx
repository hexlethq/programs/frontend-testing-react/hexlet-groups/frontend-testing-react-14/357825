import React from 'react';
import PropTypes from 'prop-types';
import 'components/common/Table/TableHead/TableHead.less';

const TableHead = ({ children }) => <thead className="table-head">{children}</thead>;

TableHead.propTypes = {
    children: PropTypes.node,
};

export default TableHead;
