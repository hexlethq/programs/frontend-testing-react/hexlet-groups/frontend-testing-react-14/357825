import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import 'components/common/Table/TableHeadCell/TableHeadCell.less';

const TableHeadCell = ({ children, lines = [], position, className, ...props }) => (
    <th
        className={cn(
            'table-head-cell',
            className,
            lines?.length > 0 && lines.map((el) => `table-head-cell_line-${el}`),
            position && `table-head-cell_text-${position}`
        )}
        {...props}
    >
        {children}
    </th>
);

TableHeadCell.positions = {
    left: 'left',
    center: 'center',
    right: 'right',
};

TableHeadCell.lines = {
    left: 'left',
    right: 'right',
    top: 'top',
};

TableHeadCell.defaultProps = {
    position: TableHeadCell.positions.left,
};

TableHeadCell.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    lines: PropTypes.array,
    position: PropTypes.string,
};

export default TableHeadCell;
