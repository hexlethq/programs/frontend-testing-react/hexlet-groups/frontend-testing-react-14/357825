import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import 'components/common/Table/TableRow/TableRow.less';

const TableRow = ({ children, kind, ...props }) => (
    <tr className={cn('table-row', kind && `table-row_kind-${kind}`)} {...props}>
        {children}
    </tr>
);

TableRow.propTypes = {
    children: PropTypes.node,
};

TableRow.kinds = {
    default: 'default',
    primary: 'primary',
    impact: 'impact',
};

TableRow.defaultProps = {
    kind: TableRow.kinds.default,
};

TableRow.propTypes = {
    kind: PropTypes.oneOf(Object.values(TableRow.kinds)),
};

export default TableRow;
