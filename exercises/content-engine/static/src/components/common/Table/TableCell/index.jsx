import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import 'components/common/Table/TableCell/TableCell.less';

const TableCell = ({ children, lines = [], position, ...props }) => (
    <td
        className={cn(
            'table-cell',
            lines?.length > 0 && lines.map((el) => `table-cell_line-${el}`),
            position && `table-cell_text-${position}`
        )}
        {...props}
    >
        {children}
    </td>
);

TableCell.positions = {
    left: 'left',
    center: 'center',
    right: 'right',
};

TableCell.lines = {
    left: 'left',
    right: 'right',
    bottom: 'bottom',
    top: 'top',
};

TableCell.defaultProps = {
    position: TableCell.positions.left,
};

TableCell.propTypes = {
    children: PropTypes.node,
    lines: PropTypes.array,
    position: PropTypes.oneOf(Object.values(TableCell.positions)),
};

export default TableCell;
