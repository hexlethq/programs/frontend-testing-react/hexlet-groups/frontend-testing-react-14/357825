import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import Icon from 'bloko/blocks/icon';
import { SORT_ASCENDING } from 'constants/common';

const SortLinkHeader = ({ onClick, text, sortType, isShowArrow }) => (
    <Fragment>
        <LinkSwitch kind={LinkSwitch.kinds.inherited} onClick={onClick}>
            {text}
        </LinkSwitch>
        {isShowArrow && (
            <Fragment>
                {sortType === SORT_ASCENDING ? (
                    <Icon view={Icon.views.chevronDown} />
                ) : (
                    <Icon view={Icon.views.chevronUp} />
                )}
            </Fragment>
        )}
    </Fragment>
);

SortLinkHeader.propTypes = {
    onClick: PropTypes.func,
    text: PropTypes.string,
    sortType: PropTypes.string,
    isShowArrow: PropTypes.bool,
};

export default SortLinkHeader;
