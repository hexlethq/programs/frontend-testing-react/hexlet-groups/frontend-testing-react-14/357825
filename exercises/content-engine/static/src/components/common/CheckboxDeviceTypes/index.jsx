import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { FormItem } from 'bloko/blocks/form';
import Checkbox from 'bloko/blocks/checkbox';
import trl from 'modules/translation';
import { addOrDeleteInArray } from 'modules/common';
import { CAMPAIGN_DEVICE_TYPES } from 'constants/campaign';

const CheckboxDeviceTypes = ({ checkedDeviceTypes, setCheckedDeviceTypes }) => {
    const setType = (e) => {
        const result = addOrDeleteInArray(e, checkedDeviceTypes);
        setCheckedDeviceTypes(result);
    };

    return (
        <Fragment>
            {CAMPAIGN_DEVICE_TYPES.map((device) => (
                <FormItem key={device} baseline={true}>
                    <Checkbox
                        value={device}
                        checked={checkedDeviceTypes.includes(device)}
                        onChange={setType}
                        data-qa={`checkbox-${device.toLowerCase()}`}
                    >
                        {trl(`deviceType.${device}.checkbox`)}
                    </Checkbox>
                </FormItem>
            ))}
        </Fragment>
    );
};

CheckboxDeviceTypes.propTypes = {
    checkedDeviceTypes: PropTypes.array,
    setCheckedDeviceTypes: PropTypes.func,
};

export default CheckboxDeviceTypes;
