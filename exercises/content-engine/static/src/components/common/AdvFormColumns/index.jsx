import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup } from 'bloko/blocks/form';
import Column from 'bloko/blocks/column';

const AdvFormColumns = ({ legend, group }) => (
    <FormGroup>
        <Column xs="4" s="6" m="10" l="12" container>
            <Column xs="4" s="6" m="3" l="3" container>
                {legend}
            </Column>
            <Column xs="4" s="6" m="7" l="9" container>
                {group}
            </Column>
        </Column>
    </FormGroup>
);

AdvFormColumns.propTypes = {
    legend: PropTypes.node,
    group: PropTypes.node,
};

export default AdvFormColumns;
