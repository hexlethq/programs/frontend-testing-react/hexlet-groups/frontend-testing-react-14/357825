import React from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';
import Tabs, { Tab } from 'bloko/blocks/tabs';
import trl from 'modules/translation';
import { ANYTHING_OF_THE_DAY, STATISTICS_BY_DATE, STATISTICS_BY_REGION } from 'constants/campaign';

const STATISTICS_TABS = [STATISTICS_BY_DATE, STATISTICS_BY_REGION];

const StatisticsTabs = ({ adminType }) => {
    const dispatch = useDispatch();

    const { campaignId, type } = useParams();

    return (
        <Tabs onChange={(item) => dispatch(push(`/${adminType}/statistics/${campaignId}/${item}`))}>
            {STATISTICS_TABS.map((tab) => {
                return (
                    <Tab key={tab} active={type === tab} id={tab}>
                        {trl(`statistics.${tab}.tab.text`)}
                    </Tab>
                );
            })}
        </Tabs>
    );
};

StatisticsTabs.propTypes = {
    adminType: PropTypes.oneOf(ANYTHING_OF_THE_DAY),
};

export default StatisticsTabs;
