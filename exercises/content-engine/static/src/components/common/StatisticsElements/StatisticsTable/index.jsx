import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { parseISO } from 'date-fns';
import Gap from 'bloko/blocks/gap';
import Header from 'bloko/blocks/header';
import trl from 'modules/translation';
import format from 'modules/date-format';
import { sortingByDate, sortingByNumber, sortingByString, updateSortInfo } from 'modules/sorting';
import Loader from 'components/common/Loader';
import Table from 'components/common/Table';
import TableHead from 'components/common/Table/TableHead';
import TableRow from 'components/common/Table/TableRow';
import TableHeadCell from 'components/common/Table/TableHeadCell';
import TableBody from 'components/common/Table/TableBody';
import SortLinkHeader from 'components/common/SortLinkHeader';
import StatisticsRow from 'components/common/StatisticsElements/StatisticsTable/StatisticsRow';
import { VIEW_FORMAT } from 'constants/date-formats';
import { STATISTICS_TABLE_FIRST_HEADERS, STATISTICS_BY_DATE, STATISTICS_BY_REGION } from 'constants/campaign';
import 'components/common/StatisticsElements/StatisticsTable/StatisticsTable.less';

const StatisticsTable = ({
    statistics,
    totalStatistics,
    statisticsType,
    statisticsSortInfo,
    secondHeaders,
    setStatistics,
    setStatisticsSortInfo,
}) => {
    const setSortInfo = (info) => {
        setStatisticsSortInfo(info);
    };

    const sortStatisticsByData = (firstHead, secondHead) => {
        statistics.data.sort((firstRow, secondRow) =>
            sortingByNumber(
                firstRow.statisticRow[firstHead][secondHead],
                secondRow.statisticRow[firstHead][secondHead],
                statisticsSortInfo.type
            )
        );
        setStatistics({ data: [...statistics.data], loading: false });
        updateSortInfo(`${firstHead}_${secondHead}`, statisticsSortInfo, setSortInfo);
    };

    const sortStatisticsByDateOrRegion = () => {
        switch (statisticsType) {
            case STATISTICS_BY_DATE:
                statistics.data.sort((firstRow, secondRow) =>
                    sortingByDate(parseISO(firstRow.date), parseISO(secondRow.date), statisticsSortInfo.type)
                );
                break;
            case STATISTICS_BY_REGION:
                statistics.data.sort((firstRow, secondRow) =>
                    sortingByString(firstRow.regionName, secondRow.regionName, statisticsSortInfo.type)
                );
                break;
        }
        setStatistics({ data: [...statistics.data], loading: false });
        updateSortInfo(statisticsType, statisticsSortInfo, setSortInfo);
    };

    return (
        <Gap top>
            {statistics.loading ? (
                <Loader />
            ) : (
                <Fragment>
                    {statistics.data.length > 0 ? (
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableHeadCell />
                                    {STATISTICS_TABLE_FIRST_HEADERS.map((head) => (
                                        <TableHeadCell key={head} colSpan={secondHeaders.length}>
                                            <div className="statistics-head-cell">
                                                {trl(`statistics.${head}.table.head`)}
                                            </div>
                                        </TableHeadCell>
                                    ))}
                                </TableRow>
                                <TableRow>
                                    <TableHeadCell>
                                        <SortLinkHeader
                                            onClick={sortStatisticsByDateOrRegion}
                                            text={trl(`statistics.${statisticsType}.table.head`)}
                                            sortType={statisticsSortInfo.type}
                                            isShowArrow={statisticsSortInfo.head === statisticsType}
                                        />
                                    </TableHeadCell>
                                    {STATISTICS_TABLE_FIRST_HEADERS.map((firstHead) =>
                                        secondHeaders.map((secondHead, index) => (
                                            <TableHeadCell
                                                key={`${firstHead}_${secondHead}`}
                                                lines={index === 0 ? [TableHeadCell.lines.left] : []}
                                            >
                                                <SortLinkHeader
                                                    onClick={() => sortStatisticsByData(firstHead, secondHead)}
                                                    text={trl(`statistics.${secondHead}.table.head`)}
                                                    sortType={statisticsSortInfo.type}
                                                    isShowArrow={
                                                        statisticsSortInfo.head === `${firstHead}_${secondHead}`
                                                    }
                                                />
                                            </TableHeadCell>
                                        ))
                                    )}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {statistics.data.map((row) => {
                                    const type =
                                        statisticsType === STATISTICS_BY_DATE
                                            ? format(parseISO(row.date), VIEW_FORMAT)
                                            : row.regionName;
                                    return (
                                        <StatisticsRow
                                            key={type}
                                            rowHeader={type}
                                            rowData={row.statisticRow}
                                            secondHeaders={secondHeaders}
                                        />
                                    );
                                })}
                                <StatisticsRow
                                    rowHeader={trl('statistics.total.table.row')}
                                    rowData={totalStatistics}
                                    highlight
                                    secondHeaders={secondHeaders}
                                />
                            </TableBody>
                        </Table>
                    ) : (
                        <Gap top>
                            <Header level={3}>{trl('statistics.empty.text')}</Header>
                        </Gap>
                    )}
                </Fragment>
            )}
        </Gap>
    );
};

StatisticsTable.propTypes = {
    statistics: PropTypes.object,
    totalStatistics: PropTypes.object,
    statisticsSortInfo: PropTypes.object,
    statisticsType: PropTypes.string,
    secondHeaders: PropTypes.array,
    setStatistics: PropTypes.func,
    setStatisticsSortInfo: PropTypes.func,
};

export default StatisticsTable;
