import React from 'react';
import PropTypes from 'prop-types';
import { TextTertiary } from 'bloko/blocks/text';
import TableRow from 'components/common/Table/TableRow';
import TableCell from 'components/common/Table/TableCell';
import { STATISTICS_TABLE_FIRST_HEADERS, STATISTICS_CTR, STATISTICS_TOTAL } from 'constants/campaign';

const StatisticsRow = ({ rowHeader, rowData, highlight = false, secondHeaders }) => (
    <TableRow kind={highlight ? TableRow.kinds.primary : TableRow.kinds.default}>
        <TableCell lines={[TableCell.lines.bottom]}>{rowHeader}</TableCell>
        {STATISTICS_TABLE_FIRST_HEADERS.map((firstHead) =>
            secondHeaders.map((secondHead, index) => (
                <TableCell
                    position={TableCell.positions.center}
                    key={`${firstHead}_${secondHead}`}
                    lines={index === 0 ? [TableCell.lines.left, TableCell.lines.bottom] : [TableCell.lines.bottom]}
                >
                    {rowData[firstHead][secondHead]}
                    {firstHead === STATISTICS_CTR && '%'}
                    {rowData[firstHead].percent?.[secondHead] > 0 && secondHead !== STATISTICS_TOTAL && (
                        <TextTertiary>{` (${rowData[firstHead].percent[secondHead]}%)`}</TextTertiary>
                    )}
                </TableCell>
            ))
        )}
    </TableRow>
);

StatisticsRow.propTypes = {
    rowHeader: PropTypes.string,
    rowData: PropTypes.object,
    highlight: PropTypes.bool,
    secondHeaders: PropTypes.array,
};

export default StatisticsRow;
