import React from 'react';
import PropTypes from 'prop-types';
import Gap from 'bloko/blocks/gap';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import Button from 'bloko/blocks/button';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import InputWithIntervalCalendar from 'components/common/InputWithIntervalCalendar';

const Filter = ({ period, setPeriod, fetchStatistics }) => {
    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl('campaigns.filter.period.label')}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <InputWithIntervalCalendar
                        action={(period) => setPeriod(period)}
                        from={period.start}
                        to={period.end}
                    />
                    <Gap left Element="span">
                        <Button kind={Button.kinds.primaryMinor} onClick={fetchStatistics}>
                            {trl('filter.button')}
                        </Button>
                    </Gap>
                </FormRow>
            }
        />
    );
};

Filter.propTypes = {
    period: PropTypes.shape({
        start: PropTypes.instanceOf(Date),
        end: PropTypes.instanceOf(Date),
    }),
    setPeriod: PropTypes.func,
    fetchStatistics: PropTypes.func,
};

export default Filter;
