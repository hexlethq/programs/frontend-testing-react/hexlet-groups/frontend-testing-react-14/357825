import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend } from 'recharts';
import Gap from 'bloko/blocks/gap';
import Checkbox from 'bloko/blocks/checkbox';
import { FormItem } from 'bloko/blocks/form';
import Column from 'bloko/blocks/column';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import useToggleState from 'hooks/useToggleState';
import trl from 'modules/translation';
import { addOrDeleteInArray, capitalize } from 'modules/common';
import { STATISTICS_TABLE_FIRST_HEADERS } from 'constants/campaign';

const COLOR_LINES = {
    viewsDesktop: '#FF8118',
    viewsMobile: '#894310',
    viewsTotal: '#392313',
    clicksDesktop: '#3F51F8',
    clicksMobile: '#0C21E4',
    clicksTotal: '#191339',
    ctrDesktop: '#6FF53D',
    ctrMobile: '#37831B',
    ctrTotal: '#244319',
};

const Chart = ({ chartData, secondHeaders }) => {
    const [isShowChart, setShowChart] = useToggleState(false);
    const [isShowData, setShowData] = useState([]);

    const setData = (e) => {
        const result = addOrDeleteInArray(e, isShowData);
        setShowData(result);
    };

    if (chartData?.length === 0) {
        return null;
    }

    return (
        <div>
            <Gap top>
                <LinkSwitch onClick={setShowChart}>
                    {isShowChart ? trl('statistics.chart.close.link') : trl('statistics.chart.show.link')}
                </LinkSwitch>
            </Gap>
            {isShowChart && (
                <Fragment>
                    <Gap top bottom>
                        <LineChart width={900} height={400} data={chartData}>
                            <CartesianGrid strokeDasharray="3 3" />
                            <XAxis dataKey="name" />
                            <YAxis />
                            <Tooltip />
                            <Legend />
                            {isShowData.map((item) => (
                                <Line key={item} type="monotone" dataKey={item} stroke={COLOR_LINES[item]} />
                            ))}
                        </LineChart>
                    </Gap>
                    <Gap bottom>
                        <Column xs="4" s="8" m="12" l="12" container>
                            {STATISTICS_TABLE_FIRST_HEADERS.map((firstHead) => (
                                <Column key={firstHead} xs="4" s="8" m="4" l="4">
                                    {secondHeaders.map((secondHead) => (
                                        <FormItem key={`${firstHead}${capitalize(secondHead)}`} baseline={true}>
                                            <Checkbox
                                                value={`${firstHead}${capitalize(secondHead)}`}
                                                checked={isShowData.includes(`${firstHead}${capitalize(secondHead)}`)}
                                                onChange={setData}
                                            >
                                                {`${trl(`statistics.${firstHead}.table.head`)} (${trl(
                                                    `statistics.${secondHead}.table.head`
                                                )})`}
                                            </Checkbox>
                                        </FormItem>
                                    ))}
                                </Column>
                            ))}
                        </Column>
                    </Gap>
                </Fragment>
            )}
        </div>
    );
};

Chart.propTypes = {
    chartData: PropTypes.array,
    secondHeaders: PropTypes.array,
};

export default Chart;
