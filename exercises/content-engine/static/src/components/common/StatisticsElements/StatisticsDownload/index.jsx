import React from 'react';
import PropTypes from 'prop-types';
import Icon, { IconLink } from 'bloko/blocks/icon';
import Gap from 'bloko/blocks/gap';
import urlParser from 'bloko/common/urlParser';
import format from 'modules/date-format';
import trl from 'modules/translation';
import ElementWithHint from 'components/common/ElementWithHint';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import {
    ANYTHING_OF_THE_DAY,
    ANYTHING_OF_THE_DAY_ID,
    URL_VACANCY_OF_THE_DAY,
    VACANCY_OF_THE_DAY,
} from 'constants/campaign';

const StatisticsDownload = ({ campaignId, statisticsType, period, adminType }) => {
    const params = {
        fromDate: format(period.start, CALENDAR_FORMAT),
        toDate: format(period.end, CALENDAR_FORMAT),
        [ANYTHING_OF_THE_DAY_ID[adminType]]: campaignId,
    };

    const queryStr = urlParser.stringify(params);

    let url = '/api/v1/';

    if (adminType === VACANCY_OF_THE_DAY) {
        url += `${URL_VACANCY_OF_THE_DAY}/`;
    }

    return (
        <Gap top>
            <IconLink Element="a" href={`${url}statistic/by_${statisticsType}/xls?${queryStr}`}>
                <ElementWithHint
                    element={<Icon view={Icon.views.download} size={32} initial={Icon.kinds.primary} />}
                    hint={trl('statistics.download.hint')}
                />
            </IconLink>
        </Gap>
    );
};

StatisticsDownload.propTypes = {
    campaignId: PropTypes.string,
    statisticsType: PropTypes.string,
    period: PropTypes.shape({
        start: PropTypes.instanceOf(Date),
        end: PropTypes.instanceOf(Date),
    }),
    adminType: PropTypes.oneOf(ANYTHING_OF_THE_DAY),
};

export default StatisticsDownload;
