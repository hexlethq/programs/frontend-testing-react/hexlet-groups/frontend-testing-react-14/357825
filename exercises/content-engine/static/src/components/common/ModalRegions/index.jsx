import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Modal, { ModalHeader, ModalTitle, ModalContent } from 'bloko/blocks/modal';
import Column from 'bloko/blocks/column';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import { sortingByString } from 'modules/sorting';
import { SORT_ASCENDING } from 'constants/common';
import { CAMPAIGNS_LIST_REGIONS } from 'constants/campaign';

const ModalRegions = ({ regions }) => {
    const allRegionsDict = useSelector((state) => state.regions.allRegionsDict);

    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    const sortRegions = regions
        .map((regionId) => allRegionsDict[regionId])
        .sort((regionFirst, regionSecond) => sortingByString(regionFirst, regionSecond, SORT_ASCENDING));

    if (Object.keys(allRegionsDict).length === 0) {
        return null;
    }

    return (
        <Fragment>
            {regions?.length === 1 ? (
                allRegionsDict[regions[0]]
            ) : (
                <LinkSwitch onClick={setVisible}>{regions?.length}</LinkSwitch>
            )}
            <Modal visible={isVisible} onClose={setHidden}>
                <ModalHeader>
                    <ModalTitle>{trl(`campaigns.${CAMPAIGNS_LIST_REGIONS}.table.head`)}</ModalTitle>
                </ModalHeader>
                <ModalContent>
                    <Column xs="4" s="4" m="4" l="4" container>
                        {sortRegions.map((region) => (
                            <p key={region}>{region}</p>
                        ))}
                    </Column>
                </ModalContent>
            </Modal>
        </Fragment>
    );
};

ModalRegions.propTypes = {
    regions: PropTypes.array,
};

export default ModalRegions;
