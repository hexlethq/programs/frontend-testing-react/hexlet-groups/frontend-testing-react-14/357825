import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import { Switch, Route } from 'react-router-dom';
import NotificationManager from 'bloko/blocks/notificationManager';
import TipProvider from 'bloko/blocks/drop/Tip/Context';
import routes, { systemRoutes, NotFoundRoute } from 'routes';
import { history } from 'store';
import fetchUser from 'modules/fetchUser';
import fetchRegionsDict from 'modules/fetchRegionsDict';
import EmptyPage from 'pages/System/EmptyPage';
import Header from 'components/Header';
import Notifications from 'components/common/Notifications';
import 'bloko/common/styles/_defaults.less';

const MainComponent = () => {
    const location = useSelector((state) => state.router.location);
    const { isLoad, userId } = useSelector((state) => state.user);

    const dispatch = useDispatch();

    const getRoute = (route) => {
        const { component, path, exact } = route;
        return <Route key={path} path={path} component={component} exact={exact} />;
    };

    useEffect(() => {
        dispatch(fetchUser());
        dispatch(fetchRegionsDict());
    }, [dispatch]);

    return (
        <NotificationManager>
            <TipProvider>
                <ConnectedRouter history={history}>
                    <Header />
                    {isLoad ? (
                        <EmptyPage />
                    ) : (
                        <div className="content">
                            {userId ? (
                                <Switch location={location}>
                                    {routes.map((route) => getRoute(route))}
                                    <Route component={NotFoundRoute.component} />;
                                </Switch>
                            ) : (
                                <Switch location={location}>{systemRoutes.map((route) => getRoute(route))}</Switch>
                            )}
                            <Notifications />
                        </div>
                    )}
                </ConnectedRouter>
            </TipProvider>
        </NotificationManager>
    );
};

export default MainComponent;
