import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Modal, { ModalHeader, ModalTitle, ModalContent, ModalFooter } from 'bloko/blocks/modal';
import Column from 'bloko/blocks/column';
import Textarea from 'bloko/blocks/textarea';
import Button from 'bloko/blocks/button';
import trl from 'modules/translation';
import moderation from 'components/VacancyOfTheDay/moderation';
import { resetModerationData, setModerationRejectReason } from 'store/models/vacancyOfTheDay/campaignsModeration';
import { CAMPAIGN_ACTION_REJECT } from 'constants/campaign';

const ModalRejectReason = ({ isVisible, setHidden, fromCampaignInfo }) => {
    const { campaignIds, rejectReason } = useSelector((state) => state.campaignsModerationVOTD);
    const dispatch = useDispatch();

    return (
        <Modal
            visible={isVisible}
            onClose={() => {
                setHidden();
                dispatch(resetModerationData());
            }}
        >
            <ModalHeader>
                <ModalTitle>
                    {campaignIds.length > 1
                        ? trl('campaign.reject.reason.multi')
                        : trl('campaign.reject.reason.single', campaignIds)}
                </ModalTitle>
            </ModalHeader>
            <ModalContent>
                <Column xs="4" s="6" m="6" l="6" container>
                    <Textarea
                        placeholder={trl('input.text.placeholder')}
                        value={rejectReason}
                        onChange={(event) => dispatch(setModerationRejectReason(event.target.value))}
                        rows={5}
                    />
                </Column>
            </ModalContent>
            <ModalFooter>
                <Button
                    kind={Button.kinds.primaryMinor}
                    onClick={() => {
                        setHidden();
                        dispatch(moderation(CAMPAIGN_ACTION_REJECT, fromCampaignInfo));
                    }}
                >
                    {trl('campaign.moderation.reject')}
                </Button>
            </ModalFooter>
        </Modal>
    );
};

ModalRejectReason.propTypes = {
    isVisible: PropTypes.bool.isRequired,
    setHidden: PropTypes.func.isRequired,
    fromCampaignInfo: PropTypes.bool,
};

export default ModalRejectReason;
