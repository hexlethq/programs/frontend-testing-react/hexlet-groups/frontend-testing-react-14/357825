import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import Button from 'bloko/blocks/button';
import Header from 'bloko/blocks/header';
import Gap from 'bloko/blocks/gap';
import { FormSpacer } from 'bloko/blocks/form';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import Filter from 'components/VacancyOfTheDay/Filter';
import StatisticsTable from 'components/VacancyOfTheDay/CommonStatistics/StatisticsTable';
import fetchCommonStatistics from 'components/VacancyOfTheDay/CommonStatistics/fetchCommonStatistics';
import fetchPackets from 'components/VacancyOfTheDay/fetchPackets';
import {
    resetCommonStatisticsFilter,
    setCommonStatisticsFilterField,
} from 'store/models/vacancyOfTheDay/commonStatistics';
import { VOTD_COMMON_STATISTICS_FILTER_FIELDS_CONFIG } from 'constants/campaign';

const CommonStatistics = () => {
    const { filter, filterCompleted } = useSelector((state) => state.commonStatisticsVOTD);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(resetCommonStatisticsFilter());
        dispatch(fetchCommonStatistics());
        dispatch(fetchPackets());
    }, [dispatch]);

    return (
        <Fragment>
            <ShiftWrapper>
                <Button
                    kind={Button.kinds.primary}
                    onClick={() => dispatch(push('/vacancy-of-the-day/aggregated-statistics/'))}
                >
                    {trl('commonStatistics.aggregatedStatistics.button')}
                </Button>
                <FormSpacer>
                    <Button
                        kind={Button.kinds.primary}
                        onClick={() => dispatch(push('/vacancy-of-the-day/campaigns/'))}
                    >
                        {trl('toCampaigns.return.button')}
                    </Button>
                </FormSpacer>
            </ShiftWrapper>
            <Header level={2}>{trl('commonStatistics.header')}</Header>
            <Gap top>
                <Filter
                    filter={filter}
                    filterFieldsConfig={VOTD_COMMON_STATISTICS_FILTER_FIELDS_CONFIG}
                    setFilterField={(field, value) => dispatch(setCommonStatisticsFilterField(field, value))}
                    onClick={() => dispatch(fetchCommonStatistics())}
                    filterCompleted={filterCompleted}
                    resetFilter={() => {
                        dispatch(resetCommonStatisticsFilter());
                        dispatch(fetchCommonStatistics());
                    }}
                />
            </Gap>
            <StatisticsTable />
        </Fragment>
    );
};

export default CommonStatistics;
