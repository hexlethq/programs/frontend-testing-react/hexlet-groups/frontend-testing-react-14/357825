import { STATISTICS_CLICKS, STATISTICS_VIEWS, STATISTICS_CTR } from 'constants/campaign';
import { sortingByNumber } from 'modules/sorting';
import SORT_CONFIG from 'components/VacancyOfTheDay/Campaigns/CampaignsList/sortConfig';

export default {
    ...SORT_CONFIG,
    [STATISTICS_VIEWS]: (statisticsFirst, statisticsSecond, sortType) =>
        sortingByNumber(statisticsFirst.views, statisticsSecond.views, sortType),
    [STATISTICS_CLICKS]: (statisticsFirst, statisticsSecond, sortType) =>
        sortingByNumber(statisticsFirst.clicks, statisticsSecond.clicks, sortType),
    [STATISTICS_CTR]: (statisticsFirst, statisticsSecond, sortType) =>
        sortingByNumber(statisticsFirst.ctr, statisticsSecond.ctr, sortType),
};
