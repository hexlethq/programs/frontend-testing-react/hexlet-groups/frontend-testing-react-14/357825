import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { parseISO } from 'date-fns';
import Header from 'bloko/blocks/header';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import format from 'modules/date-format';
import { updateSortInfo } from 'modules/sorting';
import Loader from 'components/common/Loader';
import Table from 'components/common/Table';
import TableHead from 'components/common/Table/TableHead';
import TableRow from 'components/common/Table/TableRow';
import TableHeadCell from 'components/common/Table/TableHeadCell';
import TableCell from 'components/common/Table/TableCell';
import TableBody from 'components/common/Table/TableBody';
import SortLinkHeader from 'components/common/SortLinkHeader';
import ModalRegions from 'components/common/ModalRegions';
import EntityInfo from 'components/VacancyOfTheDay/EntityInfo';
import SORT_CONFIG from 'components/VacancyOfTheDay/CommonStatistics/StatisticsTable/sortConfig';
import { setCommonStatistics, setCommonStatisticsSortInfo } from 'store/models/vacancyOfTheDay/commonStatistics';
import { VIEW_FORMAT } from 'constants/date-formats';
import {
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_VACANCY,
    ENTITY_EMPLOYER,
    ENTITY_VACANCY,
    STATISTICS_BY_DATE,
    STATISTICS_CLICKS,
    STATISTICS_CTR,
    STATISTICS_VIEWS,
    VOTD_COMMON_STATISTICS_HEADERS,
} from 'constants/campaign';

const EMPLOYER_NAME_VIEW_LENGTH = 9;
const VACANCY_NAME_VIEW_LENGTH = 10;

const StatisticsTable = () => {
    const { statistics, statisticsSortInfo } = useSelector((state) => state.commonStatisticsVOTD);
    const { allRegionsDict, allPacketsDict } = useSelector((state) => state.regions);

    const dispatch = useDispatch();

    const sortStatistics = (head) => {
        const newStatistics = [...statistics.data];
        newStatistics.sort((statisticsFirst, statisticsSecond) =>
            SORT_CONFIG[head](statisticsFirst, statisticsSecond, statisticsSortInfo.type, {
                allRegionsDict,
                allPacketsDict,
            })
        );
        dispatch(setCommonStatistics({ data: newStatistics, loading: false }));
        updateSortInfo(head, statisticsSortInfo, (info) => dispatch(setCommonStatisticsSortInfo(info)));
    };

    return (
        <Gap top>
            {statistics.loading ? (
                <Loader />
            ) : (
                <Fragment>
                    {statistics.data.length > 0 ? (
                        <Table bordered fullWidth>
                            <TableHead>
                                <TableRow>
                                    {VOTD_COMMON_STATISTICS_HEADERS.map((head) => (
                                        <TableHeadCell position={TableHeadCell.positions.center} key={head}>
                                            <SortLinkHeader
                                                onClick={() => sortStatistics(head)}
                                                field={head}
                                                text={trl(`campaigns.${head}.table.head`)}
                                                sortType={statisticsSortInfo.type}
                                                isShowArrow={statisticsSortInfo.head === head}
                                            />
                                        </TableHeadCell>
                                    ))}
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {statistics.data.map(
                                    ({
                                        vacancyOfTheDayId,
                                        employer,
                                        vacancy,
                                        dateFrom,
                                        dateTo,
                                        packet,
                                        deviceTypes,
                                        regionIds,
                                        creator,
                                        campaignType,
                                        views,
                                        clicks,
                                        ctr,
                                    }) => {
                                        const TABLE_CELL_CONTENT = {
                                            [CAMPAIGNS_LIST_ID]: (
                                                <Link
                                                    to={`/vacancy-of-the-day/statistics/${vacancyOfTheDayId}/${STATISTICS_BY_DATE}`}
                                                >
                                                    {vacancyOfTheDayId}
                                                </Link>
                                            ),
                                            [CAMPAIGNS_LIST_EMPLOYER]: (
                                                <EntityInfo
                                                    entity={ENTITY_EMPLOYER}
                                                    info={employer}
                                                    length={EMPLOYER_NAME_VIEW_LENGTH}
                                                />
                                            ),
                                            [CAMPAIGNS_LIST_VACANCY]: (
                                                <EntityInfo
                                                    entity={ENTITY_VACANCY}
                                                    info={vacancy}
                                                    length={VACANCY_NAME_VIEW_LENGTH}
                                                />
                                            ),
                                            [CAMPAIGNS_LIST_PERIOD_START]: format(parseISO(dateFrom), VIEW_FORMAT),
                                            [CAMPAIGNS_LIST_PERIOD_END]: format(parseISO(dateTo), VIEW_FORMAT),
                                            [CAMPAIGNS_LIST_PACKET]: allPacketsDict[packet]?.name,
                                            [CAMPAIGNS_LIST_REGIONS]: <ModalRegions regions={regionIds} />,
                                            [CAMPAIGNS_LIST_DEVICE_TYPES]: deviceTypes.join(', '),
                                            [CAMPAIGNS_LIST_MANAGER]: creator.name,
                                            [CAMPAIGNS_LIST_CAMPAIGN_TYPE]: trl(`campaign.type.${campaignType}`),
                                            [STATISTICS_VIEWS]: views,
                                            [STATISTICS_CLICKS]: clicks,
                                            [STATISTICS_CTR]: `${ctr}%`,
                                        };
                                        return (
                                            <TableRow key={vacancyOfTheDayId}>
                                                {VOTD_COMMON_STATISTICS_HEADERS.map((head) => (
                                                    <TableCell position={TableCell.positions.center} key={head}>
                                                        {TABLE_CELL_CONTENT[head]}
                                                    </TableCell>
                                                ))}
                                            </TableRow>
                                        );
                                    }
                                )}
                            </TableBody>
                        </Table>
                    ) : (
                        <Gap top>
                            <Header level={3}>{trl('statistics.empty.text')}</Header>
                        </Gap>
                    )}
                </Fragment>
            )}
        </Gap>
    );
};

export default StatisticsTable;
