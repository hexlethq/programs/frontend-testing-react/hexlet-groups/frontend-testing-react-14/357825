import urlParser from 'bloko/common/urlParser';
import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import { deleteEmptyFields } from 'modules/common';
import { setCommonStatistics, setCommonStatisticsFilterCompleted } from 'store/models/vacancyOfTheDay/commonStatistics';
import { CALENDAR_FORMAT } from 'constants/date-formats';

export default () => {
    return (dispatch, getState) => {
        dispatch(setCommonStatistics({ data: [], loading: true }));
        const filter = getState().commonStatisticsVOTD.filter;
        const { period, ...otherFields } = filter;
        const params = {
            dateFrom: format(period.start, CALENDAR_FORMAT),
            dateTo: format(period.end, CALENDAR_FORMAT),
            ...deleteEmptyFields(otherFields),
        };
        const queryStr = urlParser.stringify(params);
        fetcher
            .get(`/api/v1/vacancy_of_the_day/statistic?${queryStr}`)
            .then((data) => {
                dispatch([setCommonStatistics({ data, loading: false }), setCommonStatisticsFilterCompleted(filter)]);
            })
            .catch(() => {
                dispatch(setCommonStatistics({ data: [], loading: false }));
            });
    };
};
