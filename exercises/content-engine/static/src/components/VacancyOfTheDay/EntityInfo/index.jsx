import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Link from 'bloko/blocks/link';
import { TextEmphasis } from 'bloko/blocks/text';
import trl from 'modules/translation';
import ModalFullString from 'components/common/ModalFullString';

const EntityInfo = ({ entity, info, length }) => (
    <Fragment>
        {info.id !== 0 && (
            <Link href={`https://hh.ru/${entity}/${info.id}`} target="_blank" rel="noopener noreferrer">
                {info.id}
            </Link>
        )}
        {info.name && (
            <TextEmphasis>
                <ModalFullString title={trl(`campaigns.${entity}.name.text`)} string={info.name} length={length} />
            </TextEmphasis>
        )}
    </Fragment>
);

EntityInfo.propTypes = {
    entity: PropTypes.string,
    info: PropTypes.object,
    length: PropTypes.number,
};

export default EntityInfo;
