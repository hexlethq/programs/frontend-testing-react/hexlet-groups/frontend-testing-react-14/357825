import fetcher from 'modules/fetcher';
import { setCampaignInfo } from 'store/models/vacancyOfTheDay/campaignInfo';

export default (vacancyOfTheDayId) => {
    return (dispatch) => {
        fetcher
            .get('/api/v1/vacancy_of_the_day/vacancy_of_the_day_list', {
                params: { vacancyOfTheDayId },
            })
            .then((data) => {
                dispatch(setCampaignInfo(data?.[0]));
            });
    };
};
