import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import fetchCampaignInfo from 'components/VacancyOfTheDay/CampaignInfo/fetchCampaignInfo';
import { PAYMENT_UPDATE_STATUS_SUCCESS } from 'constants/notifications';

export default (campaignId, newPaymentStatus) => {
    return (dispatch) => {
        fetcher
            .put(`/api/v1/vacancy_of_the_day/${campaignId}/payment_status`, null, {
                params: { status: newPaymentStatus },
            })
            .then(() => {
                dispatch(fetchCampaignInfo(campaignId));
                dispatch(addNotification(PAYMENT_UPDATE_STATUS_SUCCESS, [campaignId]));
            });
    };
};
