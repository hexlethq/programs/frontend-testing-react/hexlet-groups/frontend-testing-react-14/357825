import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Modal, { ModalContent, ModalFooter, ModalHeader, ModalTitle } from 'bloko/blocks/modal';
import Column from 'bloko/blocks/column';
import { FormItem, FormItemGroup } from 'bloko/blocks/form';
import Radio from 'bloko/blocks/radio';
import Button from 'bloko/blocks/button';
import trl from 'modules/translation';
import changePaymentStatus from 'components/VacancyOfTheDay/CampaignInfo/TableInfo/changePaymentStatus';
import { CAMPAIGN_PAYMENT_STATUSES } from 'constants/campaign';

const PaymentStatuses = ({ isVisible, setHidden }) => {
    const { vacancyOfTheDayId, paymentStatus } = useSelector((state) => state.campaignInfoVOTD.info);
    const dispatch = useDispatch();

    const [newPaymentStatus, setNewPaymentStatus] = useState(paymentStatus);

    useEffect(() => {
        setNewPaymentStatus(paymentStatus);
    }, [paymentStatus]);

    return (
        <Modal visible={isVisible} onClose={setHidden}>
            <ModalHeader>
                <ModalTitle>{trl('payment.status.choose')}</ModalTitle>
            </ModalHeader>
            <ModalContent>
                <Column xs="4" s="4" m="4" l="4" container>
                    <FormItemGroup>
                        {CAMPAIGN_PAYMENT_STATUSES.map((item) => (
                            <FormItem key={item} baseline>
                                <Radio
                                    name="status"
                                    onChange={(event) => setNewPaymentStatus(event.target.value)}
                                    checked={item === newPaymentStatus}
                                    value={item}
                                >
                                    {trl(`payment.status.${item}`)}
                                </Radio>
                            </FormItem>
                        ))}
                    </FormItemGroup>
                </Column>
            </ModalContent>
            <ModalFooter>
                <Button
                    kind={Button.kinds.primary}
                    onClick={() => {
                        setHidden();
                        if (paymentStatus !== newPaymentStatus) {
                            dispatch(changePaymentStatus(vacancyOfTheDayId, newPaymentStatus));
                        }
                    }}
                >
                    {trl('choose.button')}
                </Button>
            </ModalFooter>
        </Modal>
    );
};

PaymentStatuses.propTypes = {
    isVisible: PropTypes.bool.isRequired,
    setHidden: PropTypes.func.isRequired,
};

export default PaymentStatuses;
