import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { parseISO } from 'date-fns';
import { TextEmphasis, TextTertiary } from 'bloko/blocks/text';
import Link from 'bloko/blocks/link';
import Icon, { IconLink } from 'bloko/blocks/icon';
import { FormSpacer } from 'bloko/blocks/form';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import format from 'modules/date-format';
import BracketsText from 'components/common/BracketsText';
import Table from 'components/common/Table';
import TableBody from 'components/common/Table/TableBody';
import TableRow from 'components/common/Table/TableRow';
import TableCell from 'components/common/Table/TableCell';
import ModalFullString from 'components/common/ModalFullString';
import ModalRegions from 'components/common/ModalRegions';
import ElementWithHint from 'components/common/ElementWithHint';
import PaymentStatuses from 'components/VacancyOfTheDay/CampaignInfo/TableInfo/PaymentStatuses';
import { VIEW_FORMAT } from 'constants/date-formats';
import {
    CAMPAIGNS_LIST_CAMPAIGN_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    CAMPAIGNS_LIST_CRM_URL,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_PAYMENT_STATUS,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_VACANCY,
    VOTD_CAMPAIGN_INFO_ROWS,
} from 'constants/campaign';

const CRM_URL_VIEW_LENGTH = 40;

const TableInfo = () => {
    const { superAdmin, userId } = useSelector((state) => state.user);
    const allPacketsDict = useSelector((state) => state.regions.allPacketsDict);
    const info = useSelector((state) => state.campaignInfoVOTD.info);

    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    if (Object.keys(info).length === 0) {
        return null;
    }

    const {
        vacancyOfTheDayId,
        employer,
        vacancy,
        dateFrom,
        dateTo,
        packet,
        regionList,
        deviceTypes,
        paymentStatus,
        campaignStatus,
        creator,
        campaignType,
        crmUrl,
    } = info;

    const admin = superAdmin || creator.id === userId;

    const TABLE_CELL_CONTENT = {
        [CAMPAIGNS_LIST_EMPLOYER]: (
            <Fragment>
                {employer.name}
                {employer.id !== 0 && (
                    <BracketsText withUsualSpace>
                        <Link href={`https://hh.ru/employer/${employer.id}`} target="_blank" rel="noopener noreferrer">
                            {employer.id}
                        </Link>
                    </BracketsText>
                )}
            </Fragment>
        ),
        [CAMPAIGNS_LIST_VACANCY]: (
            <Fragment>
                {vacancy.name}
                {vacancy.id !== 0 && (
                    <BracketsText withUsualSpace>
                        <Link href={`https://hh.ru/vacancy/${vacancy.id}`} target="_blank" rel="noopener noreferrer">
                            {vacancy.id}
                        </Link>
                    </BracketsText>
                )}
            </Fragment>
        ),
        [CAMPAIGNS_LIST_PERIOD_START]: format(parseISO(dateFrom), VIEW_FORMAT),
        [CAMPAIGNS_LIST_PERIOD_END]: format(parseISO(dateTo), VIEW_FORMAT),
        [CAMPAIGNS_LIST_PACKET]: allPacketsDict[packet]?.name,
        [CAMPAIGNS_LIST_REGIONS]: <ModalRegions regions={regionList} />,
        [CAMPAIGNS_LIST_DEVICE_TYPES]: deviceTypes.join(', '),
        [CAMPAIGNS_LIST_PAYMENT_STATUS]: (
            <Fragment>
                {trl(`payment.status.${paymentStatus}`)}
                {admin && (
                    <FormSpacer>
                        <IconLink onClick={setVisible}>
                            <ElementWithHint
                                element={<Icon view={Icon.views.edit} size={16} initial={Icon.kinds.primary} />}
                                hint={trl('payment.status.change')}
                            />
                        </IconLink>
                    </FormSpacer>
                )}
            </Fragment>
        ),
        [CAMPAIGNS_LIST_CAMPAIGN_STATUS]: trl(`campaign.status.${campaignStatus}`),
        [CAMPAIGNS_LIST_CAMPAIGN_TYPE]: trl(`campaign.type.${campaignType}`),
        [CAMPAIGNS_LIST_CRM_URL]: (
            <ModalFullString
                title={trl(`campaigns.${CAMPAIGNS_LIST_CRM_URL}.table.head`)}
                string={crmUrl}
                length={CRM_URL_VIEW_LENGTH}
            />
        ),
        [CAMPAIGNS_LIST_MANAGER]: (
            <Fragment>
                {creator.name}
                <TextTertiary>
                    <BracketsText>{creator.id}</BracketsText>
                </TextTertiary>
                <Link href={`mailto:${creator.email}`}>{creator.email}</Link>
            </Fragment>
        ),
    };

    return (
        <Fragment>
            <Table fullWidth bordered withStripes>
                <TableBody>
                    {VOTD_CAMPAIGN_INFO_ROWS.map((row) => (
                        <TableRow key={row}>
                            <TableCell>
                                <TextEmphasis>{trl(`campaigns.${row}.table.head`)}</TextEmphasis>
                            </TableCell>
                            <TableCell>{TABLE_CELL_CONTENT[row]}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
            <PaymentStatuses
                isVisible={isVisible}
                setHidden={setHidden}
                currentStatus={paymentStatus}
                campaignId={vacancyOfTheDayId}
            />
        </Fragment>
    );
};

export default TableInfo;
