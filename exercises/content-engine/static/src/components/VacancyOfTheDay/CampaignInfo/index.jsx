import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { Link } from 'react-router-dom';
import { push } from 'connected-react-router';
import Header from 'bloko/blocks/header';
import Button from 'bloko/blocks/button';
import Column, { ColumnsRow } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import ModalWarning from 'components/common/ModalWarning';
import TableInfo from 'components/VacancyOfTheDay/CampaignInfo/TableInfo';
import Screenshots from 'components/VacancyOfTheDay/CampaignInfo/Screenshots';
import ModalRejectReason from 'components/VacancyOfTheDay/ModalRejectReason';
import moderation from 'components/VacancyOfTheDay/moderation';
import fetchPackets from 'components/VacancyOfTheDay/fetchPackets';
import fetchCampaignInfo from 'components/VacancyOfTheDay/CampaignInfo/fetchCampaignInfo';
import changeStatus from 'components/VacancyOfTheDay/CampaignInfo/changeStatus';
import { setModerationCampaignIds, resetModerationData } from 'store/models/vacancyOfTheDay/campaignsModeration';
import {
    CAMPAIGN_STATUS_NEW,
    CAMPAIGN_STATUS_APPROVED,
    CAMPAIGN_STATUS_ACTIVE,
    CAMPAIGN_STATUS_PAUSED,
    CAMPAIGN_STATUS_REJECTED,
    CAMPAIGNS_LIST_COMMENT,
    URL_VACANCY_OF_THE_DAY,
    CAMPAIGN_ACTION_APPROVE,
    CAMPAIGN_STATUS_ON_MODERATION,
    STATISTICS_BY_DATE,
} from 'constants/campaign';

const URL_ACTIVATE = 'activate';
const URL_PAUSE = 'pause';
const URL_TO_MODERATION = 'to_moderation';

const CampaignInfo = () => {
    const { superAdmin, userId } = useSelector((state) => state.user);
    const { campaignStatus, comment, creator } = useSelector((state) => state.campaignInfoVOTD.info);
    const dispatch = useDispatch();

    const [isVisibleWarning, setVisibleWarning, setHiddenWarning] = useOnOffState(false);
    const [isVisibleReason, setVisibleReason, setHiddenReason] = useOnOffState(false);

    const campaignId = useParams().campaignId;

    const admin = superAdmin || creator?.id === userId;

    useEffect(() => {
        dispatch(fetchCampaignInfo(campaignId));
        dispatch(fetchPackets());
        dispatch(resetModerationData());
    }, [campaignId, dispatch]);

    const TO_MODERATION_BUTTON = (
        <Button
            onClick={() => dispatch(changeStatus(campaignId, URL_TO_MODERATION))}
            kind={Button.kinds.primaryDimmedMinor}
            stretched
        >
            {trl('campaign.toModeration.button')}
        </Button>
    );

    const CHANGE_STATUS_BUTTON = {
        [CAMPAIGN_STATUS_PAUSED]: (
            <Button
                onClick={() => dispatch(changeStatus(campaignId, URL_ACTIVATE))}
                kind={Button.kinds.secondaryMinor}
                stretched
            >
                {trl(`campaign.status.${CAMPAIGN_STATUS_ACTIVE}.button`)}
            </Button>
        ),
        [CAMPAIGN_STATUS_APPROVED]: (
            <Button onClick={() => dispatch(changeStatus(campaignId, URL_PAUSE))} kind={Button.kinds.minor} stretched>
                {trl(`campaign.status.${CAMPAIGN_STATUS_PAUSED}.button`)}
            </Button>
        ),
        [CAMPAIGN_STATUS_NEW]: TO_MODERATION_BUTTON,
        [CAMPAIGN_STATUS_REJECTED]: TO_MODERATION_BUTTON,
    };

    return (
        <Fragment>
            <ShiftWrapper>
                <Button kind={Button.kinds.primary} onClick={() => dispatch(push('/vacancy-of-the-day/campaigns/'))}>
                    {trl('toCampaigns.return.button')}
                </Button>
            </ShiftWrapper>
            <Header level={2}>{trl('campaignInfo.header', [campaignId])}</Header>
            {superAdmin && campaignStatus === CAMPAIGN_STATUS_ON_MODERATION && (
                <Gap top>
                    <Gap right Element="span">
                        <Button
                            kind={Button.kinds.secondaryMinor}
                            onClick={() => {
                                dispatch(setModerationCampaignIds([campaignId]));
                                dispatch(moderation(CAMPAIGN_ACTION_APPROVE, true));
                            }}
                        >
                            {trl('campaign.moderation.approve')}
                        </Button>
                    </Gap>
                    <Button
                        kind={Button.kinds.tertiaryMinor}
                        onClick={() => {
                            dispatch(setModerationCampaignIds([campaignId]));
                            setVisibleReason();
                        }}
                    >
                        {trl('campaign.moderation.reject')}
                    </Button>
                </Gap>
            )}
            <Gap top bottom>
                <ColumnsRow>
                    <Column xs="4" s="8" m="6" l="8" container>
                        <TableInfo />
                    </Column>
                    <Column xs="4" s="8" m="6" l="8" container>
                        <Gap lLeft mLeft sTop xsTop>
                            {comment && (
                                <Gap bottom>
                                    <Header level={3}>{trl(`campaigns.${CAMPAIGNS_LIST_COMMENT}.table.head`)}</Header>
                                    <i>{comment}</i>
                                </Gap>
                            )}
                            <Link to={`/vacancy-of-the-day/statistics/${campaignId}/${STATISTICS_BY_DATE}`}>
                                {trl('campaignInfo.statistics.link')}
                            </Link>
                            <Gap top>
                                <Screenshots />
                            </Gap>
                            {admin && (
                                <ShiftWrapper>
                                    <Column xs="4" s="4" m="4" l="4" container>
                                        <Gap top>{CHANGE_STATUS_BUTTON[campaignStatus]}</Gap>
                                        <Gap top>
                                            <Button
                                                kind={Button.kinds.primaryMinor}
                                                onClick={() => dispatch(push(`/vacancy-of-the-day/edit/${campaignId}`))}
                                                stretched
                                            >
                                                {trl('campaign.edit.button')}
                                            </Button>
                                        </Gap>
                                        <Gap top>
                                            <Button
                                                kind={Button.kinds.tertiaryMinor}
                                                onClick={setVisibleWarning}
                                                stretched
                                            >
                                                {trl('campaign.delete.button')}
                                            </Button>
                                        </Gap>
                                    </Column>
                                </ShiftWrapper>
                            )}
                        </Gap>
                    </Column>
                </ColumnsRow>
            </Gap>
            <ModalWarning
                visible={isVisibleWarning}
                setHidden={setHiddenWarning}
                campaignId={campaignId}
                adminUrl={URL_VACANCY_OF_THE_DAY}
                actionAfterDelete={() => dispatch(push('/vacancy-of-the-day/campaigns'))}
            />
            <ModalRejectReason isVisible={isVisibleReason} setHidden={setHiddenReason} fromCampaignInfo={true} />
        </Fragment>
    );
};

export default CampaignInfo;
