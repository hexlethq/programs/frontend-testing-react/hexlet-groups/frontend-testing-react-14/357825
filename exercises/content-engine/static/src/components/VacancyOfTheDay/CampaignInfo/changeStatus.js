import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import fetchCampaignInfo from 'components/VacancyOfTheDay/CampaignInfo/fetchCampaignInfo';
import { CAMPAIGN_UPDATE_STATUS_SUCCESS } from 'constants/notifications';

export default (vacancyOfTheDayId, url) => {
    return (dispatch) => {
        fetcher
            .put(`/api/v1/vacancy_of_the_day/status/${url}`, null, {
                params: { vacancyOfTheDayId },
            })
            .then(() => {
                dispatch(fetchCampaignInfo(vacancyOfTheDayId));
                dispatch(addNotification(CAMPAIGN_UPDATE_STATUS_SUCCESS, [vacancyOfTheDayId]));
            });
    };
};
