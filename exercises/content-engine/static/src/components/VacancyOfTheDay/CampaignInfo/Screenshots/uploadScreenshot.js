import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import fetchScreenshots from 'components/VacancyOfTheDay/CampaignInfo/Screenshots/fetchScreenshots';
import { SCREENSHOT_UPLOAD_SUCCESS } from 'constants/notifications';

export default (file, vacancyOfTheDayId, setLoadingScreenshot, resetLoadingScreenshot) => {
    return (dispatch) => {
        setLoadingScreenshot();
        const config = { headers: { 'Content-Type': 'multipart/form-data' }, params: { vacancyOfTheDayId } };
        const data = new FormData();
        data.append('file', file);
        fetcher
            .put('/api/v1/vacancy_of_the_day/screenshot', data, config)
            .then(() => {
                resetLoadingScreenshot();
                dispatch(fetchScreenshots(vacancyOfTheDayId));
                dispatch(addNotification(SCREENSHOT_UPLOAD_SUCCESS));
            })
            .catch(() => {
                resetLoadingScreenshot();
            });
    };
};
