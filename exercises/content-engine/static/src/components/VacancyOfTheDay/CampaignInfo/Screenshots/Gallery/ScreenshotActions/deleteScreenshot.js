import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import fetchScreenshots from 'components/VacancyOfTheDay/CampaignInfo/Screenshots/fetchScreenshots';
import { SCREENSHOT_DELETE_SUCCESS } from 'constants/notifications';

export default (vacancyOfTheDayId, screenId) => {
    return (dispatch) => {
        fetcher.delete('/api/v1/vacancy_of_the_day/screenshot', { params: { screenId } }).then(() => {
            dispatch(addNotification(SCREENSHOT_DELETE_SUCCESS));
            dispatch(fetchScreenshots(vacancyOfTheDayId));
        });
    };
};
