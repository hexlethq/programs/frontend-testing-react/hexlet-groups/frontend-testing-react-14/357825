import fetcher from 'modules/fetcher';
import { setCampaignScreenshots } from 'store/models/vacancyOfTheDay/campaignInfo';

export default (vacancyOfTheDayId) => {
    return (dispatch) => {
        fetcher.get('/api/v1/vacancy_of_the_day/screenshot', { params: { vacancyOfTheDayId } }).then((data) => {
            dispatch(setCampaignScreenshots(data));
        });
    };
};
