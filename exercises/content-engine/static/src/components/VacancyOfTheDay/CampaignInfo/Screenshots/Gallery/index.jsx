import React, { Fragment, useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import cn from 'classnames';
import Modal, { ModalContent, ModalHeader, ModalTitle } from 'bloko/blocks/modal';
import Column from 'bloko/blocks/column';
import Icon from 'bloko/blocks/icon';
import trl from 'modules/translation';
import ScreenshotActions from 'components/VacancyOfTheDay/CampaignInfo/Screenshots/Gallery/ScreenshotActions';
import 'components/VacancyOfTheDay/CampaignInfo/Screenshots/Gallery/Gallery.less';

const Gallery = ({ isVisible, setHidden }) => {
    const screenshots = useSelector((state) => state.campaignInfoVOTD.screenshots);

    const [activeItem, setActiveItem] = useState(0);

    const dots = screenshots.map((screenshot, index) => index);

    const SCREENSHOTS_LENGTH = screenshots.length;

    useEffect(() => {
        if (isVisible && screenshots.length === 0) {
            setHidden();
        }
    }, [isVisible, screenshots, setHidden]);

    return (
        <Modal visible={isVisible} onClose={setHidden}>
            <ModalHeader>
                <ModalTitle>{trl('campaignInfo.screenshots.title')}</ModalTitle>
            </ModalHeader>
            <ModalContent>
                <Column xs="4" s="6" m="10" l="14" container>
                    <div className="gallery-slider">
                        {screenshots.length > 1 && (
                            <div
                                onClick={() => setActiveItem((activeItem === 0 ? SCREENSHOTS_LENGTH : activeItem) - 1)}
                                className="slider-arrow"
                            >
                                <Icon view={Icon.views.chevronLeft} initial={Icon.kinds.impact} size={32} />
                            </div>
                        )}
                        <div className="gallery-slider-body">
                            <div className="gallery-image-container">
                                {screenshots.map((screenshot, index) => {
                                    const { screenId, src } = screenshot;
                                    return (
                                        <Fragment key={screenId}>
                                            {index === activeItem && (
                                                <Fragment>
                                                    <ScreenshotActions screenId={screenId} src={src} />
                                                    <img className="gallery-image" src={src} alt={screenId} />
                                                </Fragment>
                                            )}
                                        </Fragment>
                                    );
                                })}
                            </div>
                            <div className="slider-dots-container">
                                {dots.map((item) => (
                                    <div
                                        key={item}
                                        className={cn('slider-dot', { 'slider-dot_active': item === activeItem })}
                                    />
                                ))}
                            </div>
                        </div>
                        {screenshots.length > 1 && (
                            <div
                                onClick={() => setActiveItem((activeItem + 1) % SCREENSHOTS_LENGTH)}
                                className="slider-arrow"
                            >
                                <Icon view={Icon.views.chevronRight} initial={Icon.kinds.impact} size={32} />
                            </div>
                        )}
                    </div>
                </Column>
            </ModalContent>
        </Modal>
    );
};

Gallery.propTypes = {
    isVisible: PropTypes.bool.isRequired,
    setHidden: PropTypes.func.isRequired,
};

export default Gallery;
