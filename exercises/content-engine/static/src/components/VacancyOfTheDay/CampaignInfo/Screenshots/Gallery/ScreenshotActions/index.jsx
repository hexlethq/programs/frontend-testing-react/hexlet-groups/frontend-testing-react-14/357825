import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import PropTypes from 'prop-types';
import Icon, { IconLink } from 'bloko/blocks/icon';
import Info from 'bloko/blocks/drop/Info';
import Down from 'bloko/blocks/drop/Down';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import { FormSpacer } from 'bloko/blocks/form';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import ElementWithHint from 'components/common/ElementWithHint';
import deleteScreenshot from 'components/VacancyOfTheDay/CampaignInfo/Screenshots/Gallery/ScreenshotActions/deleteScreenshot';
import 'components/VacancyOfTheDay/CampaignInfo/Screenshots/Gallery/ScreenshotActions/ScreenshotActions.less';

const ScreenshotActions = ({ screenId, src }) => {
    const { superAdmin, userId } = useSelector((state) => state.user);
    const creator = useSelector((state) => state.campaignInfoVOTD.info?.creator);
    const dispatch = useDispatch();

    const [isShow, setShow, setHidden] = useOnOffState(false);

    const campaignId = useParams().campaignId;

    const admin = superAdmin || creator.id === userId;

    return (
        <div className="screenshot-actions-container">
            <div className="screenshot-action">
                <IconLink Element="a" href={src} target="_blank" rel="noopener noreferrer">
                    <ElementWithHint
                        element={<Icon view={Icon.views.download} size={24} initial={Icon.kinds.primary} />}
                        hint={trl('campaignInfo.screenshot.download.hint')}
                    />
                </IconLink>
            </div>
            {admin && (
                <Info
                    key={screenId}
                    show={isShow}
                    render={() => (
                        <Fragment>
                            <Gap bottom>{trl('campaignInfo.screenshot.delete.text')}</Gap>
                            <Button
                                kind={Button.kinds.tertiaryMinor}
                                onClick={() => {
                                    setHidden();
                                    dispatch(deleteScreenshot(campaignId, screenId));
                                }}
                            >
                                {trl('campaignInfo.screenshot.delete.button')}
                            </Button>
                            <FormSpacer>
                                <Button kind={Button.kinds.primaryMinor} onClick={setHidden}>
                                    {trl('campaignInfo.screenshot.cancel.button')}
                                </Button>
                            </FormSpacer>
                        </Fragment>
                    )}
                    theme={Info.themes.light}
                    layer={Down.layers.aboveOverlayContent}
                    showCloseButton={false}
                >
                    <div className="screenshot-action">
                        <IconLink onClick={setShow}>
                            <ElementWithHint
                                element={<Icon view={Icon.views.remove} size={24} initial={Icon.kinds.tertiary} />}
                                hint={trl('campaignInfo.screenshot.remove.hint')}
                            />
                        </IconLink>
                    </div>
                </Info>
            )}
        </div>
    );
};

ScreenshotActions.propTypes = {
    screenId: PropTypes.number,
    src: PropTypes.string,
};

export default ScreenshotActions;
