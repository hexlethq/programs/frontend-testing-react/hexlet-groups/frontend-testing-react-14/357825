import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import Dropzone from 'react-dropzone';
import Button from 'bloko/blocks/button';
import Loading from 'bloko/blocks/loading';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import Gallery from 'components/VacancyOfTheDay/CampaignInfo/Screenshots/Gallery';
import fetchScreenshots from 'components/VacancyOfTheDay/CampaignInfo/Screenshots/fetchScreenshots';
import uploadScreenshot from 'components/VacancyOfTheDay/CampaignInfo/Screenshots/uploadScreenshot';
import { ACCEPT_IMAGE_TYPES } from 'constants/campaign';
import 'components/VacancyOfTheDay/CampaignInfo/Screenshots/Screenshots.less';

const Screenshots = () => {
    const screenshots = useSelector((state) => state.campaignInfoVOTD.screenshots);
    const dispatch = useDispatch();

    const [isLoadingScreenshot, setLoadingScreenshot, resetLoadingScreenshot] = useOnOffState(false);
    const [isVisibleGallery, setVisibleGallery, setHiddenGallery] = useOnOffState(false);

    const campaignId = useParams().campaignId;

    const SCREENSHOTS_LENGTH = screenshots?.length;

    useEffect(() => {
        if (campaignId) {
            dispatch(fetchScreenshots(campaignId));
        }
    }, [campaignId, dispatch]);

    return (
        <Fragment>
            <div className="screenshots-container">
                <div className="screenshots-count">
                    {SCREENSHOTS_LENGTH > 0 ? (
                        <LinkSwitch onClick={setVisibleGallery}>
                            {trl('campaignInfo.screenshots.count', [SCREENSHOTS_LENGTH])}
                        </LinkSwitch>
                    ) : (
                        trl('campaignInfo.screenshots.count', [SCREENSHOTS_LENGTH])
                    )}
                </div>
                <Dropzone
                    onDrop={(files) =>
                        dispatch(uploadScreenshot(files[0], campaignId, setLoadingScreenshot, resetLoadingScreenshot))
                    }
                    accept={ACCEPT_IMAGE_TYPES}
                >
                    {({ getRootProps, getInputProps }) => (
                        <div {...getRootProps()}>
                            <input {...getInputProps()} />
                            <Button kind={Button.kinds.primaryMinor} loading={isLoadingScreenshot ? <Loading /> : null}>
                                {trl('campaignInfo.screenshot.upload.placeholder')}
                            </Button>
                        </div>
                    )}
                </Dropzone>
            </div>
            {screenshots && <Gallery isVisible={isVisibleGallery} setHidden={setHiddenGallery} />}
        </Fragment>
    );
};

export default Screenshots;
