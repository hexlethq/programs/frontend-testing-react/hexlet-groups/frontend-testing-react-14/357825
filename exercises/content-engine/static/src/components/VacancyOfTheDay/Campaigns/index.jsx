import React, { useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';
import Header from 'bloko/blocks/header';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import Filter from 'components/VacancyOfTheDay/Filter';
import Settings from 'components/VacancyOfTheDay/Campaigns/Settings';
import CampaignsList from 'components/VacancyOfTheDay/Campaigns/CampaignsList';
import ModalRejectReason from 'components/VacancyOfTheDay/ModalRejectReason';
import fetchCampaigns from 'components/VacancyOfTheDay/Campaigns/fetchCampaigns';
import moderation from 'components/VacancyOfTheDay/moderation';
import fetchPackets from 'components/VacancyOfTheDay/fetchPackets';
import { resetModerationData } from 'store/models/vacancyOfTheDay/campaignsModeration';
import { setCampaignsFilterField, resetCampaignsFilter } from 'store/models/vacancyOfTheDay/campaigns';
import {
    CAMPAIGN_ACTION_APPROVE,
    FILTER_CAMPAIGN_STATUSES,
    VOTD_CAMPAIGNS_FILTER_FIELDS_CONFIG,
} from 'constants/campaign';

const Campaigns = ({ isModeration = false }) => {
    const superAdmin = useSelector((state) => state.user.superAdmin);
    const campaignIds = useSelector((state) => state.campaignsModerationVOTD.campaignIds);
    const { filter, filterCompleted } = useSelector((state) => state.campaignsVOTD);
    const dispatch = useDispatch();

    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    const filterFieldsConfig = isModeration
        ? VOTD_CAMPAIGNS_FILTER_FIELDS_CONFIG.filter(({ field }) => field !== FILTER_CAMPAIGN_STATUSES)
        : VOTD_CAMPAIGNS_FILTER_FIELDS_CONFIG;

    useEffect(() => {
        dispatch([resetCampaignsFilter(), resetModerationData()]);
        dispatch(fetchCampaigns(isModeration));
        dispatch(fetchPackets());
    }, [dispatch, isModeration]);

    return (
        <Fragment>
            <ShiftWrapper>
                {isModeration ? (
                    <Button
                        kind={Button.kinds.primary}
                        onClick={() => dispatch(push('/vacancy-of-the-day/campaigns/'))}
                    >
                        {trl('toCampaigns.return.button')}
                    </Button>
                ) : (
                    <Fragment>
                        {superAdmin && (
                            <Button
                                kind={Button.kinds.secondary}
                                onClick={() => dispatch(push('/vacancy-of-the-day/campaigns/moderation'))}
                            >
                                {trl('campaigns.toModeration.button')}
                            </Button>
                        )}
                        <Gap left>
                            <Button
                                kind={Button.kinds.primary}
                                onClick={() => dispatch(push('/vacancy-of-the-day/statistics/'))}
                            >
                                {trl('campaigns.toCommonStatistics.button')}
                            </Button>
                        </Gap>
                        <Gap left>
                            <Button
                                kind={Button.kinds.primary}
                                onClick={() => dispatch(push('/vacancy-of-the-day/create/'))}
                                data-qa="add-campaign"
                            >
                                {trl('campaigns.create.button')}
                            </Button>
                        </Gap>
                    </Fragment>
                )}
            </ShiftWrapper>
            <Header level={2}>
                {trl('campaigns.header')} {isModeration && trl('campaigns.moderation.additional.header')}
            </Header>
            <Filter
                filter={filter}
                filterFieldsConfig={filterFieldsConfig}
                setFilterField={(field, value) => dispatch(setCampaignsFilterField(field, value))}
                onClick={() => dispatch(fetchCampaigns(isModeration))}
                filterCompleted={filterCompleted}
                resetFilter={() => {
                    dispatch(resetCampaignsFilter());
                    dispatch(fetchCampaigns(isModeration));
                }}
            />
            <ShiftWrapper>
                <Settings />
            </ShiftWrapper>
            {isModeration && (
                <Gap bottom>
                    <Gap right Element="span">
                        <Button
                            kind={Button.kinds.secondaryMinor}
                            onClick={() => dispatch(moderation(CAMPAIGN_ACTION_APPROVE))}
                            disabled={campaignIds.length === 0}
                        >
                            {trl('campaign.moderation.approve.multi')}
                        </Button>
                    </Gap>
                    <Button kind={Button.kinds.tertiaryMinor} onClick={setVisible} disabled={campaignIds.length === 0}>
                        {trl('campaign.moderation.reject.multi')}
                    </Button>
                </Gap>
            )}
            <CampaignsList isModeration={isModeration} />
            <ModalRejectReason isVisible={isVisible} setHidden={setHidden} />
        </Fragment>
    );
};

Campaigns.propTypes = {
    isModeration: PropTypes.bool,
};

export default Campaigns;
