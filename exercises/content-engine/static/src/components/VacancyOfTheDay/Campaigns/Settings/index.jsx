import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Icon, { IconLink } from 'bloko/blocks/icon';
import Gap from 'bloko/blocks/gap';
import Modal, { ModalContent, ModalHeader, ModalTitle } from 'bloko/blocks/modal';
import Column from 'bloko/blocks/column';
import { FormItem } from 'bloko/blocks/form';
import Checkbox from 'bloko/blocks/checkbox';
import Button from 'bloko/blocks/button';
import LocalStorageWrapper from 'bloko/common/storage/LocalStorageWrapper';
import useOnOffState from 'hooks/useOnOffState';
import trl from 'modules/translation';
import { addOrDeleteInArray } from 'modules/common';
import { sortingByNumber } from 'modules/sorting';
import ShiftWrapper from 'components/common/ShiftWrapper';
import ElementWithHint from 'components/common/ElementWithHint';
import { setCampaignsHeaders } from 'store/models/vacancyOfTheDay/campaigns';
import { SORT_ASCENDING } from 'constants/common';
import { CAMPAIGNS_LIST_ID, VOTD_CAMPAIGNS_LIST_TABLE_HEADERS } from 'constants/campaign';

const ADMIN_VOTD_SAVED_HEADERS = 'ADMIN_VOTD_SAVED_HEADERS';
const SEPARATOR = ',';

const Settings = () => {
    const headers = useSelector((state) => state.campaignsVOTD.headers);
    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    const dispatch = useDispatch();

    const headerOrder = VOTD_CAMPAIGNS_LIST_TABLE_HEADERS.reduce((headerOrder, item, index) => {
        headerOrder[item] = index;
        return headerOrder;
    }, {});

    useEffect(() => {
        const savedHeaders = LocalStorageWrapper.getItem(ADMIN_VOTD_SAVED_HEADERS);
        if (savedHeaders) {
            dispatch(setCampaignsHeaders(savedHeaders.split(SEPARATOR)));
        }
    }, [dispatch]);

    const setHeaders = (head) => {
        const newHeaders = addOrDeleteInArray(head, headers);
        newHeaders.sort((firstHeader, secondHeader) =>
            sortingByNumber(headerOrder[firstHeader], headerOrder[secondHeader], SORT_ASCENDING)
        );
        dispatch(setCampaignsHeaders(newHeaders));
        LocalStorageWrapper.setItem(ADMIN_VOTD_SAVED_HEADERS, newHeaders.join(SEPARATOR));
    };

    return (
        <Fragment>
            <Gap bottom>
                <IconLink onClick={setVisible}>
                    <ElementWithHint
                        element={<Icon view={Icon.views.settings} size={24} initial={Icon.kinds.primary} />}
                        hint={trl('campaigns.headers.settings')}
                    />
                </IconLink>
            </Gap>
            <Modal visible={isVisible} onClose={setHidden}>
                <ModalHeader>
                    <ModalTitle>{trl('campaigns.headers.settings')}</ModalTitle>
                </ModalHeader>
                <ModalContent>
                    <Column xs="4" s="5" m="5" l="5" container>
                        {VOTD_CAMPAIGNS_LIST_TABLE_HEADERS.map((head) => (
                            <FormItem key={head} baseline={true}>
                                <Checkbox
                                    value={head}
                                    checked={headers.includes(head)}
                                    onChange={setHeaders}
                                    disabled={head === CAMPAIGNS_LIST_ID}
                                >
                                    {trl(`campaigns.${head}.table.head`)}
                                </Checkbox>
                            </FormItem>
                        ))}
                        <ShiftWrapper>
                            <Button kind={Button.kinds.primaryMinor} onClick={setHidden}>
                                {trl('close.button')}
                            </Button>
                        </ShiftWrapper>
                    </Column>
                </ModalContent>
            </Modal>
        </Fragment>
    );
};

export default Settings;
