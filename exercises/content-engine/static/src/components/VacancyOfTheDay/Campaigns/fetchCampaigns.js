import urlParser from 'bloko/common/urlParser';
import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import { deleteEmptyFields } from 'modules/common';
import {
    setCampaigns,
    setCampaignsFilterCompleted,
    setCampaignsSortInfo,
} from 'store/models/vacancyOfTheDay/campaigns';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import { FILTER_CAMPAIGN_STATUSES, CAMPAIGN_STATUS_ON_MODERATION, INIT_SORT_INFO } from 'constants/campaign';

export default (isModeration) => {
    return (dispatch, getState) => {
        const filter = getState().campaignsVOTD.filter;
        const { period, ...otherFields } = filter;
        const params = {
            dateTo: format(period.start, CALENDAR_FORMAT),
            dateFrom: format(period.end, CALENDAR_FORMAT),
            ...deleteEmptyFields(otherFields),
        };
        if (isModeration) {
            params[FILTER_CAMPAIGN_STATUSES] = [CAMPAIGN_STATUS_ON_MODERATION];
        }
        const queryStr = urlParser.stringify(params);
        fetcher.get(`/api/v1/vacancy_of_the_day/vacancy_of_the_day_list?${queryStr}`).then((data) => {
            dispatch([setCampaigns(data), setCampaignsSortInfo(INIT_SORT_INFO), setCampaignsFilterCompleted(filter)]);
        });
    };
};
