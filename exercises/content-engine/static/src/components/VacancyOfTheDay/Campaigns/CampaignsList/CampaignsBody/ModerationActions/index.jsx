import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Icon, { IconLink } from 'bloko/blocks/icon';
import Gap from 'bloko/blocks/gap';
import Checkbox from 'bloko/blocks/checkbox';
import useOnOffState from 'hooks/useOnOffState';
import { addOrDeleteInArray } from 'modules/common';
import trl from 'modules/translation';
import TableCell from 'components/common/Table/TableCell';
import ElementWithHint from 'components/common/ElementWithHint';
import TableHeadCell from 'components/common/Table/TableHeadCell';
import ModalRejectReason from 'components/VacancyOfTheDay/ModalRejectReason';
import moderation from 'components/VacancyOfTheDay/moderation';
import { setModerationCampaignIds } from 'store/models/vacancyOfTheDay/campaignsModeration';
import { CAMPAIGN_ACTION_APPROVE } from 'constants/campaign';

const ModerationActions = ({ children, campaignId }) => {
    const moderationCampaignIds = useSelector((state) => state.campaignsModerationVOTD.campaignIds);
    const dispatch = useDispatch();

    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    return (
        <Fragment>
            <TableCell position={TableCell.positions.center}>
                <IconLink
                    onClick={() => {
                        dispatch(setModerationCampaignIds([campaignId]));
                        dispatch(moderation(CAMPAIGN_ACTION_APPROVE));
                    }}
                >
                    <ElementWithHint
                        element={<Icon view={Icon.views.done} size={24} initial={Icon.kinds.secondary} />}
                        hint={trl('campaign.moderation.approve')}
                    />
                </IconLink>
            </TableCell>
            <TableCell position={TableCell.positions.center}>
                <IconLink
                    onClick={() => {
                        dispatch(setModerationCampaignIds([campaignId]));
                        setVisible();
                    }}
                >
                    <ElementWithHint
                        element={<Icon view={Icon.views.ban} size={24} initial={Icon.kinds.tertiary} />}
                        hint={trl('campaign.moderation.reject')}
                    />
                </IconLink>
            </TableCell>
            {children}
            <TableCell position={TableHeadCell.positions.center}>
                <Gap bottom>
                    <Checkbox
                        value={campaignId}
                        checked={moderationCampaignIds.includes(campaignId.toString())}
                        onChange={(event) =>
                            dispatch(setModerationCampaignIds(addOrDeleteInArray(event, moderationCampaignIds)))
                        }
                    />
                </Gap>
            </TableCell>
            <ModalRejectReason isVisible={isVisible} setHidden={setHidden} />
        </Fragment>
    );
};

ModerationActions.propTypes = {
    children: PropTypes.node,
    campaignId: PropTypes.number,
};

export default ModerationActions;
