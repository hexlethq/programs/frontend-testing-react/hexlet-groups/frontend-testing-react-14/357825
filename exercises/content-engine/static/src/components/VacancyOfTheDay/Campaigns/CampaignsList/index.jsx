import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Gap from 'bloko/blocks/gap';
import Header from 'bloko/blocks/header';
import trl from 'modules/translation';
import { updateSortInfo } from 'modules/sorting';
import Table from 'components/common/Table';
import TableHead from 'components/common/Table/TableHead';
import TableRow from 'components/common/Table/TableRow';
import TableHeadCell from 'components/common/Table/TableHeadCell';
import SortLinkHeader from 'components/common/SortLinkHeader';
import CampaignsBody from 'components/VacancyOfTheDay/Campaigns/CampaignsList/CampaignsBody';
import { setCampaigns, setCampaignsSortInfo } from 'store/models/vacancyOfTheDay/campaigns';
import { VOTD_CAMPAIGNS_LIST_SORT_HEADERS } from 'constants/campaign';
import SORT_CONFIG from 'components/VacancyOfTheDay/Campaigns/CampaignsList/sortConfig';
import 'components/VacancyOfTheDay/Campaigns/CampaignsList/CampaignsList.less';

const CampaignsList = ({ isModeration = false }) => {
    const { campaigns, headers, campaignsSortInfo } = useSelector((state) => state.campaignsVOTD);
    const { allRegionsDict, allPacketsDict } = useSelector((state) => state.regions);

    const dispatch = useDispatch();

    const sortCampaigns = (head) => {
        const newCampaigns = [...campaigns];
        newCampaigns.sort((campaignFirst, campaignSecond) =>
            SORT_CONFIG[head](campaignFirst, campaignSecond, campaignsSortInfo.type, { allRegionsDict, allPacketsDict })
        );
        dispatch(setCampaigns(newCampaigns));
        updateSortInfo(head, campaignsSortInfo, (info) => dispatch(setCampaignsSortInfo(info)));
    };

    return campaigns.length > 0 ? (
        <Table bordered fullWidth>
            <TableHead>
                <TableRow>
                    {isModeration && <TableHeadCell colSpan={2} />}
                    {headers.map((head) => (
                        <TableHeadCell
                            position={TableHeadCell.positions.center}
                            key={head}
                            className="campaigns-list-table-head"
                        >
                            {VOTD_CAMPAIGNS_LIST_SORT_HEADERS.includes(head) ? (
                                <SortLinkHeader
                                    onClick={() => sortCampaigns(head)}
                                    field={head}
                                    text={trl(`campaigns.${head}.table.head`)}
                                    sortType={campaignsSortInfo.type}
                                    isShowArrow={campaignsSortInfo.head === head}
                                />
                            ) : (
                                trl(`campaigns.${head}.table.head`)
                            )}
                        </TableHeadCell>
                    ))}
                    {isModeration && <TableHeadCell />}
                </TableRow>
            </TableHead>
            <CampaignsBody isModeration={isModeration} />
        </Table>
    ) : (
        <Gap top>
            <Header level={3}>{trl('campaigns.empty.text')}</Header>
        </Gap>
    );
};

CampaignsList.propTypes = {
    isModeration: PropTypes.bool,
};

export default CampaignsList;
