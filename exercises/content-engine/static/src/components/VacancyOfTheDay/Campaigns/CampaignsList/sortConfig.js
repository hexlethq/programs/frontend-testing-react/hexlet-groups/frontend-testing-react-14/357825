import { parseISO } from 'date-fns';
import { sortingByDate, sortingByNumber, sortingByString } from 'modules/sorting';
import {
    CAMPAIGNS_LIST_CAMPAIGN_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    CAMPAIGNS_LIST_CRM_URL,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_PAYMENT_STATUS,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_VACANCY,
} from 'constants/campaign';
import trl from 'modules/translation';

export default {
    [CAMPAIGNS_LIST_ID]: (campaignFirst, campaignSecond, sortType) =>
        sortingByNumber(campaignFirst.vacancyOfTheDayId, campaignSecond.vacancyOfTheDayId, sortType),
    [CAMPAIGNS_LIST_EMPLOYER]: (campaignFirst, campaignSecond, sortType) =>
        sortingByNumber(campaignFirst.employer.id, campaignSecond.employer.id, sortType),
    [CAMPAIGNS_LIST_VACANCY]: (campaignFirst, campaignSecond, sortType) =>
        sortingByNumber(campaignFirst.vacancy.id, campaignSecond.vacancy.id, sortType),
    [CAMPAIGNS_LIST_PERIOD_START]: (campaignFirst, campaignSecond, sortType) =>
        sortingByDate(parseISO(campaignFirst.dateFrom), parseISO(campaignSecond.dateFrom), sortType),
    [CAMPAIGNS_LIST_PERIOD_END]: (campaignFirst, campaignSecond, sortType) =>
        sortingByDate(parseISO(campaignFirst.dateTo), parseISO(campaignSecond.dateTo), sortType),
    [CAMPAIGNS_LIST_PACKET]: (campaignFirst, campaignSecond, sortType, dicts) =>
        sortingByString(
            dicts.allPacketsDict[campaignFirst.packet].name,
            dicts.allPacketsDict[campaignSecond.packet].name,
            sortType
        ),
    [CAMPAIGNS_LIST_REGIONS]: (campaignFirst, campaignSecond, sortType, dicts) => {
        if (campaignFirst.regionList.length === 1 && campaignSecond.regionList.length === 1) {
            return sortingByString(
                dicts.allRegionsDict[campaignFirst.regionList[0]],
                dicts.allRegionsDict[campaignSecond.regionList[0]],
                sortType
            );
        }
        return sortingByNumber(campaignFirst.regionList.length, campaignSecond.regionList.length, sortType);
    },
    [CAMPAIGNS_LIST_DEVICE_TYPES]: (campaignFirst, campaignSecond, sortType) => {
        if (campaignFirst.deviceTypes.length === 1 && campaignSecond.deviceTypes.length === 1) {
            return sortingByString(campaignFirst.deviceTypes[0], campaignSecond.deviceTypes[0], sortType);
        }
        return sortingByNumber(campaignFirst.deviceTypes.length, campaignSecond.deviceTypes.length, sortType);
    },
    [CAMPAIGNS_LIST_PAYMENT_STATUS]: (campaignFirst, campaignSecond, sortType) =>
        sortingByString(
            trl(`payment.status.${campaignFirst.paymentStatus}`),
            trl(`payment.status.${campaignSecond.paymentStatus}`),
            sortType
        ),
    [CAMPAIGNS_LIST_CAMPAIGN_STATUS]: (campaignFirst, campaignSecond, sortType) =>
        sortingByString(
            trl(`campaign.status.${campaignFirst.campaignStatus}`),
            trl(`campaign.status.${campaignSecond.campaignStatus}`),
            sortType
        ),
    [CAMPAIGNS_LIST_MANAGER]: (campaignFirst, campaignSecond, sortType) =>
        sortingByString(campaignFirst.creator.name, campaignSecond.creator.name, sortType),
    [CAMPAIGNS_LIST_CAMPAIGN_TYPE]: (campaignFirst, campaignSecond, sortType) =>
        sortingByString(
            trl(`campaign.type.${campaignFirst.campaignType}`),
            trl(`campaign.type.${campaignSecond.campaignType}`),
            sortType
        ),
    [CAMPAIGNS_LIST_CRM_URL]: (campaignFirst, campaignSecond, sortType) =>
        sortingByString(campaignFirst.crmUrl, campaignSecond.crmUrl, sortType),
};
