import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { parseISO } from 'date-fns';
import PropTypes from 'prop-types';
import BlokoLink from 'bloko/blocks/link';
import { TextTertiary } from 'bloko/blocks/text';
import trl from 'modules/translation';
import format from 'modules/date-format';
import TableRow from 'components/common/Table/TableRow';
import TableBody from 'components/common/Table/TableBody';
import TableCell from 'components/common/Table/TableCell';
import BracketsText from 'components/common/BracketsText';
import ModalRegions from 'components/common/ModalRegions';
import ModalFullString from 'components/common/ModalFullString';
import ElementWithHint from 'components/common/ElementWithHint';
import EntityInfo from 'components/VacancyOfTheDay/EntityInfo';
import ModerationActions from 'components/VacancyOfTheDay/Campaigns/CampaignsList/CampaignsBody/ModerationActions';
import { VIEW_FORMAT } from 'constants/date-formats';
import {
    CAMPAIGNS_LIST_CAMPAIGN_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    CAMPAIGNS_LIST_COMMENT,
    CAMPAIGNS_LIST_CRM_URL,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_PAYMENT_STATUS,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_VACANCY,
    ENTITY_EMPLOYER,
    ENTITY_VACANCY,
} from 'constants/campaign';

const COMMENT_VIEW_LENGTH = 12;
const EMPLOYER_NAME_VIEW_LENGTH = 10;
const VACANCY_NAME_VIEW_LENGTH = 10;
const CRM_URL_VIEW_LENGTH = 15;

const CampaignsBody = ({ isModeration = false }) => {
    const { campaigns, headers } = useSelector((state) => state.campaignsVOTD);
    const allPacketsDict = useSelector((state) => state.regions.allPacketsDict);

    return (
        <Fragment>
            <TableBody>
                {campaigns.map(
                    ({
                        vacancyOfTheDayId,
                        employer,
                        vacancy,
                        dateFrom,
                        dateTo,
                        packet,
                        regionList,
                        deviceTypes,
                        paymentStatus,
                        campaignStatus,
                        creator,
                        campaignType,
                        crmUrl,
                        comment,
                    }) => {
                        const TABLE_CELL_CONTENT = {
                            [CAMPAIGNS_LIST_ID]: (
                                <Link to={`/vacancy-of-the-day/campaign/${vacancyOfTheDayId}`} data-qa="campaign-id">
                                    {vacancyOfTheDayId}
                                </Link>
                            ),
                            [CAMPAIGNS_LIST_EMPLOYER]: (
                                <EntityInfo
                                    entity={ENTITY_EMPLOYER}
                                    info={employer}
                                    length={EMPLOYER_NAME_VIEW_LENGTH}
                                />
                            ),
                            [CAMPAIGNS_LIST_VACANCY]: (
                                <EntityInfo entity={ENTITY_VACANCY} info={vacancy} length={VACANCY_NAME_VIEW_LENGTH} />
                            ),
                            [CAMPAIGNS_LIST_PERIOD_START]: format(parseISO(dateFrom), VIEW_FORMAT),
                            [CAMPAIGNS_LIST_PERIOD_END]: format(parseISO(dateTo), VIEW_FORMAT),
                            [CAMPAIGNS_LIST_PACKET]: allPacketsDict[packet]?.name,
                            [CAMPAIGNS_LIST_REGIONS]: <ModalRegions regions={regionList} />,
                            [CAMPAIGNS_LIST_DEVICE_TYPES]: deviceTypes.join(', '),
                            [CAMPAIGNS_LIST_PAYMENT_STATUS]: trl(`payment.status.${paymentStatus}`),
                            [CAMPAIGNS_LIST_CAMPAIGN_STATUS]: trl(`campaign.status.${campaignStatus}`),
                            [CAMPAIGNS_LIST_MANAGER]: (
                                <Fragment>
                                    {creator.name}
                                    <TextTertiary>
                                        <BracketsText>{creator.id}</BracketsText>
                                    </TextTertiary>
                                    <BlokoLink href={`mailto:${creator.email}`}>{creator.email}</BlokoLink>
                                </Fragment>
                            ),
                            [CAMPAIGNS_LIST_CAMPAIGN_TYPE]: trl(`campaign.type.${campaignType}`),
                            [CAMPAIGNS_LIST_CRM_URL]: (
                                <ModalFullString
                                    title={trl(`campaigns.${CAMPAIGNS_LIST_CRM_URL}.table.head`)}
                                    string={crmUrl}
                                    length={CRM_URL_VIEW_LENGTH}
                                />
                            ),
                            [CAMPAIGNS_LIST_COMMENT]: (
                                <Fragment>
                                    {comment && (
                                        <ElementWithHint
                                            element={
                                                comment.length > COMMENT_VIEW_LENGTH
                                                    ? `${comment.slice(0, COMMENT_VIEW_LENGTH)}...`
                                                    : comment
                                            }
                                            hint={comment}
                                        />
                                    )}
                                </Fragment>
                            ),
                        };
                        return (
                            <TableRow key={vacancyOfTheDayId}>
                                {isModeration ? (
                                    <ModerationActions campaignId={vacancyOfTheDayId}>
                                        {headers.map((head) => (
                                            <TableCell position={TableCell.positions.center} key={head}>
                                                {TABLE_CELL_CONTENT[head]}
                                            </TableCell>
                                        ))}
                                    </ModerationActions>
                                ) : (
                                    <Fragment>
                                        {headers.map((head) => (
                                            <TableCell position={TableCell.positions.center} key={head}>
                                                {TABLE_CELL_CONTENT[head]}
                                            </TableCell>
                                        ))}
                                    </Fragment>
                                )}
                            </TableRow>
                        );
                    }
                )}
            </TableBody>
        </Fragment>
    );
};

CampaignsBody.propTypes = {
    isModeration: PropTypes.bool,
};

export default CampaignsBody;
