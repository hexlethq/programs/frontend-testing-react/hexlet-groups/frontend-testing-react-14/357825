import fetcher from 'modules/fetcher';
import { setAllRegionsVOTD } from 'store/models/regions';

export default () => {
    return (dispatch) => {
        fetcher.get('/api/v1/regions').then((data) => {
            dispatch(setAllRegionsVOTD(data));
        });
    };
};
