import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import CompositeSelection from 'bloko/blocks/compositeSelection';
import { fromTree } from 'bloko/common/tree/treeCollectionHelper';
import createStaticDataProvider from 'bloko/blocks/suggest/createStaticDataProvider';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import { FormItem } from 'bloko/blocks/form';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import fetchRegions from 'components/VacancyOfTheDay/SelectRegions/fetchRegions';

const MAX_TAGS = 5;

const SelectRegions = ({ regions, setRegions }) => {
    const { allRegionsVOTD } = useSelector((state) => state.regions);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchRegions());
    }, [dispatch]);

    const collection = fromTree(allRegionsVOTD);
    const dataProvider = createStaticDataProvider(collection.toList());

    return (
        <Fragment>
            <CompositeSelection
                collection={collection}
                value={regions}
                onChange={(regions) => setRegions(regions)}
                title={trl('regions.select.title')}
                trl={{
                    submit: trl('regions.select.submit'),
                    cancel: trl('regions.select.cancel'),
                    searchPlaceholder: trl('regions.select.searchPlaceholder'),
                    notFound: trl('regions.select.notFound'),
                }}
                leavesOnly
            >
                {({ renderInput, renderTagList, showTreeSelectorPopup }) => (
                    <div>
                        {regions.length < MAX_TAGS ? (
                            renderTagList()
                        ) : (
                            <FormItem>
                                <LinkSwitch onClick={showTreeSelectorPopup}>
                                    {trl('regions.select.tags.length', [regions.length])}
                                </LinkSwitch>
                            </FormItem>
                        )}
                        {renderInput({ suggest: true, dataProvider })}
                    </div>
                )}
            </CompositeSelection>
            {regions.length > 0 && (
                <ShiftWrapper>
                    <LinkSwitch onClick={() => setRegions([])}>{trl('regions.clear.link')}</LinkSwitch>
                </ShiftWrapper>
            )}
        </Fragment>
    );
};

SelectRegions.propTypes = {
    regions: PropTypes.array,
    setRegions: PropTypes.func,
};

export default SelectRegions;
