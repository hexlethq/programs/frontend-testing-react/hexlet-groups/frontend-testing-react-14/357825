import urlParser from 'bloko/common/urlParser';
import fetcher from 'modules/fetcher';
import addNotification from 'modules/notification';
import fetchCampaigns from 'components/VacancyOfTheDay/Campaigns/fetchCampaigns';
import fetchCampaignInfo from 'components/VacancyOfTheDay/CampaignInfo/fetchCampaignInfo';
import { resetModerationData } from 'store/models/vacancyOfTheDay/campaignsModeration';
import { CAMPAIGN_ACTION_REJECT } from 'constants/campaign';
import { CAMPAIGN_MODERATION_SUCCESS, CAMPAIGN_MODERATION_VACANCY_ERROR } from 'constants/notifications';

const ERROR_VACANCY_IS_NULL = 'VACANCY_IS_NULL';

export default (action, fromCampaignInfo) => {
    return (dispatch, getState) => {
        const { campaignIds, rejectReason } = getState().campaignsModerationVOTD;
        const params = {
            vacancyOfTheDayId: campaignIds,
        };
        if (action === CAMPAIGN_ACTION_REJECT) {
            params.reason = rejectReason;
        }
        const queryStr = urlParser.stringify(params);
        fetcher
            .put(`/api/v1/vacancy_of_the_day/status/${action}?${queryStr}`)
            .then((data) => {
                const errorIds = data?.errorToVacancyOfTheDayIds?.[ERROR_VACANCY_IS_NULL];
                if (errorIds?.length > 0) {
                    dispatch(addNotification(CAMPAIGN_MODERATION_VACANCY_ERROR, errorIds));
                } else {
                    dispatch(addNotification(CAMPAIGN_MODERATION_SUCCESS));
                }
                dispatch(fromCampaignInfo ? fetchCampaignInfo(campaignIds[0]) : fetchCampaigns(true));
            })
            .finally(() => dispatch(resetModerationData()));
    };
};
