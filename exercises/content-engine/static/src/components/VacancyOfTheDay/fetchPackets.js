import fetcher from 'modules/fetcher';
import { setAllPacketsDictionary, setAllPacketsVOTD } from 'store/models/regions';

export default () => {
    return (dispatch) => {
        fetcher.get('/api/v1/packets').then((data) => {
            const packetsDictionary = {};
            data?.forEach(({ code, name, useRegions }) => (packetsDictionary[code] = { name, useRegions }));
            dispatch([setAllPacketsVOTD(data), setAllPacketsDictionary(packetsDictionary)]);
        });
    };
};
