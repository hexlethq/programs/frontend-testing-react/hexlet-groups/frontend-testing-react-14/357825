import React, { Fragment, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import Button from 'bloko/blocks/button';
import Header from 'bloko/blocks/header';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import Filter from 'components/common/StatisticsElements/Filter';
import StatisticsTabs from 'components/common/StatisticsElements/StatisticsTabs';
import Chart from 'components/common/StatisticsElements/Chart';
import StatisticsDownload from 'components/common/StatisticsElements/StatisticsDownload';
import StatisticsTable from 'components/common/StatisticsElements/StatisticsTable';
import fetchCampaignStatistics from 'components/VacancyOfTheDay/CampaignStatistics/fetchCampaignStatistics';
import {
    setCampaignStatistics,
    setCampaignStatisticsType,
    setCampaignStatisticsPeriod,
    setCampaignStatisticsSortInfo,
} from 'store/models/vacancyOfTheDay/campaignStatistics';
import {
    STATISTICS_TABLE_SECOND_HEADERS_FULL,
    STATISTICS_TABLE_SECOND_HEADERS_SHORT,
    VACANCY_OF_THE_DAY,
} from 'constants/campaign';
import 'components/VacancyOfTheDay/CampaignStatistics/CampaignStatistics.less';

const CampaignStatistics = () => {
    const {
        employerInfo,
        vacancyInfo,
        statisticsType,
        period,
        statistics,
        totalStatistics,
        statisticsSortInfo,
        chartData,
    } = useSelector((state) => state.campaignStatisticsVOTD);
    const dispatch = useDispatch();

    const [fullStatistics, setFullStatistics] = useState(false);

    const { campaignId, type } = useParams();

    const STATISTICS_TABLE_SECOND_HEADERS = fullStatistics
        ? STATISTICS_TABLE_SECOND_HEADERS_FULL
        : STATISTICS_TABLE_SECOND_HEADERS_SHORT;

    useEffect(() => {
        if (type !== statisticsType) {
            dispatch([setCampaignStatistics({ data: [], loading: true }), setCampaignStatisticsType(type)]);
        }
    }, [type, dispatch, statisticsType]);

    useEffect(() => {
        dispatch(fetchCampaignStatistics(campaignId));
    }, [campaignId, dispatch, statisticsType]);

    return (
        <Fragment>
            <ShiftWrapper>
                <Button kind={Button.kinds.primary} onClick={() => window.history.back()}>
                    {trl('return.button')}
                </Button>
            </ShiftWrapper>
            <Header level={2}>{trl('statistics.header', [campaignId])}</Header>
            <Header level={3}>{trl('statistics.header.employer', [employerInfo?.name, employerInfo?.id])}</Header>
            <Header level={3}>{trl('statistics.header.vacancy', [vacancyInfo?.name, vacancyInfo?.id])}</Header>
            <Filter
                campaignId={campaignId}
                period={period}
                setPeriod={(period) => dispatch(setCampaignStatisticsPeriod(period))}
                fetchStatistics={() => dispatch(fetchCampaignStatistics(campaignId))}
            />
            <StatisticsTabs adminType={VACANCY_OF_THE_DAY} />
            {statistics?.data?.length > 0 && (
                <Fragment>
                    <div className="statistics-container">
                        <Chart chartData={chartData} secondHeaders={STATISTICS_TABLE_SECOND_HEADERS} />
                        <StatisticsDownload
                            campaignId={campaignId}
                            statisticsType={statisticsType}
                            period={period}
                            adminType={VACANCY_OF_THE_DAY}
                        />
                    </div>
                    <LinkSwitch onClick={() => setFullStatistics(!fullStatistics)}>
                        {fullStatistics ? trl('statistics.full.close.link') : trl('statistics.full.show.link')}
                    </LinkSwitch>
                </Fragment>
            )}
            <StatisticsTable
                statistics={statistics}
                totalStatistics={totalStatistics}
                statisticsType={statisticsType}
                statisticsSortInfo={statisticsSortInfo}
                secondHeaders={STATISTICS_TABLE_SECOND_HEADERS}
                setStatistics={(statistics) => dispatch(setCampaignStatistics(statistics))}
                setStatisticsSortInfo={(info) => dispatch(setCampaignStatisticsSortInfo(info))}
            />
        </Fragment>
    );
};

export default CampaignStatistics;
