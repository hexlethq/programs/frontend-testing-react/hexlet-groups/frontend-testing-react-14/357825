import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import { getChartData } from 'modules/common';
import {
    setCampaignStatistics,
    setCampaignStatisticsChart,
    setCampaignStatisticsSortInfo,
    setCampaignTotalStatistics,
    setCampaignStatisticsEmployerInfo,
    setCampaignStatisticsVacancyInfo,
} from 'store/models/vacancyOfTheDay/campaignStatistics';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import { STATISTICS_BY_DATE, INIT_SORT_INFO } from 'constants/campaign';

export default (vacancyOfTheDayId) => {
    return (dispatch, getState) => {
        dispatch(setCampaignStatistics({ data: [], loading: true }));
        const { period, statisticsType } = getState().campaignStatisticsVOTD;
        const params = {
            fromDate: format(period.start, CALENDAR_FORMAT),
            toDate: format(period.end, CALENDAR_FORMAT),
            vacancyOfTheDayId,
        };
        fetcher
            .get(`/api/v1/vacancy_of_the_day/statistic/by_${statisticsType}`, { params })
            .then((data) => {
                const statistics =
                    statisticsType === STATISTICS_BY_DATE ? data.statisticsByDates : data.statisticByRegions;
                dispatch([
                    setCampaignStatisticsEmployerInfo(data?.employerInfo),
                    setCampaignStatisticsVacancyInfo(data?.vacancyInfo),
                    setCampaignStatistics({ data: statistics, loading: false }),
                    setCampaignStatisticsChart(getChartData(statistics, statisticsType)),
                    setCampaignTotalStatistics(data?.totalStatistic),
                    setCampaignStatisticsSortInfo(INIT_SORT_INFO),
                ]);
            })
            .catch(() => {
                dispatch(setCampaignStatistics({ data: [], loading: false }));
            });
    };
};
