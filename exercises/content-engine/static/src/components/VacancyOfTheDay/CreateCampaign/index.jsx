import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router';
import { push } from 'connected-react-router';
import PropTypes from 'prop-types';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import CampaignForm from 'components/VacancyOfTheDay/CreateCampaign/CampaignForm';
import getCampaignInfo from 'components/VacancyOfTheDay/CreateCampaign/getCampaignInfo';
import { resetCampaignInfo } from 'store/models/vacancyOfTheDay/createCampaign';

const CreateCampaign = ({ isEdit = false }) => {
    const campaignId = useSelector((state) => state.createCampaignVOTD.campaignId);
    const dispatch = useDispatch();

    const campaignIdFromPath = useParams().campaignId;

    useEffect(() => {
        dispatch(resetCampaignInfo());
        if (isEdit) {
            dispatch(getCampaignInfo(campaignIdFromPath));
        }
    }, [campaignIdFromPath, dispatch, isEdit]);

    if (isEdit && !campaignId) {
        return null;
    }

    return (
        <Fragment>
            <ShiftWrapper>
                <Button kind={Button.kinds.primary} onClick={() => dispatch(push('/vacancy-of-the-day/campaigns/'))}>
                    {trl('toCampaigns.return.button')}
                </Button>
            </ShiftWrapper>
            <Gap top bottom>
                <CampaignForm />
            </Gap>
        </Fragment>
    );
};

CreateCampaign.propTypes = {
    isEdit: PropTypes.bool,
};

export default CreateCampaign;
