import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow } from 'bloko/blocks/form';
import Input from 'bloko/blocks/input';
import AdvFormColumns from 'components/common/AdvFormColumns';
import trl from 'modules/translation';
import { setCrmUrl } from 'store/models/vacancyOfTheDay/createCampaign';

const CrmUrl = () => {
    const crmUrl = useSelector((state) => state.createCampaignVOTD.crmUrl);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label" htmlFor="crm">
                        {trl('createCampaign.crmUrl.legend')}
                    </FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <Input
                        id="crm"
                        value={crmUrl}
                        onChange={(e) => dispatch(setCrmUrl(e.target.value))}
                        data-qa="crm"
                    />
                </FormRow>
            }
        />
    );
};

export default CrmUrl;
