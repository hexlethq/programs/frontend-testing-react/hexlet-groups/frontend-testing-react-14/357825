import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Button from 'bloko/blocks/button';
import Column from 'bloko/blocks/column';
import trl from 'modules/translation';
import ShiftWrapper from 'components/common/ShiftWrapper';
import AdvFormWrapper from 'components/common/AdvFormWrapper';
import DeviceType from 'components/common/FormElements/DeviceType';
import Period from 'components/common/FormElements/Period';
import Identifier from 'components/common/FormElements/Identifier';
import Busyness from 'components/common/FormElements/Busyness';
import CrmUrl from 'components/VacancyOfTheDay/CreateCampaign/CampaignForm/CrmUrl';
import Comment from 'components/common/FormElements/Comment';
import Regions from 'components/VacancyOfTheDay/CreateCampaign/CampaignForm/Regions';
import CampaignType from 'components/VacancyOfTheDay/CreateCampaign/CampaignForm/CampaignType';
import {
    setDeviceTypes,
    setPeriod,
    setVacancyId,
    setVacancyName,
    setBusyness,
    setComment,
} from 'store/models/vacancyOfTheDay/createCampaign';
import saveCampaign from 'components/VacancyOfTheDay/CreateCampaign/CampaignForm/saveCampaign';
import { ENTITY_VACANCY, URL_VACANCY_OF_THE_DAY } from 'constants/campaign';

const CampaignForm = () => {
    const { campaignId, period, deviceTypes, comment, vacancyName, busyness, regions, packet } = useSelector(
        (state) => state.createCampaignVOTD
    );
    const dispatch = useDispatch();

    return (
        <AdvFormWrapper>
            <DeviceType
                deviceTypes={deviceTypes}
                setDeviceTypes={(deviceTypes) => dispatch(setDeviceTypes(deviceTypes))}
            />
            <Period period={period} setPeriod={(period) => dispatch(setPeriod(period))} />
            <Regions />
            <Busyness
                busyness={busyness}
                period={period}
                adminUrl={URL_VACANCY_OF_THE_DAY}
                setBusyness={(busyness) => dispatch(setBusyness(busyness))}
                busynessInfo={{ period, regions, deviceTypes, packet }}
            />
            <CampaignType />
            <Identifier
                entity={ENTITY_VACANCY}
                name={vacancyName}
                saveId={(id) => dispatch(setVacancyId(id))}
                saveName={(name) => dispatch(setVacancyName(name))}
            />
            <CrmUrl />
            <Comment comment={comment} setComment={(comment) => dispatch(setComment(comment))} />
            <Column xs="4" s="6" m="10" l="12" container>
                <ShiftWrapper>
                    <Button
                        kind={Button.kinds.primary}
                        onClick={() => dispatch(saveCampaign())}
                        data-qa="save-campaign"
                    >
                        {!campaignId ? trl('createCampaign.create.button') : trl('createCampaign.edit.button')}
                    </Button>
                </ShiftWrapper>
            </Column>
        </AdvFormWrapper>
    );
};

export default CampaignForm;
