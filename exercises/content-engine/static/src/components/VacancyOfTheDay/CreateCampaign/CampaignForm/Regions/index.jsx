import React, { Fragment, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Radio from 'bloko/blocks/radio';
import { FormItem, FormItemGroup, FormLegend, FormRow } from 'bloko/blocks/form';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import SelectRegions from 'components/VacancyOfTheDay/SelectRegions';
import fetchPackets from 'components/VacancyOfTheDay/fetchPackets';
import { setPacket, setRegions } from 'store/models/vacancyOfTheDay/createCampaign';

const Regions = () => {
    const allPacketsVOTD = useSelector((state) => state.regions.allPacketsVOTD);
    const { packet, regions } = useSelector((state) => state.createCampaignVOTD);

    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(fetchPackets());
    }, [dispatch]);

    useEffect(() => {
        if (allPacketsVOTD.length > 0 && !packet) {
            dispatch(setPacket(allPacketsVOTD[0]?.code));
        }
    }, [allPacketsVOTD, dispatch, packet]);

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label">{trl('createCampaign.regions.legend')}</FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <FormItemGroup>
                        {allPacketsVOTD.map(({ code, name, useRegions }) => (
                            <Fragment key={code}>
                                <FormItem baseline>
                                    <Radio
                                        name="packet"
                                        onChange={(event) => dispatch(setPacket(event.target.value))}
                                        checked={code === packet}
                                        value={code}
                                    >
                                        {name}
                                    </Radio>
                                </FormItem>
                                {code === packet && useRegions && (
                                    <SelectRegions
                                        regions={regions}
                                        setRegions={(regions) => dispatch(setRegions(regions))}
                                    />
                                )}
                            </Fragment>
                        ))}
                    </FormItemGroup>
                </FormRow>
            }
        />
    );
};

export default Regions;
