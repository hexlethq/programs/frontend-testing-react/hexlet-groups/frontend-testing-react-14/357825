import { push } from 'connected-react-router';
import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import addNotification from 'modules/notification';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import {
    CAMPAIGN_SAVE_SUCCESS,
    CAMPAIGN_SAVE_DATA_ERROR,
    CAMPAIGN_SAVE_BUSY_ERROR,
    CAMPAIGN_NOTIFICATION_ERROR,
    CAMPAIGN_EDIT_SUCCESS,
} from 'constants/notifications';

export default () => {
    return (dispatch, getState) => {
        const {
            campaignId,
            deviceTypes,
            packet,
            regions,
            period,
            campaignType,
            vacancyId,
            comment,
            crmUrl,
        } = getState().createCampaignVOTD;
        const regionIds = regions.map((el) => parseInt(el, 10));
        const body = {
            deviceTypes,
            packetType: packet,
            regionIds,
            dateStart: format(period.start, CALENDAR_FORMAT),
            dateEnd: format(period.end, CALENDAR_FORMAT),
            campaignType,
            vacancyId,
            comment,
            crmUrl,
        };
        const url = campaignId ? `/api/v1/vacancy_of_the_day/${campaignId}/edit` : '/api/v1/vacancy_of_the_day';
        const method = campaignId ? 'put' : 'post';
        fetcher[method](url, body)
            .then((data) => {
                if (!campaignId) {
                    dispatch(addNotification(CAMPAIGN_SAVE_SUCCESS, [data]));
                    dispatch(push('/vacancy-of-the-day/campaigns'));
                } else {
                    dispatch(addNotification(CAMPAIGN_EDIT_SUCCESS, [campaignId]));
                    dispatch(push(`/vacancy-of-the-day/campaign/${campaignId}`));
                }
            })
            .catch((err) => {
                const status = err?.response?.status;
                if (status === 400) {
                    const errorDescription = err.response.data;
                    const errorMessage = err.response.data?.message;
                    if (errorDescription && !errorMessage) {
                        dispatch(addNotification(CAMPAIGN_NOTIFICATION_ERROR[errorDescription]));
                    } else {
                        dispatch(addNotification(CAMPAIGN_SAVE_DATA_ERROR));
                    }
                }
                if (status === 412) {
                    dispatch(addNotification(CAMPAIGN_SAVE_BUSY_ERROR));
                }
            });
    };
};
