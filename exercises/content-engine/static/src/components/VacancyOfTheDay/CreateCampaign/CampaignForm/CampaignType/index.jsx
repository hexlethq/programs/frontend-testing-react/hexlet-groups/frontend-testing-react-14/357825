import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FormLegend, FormRow, FormItem, FormItemGroup } from 'bloko/blocks/form';
import Radio from 'bloko/blocks/radio';
import trl from 'modules/translation';
import AdvFormColumns from 'components/common/AdvFormColumns';
import { setCampaignType } from 'store/models/vacancyOfTheDay/createCampaign';

import { CAMPAIGN_TYPES } from 'constants/campaign';

const Type = () => {
    const campaignType = useSelector((state) => state.createCampaignVOTD.campaignType);
    const dispatch = useDispatch();

    return (
        <AdvFormColumns
            legend={
                <FormRow>
                    <FormLegend Element="label" htmlFor="type">
                        {trl('createCampaign.campaignType.legend')}
                    </FormLegend>
                </FormRow>
            }
            group={
                <FormRow>
                    <FormItemGroup>
                        {CAMPAIGN_TYPES.map((item) => (
                            <FormItem key={item} baseline>
                                <Radio
                                    name="type"
                                    onChange={(event) => dispatch(setCampaignType(event.target.value))}
                                    checked={item === campaignType}
                                    value={item}
                                >
                                    {trl(`campaign.type.${item}`)}
                                </Radio>
                            </FormItem>
                        ))}
                    </FormItemGroup>
                </FormRow>
            }
        />
    );
};

export default Type;
