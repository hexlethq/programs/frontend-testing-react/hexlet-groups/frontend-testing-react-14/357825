import { parseISO } from 'date-fns';
import fetcher from 'modules/fetcher';
import {
    setDeviceTypes,
    setPeriod,
    setPacket,
    setRegions,
    setCampaignType,
    setVacancyId,
    setVacancyName,
    setComment,
    setCrmUrl,
    setCampaignId,
} from 'store/models/vacancyOfTheDay/createCampaign';

export default (vacancyOfTheDayId) => {
    return (dispatch) => {
        fetcher
            .get('/api/v1/vacancy_of_the_day/vacancy_of_the_day_list', { params: { vacancyOfTheDayId } })
            .then((data) => {
                const {
                    deviceTypes,
                    dateFrom,
                    dateTo,
                    packet,
                    regionList,
                    campaignType,
                    vacancy,
                    comment,
                    crmUrl,
                } = data[0];
                const actions = [
                    setCampaignId(vacancyOfTheDayId),
                    setDeviceTypes(deviceTypes),
                    setPeriod({ start: parseISO(dateFrom), end: parseISO(dateTo) }),
                    setPacket(packet),
                    setRegions(regionList.map((id) => id.toString())),
                    setCampaignType(campaignType),
                    setComment(comment),
                    setCrmUrl(crmUrl),
                ];
                if (vacancy.id !== 0) {
                    actions.push(setVacancyId(vacancy.id));
                    actions.push(setVacancyName(vacancy.name));
                }
                dispatch(actions);
            });
    };
};
