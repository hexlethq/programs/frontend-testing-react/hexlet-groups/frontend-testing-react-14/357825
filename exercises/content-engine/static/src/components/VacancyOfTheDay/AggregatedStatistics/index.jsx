import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { push } from 'connected-react-router';
import classNames from 'classnames';
import Button from 'bloko/blocks/button';
import Header from 'bloko/blocks/header';
import Column from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import Loader from 'components/common/Loader';
import Table from 'components/common/Table';
import TableHead from 'components/common/Table/TableHead';
import TableRow from 'components/common/Table/TableRow';
import TableHeadCell from 'components/common/Table/TableHeadCell';
import TableBody from 'components/common/Table/TableBody';
import TableCell from 'components/common/Table/TableCell';
import ShiftWrapper from 'components/common/ShiftWrapper';
import FilterFields from 'components/VacancyOfTheDay/Filter/FilterFields';
import fetchAggregatedStatistics from 'components/VacancyOfTheDay/AggregatedStatistics/fetchAggregatedStatistics';
import { setAggregatedStatisticsFilterField } from 'store/models/vacancyOfTheDay/aggregatedStatistics';
import {
    VOTD_AGGREGATED_STATISTICS_FILTER_FIELDS_CONFIG,
    VOTD_AGGREGATED_STATISTICS_HEADERS,
} from 'constants/campaign';
import 'components/VacancyOfTheDay/AggregatedStatistics/AggregatedStatistics.less';

const AggregatedStatistics = () => {
    const { filter, statistics } = useSelector((state) => state.aggregatedStatisticsVOTD);
    const dispatch = useDispatch();

    return (
        <Fragment>
            <ShiftWrapper>
                <Button kind={Button.kinds.primary} onClick={() => dispatch(push('/vacancy-of-the-day/statistics/'))}>
                    {trl('toCommonStatistics.return.button')}
                </Button>
            </ShiftWrapper>
            <Header level={2}>{trl('aggregatedStatistics.header')}</Header>
            <Gap top>
                <Column xs="4" s="8" m="8" l="6" container>
                    <div className={classNames({ 'aggregated-statistics-filter-disabled': statistics.loading })}>
                        <FilterFields
                            filter={filter}
                            filterFieldsConfig={VOTD_AGGREGATED_STATISTICS_FILTER_FIELDS_CONFIG}
                            setFilterField={(field, value) =>
                                dispatch(setAggregatedStatisticsFilterField(field, value))
                            }
                        />
                        <ShiftWrapper>
                            <Button kind={Button.kinds.primary} onClick={() => dispatch(fetchAggregatedStatistics())}>
                                {trl('aggregatedStatistics.show.button')}
                            </Button>
                        </ShiftWrapper>
                    </div>
                </Column>
                <Column xs="4" s="8" m="8" l="10" container>
                    <Gap mTop sTop xsTop>
                        <div className="aggregated-statistics-table">
                            {statistics.loading ? (
                                <Loader />
                            ) : (
                                <Fragment>
                                    {Object.keys(statistics.data).length > 0 ? (
                                        <Table bordered>
                                            <TableHead>
                                                <TableRow>
                                                    {VOTD_AGGREGATED_STATISTICS_HEADERS.map((head) => (
                                                        <TableHeadCell
                                                            position={TableHeadCell.positions.center}
                                                            key={head}
                                                            className="aggregated-statistics-head-cell"
                                                        >
                                                            {trl(`campaigns.${head}.table.head`)}
                                                        </TableHeadCell>
                                                    ))}
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                <TableRow>
                                                    <TableCell position={TableCell.positions.center}>
                                                        {statistics.data.impressions}
                                                    </TableCell>
                                                    <TableCell position={TableCell.positions.center}>
                                                        {statistics.data.clicks}
                                                    </TableCell>
                                                </TableRow>
                                            </TableBody>
                                        </Table>
                                    ) : (
                                        <Header level={3}>{trl('statistics.empty.text')}</Header>
                                    )}
                                </Fragment>
                            )}
                        </div>
                    </Gap>
                </Column>
            </Gap>
        </Fragment>
    );
};

export default AggregatedStatistics;
