import urlParser from 'bloko/common/urlParser';
import fetcher from 'modules/fetcher';
import format from 'modules/date-format';
import addNotification from 'modules/notification';
import { deleteEmptyFields } from 'modules/common';
import { setAggregatedStatistics } from 'store/models/vacancyOfTheDay/aggregatedStatistics';
import { CALENDAR_FORMAT } from 'constants/date-formats';
import { INCORRECT_FIELDS_ERROR } from 'constants/notifications';

export default () => {
    return (dispatch, getState) => {
        dispatch(setAggregatedStatistics({ data: {}, loading: true }));
        const filter = getState().aggregatedStatisticsVOTD.filter;
        const { period, ...otherFields } = filter;
        const params = {
            dateFrom: format(period.start, CALENDAR_FORMAT),
            dateTo: format(period.end, CALENDAR_FORMAT),
            ...deleteEmptyFields(otherFields),
        };
        const queryStr = urlParser.stringify(params);
        fetcher
            .get(`/api/v1/vacancy_of_the_day/statistic/aggregated?${queryStr}`, {
                timeout: 300000,
            })
            .then((data) => {
                dispatch(setAggregatedStatistics({ data, loading: false }));
            })
            .catch((err) => {
                const status = err.response.status;
                if (status === 400) {
                    dispatch(addNotification(INCORRECT_FIELDS_ERROR));
                }
                dispatch(setAggregatedStatistics({ data: {}, loading: false }));
            });
    };
};
