import React, { useEffect, Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import { FormItemGroup, FormItem } from 'bloko/blocks/form';
import Checkbox from 'bloko/blocks/checkbox';
import Gap from 'bloko/blocks/gap';
import Radio from 'bloko/blocks/radio';
import trl from 'modules/translation';
import { addOrDeleteInArray } from 'modules/common';
import SelectRegions from 'components/VacancyOfTheDay/SelectRegions';
import fetchPackets from 'components/VacancyOfTheDay/fetchPackets';
import {
    FILTER_CHECKBOX,
    FILTER_EXACTLY_REGION,
    FILTER_PACKETS,
    FILTER_REGIONS,
    FILTER_PACKET,
    FILTER_RADIO,
} from 'constants/campaign';

const Regions = ({ filter, setFilterField, params }) => {
    const { allPacketsVOTD, allPacketsDict } = useSelector((state) => state.regions);
    const dispatch = useDispatch();

    const fieldPacket = params.type === FILTER_CHECKBOX ? FILTER_PACKETS : FILTER_PACKET;
    const fieldPacketValue = filter[fieldPacket];

    const regions = filter[FILTER_REGIONS];
    const exactlyRegion = filter[FILTER_EXACTLY_REGION];

    useEffect(() => {
        dispatch(fetchPackets());
    }, [dispatch]);

    useEffect(() => {
        if (regions.length > 0) {
            const needRegions =
                fieldPacket === FILTER_PACKETS
                    ? fieldPacketValue.some((packet) => allPacketsDict[packet]?.useRegions)
                    : allPacketsDict[fieldPacketValue].useRegions;
            if (!needRegions) {
                setFilterField(FILTER_REGIONS, []);
            }
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [allPacketsDict, dispatch, fieldPacketValue, regions]);

    return (
        <FormItemGroup>
            {allPacketsVOTD.map(({ code, name, useRegions }) => (
                <Fragment key={code}>
                    <FormItem baseline>
                        {params.type === FILTER_CHECKBOX && (
                            <Checkbox
                                onChange={(event) => {
                                    setFilterField(fieldPacket, addOrDeleteInArray(event, fieldPacketValue));
                                }}
                                checked={fieldPacketValue.includes(code)}
                                value={code}
                            >
                                {name}
                            </Checkbox>
                        )}
                        {params.type === FILTER_RADIO && (
                            <Radio
                                name="packet"
                                onChange={(event) => setFilterField(fieldPacket, event.target.value)}
                                checked={code === fieldPacketValue}
                                value={code}
                            >
                                {name}
                            </Radio>
                        )}
                    </FormItem>
                    {(fieldPacket === FILTER_PACKETS ? fieldPacketValue.includes(code) : fieldPacketValue === code) &&
                        useRegions && (
                            <SelectRegions
                                regions={regions}
                                setRegions={(regions) => setFilterField(FILTER_REGIONS, regions)}
                            />
                        )}
                </Fragment>
            ))}
            {params.isExactlyRegion && (
                <Gap top>
                    <FormItem baseline={true}>
                        <Checkbox
                            checked={exactlyRegion}
                            onChange={() => setFilterField(FILTER_EXACTLY_REGION, !exactlyRegion)}
                        >
                            {trl(`campaigns.filter.${FILTER_EXACTLY_REGION}.label`)}
                        </Checkbox>
                    </FormItem>
                </Gap>
            )}
        </FormItemGroup>
    );
};

Regions.propTypes = {
    filter: PropTypes.object,
    setFilterField: PropTypes.func,
    params: PropTypes.object,
};

export default Regions;
