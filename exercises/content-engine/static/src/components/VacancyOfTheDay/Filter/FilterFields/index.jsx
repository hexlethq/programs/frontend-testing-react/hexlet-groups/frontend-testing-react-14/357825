import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import Input from 'bloko/blocks/input';
import Checkbox from 'bloko/blocks/checkbox';
import { FormLegend, FormItem } from 'bloko/blocks/form';
import trl from 'modules/translation';
import { addOrDeleteInArray } from 'modules/common';
import NumberInput from 'components/common/NumberInput';
import InputWithIntervalCalendar from 'components/common/InputWithIntervalCalendar';
import FilterColumns from 'components/VacancyOfTheDay/Filter/FilterColumns';
import Regions from 'components/VacancyOfTheDay/Filter/Regions';
import { FILTER_NUMBER, FILTER_TEXT, FILTER_CHECKBOX, FILTER_PERIOD, FILTER_REGIONS } from 'constants/campaign';

const FilterFields = ({ filter, filterFieldsConfig, setFilterField }) => (
    <Fragment>
        {filterFieldsConfig.map(({ field, type, items = [], params }) => {
            const value = filter[field];
            const onInputChange = (event) => setFilterField(field, event.target.value);
            return (
                <FilterColumns
                    key={field}
                    legend={<FormLegend Element="label">{trl(`campaigns.filter.${field}.label`)}</FormLegend>}
                    group={
                        <Fragment>
                            {type === FILTER_NUMBER && <NumberInput value={value} onChange={onInputChange} />}
                            {type === FILTER_TEXT && (
                                <Input
                                    value={value}
                                    onChange={onInputChange}
                                    placeholder={trl('input.text.placeholder')}
                                />
                            )}
                            {type === FILTER_CHECKBOX &&
                                items.map((item) => (
                                    <FormItem key={item} baseline={true}>
                                        <Checkbox
                                            value={item}
                                            checked={value.includes(item)}
                                            onChange={(event) =>
                                                setFilterField(field, addOrDeleteInArray(event, value))
                                            }
                                        >
                                            {trl(`campaigns.filter.${field}.${item}.checkbox`)}
                                        </Checkbox>
                                    </FormItem>
                                ))}
                            {type === FILTER_PERIOD && (
                                <InputWithIntervalCalendar
                                    action={(period) => setFilterField(field, period)}
                                    from={filter[field].start}
                                    to={filter[field].end}
                                />
                            )}
                            {type === FILTER_REGIONS && (
                                <Regions filter={filter} setFilterField={setFilterField} params={params} />
                            )}
                        </Fragment>
                    }
                />
            );
        })}
    </Fragment>
);

FilterFields.propTypes = {
    filter: PropTypes.object,
    filterFieldsConfig: PropTypes.array,
    setFilterField: PropTypes.func,
};

export default FilterFields;
