import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import Column from 'bloko/blocks/column';
import { TextTertiary } from 'bloko/blocks/text';
import Modal, { ModalContent, ModalFooter, ModalHeader, ModalTitle } from 'bloko/blocks/modal';
import Icon, { IconLink } from 'bloko/blocks/icon';
import LinkSwitch from 'bloko/blocks/linkSwitch';
import useOnOffState from 'hooks/useOnOffState';
import format from 'modules/date-format';
import trl from 'modules/translation';
import { deleteEmptyFields } from 'modules/common';
import ElementWithHint from 'components/common/ElementWithHint';
import FilterFields from 'components/VacancyOfTheDay/Filter/FilterFields';
import { VIEW_FORMAT } from 'constants/date-formats';
import {
    FILTER_NUMBER,
    FILTER_TEXT,
    FILTER_CHECKBOX,
    FILTER_PERIOD,
    FILTER_REGIONS,
    FILTER_PACKETS,
    FILTER_EXACTLY_REGION,
} from 'constants/campaign';

const Filter = ({ filter, filterFieldsConfig, setFilterField, onClick, filterCompleted, resetFilter }) => {
    const { allRegionsDict, allPacketsDict } = useSelector((state) => state.regions);

    const [isVisible, setVisible, setHidden] = useOnOffState(false);

    const isEmptyFilter =
        Object.keys(deleteEmptyFields(filterCompleted)).length === 1 &&
        deleteEmptyFields(filterCompleted)[FILTER_PERIOD];

    const renderFilterCompletedFields = ({ field, type }) => (
        <Fragment key={field}>
            {(type === FILTER_NUMBER || type === FILTER_TEXT) && filterCompleted[field] !== '' && (
                <TextTertiary>
                    {trl(`campaigns.filter.${field}.label`)}
                    {filterCompleted[field]}
                </TextTertiary>
            )}
            {type === FILTER_CHECKBOX && filterCompleted[field].length > 0 && (
                <TextTertiary>
                    {trl(`campaigns.filter.${field}.label`)}
                    {filterCompleted[field].map((item) => trl(`campaigns.filter.${field}.${item}.checkbox`)).join(', ')}
                </TextTertiary>
            )}
            {type === FILTER_PERIOD && filterCompleted[field].start && filterCompleted[field].end && (
                <TextTertiary>
                    {trl(`campaigns.filter.${field}.label`)}
                    {`${format(filterCompleted[field].start, VIEW_FORMAT)} - ${format(
                        filterCompleted[field].end,
                        VIEW_FORMAT
                    )}`}
                </TextTertiary>
            )}
            {type === FILTER_REGIONS && (
                <Fragment>
                    {filterCompleted[FILTER_PACKETS].length > 0 && (
                        <TextTertiary>
                            {trl(`campaigns.filter.${FILTER_PACKETS}.label`)}
                            {filterCompleted[FILTER_PACKETS].map((item) => allPacketsDict[item].name).join(', ')}
                        </TextTertiary>
                    )}
                    {filterCompleted[field].length > 0 && (
                        <TextTertiary>
                            {trl(`campaigns.filter.${field}.label`)}
                            {filterCompleted[field].map((item) => allRegionsDict[item]).join(', ')}
                        </TextTertiary>
                    )}
                    <TextTertiary>
                        {filterCompleted[FILTER_EXACTLY_REGION] &&
                            trl(`campaigns.filter.${FILTER_EXACTLY_REGION}.label`)}
                    </TextTertiary>
                </Fragment>
            )}
        </Fragment>
    );

    return (
        <Fragment>
            <Gap bottom>
                <IconLink onClick={setVisible}>
                    <ElementWithHint
                        element={<Icon view={Icon.views.filters} size={24} initial={Icon.kinds.primary} />}
                        hint={trl('campaigns.filter')}
                    />
                </IconLink>
            </Gap>
            <Gap top bottom>
                {filterFieldsConfig.map((item) => renderFilterCompletedFields(item))}
                {!isEmptyFilter && <LinkSwitch onClick={resetFilter}>{trl('filter.clear.link')}</LinkSwitch>}
            </Gap>
            <Modal visible={isVisible} onClose={setHidden}>
                <ModalHeader>
                    <ModalTitle>{trl('campaigns.filter')}</ModalTitle>
                </ModalHeader>
                <ModalContent>
                    <Column xs="4" s="8" m="8" l="14" container>
                        <FilterFields
                            filter={filter}
                            filterFieldsConfig={filterFieldsConfig}
                            setFilterField={setFilterField}
                        />
                    </Column>
                </ModalContent>
                <ModalFooter>
                    <Button
                        kind={Button.kinds.primaryMinor}
                        onClick={() => {
                            setHidden();
                            onClick();
                        }}
                    >
                        {trl('filter.button')}
                    </Button>
                </ModalFooter>
            </Modal>
        </Fragment>
    );
};

Filter.propTypes = {
    filter: PropTypes.object,
    filterFieldsConfig: PropTypes.array,
    setFilterField: PropTypes.func,
    onClick: PropTypes.func,
    filterCompleted: PropTypes.object,
    resetFilter: PropTypes.func,
};

export default Filter;
