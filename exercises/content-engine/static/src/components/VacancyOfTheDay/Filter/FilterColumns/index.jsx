import React from 'react';
import PropTypes from 'prop-types';
import { FormGroup, FormRow } from 'bloko/blocks/form';
import Column, { ColumnsRow } from 'bloko/blocks/column';

const FilterColumns = ({ legend, group }) => (
    <ColumnsRow>
        <FormGroup>
            <FormRow>
                <Column xs="4" s="8" m="8" l="7" container>
                    <Column xs="4" s="4" m="4" l="3" container>
                        {legend}
                    </Column>
                    <Column xs="4" s="4" m="4" l="3" container>
                        {group}
                    </Column>
                </Column>
            </FormRow>
        </FormGroup>
    </ColumnsRow>
);

FilterColumns.propTypes = {
    legend: PropTypes.node,
    group: PropTypes.node,
};

export default FilterColumns;
