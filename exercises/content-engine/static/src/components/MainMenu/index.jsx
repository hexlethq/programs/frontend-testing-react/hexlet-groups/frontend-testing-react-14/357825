import React, { Fragment } from 'react';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import Column from 'bloko/blocks/column';
import Button from 'bloko/blocks/button';
import Gap from 'bloko/blocks/gap';
import trl from 'modules/translation';
import ImgCOTD from 'components/MainMenu/cotd.png';
import ImgVOTD from 'components/MainMenu/votd.png';
import { ANYTHING_OF_THE_DAY, COMPANY_OF_THE_DAY, VACANCY_OF_THE_DAY } from 'constants/campaign';
import 'components/MainMenu/MainMenu.less';

const IMG = {
    [COMPANY_OF_THE_DAY]: ImgCOTD,
    [VACANCY_OF_THE_DAY]: ImgVOTD,
};

const MainMenu = () => {
    const dispatch = useDispatch();

    return (
        <Fragment>
            {ANYTHING_OF_THE_DAY.map((item) => (
                <Column key={item} xs="4" s="8" m="6" l="8" container>
                    <div className="main-menu-item">
                        <Gap bottom>
                            <Button
                                kind={Button.kinds.primary}
                                onClick={() => dispatch(push(`/${item}/campaigns/`))}
                                stretched
                            >
                                {trl(`main.menu.${item}`)}
                            </Button>
                        </Gap>
                        <img className="main-menu-item-img" src={IMG[item]} alt={trl(`main.menu.${item}`)} />
                    </div>
                </Column>
            ))}
        </Fragment>
    );
};

export default MainMenu;
