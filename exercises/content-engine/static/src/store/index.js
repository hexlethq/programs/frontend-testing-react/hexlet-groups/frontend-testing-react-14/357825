import { applyMiddleware, createStore, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

import batchedMiddleware, { batchReducer } from 'store/batchedMiddleware';
import createMainReducer from 'store/mainReducer';

export const history = createBrowserHistory();

const middleware = [thunkMiddleware, batchedMiddleware, routerMiddleware(history)];

const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const store = createStore(batchReducer(createMainReducer(history)), composeEnhancers(applyMiddleware(...middleware)));

export default store;
