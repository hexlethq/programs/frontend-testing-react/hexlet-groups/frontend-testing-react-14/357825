import { createReducer } from 'redux-create-reducer';

const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
const DELETE_NOTIFICATION = 'DELETE_NOTIFICATION';

export const addNotificationAction = (notification) => {
    return {
        type: ADD_NOTIFICATION,
        notification,
    };
};

export const deleteNotificationAction = (id) => {
    return {
        type: DELETE_NOTIFICATION,
        id,
    };
};

const INITIAL_STATE = [];

export default createReducer(INITIAL_STATE, {
    [ADD_NOTIFICATION]: (state, action) => [action.notification, ...state],
    [DELETE_NOTIFICATION]: (state, action) => state.filter((notification) => notification.id !== action.id),
});
