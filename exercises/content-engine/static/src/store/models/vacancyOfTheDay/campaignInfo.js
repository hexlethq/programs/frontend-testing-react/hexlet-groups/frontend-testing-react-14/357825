import { createReducer } from 'redux-create-reducer';

const SET_CAMPAIGN_INFO_VOTD = 'SET_CAMPAIGN_INFO_VOTD';
const SET_CAMPAIGN_SCREENSHOTS_VOTD = 'SET_CAMPAIGN_SCREENSHOTS_VOTD';

export const setCampaignInfo = (info) => ({
    type: SET_CAMPAIGN_INFO_VOTD,
    info,
});

export const setCampaignScreenshots = (screenshots) => ({
    type: SET_CAMPAIGN_SCREENSHOTS_VOTD,
    screenshots,
});

const INITIAL_STATE = {
    info: {},
    screenshots: [],
};

export default createReducer(INITIAL_STATE, {
    [SET_CAMPAIGN_INFO_VOTD](state, action) {
        return {
            ...state,
            info: action.info,
        };
    },
    [SET_CAMPAIGN_SCREENSHOTS_VOTD](state, action) {
        return {
            ...state,
            screenshots: action.screenshots,
        };
    },
});
