import { createReducer } from 'redux-create-reducer';

const SET_MODERATION_CAMPAIGN_IDS_VOTD = 'SET_MODERATION_CAMPAIGN_ID_VOTD';
const SET_MODERATION_REJECT_REASON = 'SET_MODERATION_REJECT_REASON';
const RESET_MODERATION_DATA = 'RESET_MODERATION_DATA';

export const setModerationCampaignIds = (campaignIds) => ({
    type: SET_MODERATION_CAMPAIGN_IDS_VOTD,
    campaignIds,
});

export const setModerationRejectReason = (rejectReason) => ({
    type: SET_MODERATION_REJECT_REASON,
    rejectReason,
});

export const resetModerationData = () => ({
    type: RESET_MODERATION_DATA,
});

const INITIAL_STATE = {
    campaignIds: [],
    rejectReason: '',
};

export default createReducer(INITIAL_STATE, {
    [SET_MODERATION_CAMPAIGN_IDS_VOTD](state, action) {
        return {
            ...state,
            campaignIds: action.campaignIds,
        };
    },
    [SET_MODERATION_REJECT_REASON](state, action) {
        return {
            ...state,
            rejectReason: action.rejectReason,
        };
    },
    [RESET_MODERATION_DATA]() {
        return INITIAL_STATE;
    },
});
