import { createReducer } from 'redux-create-reducer';
import {
    DEFAULT_MONTH_PERIOD_BEFORE,
    FILTER_VOTD_ID,
    FILTER_CAMPAIGN_TYPES,
    FILTER_CREATOR_ID,
    FILTER_CREATOR_NAME,
    FILTER_DEVICE_TYPES,
    FILTER_EMPLOYER_ID,
    FILTER_EMPLOYER_NAME,
    FILTER_EXACTLY_REGION,
    FILTER_PACKETS,
    FILTER_PERIOD,
    FILTER_REGIONS,
    FILTER_VACANCY_ID,
    FILTER_VACANCY_NAME,
    INIT_SORT_INFO,
} from 'constants/campaign';

const INIT_FILTER = {
    [FILTER_PERIOD]: DEFAULT_MONTH_PERIOD_BEFORE,
    [FILTER_PACKETS]: [],
    [FILTER_REGIONS]: [],
    [FILTER_EXACTLY_REGION]: false,
    [FILTER_VOTD_ID]: '',
    [FILTER_EMPLOYER_ID]: '',
    [FILTER_EMPLOYER_NAME]: '',
    [FILTER_VACANCY_ID]: '',
    [FILTER_VACANCY_NAME]: '',
    [FILTER_CREATOR_ID]: '',
    [FILTER_CREATOR_NAME]: '',
    [FILTER_CAMPAIGN_TYPES]: [],
    [FILTER_DEVICE_TYPES]: [],
};

const SET_COMMON_STATISTICS_VOTD = 'SET_COMMON_STATISTICS_VOTD';
const SET_COMMON_STATISTICS_SORT_INFO_VOTD = 'SET_COMMON_STATISTICS_SORT_INFO_VOTD';
const SET_COMMON_STATISTICS_FILTER_FIELD = 'SET_COMMON_STATISTICS_FILTER_FIELD';
const SET_COMMON_STATISTICS_COMPLETED = 'SET_COMMON_STATISTICS_COMPLETED';
const RESET_COMMON_STATISTICS_FILTER = 'RESET_COMMON_STATISTICS_FILTER';

export const setCommonStatistics = (statistics) => ({
    type: SET_COMMON_STATISTICS_VOTD,
    statistics,
});

export const setCommonStatisticsSortInfo = (statisticsSortInfo) => ({
    type: SET_COMMON_STATISTICS_SORT_INFO_VOTD,
    statisticsSortInfo,
});

export const setCommonStatisticsFilterField = (field, value) => ({
    type: SET_COMMON_STATISTICS_FILTER_FIELD,
    field,
    value,
});

export const setCommonStatisticsFilterCompleted = (value) => ({
    type: SET_COMMON_STATISTICS_COMPLETED,
    value,
});

export const resetCommonStatisticsFilter = () => ({
    type: RESET_COMMON_STATISTICS_FILTER,
});

const INITIAL_STATE = {
    statistics: { data: [], loading: true },
    statisticsSortInfo: INIT_SORT_INFO,
    filter: INIT_FILTER,
    filterCompleted: INIT_FILTER,
};

export default createReducer(INITIAL_STATE, {
    [SET_COMMON_STATISTICS_VOTD](state, action) {
        return {
            ...state,
            statistics: action.statistics,
        };
    },
    [SET_COMMON_STATISTICS_SORT_INFO_VOTD](state, action) {
        return {
            ...state,
            statisticsSortInfo: action.statisticsSortInfo,
        };
    },
    [SET_COMMON_STATISTICS_FILTER_FIELD](state, action) {
        return {
            ...state,
            filter: { ...state.filter, [action.field]: action.value },
        };
    },
    [SET_COMMON_STATISTICS_COMPLETED](state, action) {
        return {
            ...state,
            filterCompleted: action.value,
        };
    },
    [RESET_COMMON_STATISTICS_FILTER](state) {
        return {
            ...state,
            filter: INIT_FILTER,
            filterCompleted: INIT_FILTER,
        };
    },
});
