import { createReducer } from 'redux-create-reducer';
import {
    DEFAULT_MONTH_PERIOD_AFTER,
    FILTER_VOTD_ID,
    FILTER_CAMPAIGN_STATUSES,
    FILTER_CAMPAIGN_TYPES,
    FILTER_CREATOR_ID,
    FILTER_CREATOR_NAME,
    FILTER_CRM_URL,
    FILTER_PERIOD,
    FILTER_DEVICE_TYPES,
    FILTER_EMPLOYER_ID,
    FILTER_EMPLOYER_NAME,
    FILTER_PAYMENT_STATUSES,
    FILTER_VACANCY_ID,
    FILTER_VACANCY_NAME,
    INIT_SORT_INFO,
    VOTD_CAMPAIGNS_LIST_DEFAULT_TABLE_HEADERS,
    FILTER_REGIONS,
    FILTER_EXACTLY_REGION,
    FILTER_PACKETS,
} from 'constants/campaign';

const INIT_FILTER = {
    [FILTER_PERIOD]: DEFAULT_MONTH_PERIOD_AFTER,
    [FILTER_PACKETS]: [],
    [FILTER_REGIONS]: [],
    [FILTER_EXACTLY_REGION]: false,
    [FILTER_VOTD_ID]: '',
    [FILTER_EMPLOYER_ID]: '',
    [FILTER_EMPLOYER_NAME]: '',
    [FILTER_VACANCY_ID]: '',
    [FILTER_VACANCY_NAME]: '',
    [FILTER_CREATOR_ID]: '',
    [FILTER_CREATOR_NAME]: '',
    [FILTER_CRM_URL]: '',
    [FILTER_CAMPAIGN_TYPES]: [],
    [FILTER_CAMPAIGN_STATUSES]: [],
    [FILTER_DEVICE_TYPES]: [],
    [FILTER_PAYMENT_STATUSES]: [],
};

const SET_CAMPAIGNS_VOTD = 'SET_CAMPAIGNS_VOTD';
const SET_CAMPAIGNS_HEADERS_VOTD = 'SET_CAMPAIGNS_HEADERS_VOTD';
const SET_CAMPAIGNS_SORT_INFO_VOTD = 'SET_CAMPAIGNS_SORT_INFO_VOTD';
const SET_CAMPAIGNS_FILTER_FIELD = 'SET_CAMPAIGNS_FILTER_FIELD';
const SET_CAMPAIGNS_FILTER_COMPLETED = 'SET_CAMPAIGNS_FILTER_COMPLETED';
const RESET_CAMPAIGNS_FILTER = 'RESET_CAMPAIGNS_FILTER';

export const setCampaigns = (campaigns) => ({
    type: SET_CAMPAIGNS_VOTD,
    campaigns,
});

export const setCampaignsHeaders = (headers) => ({
    type: SET_CAMPAIGNS_HEADERS_VOTD,
    headers,
});

export const setCampaignsSortInfo = (campaignsSortInfo) => ({
    type: SET_CAMPAIGNS_SORT_INFO_VOTD,
    campaignsSortInfo,
});

export const setCampaignsFilterField = (field, value) => ({
    type: SET_CAMPAIGNS_FILTER_FIELD,
    field,
    value,
});

export const setCampaignsFilterCompleted = (value) => ({
    type: SET_CAMPAIGNS_FILTER_COMPLETED,
    value,
});

export const resetCampaignsFilter = () => ({
    type: RESET_CAMPAIGNS_FILTER,
});

const INITIAL_STATE = {
    campaigns: [],
    headers: VOTD_CAMPAIGNS_LIST_DEFAULT_TABLE_HEADERS,
    campaignsSortInfo: INIT_SORT_INFO,
    filter: INIT_FILTER,
    filterCompleted: INIT_FILTER,
};

export default createReducer(INITIAL_STATE, {
    [SET_CAMPAIGNS_VOTD](state, action) {
        return {
            ...state,
            campaigns: action.campaigns,
        };
    },
    [SET_CAMPAIGNS_HEADERS_VOTD](state, action) {
        return {
            ...state,
            headers: action.headers,
        };
    },
    [SET_CAMPAIGNS_SORT_INFO_VOTD](state, action) {
        return {
            ...state,
            campaignsSortInfo: action.campaignsSortInfo,
        };
    },
    [SET_CAMPAIGNS_FILTER_FIELD](state, action) {
        return {
            ...state,
            filter: { ...state.filter, [action.field]: action.value },
        };
    },
    [SET_CAMPAIGNS_FILTER_COMPLETED](state, action) {
        return {
            ...state,
            filterCompleted: action.value,
        };
    },
    [RESET_CAMPAIGNS_FILTER](state) {
        return {
            ...state,
            filter: INIT_FILTER,
            filterCompleted: INIT_FILTER,
        };
    },
});
