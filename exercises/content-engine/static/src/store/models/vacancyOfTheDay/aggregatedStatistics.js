import { createReducer } from 'redux-create-reducer';
import {
    DEFAULT_MONTH_PERIOD_BEFORE,
    FILTER_DEVICE_TYPES,
    FILTER_PACKET,
    FILTER_PERIOD,
    FILTER_REGIONS,
} from 'constants/campaign';

const SET_AGGREGATED_STATISTICS_VOTD = 'SET_AGGREGATED_STATISTICS_VOTD';
const SET_AGGREGATED_STATISTICS_FILTER_FIELD = 'SET_AGGREGATED_STATISTICS_FILTER_FIELD';

export const setAggregatedStatistics = (statistics) => ({
    type: SET_AGGREGATED_STATISTICS_VOTD,
    statistics,
});

export const setAggregatedStatisticsFilterField = (field, value) => ({
    type: SET_AGGREGATED_STATISTICS_FILTER_FIELD,
    field,
    value,
});

const INITIAL_STATE = {
    statistics: { data: {}, loading: false },
    filter: {
        [FILTER_PERIOD]: DEFAULT_MONTH_PERIOD_BEFORE,
        [FILTER_REGIONS]: [],
        [FILTER_PACKET]: '',
        [FILTER_DEVICE_TYPES]: [],
    },
};

export default createReducer(INITIAL_STATE, {
    [SET_AGGREGATED_STATISTICS_VOTD](state, action) {
        return {
            ...state,
            statistics: action.statistics,
        };
    },
    [SET_AGGREGATED_STATISTICS_FILTER_FIELD](state, action) {
        return {
            ...state,
            filter: { ...state.filter, [action.field]: action.value },
        };
    },
});
