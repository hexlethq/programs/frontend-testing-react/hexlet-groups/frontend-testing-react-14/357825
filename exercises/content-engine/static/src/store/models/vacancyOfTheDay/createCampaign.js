import { createReducer } from 'redux-create-reducer';
import {
    CAMPAIGN_DEVICE_TYPE_DESKTOP,
    CAMPAIGN_TYPE_COMMERCIAL,
    DEFAULT_CAMPAIGN_PERIOD_AFTER_NEXT_MONDAY,
} from 'constants/campaign';

const SET_DEVICE_TYPES_VOTD = 'SET_DEVICE_TYPES_VOTD';
const SET_PERIOD_VOTD = 'SET_PERIOD_VOTD';
const SET_REGIONS_VOTD = 'SET_REGIONS_VOTD';
const SET_PACKET_VOTD = 'SET_PACKET_VOTD';
const SET_CAMPAIGN_TYPE_VOTD = 'SET_CAMPAIGN_TYPE_VOTD';
const SET_VACANCY_ID_VOTD = 'SET_VACANCY_ID_VOTD';
const SET_VACANCY_NAME_VOTD = 'SET_VACANCY_NAME_VOTD';
const SET_COMMENT_VOTD = 'SET_COMMENT_VOTD';
const SET_BUSYNESS_VOTD = 'SET_BUSYNESS_VOTD';
const SET_CRM_URL_VOTD = 'SET_CRM_URL_VOTD';
const SET_CAMPAIGN_ID_VOTD = 'SET_CAMPAIGN_ID_VOTD';
const RESET_CAMPAIGN_INFO_VOTD = 'RESET_CAMPAIGN_INFO_VOTD';

export const setDeviceTypes = (deviceTypes) => ({
    type: SET_DEVICE_TYPES_VOTD,
    deviceTypes,
});

export const setPeriod = (period) => ({
    type: SET_PERIOD_VOTD,
    period,
});

export const setRegions = (regions) => ({
    type: SET_REGIONS_VOTD,
    regions,
});

export const setPacket = (packet) => ({
    type: SET_PACKET_VOTD,
    packet,
});

export const setCampaignType = (campaignType) => ({
    type: SET_CAMPAIGN_TYPE_VOTD,
    campaignType,
});

export const setVacancyId = (vacancyId) => ({
    type: SET_VACANCY_ID_VOTD,
    vacancyId,
});

export const setVacancyName = (vacancyName) => ({
    type: SET_VACANCY_NAME_VOTD,
    vacancyName,
});

export const setComment = (comment) => ({
    type: SET_COMMENT_VOTD,
    comment,
});

export const setBusyness = (busyness) => ({
    type: SET_BUSYNESS_VOTD,
    busyness,
});

export const setCrmUrl = (crmUrl) => ({
    type: SET_CRM_URL_VOTD,
    crmUrl,
});

export const setCampaignId = (campaignId) => ({
    type: SET_CAMPAIGN_ID_VOTD,
    campaignId,
});

export const resetCampaignInfo = () => ({
    type: RESET_CAMPAIGN_INFO_VOTD,
});

const INITIAL_STATE = {
    deviceTypes: [CAMPAIGN_DEVICE_TYPE_DESKTOP],
    period: DEFAULT_CAMPAIGN_PERIOD_AFTER_NEXT_MONDAY,
    regions: [],
    packet: '',
    campaignType: CAMPAIGN_TYPE_COMMERCIAL,
    comment: '',
    vacancyId: null,
    vacancyName: '',
    busyness: { data: [], checked: false },
    crmUrl: '',
    campaignId: null,
};

export default createReducer(INITIAL_STATE, {
    [SET_DEVICE_TYPES_VOTD](state, action) {
        return {
            ...state,
            deviceTypes: action.deviceTypes,
            busyness: { data: [], checked: false },
        };
    },
    [SET_PERIOD_VOTD](state, action) {
        return {
            ...state,
            period: action.period,
            busyness: { data: [], checked: false },
        };
    },
    [SET_REGIONS_VOTD](state, action) {
        return {
            ...state,
            regions: action.regions,
            busyness: { data: [], checked: false },
        };
    },
    [SET_PACKET_VOTD](state, action) {
        return {
            ...state,
            packet: action.packet,
            busyness: { data: [], checked: false },
        };
    },
    [SET_BUSYNESS_VOTD](state, action) {
        return {
            ...state,
            busyness: action.busyness,
        };
    },
    [SET_CRM_URL_VOTD](state, action) {
        return {
            ...state,
            crmUrl: action.crmUrl,
        };
    },
    [SET_CAMPAIGN_TYPE_VOTD](state, action) {
        return {
            ...state,
            campaignType: action.campaignType,
        };
    },
    [SET_VACANCY_ID_VOTD](state, action) {
        return {
            ...state,
            vacancyId: action.vacancyId,
        };
    },
    [SET_VACANCY_NAME_VOTD](state, action) {
        return {
            ...state,
            vacancyName: action.vacancyName,
        };
    },
    [SET_COMMENT_VOTD](state, action) {
        return {
            ...state,
            comment: action.comment,
        };
    },
    [SET_CAMPAIGN_ID_VOTD](state, action) {
        return {
            ...state,
            campaignId: action.campaignId,
        };
    },
    [RESET_CAMPAIGN_INFO_VOTD](state) {
        const { period, ...otherInitialState } = INITIAL_STATE;
        return { ...state, ...otherInitialState };
    },
});
