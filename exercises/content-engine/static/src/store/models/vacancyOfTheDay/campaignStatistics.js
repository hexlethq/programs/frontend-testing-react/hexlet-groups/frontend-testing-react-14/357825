import { createReducer } from 'redux-create-reducer';
import { DEFAULT_WEEK_PERIOD_BEFORE, STATISTICS_BY_DATE, INIT_SORT_INFO } from 'constants/campaign';

const SET_CAMPAIGN_STATISTICS_EMPLOYER_INFO_VOTD = 'SET_CAMPAIGN_STATISTICS_EMPLOYER_INFO_VOTD';
const SET_CAMPAIGN_STATISTICS_VACANCY_INFO_VOTD = 'SET_CAMPAIGN_STATISTICS_VACANCY_INFO_VOTD';
const SET_CAMPAIGN_STATISTICS_VOTD = 'SET_CAMPAIGN_STATISTICS_VOTD';
const SET_CAMPAIGN_STATISTICS_TYPE_VOTD = 'SET_CAMPAIGN_STATISTICS_TYPE_VOTD';
const SET_CAMPAIGN_STATISTICS_PERIOD_VOTD = 'SET_CAMPAIGN_STATISTICS_PERIOD_VOTD';
const SET_CAMPAIGN_STATISTICS_CHART_VOTD = 'SET_CAMPAIGN_STATISTICS_CHART_VOTD';
const SET_CAMPAIGN_TOTAL_STATISTICS_VOTD = 'SET_CAMPAIGN_TOTAL_STATISTICS_VOTD';
const SET_CAMPAIGN_STATISTICS_SORT_INFO_VOTD = 'SET_CAMPAIGN_STATISTICS_SORT_INFO_VOTD';

export const setCampaignStatisticsEmployerInfo = (employerInfo) => ({
    type: SET_CAMPAIGN_STATISTICS_EMPLOYER_INFO_VOTD,
    employerInfo,
});

export const setCampaignStatisticsVacancyInfo = (vacancyInfo) => ({
    type: SET_CAMPAIGN_STATISTICS_VACANCY_INFO_VOTD,
    vacancyInfo,
});

export const setCampaignStatistics = (statistics) => ({
    type: SET_CAMPAIGN_STATISTICS_VOTD,
    statistics,
});

export const setCampaignStatisticsType = (statisticsType) => ({
    type: SET_CAMPAIGN_STATISTICS_TYPE_VOTD,
    statisticsType,
});

export const setCampaignStatisticsPeriod = (period) => ({
    type: SET_CAMPAIGN_STATISTICS_PERIOD_VOTD,
    period,
});

export const setCampaignStatisticsChart = (chartData) => ({
    type: SET_CAMPAIGN_STATISTICS_CHART_VOTD,
    chartData,
});

export const setCampaignTotalStatistics = (totalStatistics) => ({
    type: SET_CAMPAIGN_TOTAL_STATISTICS_VOTD,
    totalStatistics,
});

export const setCampaignStatisticsSortInfo = (statisticsSortInfo) => ({
    type: SET_CAMPAIGN_STATISTICS_SORT_INFO_VOTD,
    statisticsSortInfo,
});

const INITIAL_STATE = {
    employerInfo: {},
    vacancyInfo: {},
    statistics: { data: [], loading: true },
    statisticsType: STATISTICS_BY_DATE,
    period: DEFAULT_WEEK_PERIOD_BEFORE,
    chartData: [],
    totalStatistics: {},
    statisticsSortInfo: INIT_SORT_INFO,
};

export default createReducer(INITIAL_STATE, {
    [SET_CAMPAIGN_STATISTICS_EMPLOYER_INFO_VOTD](state, action) {
        return {
            ...state,
            employerInfo: action.employerInfo,
        };
    },
    [SET_CAMPAIGN_STATISTICS_VACANCY_INFO_VOTD](state, action) {
        return {
            ...state,
            vacancyInfo: action.vacancyInfo,
        };
    },
    [SET_CAMPAIGN_STATISTICS_VOTD](state, action) {
        return {
            ...state,
            statistics: action.statistics,
        };
    },
    [SET_CAMPAIGN_STATISTICS_TYPE_VOTD](state, action) {
        return {
            ...state,
            statisticsType: action.statisticsType,
        };
    },
    [SET_CAMPAIGN_STATISTICS_PERIOD_VOTD](state, action) {
        return {
            ...state,
            period: action.period,
        };
    },
    [SET_CAMPAIGN_STATISTICS_CHART_VOTD](state, action) {
        return {
            ...state,
            chartData: action.chartData,
        };
    },
    [SET_CAMPAIGN_TOTAL_STATISTICS_VOTD](state, action) {
        return {
            ...state,
            totalStatistics: action.totalStatistics,
        };
    },
    [SET_CAMPAIGN_STATISTICS_SORT_INFO_VOTD](state, action) {
        return {
            ...state,
            statisticsSortInfo: action.statisticsSortInfo,
        };
    },
});
