import { createReducer } from 'redux-create-reducer';

const SET_USER_ID = 'SET_USER_ID';
const SET_USER_NAME = 'SET_USER_NAME';
const SET_USER_SUPER_ADMIN = 'SET_USER_SUPER_ADMIN';
const SET_USER_LOAD = 'SET_USER_LOAD';

export const setUserId = (userId) => ({
    type: SET_USER_ID,
    userId,
});

export const setUserName = (name) => ({
    type: SET_USER_NAME,
    name,
});

export const setUserSuperAdmin = (superAdmin) => ({
    type: SET_USER_SUPER_ADMIN,
    superAdmin,
});

export const setUserLoad = (isLoad) => ({
    type: SET_USER_LOAD,
    isLoad,
});

const INITIAL_STATE = {
    userId: null,
    userName: null,
    superAdmin: false,
    isLoad: true,
};

export default createReducer(INITIAL_STATE, {
    [SET_USER_ID](state, action) {
        return {
            ...state,
            userId: action.userId,
        };
    },
    [SET_USER_NAME](state, action) {
        return {
            ...state,
            name: action.name,
        };
    },
    [SET_USER_SUPER_ADMIN](state, action) {
        return {
            ...state,
            superAdmin: action.superAdmin,
        };
    },
    [SET_USER_LOAD](state, action) {
        return {
            ...state,
            isLoad: action.isLoad,
        };
    },
});
