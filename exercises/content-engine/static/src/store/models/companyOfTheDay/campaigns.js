import { createReducer } from 'redux-create-reducer';
import { DEFAULT_MONTH_PERIOD_AFTER, CAMPAIGN_STATUS_ALL, INIT_SORT_INFO } from 'constants/campaign';

const SET_CAMPAIGNS_COTD = 'SET_CAMPAIGNS_COTD';
const SET_CAMPAIGNS_SORT_INFO_COTD = 'SET_CAMPAIGNS_SORT_INFO_COTD';
const SET_CAMPAIGNS_PERIOD_COTD = 'SET_CAMPAIGNS_PERIOD_COTD';
const SET_CAMPAIGNS_CAMPAIGN_ID_COTD = 'SET_CAMPAIGNS_CAMPAIGN_ID_COTD';
const SET_CAMPAIGNS_EMPLOYER_ID_COTD = 'SET_CAMPAIGNS_EMPLOYER_ID_COTD';
const SET_CAMPAIGNS_REGIONS_COTD = 'SET_CAMPAIGNS_REGIONS_COTD';
const SET_CAMPAIGNS_EXACTLY_REGION_COTD = 'SET_CAMPAIGNS_EXACTLY_REGION_COTD';
const SET_CAMPAIGNS_DEVICE_TYPES_COTD = 'SET_CAMPAIGNS_DEVICE_TYPES_COTD';
const SET_CAMPAIGNS_STATUS_COTD = 'SET_CAMPAIGNS_STATUS_COTD';

export const setCampaigns = (campaigns) => ({
    type: SET_CAMPAIGNS_COTD,
    campaigns,
});

export const setCampaignsSortInfo = (campaignsSortInfo) => ({
    type: SET_CAMPAIGNS_SORT_INFO_COTD,
    campaignsSortInfo,
});

export const setCampaignsPeriod = (period) => ({
    type: SET_CAMPAIGNS_PERIOD_COTD,
    period,
});

export const setCampaignsCampaignId = (campaignId) => ({
    type: SET_CAMPAIGNS_CAMPAIGN_ID_COTD,
    campaignId,
});

export const setCampaignsEmployerId = (employerId) => ({
    type: SET_CAMPAIGNS_EMPLOYER_ID_COTD,
    employerId,
});

export const setCampaignsRegions = (regions) => ({
    type: SET_CAMPAIGNS_REGIONS_COTD,
    regions,
});

export const setCampaignsExactlyRegion = (exactlyRegion) => ({
    type: SET_CAMPAIGNS_EXACTLY_REGION_COTD,
    exactlyRegion,
});

export const setCampaignsDeviceTypes = (deviceTypes) => ({
    type: SET_CAMPAIGNS_DEVICE_TYPES_COTD,
    deviceTypes,
});

export const setCampaignsStatus = (status) => ({
    type: SET_CAMPAIGNS_STATUS_COTD,
    status,
});

const INITIAL_STATE = {
    campaigns: [],
    campaignsSortInfo: INIT_SORT_INFO,
    period: DEFAULT_MONTH_PERIOD_AFTER,
    campaignId: '',
    employerId: '',
    regions: [],
    exactlyRegion: false,
    deviceTypes: [],
    status: CAMPAIGN_STATUS_ALL,
};

export default createReducer(INITIAL_STATE, {
    [SET_CAMPAIGNS_COTD](state, action) {
        return {
            ...state,
            campaigns: action.campaigns,
        };
    },
    [SET_CAMPAIGNS_SORT_INFO_COTD](state, action) {
        return {
            ...state,
            campaignsSortInfo: action.campaignsSortInfo,
        };
    },
    [SET_CAMPAIGNS_PERIOD_COTD](state, action) {
        return {
            ...state,
            period: action.period,
        };
    },
    [SET_CAMPAIGNS_CAMPAIGN_ID_COTD](state, action) {
        return {
            ...state,
            campaignId: action.campaignId,
        };
    },
    [SET_CAMPAIGNS_EMPLOYER_ID_COTD](state, action) {
        return {
            ...state,
            employerId: action.employerId,
        };
    },
    [SET_CAMPAIGNS_REGIONS_COTD](state, action) {
        return {
            ...state,
            regions: action.regions,
        };
    },
    [SET_CAMPAIGNS_EXACTLY_REGION_COTD](state, action) {
        return {
            ...state,
            exactlyRegion: action.exactlyRegion,
        };
    },
    [SET_CAMPAIGNS_DEVICE_TYPES_COTD](state, action) {
        return {
            ...state,
            deviceTypes: action.deviceTypes,
        };
    },
    [SET_CAMPAIGNS_STATUS_COTD](state, action) {
        return {
            ...state,
            status: action.status,
        };
    },
});
