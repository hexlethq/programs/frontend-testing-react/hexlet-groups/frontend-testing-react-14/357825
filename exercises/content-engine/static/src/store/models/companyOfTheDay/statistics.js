import { createReducer } from 'redux-create-reducer';
import { DEFAULT_WEEK_PERIOD_BEFORE, STATISTICS_BY_DATE, INIT_SORT_INFO } from 'constants/campaign';

const SET_STATISTICS_EMPLOYER_ID_COTD = 'SET_STATISTICS_EMPLOYER_ID_COTD';
const SET_STATISTICS_EMPLOYER_NAME_COTD = 'SET_STATISTICS_EMPLOYER_NAME_COTD';
const SET_STATISTICS_COTD = 'SET_STATISTICS_COTD';
const SET_STATISTICS_TYPE_COTD = 'SET_STATISTICS_TYPE_COTD';
const SET_STATISTICS_PERIOD_COTD = 'SET_STATISTICS_PERIOD_COTD';
const SET_STATISTICS_CHART_COTD = 'SET_STATISTICS_CHART_COTD';
const SET_TOTAL_STATISTICS_COTD = 'SET_TOTAL_STATISTICS_COTD';
const SET_STATISTICS_SORT_INFO_COTD = 'SET_STATISTICS_SORT_INFO_COTD';

export const setStatisticsEmployerId = (employerId) => ({
    type: SET_STATISTICS_EMPLOYER_ID_COTD,
    employerId,
});

export const setStatisticsEmployerName = (employerName) => ({
    type: SET_STATISTICS_EMPLOYER_NAME_COTD,
    employerName,
});

export const setStatistics = (statistics) => ({
    type: SET_STATISTICS_COTD,
    statistics,
});

export const setStatisticsType = (statisticsType) => ({
    type: SET_STATISTICS_TYPE_COTD,
    statisticsType,
});

export const setStatisticsPeriod = (period) => ({
    type: SET_STATISTICS_PERIOD_COTD,
    period,
});

export const setStatisticsChart = (chartData) => ({
    type: SET_STATISTICS_CHART_COTD,
    chartData,
});

export const setTotalStatistics = (totalStatistics) => ({
    type: SET_TOTAL_STATISTICS_COTD,
    totalStatistics,
});

export const setStatisticsSortInfo = (statisticsSortInfo) => ({
    type: SET_STATISTICS_SORT_INFO_COTD,
    statisticsSortInfo,
});

const INITIAL_STATE = {
    employerId: null,
    employerName: null,
    statistics: { data: [], loading: true },
    statisticsType: STATISTICS_BY_DATE,
    period: DEFAULT_WEEK_PERIOD_BEFORE,
    chartData: [],
    totalStatistics: {},
    statisticsSortInfo: INIT_SORT_INFO,
};

export default createReducer(INITIAL_STATE, {
    [SET_STATISTICS_EMPLOYER_ID_COTD](state, action) {
        return {
            ...state,
            employerId: action.employerId,
        };
    },
    [SET_STATISTICS_EMPLOYER_NAME_COTD](state, action) {
        return {
            ...state,
            employerName: action.employerName,
        };
    },
    [SET_STATISTICS_COTD](state, action) {
        return {
            ...state,
            statistics: action.statistics,
        };
    },
    [SET_STATISTICS_TYPE_COTD](state, action) {
        return {
            ...state,
            statisticsType: action.statisticsType,
        };
    },
    [SET_STATISTICS_PERIOD_COTD](state, action) {
        return {
            ...state,
            period: action.period,
        };
    },
    [SET_STATISTICS_CHART_COTD](state, action) {
        return {
            ...state,
            chartData: action.chartData,
        };
    },
    [SET_TOTAL_STATISTICS_COTD](state, action) {
        return {
            ...state,
            totalStatistics: action.totalStatistics,
        };
    },
    [SET_STATISTICS_SORT_INFO_COTD](state, action) {
        return {
            ...state,
            statisticsSortInfo: action.statisticsSortInfo,
        };
    },
});
