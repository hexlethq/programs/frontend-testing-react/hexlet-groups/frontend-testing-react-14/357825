import { createReducer } from 'redux-create-reducer';
import { CAMPAIGN_DEVICE_TYPE_DESKTOP, DEFAULT_WEEK_PERIOD_AFTER } from 'constants/campaign';

const SET_EMPLOYER_ID_COTD = 'SET_EMPLOYER_ID_COTD';
const SET_EMPLOYER_NAME_COTD = 'SET_EMPLOYER_NAME_COTD';
const SET_CAMPAIGN_NAME_COTD = 'SET_CAMPAIGN_NAME_COTD';
const SET_DEVICE_TYPES_COTD = 'SET_DEVICE_TYPES_COTD';
const SET_LOGO_COTD = 'SET_LOGO_COTD';
const SET_REGIONS_COTD = 'SET_REGIONS_COTD';
const SET_ADDITIONAL_REGIONS_INFO_COTD = 'SET_ADDITIONAL_REGIONS_INFO_COTD';
const SET_PERIOD_COTD = 'SET_PERIOD_COTD';
const SET_BUSYNESS_COTD = 'SET_BUSYNESS_COTD';
const SET_COMMENT_COTD = 'SET_COMMENT_COTD';
const RESET_CAMPAIGN_INFO_COTD = 'RESET_CAMPAIGN_INFO_COTD';
const SET_CAMPAIGN_ID_COTD = 'SET_CAMPAIGN_ID_COTD';

export const setEmployerId = (employerId) => ({
    type: SET_EMPLOYER_ID_COTD,
    employerId,
});

export const setEmployerName = (employerName) => ({
    type: SET_EMPLOYER_NAME_COTD,
    employerName,
});

export const setCampaignName = (campaignName) => ({
    type: SET_CAMPAIGN_NAME_COTD,
    campaignName,
});

export const setDeviceTypes = (deviceTypes) => ({
    type: SET_DEVICE_TYPES_COTD,
    deviceTypes,
});

export const setLogo = (logo) => ({
    type: SET_LOGO_COTD,
    logo,
});

export const setRegions = (regions) => ({
    type: SET_REGIONS_COTD,
    regions,
});

export const setAdditionalRegionsInfo = (additionalRegionsInfo) => ({
    type: SET_ADDITIONAL_REGIONS_INFO_COTD,
    additionalRegionsInfo,
});

export const setPeriod = (period) => ({
    type: SET_PERIOD_COTD,
    period,
});

export const setBusyness = (busyness) => ({
    type: SET_BUSYNESS_COTD,
    busyness,
});

export const setComment = (comment) => ({
    type: SET_COMMENT_COTD,
    comment,
});

export const resetCampaignInfo = () => ({
    type: RESET_CAMPAIGN_INFO_COTD,
});

export const setCampaignId = (campaignId) => ({
    type: SET_CAMPAIGN_ID_COTD,
    campaignId,
});

const INITIAL_STATE = {
    employerId: null,
    employerName: '',
    campaignName: '',
    deviceTypes: [CAMPAIGN_DEVICE_TYPE_DESKTOP],
    logo: { data: {}, loading: false },
    regions: [],
    additionalRegionsInfo: { isRussia: false, excludedRegions: [] },
    period: DEFAULT_WEEK_PERIOD_AFTER,
    busyness: { data: [], checked: false },
    comment: '',
    campaignId: null,
};

export default createReducer(INITIAL_STATE, {
    [SET_EMPLOYER_ID_COTD](state, action) {
        return {
            ...state,
            employerId: action.employerId,
        };
    },
    [SET_EMPLOYER_NAME_COTD](state, action) {
        return {
            ...state,
            employerName: action.employerName,
        };
    },
    [SET_CAMPAIGN_NAME_COTD](state, action) {
        return {
            ...state,
            campaignName: action.campaignName,
        };
    },
    [SET_DEVICE_TYPES_COTD](state, action) {
        return {
            ...state,
            deviceTypes: action.deviceTypes,
            busyness: { data: [], checked: false },
        };
    },
    [SET_LOGO_COTD](state, action) {
        return {
            ...state,
            logo: action.logo,
        };
    },
    [SET_REGIONS_COTD](state, action) {
        return {
            ...state,
            regions: action.regions,
            busyness: { data: [], checked: false },
        };
    },
    [SET_ADDITIONAL_REGIONS_INFO_COTD](state, action) {
        return {
            ...state,
            additionalRegionsInfo: action.additionalRegionsInfo,
        };
    },
    [SET_PERIOD_COTD](state, action) {
        return {
            ...state,
            period: action.period,
            busyness: { data: [], checked: false },
        };
    },
    [SET_BUSYNESS_COTD](state, action) {
        return {
            ...state,
            busyness: action.busyness,
        };
    },
    [SET_COMMENT_COTD](state, action) {
        return {
            ...state,
            comment: action.comment,
        };
    },
    [RESET_CAMPAIGN_INFO_COTD](state) {
        const { period, ...otherInitialState } = INITIAL_STATE;
        return { ...state, ...otherInitialState };
    },
    [SET_CAMPAIGN_ID_COTD](state, action) {
        return {
            ...state,
            campaignId: action.campaignId,
        };
    },
});
