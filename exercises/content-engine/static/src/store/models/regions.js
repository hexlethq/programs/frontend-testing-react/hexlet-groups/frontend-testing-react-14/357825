import { createReducer } from 'redux-create-reducer';

const SET_ALL_REGIONS_DICT = 'SET_ALL_REGIONS_DICT';
const SET_ALL_PACKETS_DICT = 'SET_ALL_PACKETS_DICT';
const SET_ALL_REGIONS_COTD = 'SET_ALL_REGIONS_COTD';
const SET_ALL_PACKETS_VOTD = 'SET_ALL_PACKETS_VOTD';
const SET_ALL_REGIONS_VOTD = 'SET_ALL_REGIONS_VOTD';

export const setAllRegionsDictionary = (allRegionsDict) => ({
    type: SET_ALL_REGIONS_DICT,
    allRegionsDict,
});

export const setAllPacketsDictionary = (allPacketsDict) => ({
    type: SET_ALL_PACKETS_DICT,
    allPacketsDict,
});

export const setAllRegionsCOTD = (allRegionsCOTD) => ({
    type: SET_ALL_REGIONS_COTD,
    allRegionsCOTD,
});

export const setAllPacketsVOTD = (allPacketsVOTD) => ({
    type: SET_ALL_PACKETS_VOTD,
    allPacketsVOTD,
});

export const setAllRegionsVOTD = (allRegionsVOTD) => ({
    type: SET_ALL_REGIONS_VOTD,
    allRegionsVOTD,
});

const INITIAL_STATE = {
    allRegionsDict: {},
    allPacketsDict: {},
    allRegionsCOTD: [],
    allPacketsVOTD: [],
    allRegionsVOTD: [],
};

export default createReducer(INITIAL_STATE, {
    [SET_ALL_REGIONS_DICT](state, action) {
        return {
            ...state,
            allRegionsDict: action.allRegionsDict,
        };
    },
    [SET_ALL_PACKETS_DICT](state, action) {
        return {
            ...state,
            allPacketsDict: action.allPacketsDict,
        };
    },
    [SET_ALL_REGIONS_COTD](state, action) {
        return {
            ...state,
            allRegionsCOTD: action.allRegionsCOTD,
        };
    },
    [SET_ALL_PACKETS_VOTD](state, action) {
        return {
            ...state,
            allPacketsVOTD: action.allPacketsVOTD,
        };
    },
    [SET_ALL_REGIONS_VOTD](state, action) {
        return {
            ...state,
            allRegionsVOTD: action.allRegionsVOTD,
        };
    },
});
