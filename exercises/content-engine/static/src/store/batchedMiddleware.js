const BATCH = 'BATCHING_ACTIONS';

const batchActionCreator = (actions) => ({
    type: BATCH,
    payload: actions,
});

export const batchReducer = (rootReducer) => {
    return (state, action) => {
        if (action.type === BATCH) {
            return action.payload.reduce((state, action) => {
                try {
                    return rootReducer(state, action);
                } catch (e) {
                    return state;
                }
            }, state);
        }
        return rootReducer(state, action);
    };
};

export default ({ dispatch }) => (next) => (action) => {
    if (Array.isArray(action)) {
        return dispatch(batchActionCreator(action));
    }

    return next(action);
};
