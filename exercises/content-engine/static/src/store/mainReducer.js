import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';
import user from 'store/models/user';
import notifications from 'store/models/notifications';
import regions from 'store/models/regions';
import createCampaignCOTD from 'store/models/companyOfTheDay/createCampaign';
import campaignsCOTD from 'store/models/companyOfTheDay/campaigns';
import statisticsCOTD from 'store/models/companyOfTheDay/statistics';
import createCampaignVOTD from 'store/models/vacancyOfTheDay/createCampaign';
import campaignsVOTD from 'store/models/vacancyOfTheDay/campaigns';
import campaignInfoVOTD from 'store/models/vacancyOfTheDay/campaignInfo';
import campaignsModerationVOTD from 'store/models/vacancyOfTheDay/campaignsModeration';
import campaignStatisticsVOTD from 'store/models/vacancyOfTheDay/campaignStatistics';
import commonStatisticsVOTD from 'store/models/vacancyOfTheDay/commonStatistics';
import aggregatedStatisticsVOTD from 'store/models/vacancyOfTheDay/aggregatedStatistics';

const mainReducer = {
    user,
    notifications,
    regions,
    createCampaignCOTD,
    campaignsCOTD,
    statisticsCOTD,
    createCampaignVOTD,
    campaignsVOTD,
    campaignInfoVOTD,
    campaignsModerationVOTD,
    campaignStatisticsVOTD,
    commonStatisticsVOTD,
    aggregatedStatisticsVOTD,
};

const createMainReducer = (history) => combineReducers({ ...mainReducer, router: connectRouter(history) });

export default createMainReducer;
