import React from 'react';
import Column, { ColumnsWrapper, ColumnsRow } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import AggregatedStatisticsBody from 'components/VacancyOfTheDay/AggregatedStatistics';

const AggregatedStatistics = () => (
    <ColumnsWrapper>
        <ColumnsRow>
            <Column xs="4" s="8" m="12" l="16" container>
                <Gap top bottom>
                    <AggregatedStatisticsBody />
                </Gap>
            </Column>
        </ColumnsRow>
    </ColumnsWrapper>
);

export default AggregatedStatistics;
