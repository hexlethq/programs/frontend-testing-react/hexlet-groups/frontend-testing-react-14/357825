import React from 'react';
import { useSelector } from 'react-redux';
import Column, { ColumnsWrapper, ColumnsRow } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import CampaignsBody from 'components/VacancyOfTheDay/Campaigns';

const CampaignsModeration = () => {
    const superAdmin = useSelector((state) => state.user.superAdmin);

    return (
        <ColumnsWrapper>
            <ColumnsRow>
                <Column xs="4" s="8" m="12" l="16" container>
                    <Gap top bottom>
                        <CampaignsBody isModeration={superAdmin} />
                    </Gap>
                </Column>
            </ColumnsRow>
        </ColumnsWrapper>
    );
};

export default CampaignsModeration;
