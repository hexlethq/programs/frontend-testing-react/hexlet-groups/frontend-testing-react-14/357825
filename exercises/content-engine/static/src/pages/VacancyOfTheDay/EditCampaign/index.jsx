import React from 'react';
import Column, { ColumnsWrapper, ColumnsRow } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import EditCampaignBody from 'components/VacancyOfTheDay/CreateCampaign';

const EditCampaign = () => (
    <ColumnsWrapper>
        <ColumnsRow>
            <Column xs="4" s="8" m="12" l="16" container>
                <Gap top bottom>
                    <EditCampaignBody isEdit />
                </Gap>
            </Column>
        </ColumnsRow>
    </ColumnsWrapper>
);

export default EditCampaign;
