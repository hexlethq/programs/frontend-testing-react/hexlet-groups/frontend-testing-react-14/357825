import React from 'react';
import Column, { ColumnsWrapper, ColumnsRow } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import MainMenuBody from 'components/MainMenu';

const MainMenu = () => (
    <ColumnsWrapper>
        <ColumnsRow>
            <Column xs="4" s="8" m="12" l="16" container>
                <Gap top bottom>
                    <MainMenuBody />
                </Gap>
            </Column>
        </ColumnsRow>
    </ColumnsWrapper>
);

export default MainMenu;
