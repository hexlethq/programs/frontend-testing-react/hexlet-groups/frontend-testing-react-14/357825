import React from 'react';
import Column, { ColumnsWrapper, ColumnsRow } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import Header from 'bloko/blocks/header';
import trl from 'modules/translation';

const Forbidden = () => (
    <ColumnsWrapper>
        <ColumnsRow>
            <Column xs="4" s="8" m="12" l="16">
                <Gap top bottom>
                    <Header>{trl('forbidden.title')}</Header>
                    <p>{trl('forbidden.text')}</p>
                </Gap>
            </Column>
        </ColumnsRow>
    </ColumnsWrapper>
);

export default Forbidden;
