import React from 'react';
import Column, { ColumnsWrapper, ColumnsRow } from 'bloko/blocks/column';
import Gap from 'bloko/blocks/gap';
import Loader from 'components/common/Loader';

const EmptyPage = () => (
    <ColumnsWrapper>
        <ColumnsRow>
            <Column xs="4" s="8" m="12" l="16">
                <Gap top>
                    <Loader />
                </Gap>
            </Column>
        </ColumnsRow>
    </ColumnsWrapper>
);

export default EmptyPage;
