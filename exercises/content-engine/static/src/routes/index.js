import NotFound from 'pages/System/NotFound';
import Forbidden from 'pages/System/Forbidden';
import ServerError from 'pages/System/ServerError';

import MainMenu from 'pages/MainMenu';

import CampaignsCOTD from 'pages/CompanyOfTheDay/Campaigns';
import StatisticsCOTD from 'pages/CompanyOfTheDay/Statistics';
import CreateCampaignCOTD from 'pages/CompanyOfTheDay/CreateCampaign';
import EditCampaignCOTD from 'pages/CompanyOfTheDay/EditCampaign';

import CampaignsVOTD from 'pages/VacancyOfTheDay/Campaigns';
import CreateCampaignVOTD from 'pages/VacancyOfTheDay/CreateCampaign';
import CampaignInfo from 'pages/VacancyOfTheDay/CampaignInfo';
import EditCampaignVOTD from 'pages/VacancyOfTheDay/EditCampaign';
import CampaignsModerationVOTD from 'pages/VacancyOfTheDay/CampaignsModeration';
import CampaignStatisticsVOTD from 'pages/VacancyOfTheDay/CampaignStatistics';
import CommonStatisticsVOTD from 'pages/VacancyOfTheDay/CommonStatistics';
import AggregatedStatisticsVOTD from 'pages/VacancyOfTheDay/AggregatedStatistics';

export const NotFoundRoute = {
    component: NotFound,
    path: '/not-found/',
};

export const systemRoutes = [
    {
        component: Forbidden,
        path: '/forbidden/',
    },
    {
        component: ServerError,
        path: '/error/',
    },
    NotFoundRoute,
];

export default [
    {
        component: MainMenu,
        path: '/',
        exact: true,
    },
    {
        component: CampaignsCOTD,
        path: '/company-of-the-day/campaigns/',
        exact: true,
    },
    {
        component: StatisticsCOTD,
        path: '/company-of-the-day/statistics/:campaignId(\\d+)/:type',
        exact: true,
    },
    {
        component: CreateCampaignCOTD,
        path: '/company-of-the-day/create/',
        exact: true,
    },
    {
        component: EditCampaignCOTD,
        path: '/company-of-the-day/edit/:campaignId(\\d+)',
        exact: true,
    },
    {
        component: CampaignsVOTD,
        path: '/vacancy-of-the-day/campaigns/',
        exact: true,
    },
    {
        component: CreateCampaignVOTD,
        path: '/vacancy-of-the-day/create/',
        exact: true,
    },
    {
        component: CampaignInfo,
        path: '/vacancy-of-the-day/campaign/:campaignId(\\d+)',
        exact: true,
    },
    {
        component: EditCampaignVOTD,
        path: '/vacancy-of-the-day/edit/:campaignId(\\d+)',
        exact: true,
    },
    {
        component: CampaignsModerationVOTD,
        path: '/vacancy-of-the-day/campaigns/moderation/',
        exact: true,
    },
    {
        component: CampaignStatisticsVOTD,
        path: '/vacancy-of-the-day/statistics/:campaignId(\\d+)/:type',
        exact: true,
    },
    {
        component: CommonStatisticsVOTD,
        path: '/vacancy-of-the-day/statistics/',
        exact: true,
    },
    {
        component: AggregatedStatisticsVOTD,
        path: '/vacancy-of-the-day/aggregated-statistics/',
        exact: true,
    },
];
