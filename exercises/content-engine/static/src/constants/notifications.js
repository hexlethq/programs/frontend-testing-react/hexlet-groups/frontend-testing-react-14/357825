import {
    CAMPAIGN_PERIOD_END_ERROR_MESSAGE,
    CAMPAIGN_PERIOD_ERROR_MESSAGE,
    CAMPAIGN_PERIOD_START_ERROR_MESSAGE,
} from 'constants/campaign';
import ServerErrorNotification from 'components/common/Notifications/ServerError';
import LogoLoadSuccessNotification from 'components/common/Notifications/CreateCampaign/LogoLoadSuccess';
import LogoLoadErrorNotification from 'components/common/Notifications/CreateCampaign/LogoLoadError';
import CampaignBusynessDataErrorNotification from 'components/common/Notifications/CreateCampaign/CampaignBusynessDataError';
import CampaignSaveSuccessNotification from 'components/common/Notifications/CreateCampaign/CampaignSaveSuccess';
import CampaignSaveDataErrorNotification from 'components/common/Notifications/CreateCampaign/CampaignSaveDataError';
import CampaignPeriodErrorNotification from 'components/common/Notifications/CreateCampaign/CampaignPeriodError';
import CampaignPeriodStartErrorNotification from 'components/common/Notifications/CreateCampaign/CampaignPeriodStartError';
import CampaignPeriodEndErrorNotification from 'components/common/Notifications/CreateCampaign/CampaignPeriodEndError';
import CampaignSaveBusyErrorNotification from 'components/common/Notifications/CreateCampaign/CampaignSaveBusyError';
import CampaignEditSuccessNotification from 'components/common/Notifications/CreateCampaign/CampaignEditSuccess';
import CampaignUpdateStatusSuccessNotification from 'components/common/Notifications/Campaigns/CampaignUpdateStatusSuccess';
import CampaignDeleteSuccessNotification from 'components/common/Notifications/Campaigns/CampaignDeleteSuccess';
import PaymentUpdateStatusSuccessNotification from 'components/common/Notifications/CampaignInfo/PaymentUpdateStatusSuccess';
import ScreenshotUploadSuccessNotification from 'components/common/Notifications/CampaignInfo/ScreenshotUploadSuccess';
import CampaignModerationSuccessNotification from 'components/common/Notifications/CampaignInfo/CampaignModerationSuccess';
import ScreenshotDeleteSuccessNotification from 'components/common/Notifications/CampaignInfo/ScreenshotDeleteSuccess';
import CampaignModerationVacancyErrorNotification from 'components/common/Notifications/CampaignInfo/CampaignModerationVacancyError';
import IncorrectFieldsErrorNotification from 'components/common/Notifications/IncorrectFieldsError';

export const SERVER_ERROR = 'SERVER_ERROR';
export const LOGO_LOAD_SUCCESS = 'LOGO_LOAD_SUCCESS';
export const LOGO_LOAD_ERROR = 'LOGO_LOAD_ERROR';
export const CAMPAIGN_BUSYNESS_DATA_ERROR = 'CAMPAIGN_BUSYNESS_DATA_ERROR';
export const CAMPAIGN_SAVE_SUCCESS = 'CAMPAIGN_SAVE_SUCCESS';
export const CAMPAIGN_EDIT_SUCCESS = 'CAMPAIGN_EDIT_SUCCESS';
export const CAMPAIGN_SAVE_DATA_ERROR = 'CAMPAIGN_SAVE_DATA_ERROR';
export const CAMPAIGN_PERIOD_ERROR = 'CAMPAIGN_PERIOD_ERROR';
export const CAMPAIGN_PERIOD_START_ERROR = 'CAMPAIGN_PERIOD_START_ERROR';
export const CAMPAIGN_PERIOD_END_ERROR = 'CAMPAIGN_PERIOD_END_ERROR';
export const CAMPAIGN_SAVE_BUSY_ERROR = 'CAMPAIGN_SAVE_BUSY_ERROR';
export const CAMPAIGN_NOTIFICATION_ERROR = {
    [CAMPAIGN_PERIOD_ERROR_MESSAGE]: CAMPAIGN_PERIOD_ERROR,
    [CAMPAIGN_PERIOD_START_ERROR_MESSAGE]: CAMPAIGN_PERIOD_START_ERROR,
    [CAMPAIGN_PERIOD_END_ERROR_MESSAGE]: CAMPAIGN_PERIOD_END_ERROR,
};
export const CAMPAIGN_UPDATE_STATUS_SUCCESS = 'CAMPAIGN_UPDATE_STATUS_SUCCESS';
export const CAMPAIGN_DELETE_SUCCESS = 'CAMPAIGN_DELETE_SUCCESS';
export const PAYMENT_UPDATE_STATUS_SUCCESS = 'PAYMENT_UPDATE_STATUS_SUCCESS';
export const SCREENSHOT_UPLOAD_SUCCESS = 'SCREENSHOT_UPLOAD_SUCCESS';
export const SCREENSHOT_DELETE_SUCCESS = 'SCREENSHOT_DELETE_SUCCESS';
export const CAMPAIGN_MODERATION_SUCCESS = 'CAMPAIGN_MODERATION_SUCCESS';
export const CAMPAIGN_MODERATION_VACANCY_ERROR = 'CAMPAIGN_MODERATION_VACANCY_ERROR';
export const INCORRECT_FIELDS_ERROR = 'INCORRECT_FIELDS_ERROR';

export default {
    [SERVER_ERROR]: {
        Element: ServerErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [LOGO_LOAD_SUCCESS]: {
        Element: LogoLoadSuccessNotification,
        kind: 'ok',
        autoClose: true,
    },
    [LOGO_LOAD_ERROR]: {
        Element: LogoLoadErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [CAMPAIGN_BUSYNESS_DATA_ERROR]: {
        Element: CampaignBusynessDataErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [CAMPAIGN_SAVE_SUCCESS]: {
        Element: CampaignSaveSuccessNotification,
        kind: 'ok',
        autoClose: true,
    },
    [CAMPAIGN_SAVE_DATA_ERROR]: {
        Element: CampaignSaveDataErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [CAMPAIGN_PERIOD_ERROR]: {
        Element: CampaignPeriodErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [CAMPAIGN_PERIOD_START_ERROR]: {
        Element: CampaignPeriodStartErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [CAMPAIGN_PERIOD_END_ERROR]: {
        Element: CampaignPeriodEndErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [CAMPAIGN_SAVE_BUSY_ERROR]: {
        Element: CampaignSaveBusyErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [CAMPAIGN_EDIT_SUCCESS]: {
        Element: CampaignEditSuccessNotification,
        kind: 'ok',
        autoClose: true,
    },
    [CAMPAIGN_UPDATE_STATUS_SUCCESS]: {
        Element: CampaignUpdateStatusSuccessNotification,
        kind: 'ok',
        autoClose: true,
    },
    [CAMPAIGN_DELETE_SUCCESS]: {
        Element: CampaignDeleteSuccessNotification,
        kind: 'delete',
        autoClose: true,
    },
    [PAYMENT_UPDATE_STATUS_SUCCESS]: {
        Element: PaymentUpdateStatusSuccessNotification,
        kind: 'ok',
        autoClose: true,
    },
    [SCREENSHOT_UPLOAD_SUCCESS]: {
        Element: ScreenshotUploadSuccessNotification,
        kind: 'ok',
        autoClose: true,
    },
    [SCREENSHOT_DELETE_SUCCESS]: {
        Element: ScreenshotDeleteSuccessNotification,
        kind: 'delete',
        autoClose: true,
    },
    [CAMPAIGN_MODERATION_SUCCESS]: {
        Element: CampaignModerationSuccessNotification,
        kind: 'ok',
        autoClose: true,
    },
    [CAMPAIGN_MODERATION_VACANCY_ERROR]: {
        Element: CampaignModerationVacancyErrorNotification,
        kind: 'error',
        autoClose: true,
    },
    [INCORRECT_FIELDS_ERROR]: {
        Element: IncorrectFieldsErrorNotification,
        kind: 'error',
        autoClose: true,
    },
};
