import { addWeeks, subWeeks, addMonths, startOfWeek, addDays, subMonths } from 'date-fns';
import { SORT_ASCENDING } from 'constants/common';

export const VACANCY_OF_THE_DAY = 'vacancy-of-the-day';
export const COMPANY_OF_THE_DAY = 'company-of-the-day';
export const ANYTHING_OF_THE_DAY = [VACANCY_OF_THE_DAY, COMPANY_OF_THE_DAY];

export const URL_VACANCY_OF_THE_DAY = 'vacancy_of_the_day';
export const URL_COMPANY_OF_THE_DAY = 'work_in_company';

export const VACANCY_OF_THE_DAY_ID = 'vacancyOfTheDayId';
export const COMPANY_OF_THE_DAY_ID = 'workInCompanyId';
export const ANYTHING_OF_THE_DAY_ID = {
    [VACANCY_OF_THE_DAY]: VACANCY_OF_THE_DAY_ID,
    [COMPANY_OF_THE_DAY]: COMPANY_OF_THE_DAY_ID,
};

export const CAMPAIGN_DEVICE_TYPE_DESKTOP = 'DESKTOP';
export const CAMPAIGN_DEVICE_TYPE_MOBILE = 'MOBILE';
export const CAMPAIGN_DEVICE_TYPES = [CAMPAIGN_DEVICE_TYPE_DESKTOP, CAMPAIGN_DEVICE_TYPE_MOBILE];

export const MOSCOW_REGION = ['2019', '1'];
export const ALL_RUSSIA = 'ALL_RUSSIA';
export const ALL_RUSSIA_WITHOUT_MOSCOW = 'ALL_RUSSIA_WITHOUT_MOSCOW';
export const SPECIAL_REGIONS = [ALL_RUSSIA, ALL_RUSSIA_WITHOUT_MOSCOW];

export const CAMPAIGN_BUSY = 'BUSY';

export const CAMPAIGN_PERIOD_ERROR_MESSAGE = 'Date end before date start';
export const CAMPAIGN_PERIOD_START_ERROR_MESSAGE = 'Start day must be monday';
export const CAMPAIGN_PERIOD_END_ERROR_MESSAGE = 'End day must be sunday';

export const CAMPAIGNS_LIST_ID = 'id';
export const CAMPAIGNS_LIST_EMPLOYER = 'employer';
export const CAMPAIGNS_LIST_PERIOD_START = 'dateFrom';
export const CAMPAIGNS_LIST_PERIOD_END = 'dateTo';
export const CAMPAIGNS_LIST_REGIONS = 'regionList';
export const CAMPAIGNS_LIST_DEVICE_TYPES = 'deviceTypes';
export const CAMPAIGNS_LIST_MANAGER = 'managerName';
export const CAMPAIGNS_LIST_COMMENT = 'comment';
export const CAMPAIGNS_LIST_CAMPAIGN_TYPE = 'campaignType';
export const CAMPAIGNS_LIST_PAYMENT_STATUS = 'paymentStatus';
export const CAMPAIGNS_LIST_CAMPAIGN_STATUS = 'campaignStatus';
export const CAMPAIGNS_LIST_VACANCY = 'vacancy';
export const CAMPAIGNS_LIST_CRM_URL = 'crmUrl';
export const CAMPAIGNS_LIST_PACKET = 'packet';

export const COTD_CAMPAIGNS_LIST_TABLE_HEADERS = [
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_COMMENT,
];

export const VOTD_CAMPAIGNS_LIST_TABLE_HEADERS = [
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_VACANCY,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_PAYMENT_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_STATUS,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    CAMPAIGNS_LIST_CRM_URL,
    CAMPAIGNS_LIST_COMMENT,
];

export const VOTD_CAMPAIGNS_LIST_DEFAULT_TABLE_HEADERS = [
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_VACANCY,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_PAYMENT_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_STATUS,
    CAMPAIGNS_LIST_MANAGER,
];

export const VOTD_CAMPAIGNS_LIST_SORT_HEADERS = [
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_VACANCY,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_PAYMENT_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_STATUS,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    CAMPAIGNS_LIST_CRM_URL,
];

export const COTD_CAMPAIGNS_LIST_SORT_HEADERS = [
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_MANAGER,
];

export const INIT_SORT_INFO = { head: '', type: SORT_ASCENDING };

export const DEFAULT_WEEK_PERIOD_AFTER = { start: new Date(), end: addWeeks(new Date(), 1) };
export const DEFAULT_WEEK_PERIOD_BEFORE = { start: subWeeks(new Date(), 1), end: new Date() };
export const DEFAULT_MONTH_PERIOD_AFTER = { start: new Date(), end: addMonths(new Date(), 1) };
export const DEFAULT_MONTH_PERIOD_BEFORE = { start: subMonths(new Date(), 1), end: new Date() };

const NEXT_MONDAY = startOfWeek(addWeeks(new Date(), 1), { weekStartsOn: 1 });
export const DEFAULT_CAMPAIGN_PERIOD_AFTER_NEXT_MONDAY = {
    start: NEXT_MONDAY,
    end: addDays(NEXT_MONDAY, 6),
};

export const STATISTICS_BY_DATE = 'date';
export const STATISTICS_BY_REGION = 'region';

export const STATISTICS_VIEWS = 'views';
export const STATISTICS_CLICKS = 'clicks';
export const STATISTICS_CTR = 'ctr';
export const STATISTICS_DESKTOP = 'desktop';
export const STATISTICS_MOBILE = 'mobile';
export const STATISTICS_TOTAL = 'total';
export const STATISTICS_TABLE_FIRST_HEADERS = [STATISTICS_VIEWS, STATISTICS_CLICKS, STATISTICS_CTR];
export const STATISTICS_TABLE_SECOND_HEADERS_FULL = [STATISTICS_DESKTOP, STATISTICS_MOBILE, STATISTICS_TOTAL];
export const STATISTICS_TABLE_SECOND_HEADERS_SHORT = [STATISTICS_TOTAL];
const STATISTICS_TABLE_HEADERS = [];
STATISTICS_TABLE_FIRST_HEADERS.forEach((firstHead) => {
    STATISTICS_TABLE_SECOND_HEADERS_FULL.forEach((secondHead) => {
        STATISTICS_TABLE_HEADERS.push(`${firstHead}_${secondHead}`);
    });
});

export const CAMPAIGN_STATUS_NEW = 'NEW';
export const CAMPAIGN_STATUS_APPROVED = 'APPROVED';
export const CAMPAIGN_STATUS_REJECTED = 'REJECTED';
export const CAMPAIGN_STATUS_ON_MODERATION = 'ON_MODERATION';
export const CAMPAIGN_STATUS_PAUSED = 'PAUSED';
export const CAMPAIGN_STATUS_ACTIVE = 'ACTIVE';
export const CAMPAIGN_STATUS_ALL = 'ALL';
export const COTD_CAMPAIGN_STATUSES = [CAMPAIGN_STATUS_ALL, CAMPAIGN_STATUS_ACTIVE, CAMPAIGN_STATUS_PAUSED];
export const VOTD_CAMPAIGN_STATUSES = [
    CAMPAIGN_STATUS_NEW,
    CAMPAIGN_STATUS_APPROVED,
    CAMPAIGN_STATUS_REJECTED,
    CAMPAIGN_STATUS_ON_MODERATION,
    CAMPAIGN_STATUS_PAUSED,
];

export const CAMPAIGN_TYPE_COMMERCIAL = 'COMMERCIAL';
export const CAMPAIGN_TYPE_NOT_COMMERCIAL = 'NOT_COMMERCIAL';
export const CAMPAIGN_TYPES = [CAMPAIGN_TYPE_COMMERCIAL, CAMPAIGN_TYPE_NOT_COMMERCIAL];

export const ENTITY_EMPLOYER = 'employer';
export const ENTITY_VACANCY = 'vacancy';

export const CAMPAIGN_PAYMENT_STATUS_PAYED = 'PAYED';
export const CAMPAIGN_PAYMENT_STATUS_NOT_PAYED = 'NOT_PAYED';
export const CAMPAIGN_PAYMENT_STATUS_POST_PAYED = 'POST_PAYED';
export const CAMPAIGN_PAYMENT_STATUSES = [
    CAMPAIGN_PAYMENT_STATUS_PAYED,
    CAMPAIGN_PAYMENT_STATUS_NOT_PAYED,
    CAMPAIGN_PAYMENT_STATUS_POST_PAYED,
];

export const FILTER_NUMBER = 'number';
export const FILTER_TEXT = 'text';
export const FILTER_CHECKBOX = 'checkbox';
export const FILTER_RADIO = 'radio';

export const FILTER_PERIOD = 'period';
export const FILTER_PACKETS = 'packets';
export const FILTER_PACKET = 'packet';
export const FILTER_REGIONS = 'regionIds';
export const FILTER_EXACTLY_REGION = 'exactlyRegionFilter';
export const FILTER_COTD_ID = 'workInCompanyId';
export const FILTER_VOTD_ID = 'vacancyOfTheDayId';
export const FILTER_EMPLOYER_ID = 'employerId';
export const FILTER_EMPLOYER_NAME = 'employerName';
export const FILTER_VACANCY_ID = 'vacancyId';
export const FILTER_VACANCY_NAME = 'vacancyName';
export const FILTER_CREATOR_ID = 'creatorId';
export const FILTER_CREATOR_NAME = 'creatorName';
export const FILTER_CRM_URL = 'crmUrl';
export const FILTER_CAMPAIGN_TYPES = 'campaignTypes';
export const FILTER_CAMPAIGN_STATUSES = 'campaignStatuses';
export const FILTER_DEVICE_TYPES = 'deviceTypes';
export const FILTER_PAYMENT_STATUSES = 'paymentStatuses';

export const VOTD_CAMPAIGNS_FILTER_FIELDS_CONFIG = [
    {
        field: FILTER_PERIOD,
        type: FILTER_PERIOD,
    },
    {
        field: FILTER_VOTD_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_EMPLOYER_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_EMPLOYER_NAME,
        type: FILTER_TEXT,
    },
    {
        field: FILTER_VACANCY_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_VACANCY_NAME,
        type: FILTER_TEXT,
    },
    {
        field: FILTER_CREATOR_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_CREATOR_NAME,
        type: FILTER_TEXT,
    },
    {
        field: FILTER_CRM_URL,
        type: FILTER_TEXT,
    },
    {
        field: FILTER_CAMPAIGN_TYPES,
        type: FILTER_CHECKBOX,
        items: CAMPAIGN_TYPES,
    },
    {
        field: FILTER_DEVICE_TYPES,
        type: FILTER_CHECKBOX,
        items: CAMPAIGN_DEVICE_TYPES,
    },
    {
        field: FILTER_PAYMENT_STATUSES,
        type: FILTER_CHECKBOX,
        items: CAMPAIGN_PAYMENT_STATUSES,
    },
    {
        field: FILTER_CAMPAIGN_STATUSES,
        type: FILTER_CHECKBOX,
        items: VOTD_CAMPAIGN_STATUSES,
    },
    {
        field: FILTER_REGIONS,
        type: FILTER_REGIONS,
        params: { type: FILTER_CHECKBOX, isExactlyRegion: true },
    },
];

export const VOTD_CAMPAIGN_INFO_ROWS = [
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_VACANCY,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_PAYMENT_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_STATUS,
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    CAMPAIGNS_LIST_CRM_URL,
    CAMPAIGNS_LIST_MANAGER,
];

export const ACCEPT_IMAGE_TYPES = ['image/jpeg', 'image/png'];

export const CAMPAIGN_ACTION_APPROVE = 'approve';
export const CAMPAIGN_ACTION_REJECT = 'reject';

export const VOTD_COMMON_STATISTICS_HEADERS = [
    CAMPAIGNS_LIST_ID,
    CAMPAIGNS_LIST_EMPLOYER,
    CAMPAIGNS_LIST_VACANCY,
    CAMPAIGNS_LIST_PERIOD_START,
    CAMPAIGNS_LIST_PERIOD_END,
    CAMPAIGNS_LIST_PACKET,
    CAMPAIGNS_LIST_REGIONS,
    CAMPAIGNS_LIST_DEVICE_TYPES,
    CAMPAIGNS_LIST_MANAGER,
    CAMPAIGNS_LIST_CAMPAIGN_TYPE,
    STATISTICS_VIEWS,
    STATISTICS_CLICKS,
    STATISTICS_CTR,
];

export const VOTD_COMMON_STATISTICS_FILTER_FIELDS_CONFIG = [
    {
        field: FILTER_PERIOD,
        type: FILTER_PERIOD,
    },
    {
        field: FILTER_VOTD_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_EMPLOYER_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_EMPLOYER_NAME,
        type: FILTER_TEXT,
    },
    {
        field: FILTER_VACANCY_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_VACANCY_NAME,
        type: FILTER_TEXT,
    },
    {
        field: FILTER_CREATOR_ID,
        type: FILTER_NUMBER,
    },
    {
        field: FILTER_CREATOR_NAME,
        type: FILTER_TEXT,
    },
    {
        field: FILTER_CAMPAIGN_TYPES,
        type: FILTER_CHECKBOX,
        items: CAMPAIGN_TYPES,
    },
    {
        field: FILTER_DEVICE_TYPES,
        type: FILTER_CHECKBOX,
        items: CAMPAIGN_DEVICE_TYPES,
    },
    {
        field: FILTER_REGIONS,
        type: FILTER_REGIONS,
        params: { type: FILTER_CHECKBOX, isExactlyRegion: true },
    },
];

export const VOTD_AGGREGATED_STATISTICS_HEADERS = [STATISTICS_VIEWS, STATISTICS_CLICKS];

export const VOTD_AGGREGATED_STATISTICS_FILTER_FIELDS_CONFIG = [
    {
        field: FILTER_PERIOD,
        type: FILTER_PERIOD,
    },
    {
        field: FILTER_DEVICE_TYPES,
        type: FILTER_CHECKBOX,
        items: CAMPAIGN_DEVICE_TYPES,
    },
    {
        field: FILTER_REGIONS,
        type: FILTER_REGIONS,
        params: { type: FILTER_RADIO, isExactlyRegion: false },
    },
];
