import { SORT_ASCENDING, SORT_DESCENDING } from 'constants/common';

export const sortingByNumber = (firstNumber, secondNumber, type) => {
    if (type === SORT_ASCENDING) {
        return firstNumber - secondNumber;
    }
    return secondNumber - firstNumber;
};

export const sortingByString = (firstString, secondString, type) => {
    const resultSort = type === SORT_ASCENDING ? 1 : -1;
    if (firstString <= secondString) {
        return -resultSort;
    }
    return resultSort;
};

export const sortingByDate = (firstDate, secondDate, type) => {
    const resultSort = type === SORT_ASCENDING ? 1 : -1;
    if (firstDate.getTime() <= secondDate.getTime()) {
        return -resultSort;
    }
    return resultSort;
};

/**
 * Обновляет сортировку по столбцам в таблице
 * @param {String} head
 * @param {Object} sortInfo
 * @param {Function} setSortInfo
 */
export const updateSortInfo = (head, sortInfo, setSortInfo) => {
    setSortInfo({ head, type: sortInfo.type === SORT_ASCENDING ? SORT_DESCENDING : SORT_ASCENDING });
};
