import fetcher from 'modules/fetcher';
import { setAllRegionsDictionary } from 'store/models/regions';

export default () => {
    return (dispatch) => {
        fetcher.get('/api/v1/regions/translation_dict').then((data) => {
            dispatch(setAllRegionsDictionary(data));
        });
    };
};
