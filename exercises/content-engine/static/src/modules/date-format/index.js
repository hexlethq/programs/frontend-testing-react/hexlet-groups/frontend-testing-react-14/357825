import { format } from 'date-fns';
import RU from 'date-fns/locale/ru';

export default function (date, formatStr) {
    return format(date, formatStr, {
        locale: RU,
    });
}

export const createViewDate = (date) => {
    const checkZero = (number) => (number < 10 ? `0${number}` : number);
    return `${checkZero(date[2])}.${checkZero(date[1])}.${date[0]}`;
};
