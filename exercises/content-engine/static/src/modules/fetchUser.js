import { push } from 'connected-react-router';
import fetcher from 'modules/fetcher';
import { HH_LOGIN_LINK } from 'constants/links';
import { setUserId, setUserName, setUserSuperAdmin, setUserLoad } from 'store/models/user';

export default function () {
    return (dispatch) => {
        fetcher.get('/api/v1/user/').then(
            ({ id, name, superAdmin }) => {
                dispatch([setUserId(id), setUserName(name), setUserSuperAdmin(superAdmin), setUserLoad(false)]);
            },
            (err) => {
                const status = err?.response?.status;
                if (status === 401) {
                    window.location.href = HH_LOGIN_LINK;
                }
                if (status === 403) {
                    dispatch(push('/forbidden/'));
                }
                if (status >= 500) {
                    dispatch(push('/error/'));
                }
                dispatch(setUserLoad(false));
            }
        );
    };
}
