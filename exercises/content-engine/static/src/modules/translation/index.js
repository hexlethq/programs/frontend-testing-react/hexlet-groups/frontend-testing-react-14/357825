import trlsRu from 'modules/translation/ru';

function formatterStr(str, list) {
    return str.replace(/\{(\d+)\}/g, (match, value) => (list[value] || list[value] === 0 ? list[value] : ''));
}

export default function getTrl(key, list = []) {
    return trlsRu[key] ? formatterStr(trlsRu[key], list) : null;
}
