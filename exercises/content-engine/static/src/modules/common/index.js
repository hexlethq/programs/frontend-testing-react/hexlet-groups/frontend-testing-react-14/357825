import { parseISO } from 'date-fns';
import format from 'modules/date-format';
import { VIEW_FORMAT } from 'constants/date-formats';
import {
    STATISTICS_BY_DATE,
    STATISTICS_TABLE_FIRST_HEADERS,
    STATISTICS_TABLE_SECOND_HEADERS_FULL,
} from 'constants/campaign';

/**
 * Определяет добавлять или удалять элемент из массива выбранных значений чекбоксов
 * @param {Object} event
 * @param {Array} array
 * @return {Array}
 */
export const addOrDeleteInArray = (event, array) => {
    const result = [...array];
    if (event.target.checked) {
        result.push(event.target.value);
    } else {
        result.splice(result.indexOf(event.target.value), 1);
    }
    return result;
};

/**
 * Делает первую букву заглавной
 * @param {String} string
 * @return {String}
 */
export const capitalize = (string) => string[0].toUpperCase() + string.substring(1);

/**
 * Трансформирует массив данных для таблицы со статистикой в объект для отрисовки графика со статистикой
 * @param {Array} statistics
 * @param {String} statisticsType
 * @return {Object}
 */
export const getChartData = (statistics, statisticsType) =>
    statistics.map((item) => {
        const chartData = {};
        chartData.name =
            statisticsType === STATISTICS_BY_DATE ? format(parseISO(item.date), VIEW_FORMAT) : item.regionName;
        STATISTICS_TABLE_FIRST_HEADERS.forEach((firstHead) =>
            STATISTICS_TABLE_SECOND_HEADERS_FULL.forEach((secondHead) => {
                chartData[`${firstHead}${capitalize(secondHead)}`] = item.statisticRow[firstHead][secondHead];
            })
        );
        return chartData;
    });

/**
 * Удаляет пустые поля в объекте
 * @param {Object} fields
 * @return {Object}
 */
export const deleteEmptyFields = (fields) => {
    const objectWithoutEmptyFields = {};
    Object.keys(fields).forEach((item) => {
        switch (typeof fields[item]) {
            case 'string':
                if (fields[item] !== '') {
                    objectWithoutEmptyFields[item] = fields[item];
                }
                break;
            case 'boolean':
                if (fields[item]) {
                    objectWithoutEmptyFields[item] = fields[item];
                }
                break;
            case 'object':
                if (Array.isArray(fields[item])) {
                    if (fields[item].length > 0) {
                        objectWithoutEmptyFields[item] = fields[item];
                    }
                } else if (Object.keys(fields[item]).length > 0) {
                    objectWithoutEmptyFields[item] = fields[item];
                }
                break;
        }
    });
    return objectWithoutEmptyFields;
};
