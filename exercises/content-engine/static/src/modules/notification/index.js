import { addNotificationAction } from 'store/models/notifications';

let id = 0;

export default (type, params, props, uniqueType = false) => (dispatch, getState) => {
    if (uniqueType) {
        const state = getState();
        const alreadyInStore = state.notifications.some((notification) => notification.type === type);
        if (alreadyInStore) {
            return null;
        }
    }

    id += 1;

    return dispatch(addNotificationAction({ type, params, props, uniqueType, id }));
};
