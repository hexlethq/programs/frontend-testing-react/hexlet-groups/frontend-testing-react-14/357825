import axios from 'axios';
import store from 'store';
import addNotification from 'modules/notification';
import { SERVER_ERROR } from 'constants/notifications';

axios.interceptors.response.use(
    (response) => {
        return response.data;
    },
    (err) => {
        const status = err?.response?.status;
        if (status >= 500) {
            store.dispatch(addNotification(SERVER_ERROR));
        }
        return Promise.reject(err);
    }
);
axios.defaults.timeout = 10000;

export default Object.assign({}, axios);
