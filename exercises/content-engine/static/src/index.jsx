import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import store from 'store';
import MainComponent from 'components/MainComponent';

const App = () => (
    <Provider store={store}>
        <MainComponent />
    </Provider>
);

ReactDOM.render(<App />, document.getElementById('app'));
