const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const rootPath = path.resolve(__dirname, 'src');
const entryPath = path.resolve(rootPath, 'index.jsx');
const outputPath = path.resolve(__dirname, 'dist');
const modeEnv = process.env.NODE_ENV === 'production' ? 'production' : 'development';
const blokoPath = /node_modules\/@hh.ru\/bloko\/(?!bloko\/).*/;
const fullBlokoPath = path.resolve(__dirname, 'node_modules/@hh.ru/bloko/bloko/');

module.exports = {
    entry: {
        main: entryPath,
        agentDetails: `${fullBlokoPath}/raw/getUserAgentDetails.js`,
    },
    mode: modeEnv,
    output: {
        filename: modeEnv === 'development' ? '[name].js' : '[name]-[chunkhash].js',
        path: outputPath,
        publicPath: '/static',
    },
    resolve: {
        alias: {
            root: path.resolve(__dirname, 'src/'),
            bloko: fullBlokoPath,
            components: path.resolve(__dirname, 'src/components/'),
            store: path.resolve(__dirname, 'src/store/'),
            pages: path.resolve(__dirname, 'src/pages/'),
            modules: path.resolve(__dirname, 'src/modules/'),
            routes: path.resolve(__dirname, 'src/routes/'),
            constants: path.resolve(__dirname, 'src/constants/'),
            hooks: path.resolve(__dirname, 'src/hooks/'),
        },
        extensions: ['.jsx', '.js', '.less', '.css', '.json', '.svg', '.gif', '.jpg'],
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                enforce: 'pre',
                exclude: /node_modules/,
                use: {
                    loader: 'eslint-loader',
                    options: {
                        emitWarning: true,
                        configFIle: './.eslintrc',
                    },
                },
            },
            {
                test: /\.jsx?$/,
                exclude: blokoPath,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-react'],
                        plugins: ['@babel/plugin-proposal-class-properties'],
                    },
                },
            },
            {
                test: /\.less$/,
                use: ['style-loader', 'css-loader', 'less-loader'],
            },
            {
                test: /\.css/,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.(png|otf|svg|gif|jpe?g)$/,
                use: [
                    {
                        loader: 'url-loader',
                    },
                ],
            },
        ],
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'public/index.html',
            favicon: 'public/favicon.png',
        }),
    ],
    optimization: {
        splitChunks: {
            name: true,
            chunks: 'all',
            minSize: 300,
            maxAsyncRequests: Infinity,
            maxInitialRequests: 10,
            cacheGroups: {
                vendors: {
                    name: 'vendors',
                    test: /[\\/]node_modules[\\/]/,
                    chunks: 'all',
                    priority: -1,
                },
            },
        },
    },
};
