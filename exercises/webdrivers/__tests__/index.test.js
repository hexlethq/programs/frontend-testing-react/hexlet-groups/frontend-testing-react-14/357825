const puppeteer = require('puppeteer');
const getApp = require('../server/index.js');

const port = 5001;
const appUrl = `http://localhost:${port}`;
const articlesUrl = `http://localhost:${port}/articles`;

let browser;
let page;

const app = getApp();

describe('simple blog works', () => {
  beforeAll(async () => {
    await app.listen(port, '0.0.0.0');
  });

  beforeEach(async () => {
    browser = await puppeteer.launch({
      args: ['--no-sandbox'],
      headless: true,
    });
    page = await browser.newPage();
    await page.setViewport({
      width: 1920,
      height: 1080,
    });
  });

  // BEGIN
  it('check app', async () => {
    await page.goto(appUrl);
    await expect(page.title()).resolves.toMatch('Simple Blog');
    await page.click('.nav-link[href="/articles"]');
    const header = (await page.$eval('h3', (hdr) => hdr.textContent));
    expect(header).toBe('Articles');
  });

  it('updating articles', async () => {
    await page.goto(articlesUrl);
    await page.click('a[href="/articles/new"]');
    await page.waitForSelector('form');
    const formControlsCount = await page.$$eval('form input', (elements) => elements.length);
    expect(formControlsCount).toBeTruthy();

    await page.type('#name', 'title');
    await page.select('select', String(Math.ceil(Math.random() * 3)));
    await page.type('#content', 'some text');
    await page.click('[type="submit"]');
    await page.waitForSelector('#articles');
    const articleNames = await page.$$eval('td:nth-child(2)', (tds) => tds.map((td) => td.textContent));
    expect(articleNames).toContain('title');

    const tableSnapshot = await page.$eval('table', (table) => table.textContent);
    await page.click('td > a');
    await page.waitForSelector('#name');
    await page.type('#name', 'new title');
    await page.click('[type="submit"]');
    await page.waitForSelector('#articles');
    const updatedTableSnapshot = await page.$eval('table', (table) => table.textContent);
    expect(updatedTableSnapshot).not.toEqual(tableSnapshot);
  });
  // END

  afterEach(async () => {
    await browser.close();
  });

  afterAll(async () => {
    await app.close();
  });
});








// const puppeteer = require('puppeteer');
// const getApp = require('../server/index.js');

// const port = 5001;
// const appUrl = `http://localhost:${port}`;

// let browser;
// let page;

// const app = getApp();

// describe('it works', () => {
//   beforeAll(async () => {
//     await app.listen(port, '0.0.0.0');
//     browser = await puppeteer.launch({
//       args: ['--no-sandbox'],
//       headless: true,
//       // slowMo: 250
//     });
//     page = await browser.newPage();
//     await page.setViewport({
//       width: 1280,
//       height: 720,
//     });
//   });
//   // BEGIN
//   test('main', async () => {
//     await page.goto(appUrl);
//     expect(await page.$('#title')).toBeTruthy();

//     await page.click('a.nav-link');
//     await page.waitForSelector('h3');
//     expect(await page.$eval('h3', (el) => el.textContent)).toEqual('Articles');

//     await page.click('.container.mt-3 > a');
//     await page.waitForSelector('form');
//     expect(await page.$eval('h3', (el) => el.textContent)).toEqual('Create article');

//     await page.type('#name', 'new post');
//     await page.select('select', '1');
//     await page.type('#content', 'super post');
//     await page.click('[type="submit"]');
//     await page.waitForSelector('tbody');
//     let tds = await page.$$eval('tbody > tr > td', (els) => els.map((el) => el.textContent));
//     expect(tds).toContain('new post');

//     await page.click('td > a');
//     await page.waitForSelector('form');
//     const input = await page.$('#name');
//     await input.click({ clickCount: 3 });
//     await input.type('edited');
//     await page.click('[type="submit"]');
//     await page.waitForSelector('tbody');
//     tds = await page.$$eval('tbody > tr > td', (els) => els.map((el) => el.textContent));
//     expect(tds).toContain('edited');
//   });
//   // END
//   afterAll(async () => {
//     await browser.close();
//     await app.close();
//   });
// });
