// BEGIN
const appUrl = 'http://localhost:5000';

beforeAll(async () => {
    await page.goto(appUrl);
});

test('Main page.', async () => {
    await page.goto(appUrl);
    await expect(page).toMatch('Welcome to a Simple blog!');
});

test('Articles.', async () => {
    await page.goto(`${appUrl}/articles`);
    expect(await page.$$('[data-testid="article-name"]')).toBeTruthy();
});

test('Shows form.', async () => {
    await page.goto(`${appUrl}/articles`);
    await page.click('[data-testid="article-create-link"]');
    await expect(page).toMatchElement('[data-testid="article-create-button"]');
});

test('Create article.', async () => {
    await page.goto(`${appUrl}/articles/new`);
    await expect(page).toSelect('[name="article[categoryId]"]', '1');
    await expect(page).toFillForm('form[data-testid="new-form"]', {
        'article[name]': 'antarius',
        'article[content]': 'some decent lorem ipsum',
    });
    await expect(page).toClick('[data-testid="article-create-button"]');
    await page.waitForSelector('[data-testid="table"]');
    await expect(page).toMatch('antarius');
});

test('Edit article.', async () => {
    await page.goto(`${appUrl}/articles/new`);
    await expect(page).toSelect('[name="article[categoryId]"]', '1');
    await expect(page).toFillForm('form[data-testid="new-form"]', {
        'article[name]': 'buzzinga',
        'article[content]': 'hitchhiker\'s guide to galaxy',
    });
    await expect(page).toClick('[data-testid="article-create-button"]');
    await page.waitForSelector('[data-testid="table"]');
    const articleId = await page.$eval(
        '[data-testid="article"]:last-child [data-testid="article-id"]',
        (el) => el.innerText
    )
    await page.goto(`${appUrl}/articles/${articleId}/edit`);
    await expect(page).toFill('#name', 'changed title');
    await page.click('[data-testid="article-update-button"]');
    await page.waitForSelector('[data-testid="table"]');
    const articleName = await page.$eval(
        '[data-testid="article"]:last-child [data-testid="article-name"]',
        (el) => el.innerText
    )
    await expect(articleName).toMatch('changed title');
});
// END
