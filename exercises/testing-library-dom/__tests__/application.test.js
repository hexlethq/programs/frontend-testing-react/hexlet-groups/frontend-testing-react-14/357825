require('@testing-library/jest-dom');
const fs = require('fs');
const path = require('path');
const testingLibraryDom = require('@testing-library/dom');
const testingLibraryUserEvent = require('@testing-library/user-event');
const faker = require('faker');

const run = require('../src/application');

const { screen, configure } = testingLibraryDom;
configure({
  testIdAttribute: 'data-container',
});

const userEvent = testingLibraryUserEvent.default;

beforeEach(() => {
  document.body.innerHTML = fs.readFileSync(path.join('__fixtures__', 'index.html')).toString();
  run();
});

const createList = (listName) => {
  userEvent.type(screen.getByRole('textbox', { name: /new list name/i }), listName);
  userEvent.click(screen.getByRole('button', { name: /add list/i }));
};

const createTask = (taskName) => {
  userEvent.type(screen.getByRole('textbox', { name: /new task name/i }), taskName);
  userEvent.click(screen.getByRole('button', { name: /add task/i }));
};


// BEGIN
test('Shows the application.', () => {
  expect(screen.getByTestId('tasks')).toBeEmptyDOMElement();
  expect(screen.getByText(/general/i)).toBeVisible();
});


test('add tasks to default list', () => {
  const taskOne = faker.lorem.word();
  const taskTwo = faker.lorem.word();
  const taskThree = faker.lorem.word();

  createTask(taskOne);
  createTask(taskTwo);
  createTask(taskThree);

  expect(screen.getByText(/general/i)).toBeVisible();
  expect(screen.getByText(new RegExp(taskOne))).toBeVisible();
  expect(screen.getByText(new RegExp(taskTwo))).toBeVisible();
  expect(screen.getByText(new RegExp(taskThree))).toBeVisible();
  expect(screen.getByRole('textbox', { name: /new task name/i })).toHaveValue('');

});

test('Check that no task go to another list', () => {
  const secondListName = 'Another brick in the list';

  const taskOne = faker.lorem.word();

  createList(secondListName);
  createTask(taskOne);
  expect(screen.getByText(new RegExp(taskOne))).toBeVisible();
  userEvent.click(screen.getByRole('link', { name: new RegExp(secondListName) }));
  expect(screen.getByTestId('tasks')).toBeEmptyDOMElement();
});
// END

test('Adds a new list with tasks.', () => {
  const secondListName = 'Another brick in the list';

  const taskOne = faker.lorem.word();
  const taskTwo = faker.lorem.word();

  createList(secondListName);
  userEvent.click(screen.getByRole('link', { name: new RegExp(secondListName) }));

  createTask(taskOne);
  createTask(taskTwo);
  expect(screen.getByText(new RegExp(taskOne))).toBeVisible();
  expect(screen.getByText(new RegExp(taskTwo))).toBeVisible();
});
// END
