const nock = require('nock');
const axios = require('axios');
const { get, post } = require('../src/index.js');

axios.defaults.adapter = require('axios/lib/adapters/http');
// BEGIN
describe('testing http requests', () => {

    beforeAll(() => {
        nock.disableNetConnect
    })

    test('get', async () => {
        const userData = { 'id': 1020, 'name': 'Putin' };
        const scope = nock('https://example.com')
            .get('/users')
            .reply(200, userData)
        let data = await get('https://example.com/users');

        expect(scope.isDone()).toBeTruthy();
        expect(data).toEqual(userData)
        scope.done();
    })

    test('get throw', async () => {
        const scope = nock('https://example.com')
            .get('/users')
            .reply(404);

        await expect(get('https://example.com/users')).rejects.toThrow();
        expect(scope.isDone()).toBeTruthy();

    })

    test('post', async () => {
        const userData = { 'id': 1020, 'name': 'Putin' };
        const scope = nock('https://example.com')
            .post('/users')
            .reply(200, userData)
        let data = await post('https://example.com/users', { 'some': 'property' });

        expect(scope.isDone()).toBeTruthy();
        expect(data).toEqual(userData)
        scope.done();
    })

    afterEach(() => {
        nock.cleanAll();
    });

    afterAll(() => {
        nock.restore();
        nock.enableNetConnect();
    });

})
// END
