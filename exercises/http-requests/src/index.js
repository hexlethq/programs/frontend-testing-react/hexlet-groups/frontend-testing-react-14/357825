const axios = require('axios');

// BEGIN
const get = async (url = 'https://example.com/users') => {
    let response = await axios.get(url)
    return response.data
}
const post = async (url = 'https://example.com/users', params = {}) => {
    let response = await axios.post(url, params);
    return response.data
}
// END

module.exports = { get, post };
