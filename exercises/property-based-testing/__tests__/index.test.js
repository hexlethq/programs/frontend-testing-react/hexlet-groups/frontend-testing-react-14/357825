const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

describe('properties', () => {
    const count = (arr, element) => arr.filter(el => el === element).length;

    it('has same length', () => {
        fc.assert(
            fc.property(
                fc.array(fc.integer()),
                data => {
                    const newData = sort(data)
                    expect(data.length).toEqual(newData.length)
                }
            )
        )
    });

    it('contains same elements', () => {
        fc.assert(
            fc.property(
                fc.array(fc.integer()),
                data => {
                    const newData = sort(data)
                    for (const item in data) {
                        expect(count(data, item)).toEqual(count(newData, item))
                    }
                }
            )
        )
    });

    it('elements are sorted', () => {
        fc.assert(
            fc.property(
                fc.array(fc.integer()),
                data => {
                    const newData = sort(data)
                    expect(newData).toBeSorted();
                }
            )
        )
    });

    it('is final result', () => {
        fc.assert(
            fc.property(
                fc.array(fc.integer()),
                data => {
                    const newData = sort(data)
                    const newData2 = sort(newData)
                    expect(newData2).toStrictEqual(newData)
                }
            )
        )
    });

});
