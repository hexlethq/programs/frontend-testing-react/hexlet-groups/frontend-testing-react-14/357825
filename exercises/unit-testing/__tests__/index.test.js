describe('Object.assign: ', () => {

    test('main', () => {
        const src = { k: 'v', b: 'b' };
        const target = { k: 'v2', a: 'a' };
        const result = Object.assign(target, src);
        expect(result).toEqual({ a: 'a', b: 'b', k: 'v' })
    });

    test('mutability', () => {
        const src = { k: 'v', b: 'b' };
        const target = { k: 'v2', a: 'a' };
        const result = Object.assign(target, src);
        expect(result).toBe(target);
    });

    test('immutability', () => {
        const src = { k: 'v', b: 'b' };
        const target = { k: 'v2', a: 'a' };
        const result = Object.assign(target, src);
        expect(result).not.toBe(src)
        expect(target).not.toBe(src)
    });

    test('shallow copy', () => {
        const target = { a: 1, b: 2 };
        const src = { c: 3, d: { a: 4 } }
        const result = Object.assign(target, src)
        src.c = 10;
        src.d.a = 100;
        expect(result).toEqual({ ...target, ...{ c: 3, d: { a: 100 } } })
    })

    test('cloning', () => {
        const target = { k: 'v2', a: 'a' };
        const result = Object.assign({}, target)
        expect(result).toEqual(target)
        expect(result).not.toBe(target)
    });

    test('unchanged', () => {
        const target = { a: 1, b: 2 };
        const src = { c: 3, d: { a: 4 } }
        const result = Object.assign(target, src)
        const res1 = Object.assign(result, undefined)
        const res2 = Object.assign(result, null)
        const res3 = Object.assign(result, 100)
        expect(res1).toEqual(result)
        expect(res2).toEqual(result)
        expect(res3).toEqual(result)
        expect(res1).toBe(result)
        expect(res2).toBe(result)
        expect(res3).toBe(result)
    });

    test('string added', () => {
        const str = 'asd';
        const result = Object.assign({}, str)
        expect(result).toEqual({ ...str })
    });

    test('itterable added', () => {
        const arr = [1, 2, 3];
        const result = Object.assign({}, arr)
        expect(result).toEqual({ ...arr })
    });

    test('deep clone', () => {
        const target = { a: 1, b: 2 };
        const src = { c: 3, d: { a: 4 } }
        const result = Object.assign(target, src)
        const obj = { l: { a: 1, b: 'b' } };
        const res = Object.assign(result, obj)
        expect(res).toEqual({ ...result, ...obj })
    });

    test('readonly throw', () => {
        const target = Object.defineProperty({}, 'name', {
            value: 'Ivan',
            writable: false
        });
        const src = { name: 'Petr' }
        const result = () => Object.assign(target, src)
        expect(result).toThrow();
    })

    test('functions are not objects!', () => {
        const func1 = (a) => a + 2;
        const res = Object.assign({}, func1)
        expect(res).not.toBe({ ...func1 })
    });

})


